import {
  Button, Center, Divider, Flex, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { useIndianCurrencyFormatter } from '../../../hooks/useIndianCurrencyFormatter';
import {
  CHECKOUT_ORDER_SUMMARY, CHECKOUT_SELECT_ADDRESS
} from '../../../lib/router';
import { displayUserRazorpay } from '../../../store/actions/checkout';
import {
  selectTotalEcomPrice
} from '../../../store/selectors/cart';
import MobilePaymentInfo from '../order/MobilePaymentInfo';

// Code Review Completed

const MobileCheckout = ({ isCartCheckout, isSelectAddress, addressSelected }) => {

  const logInStatus = useSelector((state) => state.user.logInStatus);
  const handleNext = () => router.push(`/${CHECKOUT_ORDER_SUMMARY}`);
  const { isPaymentLoading, isOrderProcessing } = useSelector(
    (state) => state.feedback
  );
  const dispatch = useDispatch();
  const router = useRouter();
  const cartItems = useSelector((state) => state.cart.cartItems);
  const checkout = useSelector((state) => state.checkout);
  const [orderItems, setOrderitems] = useState();
  const cartProducts = useSelector((state) => state.cart.cartProducts);

  useEffect(() => {
    const tempItems = [];
    cartProducts.forEach((cartProduct) => {
      cartItems.forEach((cartItem) => {
        if (cartItem.materialCode === cartProduct.materialCode) {
          tempItems.push({
            ...cartProduct,
            ...cartItem,
          });
        }
      });
    });
    setOrderitems(tempItems);
  }, [cartItems, cartProducts, cartProducts.length]);

  const handlePayment = () => {
    const userInfo = Object.keys(checkout.currentUser).length > 0
      ? checkout.currentUser
      : checkout.guest;
    dispatch(displayUserRazorpay(orderItems, userInfo, router));
  };
  const handleCheckout = () => {
    if (logInStatus) {
      if (!isCartCheckout && !isSelectAddress) {
        router.push(CHECKOUT_SELECT_ADDRESS);
      } else {
        handlePayment();
      }
    } else {
      router.push(CHECKOUT_SELECT_ADDRESS);
    }
  };
  const totalEcomPrice = useIndianCurrencyFormatter(useSelector(selectTotalEcomPrice));
  return (
    <Flex
      minWidth="102vw"
      justifyContent="center"
      alignItems="center"
      className={`${css(styles.container)} row`}
    >
      <Flex py="5px" justifyContent="space-between" height="100%">
        <Flex
          width="50%"
          alignItems="center"
          flexDir="column"
          justifyContent="flex-start"
          mt={2}
          ml="5"
        >
          <Text
            textAlign="center"
            fontWeight="bold"
            fontSize="large"
          >
            {totalEcomPrice}
          </Text>
          <MobilePaymentInfo />
        </Flex>

        <Center height="50px">
          <Divider orientation="vertical" color={colors.gray} />
        </Center>

        <Flex
          width="98%"
          alignItems="center"
          justifySelf="center"
          justifyContent="flex-end"
          className={css(styles.linkStyles)}
          mt={2}
        >
          <Button
            isDisabled={addressSelected}
            variant="primary"
            isLoading={(isCartCheckout && !isSelectAddress) && isPaymentLoading}
            onClick={(!isCartCheckout && isSelectAddress) ? handleNext : handleCheckout}
          >
            {(isCartCheckout || isSelectAddress) ? 'PLACE ORDER' : 'CHECKOUT'}
          </Button>
        </Flex>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  icon: {
    borderRadius: '0%'
  },
  container: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.white,
    padding: '1 rem',
    borderTopWidth: '2.5px',
    borderTopColor: colors.light,
    zIndex: 10,
    width: '100vw',
    marginTop: 20
  },
  linkStyles: {
    ':hover': {
      textDecoration: 'none',
      color: colors.green
    }
  }
});

export default MobileCheckout;
