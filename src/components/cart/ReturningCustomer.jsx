/* eslint-disable no-lone-blocks */
/* eslint-disable max-len */
import { Flex, Text } from '@chakra-ui/react';
import Link from 'next/link';
import React from 'react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  LOGIN
} from '../../../lib/router';

// Code Review Complete

const ReturningCustomer = () => {
  const { isLg } = useMediaQuery();

  const logInStatus = useSelector((state) => state.user.logInStatus);
  return (
    <Flex
      justifyContent="flex-start"
      alignSelf={!isLg && 'center'}
      width={isLg ? '60%' : '95%'}
      paddingBottom="10px"
    >
      {!logInStatus && (
        <Flex
          flexDirection="row"
          mb={2}
          borderWidth="1px"
          borderColor={colors.gray}
          bgColor={colors.white}
          width={isLg ? '40vw' : '100%'}
          pb={2}
          pl={5}
          pt={2}
        >
          <Text pr={2}> Returning Customer? </Text>
          <Text
            as="button"
            bgColor="transparent"
            height="25px"
            _hover={{ opacity: 0.95, cursor: 'pointer' }}
            variant="link"
            color={colors.green}
            fontWeight="bold"
          >
            <Link href={LOGIN}> Click here to login </Link>
          </Text>
        </Flex>
      )}
    </Flex>
  );
};

export default ReturningCustomer;
