import {
  Button, Flex, Image, Text
} from '@chakra-ui/react';
import Link from 'next/link';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { CATEGORY } from '../../../lib/router';

// Code Review Completed

const EmptyCart = () => {
  const { isLg } = useMediaQuery();
  const url = './EC_image.PNG';
  return (
    <>
      <Flex
        justifyContent="center"
        width={isLg ? '70vw' : '85%'}
        height={isLg ? '70vh' : '65%'}
        bgColor={colors.white}
        margin={isLg ? 20 : 10}
        padding={10}
        alignItems="center"
        flexDirection="column"
      >

        <Image
          src={url}
          borderColor={colors.light}
          // width={isLg ? '40%' : '50%'}
          height={isLg ? '50%' : '60%'}
        />

        <Flex my={5} justifyContent="center" flexDirection="row" alignItems="center">
          <Text fontSize={isLg ? 'xx-large' : 'x-large'} textAlign="center" fontWeight="bold" color="gray.600">
            Your cart is Empty
          </Text>
        </Flex>
        <Flex mb="2rem" justifyContent="center" flexDirection="column">
          <Text color={colors.gray} fontSize="sm" textAlign="center" alignSelf="center">
            You may add in the Product by clicking on the Blue Button below
            which will take you to the list of Products available!
          </Text>
        </Flex>
        <Link href={CATEGORY}>
          <Button
            variant="secondary"
          >
            Add Products
          </Button>
        </Link>
      </Flex>
    </>
  );
};

export default EmptyCart;
