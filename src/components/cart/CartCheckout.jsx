/* eslint-disable max-len */
import { ArrowBackIcon } from '@chakra-ui/icons';
import { Button, Flex } from '@chakra-ui/react';
import Link from 'next/link';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  CATEGORY
} from '../../../lib/router';

// Code Review Completed

const CartCheckout = ({ isCartCheckout }) => {
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const { isLg } = useMediaQuery();

  return (
    <>
      <Flex
        pt="1rem"
        pb={isLg ? '5rem' : '1rem'}
        width="100%"
        justifyContent="space-between"
      >
        { isLg && (
        <>
          <Link href={CATEGORY}>
            <Button
              variant="outline_black"
              size={isLg ? 'md' : 'xs'}
              leftIcon={<ArrowBackIcon boxSize="1.2rem" />}
            >
              Continue Shopping
            </Button>
          </Link>
        </>
        )}
      </Flex>
    </>
  );
};

export default CartCheckout;
