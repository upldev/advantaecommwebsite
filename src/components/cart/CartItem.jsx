import { Divider, Flex } from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import { CartItemContext } from '../../context/CartItemContext';
import CartItemActions from './CartItemActions';
import CartItemDetails from './CartItemDetails';

const CartItem = ({ product, isCartCheckout }) => {
  return (
    <CartItemContext product={product} isCartCheckout={isCartCheckout}>
      <Flex
        justifyContent="space-around"
        flexDirection="column"
        width="100%"
        padding="18.5px"
        borderWidth="1px"
        borderTopWidth={0}
        bgColor={colors.white}
        borderColor={colors.gray}
      >
        <CartItemDetails />
        {!isCartCheckout && (
          <>
            <Divider backgroundColor={colors.lightGray} />
            <CartItemActions />
          </>
        )}
      </Flex>
    </CartItemContext>
  );
};

export default CartItem;
