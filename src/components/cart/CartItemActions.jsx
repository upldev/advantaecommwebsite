import { Button } from '@chakra-ui/button';
import { Flex, Icon } from '@chakra-ui/react';
import React from 'react';
import { IoCheckmarkOutline, IoHeartOutline, IoTrashOutline } from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { useCartItemContext } from '../../context/CartItemContext';

// Code Review Completed

const CartItemActions = () => {
  const { handleRemoveFromCart, handleAddToWishlist, isWishlisted } = useCartItemContext();

  const { isLg } = useMediaQuery();

  return (
    <Flex
      justifyContent="left"
      flexDirection="row"
      width="100%"
      paddingTop="18.5px"
      bgColor={colors.white}
    >
      <Button
        borderRadius="0"
        border="1px"
        mr={!isLg ? '10px' : '30px'}
        borderColor={colors.gray}
        bgColor={colors.white}
        color={colors.gray}
        onClick={handleRemoveFromCart}
        _hover={{ opacity: 0.95, cursor: 'pointer' }}
        leftIcon={<Icon as={IoTrashOutline} />}
      >
        DELETE
      </Button>
      {isWishlisted ? (
        <Button
          variant="outline_secondary"
          onClick={handleAddToWishlist}
          leftIcon={<IoCheckmarkOutline size={24} />}
          overflow="hidden"
        >
          WISHLISTED
        </Button>
      ) : (
        <Button
          variant="outline_secondary"
          onClick={handleAddToWishlist}
          leftIcon={<IoHeartOutline size={24} />}
          overflow="hidden"
        >
          MOVE TO WISHLIST
        </Button>
      )}
    </Flex>
  );
};

export default CartItemActions;
