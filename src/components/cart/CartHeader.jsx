import { Box, Flex, Text } from '@chakra-ui/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { resetCart } from '../../../store/actions/cart';
import { selectTotalItems } from '../../../store/selectors/cart';
import Availability from '../checkout/Availability';

// Code Review Complete

const CartHeader = () => {
  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();

  const getCartCount = useSelector(selectTotalItems);
  const handleClearCart = () => {
    dispatch(resetCart());
  };
  return (
    <Box alignSelf="center" width="100%">
      <Availability />
      <Flex
        flexDirection="column"
        mb="18px"
        justifyContent="space-between"
        borderWidth="0"
        width="100%"
      >
        <Flex justifyContent="space-between">
          <Flex>
            <Text variant="smTextRegular" fontWeight="bold">
              My Cart Items :
              {' '}
            </Text>
            <Text
              variant="smTextRegular"
              ml={1}
              textDecoration="underline"
              color={colors.gray}
            >
              {getCartCount}
              {' '}
              items
            </Text>
          </Flex>
          <Flex justifyContent="flex-end">
            <Text
              as="button"
              bgColor="transparent"
              onClick={handleClearCart}
              _hover={{ opacity: 0.95, cursor: 'pointer' }}
              variant="smTextRegular"
              textDecoration="underline"
              color={colors.lightRed}
              fontWeight="bold"
            >
              Clear all items
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};

export default CartHeader;
