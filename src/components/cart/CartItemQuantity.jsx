/* eslint-disable max-len */
import { AddIcon, MinusIcon } from '@chakra-ui/icons';
import {
  Flex, Icon, IconButton, Spinner, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import { LG_BREAKPOINT } from '../../../constants/mediaQueries';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { useCartItemContext } from '../../context/CartItemContext';

// Code Review Completed

const CartItemQuantity = () => {
  const {
    product: { qty, sku },
    isCartCheckout,
    handleDecrement,
    handleIncrement,
    handleRemoveFromCart,
    isCartUpdating,
  } = useCartItemContext();

  const { isLg } = useMediaQuery();

  return (
    <Flex
      mt={0}
      fontWeight="bold"
      justifyContent="center"
      alignItems="center"
      width={isLg ? '100%' : '50%'}
    >
      {!isCartCheckout && (
        <IconButton
          borderRadius="0"
          _hover={{ opacity: 0.5 }}
          bgColor={colors.green}
          size="sm"
          color={colors.white}
          onClick={qty > 1 ? handleDecrement : handleRemoveFromCart}
          disabled={isCartUpdating}
        >
          <Icon as={MinusIcon} boxSize="0.7rem" />
        </IconButton>
      )}
      <Flex
        alignItems="center"
        justifyContent="center"
        className={!isCartCheckout && css(styles.qtyContainer)}
        height="fit-content"
      >
        {isCartUpdating ? (
          <Spinner size="sm" />
        ) : (
          <Text fontWeight="bold">
            {!isLg && isCartCheckout ? `Quantity: ${qty}` : qty}
          </Text>
        )}
      </Flex>
      {!isCartCheckout && (
        <IconButton
          borderRadius="0"
          _hover={{ opacity: 0.5 }}
          bgColor={colors.green}
          size="sm"
          disabled={isCartUpdating || qty === sku.qtyAvailable}
          onClick={handleIncrement}
          color={colors.white}
        >
          <Icon as={AddIcon} boxSize="0.7rem" />
        </IconButton>
      )}
    </Flex>
  );
};

const styles = StyleSheet.create({
  qtyContainer: {
    borderWidth: '0px',
    padding: '5px 20px 5px 20px',
    margin: '0px 5px 0px 5px',
    width: '3vw',
    [LG_BREAKPOINT]: {
      padding: '0px 17px',
    },
  },
});

export default CartItemQuantity;
