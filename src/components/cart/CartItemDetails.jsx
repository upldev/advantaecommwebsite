import {
  Box, Flex, Image, Skeleton, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import React from 'react';
import { colors } from '../../../constants/colors';
import { useIndianCurrencyFormatter } from '../../../hooks/useIndianCurrencyFormatter';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { usePackSize } from '../../../hooks/usePackSize';
import { productDetailsPage } from '../../../lib/router';
import { useCartItemContext } from '../../context/CartItemContext';
import CartItemQuantity from './CartItemQuantity';

// Code Review Completed

const CartItemDetails = () => {
  const router = useRouter();

  const {
    product: {
      productImg, product, materialCode, crop, sku, qty
    },
    isCartCheckout,
    price,
  } = useCartItemContext();

  const packSize = usePackSize(sku.packSize);

  const { isLg } = useMediaQuery();

  return (
    <Flex
      justifyContent={isLg ? 'space-between' : 'flex-start'}
      flexDirection={isLg ? 'row' : 'column'}
      paddingBottom="18.5px"
    >
      <Flex
        cursor="pointer"
        onClick={() => router.push(productDetailsPage(materialCode))}
        width={isLg && '50%'}
        flexDirection="row"
      >
        <Box marginRight="18.5px" width={!isLg ? '25%' : '70px'}>
          <Image
            height="100%"
            width="100%"
            objectFit="contain"
            src={productImg[0]}
            alt={product}
            fallback={(
              <Skeleton
                width={!isLg ? '100%' : '70px'}
                height="100%"
                objectFit="contain"
              />
            )}
          />
        </Box>
        <Flex width={isLg && '60%'} flexDir="column" align="flex-start">
          <Text variant="smTextRegular" fontWeight="bold">
            {crop.toUpperCase()}
          </Text>
          <Text variant="smTextRegular" fontSize="14" color={colors.gray}>
            {product}
          </Text>
          {sku.qtyAvailable > 0 ? (
            <Text color={colors.green} fontSize="14">
              IN STOCK
            </Text>
          ) : (
            <Text color={colors.lightRed} fontSize="14">
              OUT OF STOCK
            </Text>
          )}
          <Flex justify="center" align="center">
            <Text mr="5px" color={colors.black} fontWeight="bold">
              {useIndianCurrencyFormatter(sku.ecomPrice)}
            </Text>
            {sku.discount > 0 && (
            <>
              <Text
                color={colors.gray}
                mr="5px"
                className={css(styles.strike)}
                fontSize="14"
              >
                {useIndianCurrencyFormatter(sku.MRP)}
              </Text>
              <Text fontSize="14" wrap="" color={colors.lightRed}>
                {`${sku.discount}%OFF`}
              </Text>
            </>
            )}
          </Flex>
          {!isLg && (
            <>
              <Flex width="60%" flexDir="column" align="flex-start">
                <Text>{packSize}</Text>
              </Flex>
              <Flex
                justifyContent={isLg && 'center'}
                alignItems="center"
                mt={isLg ? null : 1}
                width="100%"
              >
                <CartItemQuantity qty={qty} qtyAvailable={sku.qtyAvailable} />
              </Flex>
            </>
          )}
        </Flex>
      </Flex>
      {isLg && (
        <>
          <Flex justifyContent="center" alignItems="center" width="10%">
            <Text>{packSize}</Text>
          </Flex>
          <Flex width="30%" justifyContent="center" alignItems="center">
            <CartItemQuantity qty={qty} qtyAvailable={sku.qtyAvailable} />
          </Flex>
          <Flex justifyContent="center" alignItems="center" width="10%">
            <Text textAlign="center" fontWeight="bold">
              {price}
            </Text>
          </Flex>
        </>
      )}
    </Flex>
  );
};

const styles = StyleSheet.create({
  strike: {
    display: 'inline-block',
    textDecoration: 'none',
    position: 'relative',
    ':after': {
      content: "''",
      display: 'block',
      width: '100%',
      height: '63%',
      position: 'absolute',
      top: 0,
      left: 0,
      borderBottom: '1px solid',
    },
  },
});

export default CartItemDetails;
