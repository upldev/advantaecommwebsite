/* eslint-disable no-lonely-if */
import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';
import {
  Button, Flex, HStack, Text
} from '@chakra-ui/react';
import React, { useEffect } from 'react';

// Code Review Completed

const Pagination = ({
  currentPage,
  setCurrentPage,
  productsPerPage,
  totalProducts,
}) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalProducts / productsPerPage); i++) {
    pageNumbers.push(i);
  }
  const totalPages = pageNumbers.length;
  let startPage;
  let endPage;
  if (totalPages <= 3) {
    startPage = 1;
    endPage = totalPages;
  } else {
    if (currentPage <= 2) {
      startPage = 1;
      endPage = 3;
    } else if (currentPage + 1 >= totalPages) {
      startPage = totalPages - 2;
      endPage = totalPages;
    } else {
      startPage = currentPage - 1;
      endPage = currentPage + 1;
    }
  }
  const pages = [...Array(endPage + 1 - startPage).keys()].map(
    (i) => startPage + i
  );
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [currentPage]);
  return (
    <HStack
      width="100%"
      justifyContent="center"
      alignItems="center"
      my="10"
      spacing="10px"
    >
      <Button
        disabled={currentPage === 1}
        style={{
          cursor: currentPage === 1 ? 'not-allowed' : 'pointer',
        }}
        variant={currentPage === 1 ? 'disabledAddToCart' : 'secondary'}
        onClick={() => setCurrentPage((prev) => prev - 1)}
      >
        <Flex>
          <ChevronLeftIcon mt={0.5} />
          <Text ml={2}>Previous Page</Text>
        </Flex>
      </Button>
      <Button
        disabled={currentPage === pageNumbers.length}
        style={{
          cursor:
            currentPage === pageNumbers.length ? 'not-allowed' : 'pointer',
        }}
        onClick={() => setCurrentPage((prev) => prev + 1)}
        variant={
          currentPage === pageNumbers.length ? 'disabledAddToCart' : 'secondary'
        }
      >
        <Flex pl={3} pr={3}>
          <Text ml={2}>Next Page</Text>
          <ChevronRightIcon mt={0.5} />
        </Flex>
      </Button>
    </HStack>
  );
};

export default Pagination;
