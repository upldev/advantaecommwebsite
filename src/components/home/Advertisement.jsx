import { Box } from '@chakra-ui/layout';
import { useSelector } from 'react-redux';
import { Swiper, SwiperSlide } from 'swiper/react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { selectOffers } from '../../../store/selectors/home';
import OfferBanner from './OffersBanner';

const Advertisement = () => {
  const { isLg } = useMediaQuery();
  const offers = useSelector(selectOffers);

  return (
    <Box width="100%" paddingX={isLg && '100px'}>
      <Swiper
        pagination={{ clickable: true }}
        slidesPerView={1.15}
        spaceBetween={30}
        mousewheel={{ forceToAxis: true }}
        breakpoints={{
          992: {
            slidesPerView: 2,
          },
        }}
      >
        {offers.map((banner) => (
          <SwiperSlide key={banner.bannerId}>
            <Box
              marginBottom={
                isLg ? offers.length > 2 && '50px' : offers.length > 1 && '50px'
              }
            >
              <OfferBanner banner={banner} />
            </Box>
          </SwiperSlide>
        ))}
      </Swiper>
    </Box>
  );
};

export default Advertisement;
