import { Box, Flex, Text } from '@chakra-ui/layout';
import { Select } from '@chakra-ui/select';
import { css, StyleSheet } from 'aphrodite';
import { colors } from '../../../constants/colors';
import { useIndianCurrencyFormatter } from '../../../hooks/useIndianCurrencyFormatter';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { useProductContext } from '../../context/ProductContext';

// Code Review Completed

const ProductCardSelect = () => {
  const { isLg, isSm } = useMediaQuery();

  const {
    product: { sku },
    currentSku,
    handleCurrentSkuChange,
  } = useProductContext();

  const ecomPrice = useIndianCurrencyFormatter(currentSku.ecomPrice);
  const mrp = useIndianCurrencyFormatter(currentSku.MRP);

  const handleChange = (e) => {
    const { value } = e.target;
    const newSKU = sku.find((item) => item.ecomCode === value);
    handleCurrentSkuChange(newSKU);
  };

  return Object.keys(currentSku).length > 0 ? (
    <Box>
      <Flex mt={isSm && '5px'} alignItems="center" whiteSpace="nowrap">
        <Text
          fontSize={isLg ? 'md' : 'xs'}
          color={colors.black}
          fontWeight="bold"
        >
          {ecomPrice}
        </Text>
        {currentSku.discount > 0 && (
        <>
          {' '}
          <Text
            mx={isLg ? '10px' : '3px'}
            color={colors.gray}
            fontSize={isLg ? 'sm' : 'x-small'}
            className={css(styles.strike)}
          >
            {mrp}
          </Text>
          <Text fontSize={isLg ? 'sm' : 'x-small'} color={colors.lightRed}>
            {`${currentSku.discount}% Off`}
          </Text>
        </>
        )}
      </Flex>
      <Box mt="10px" align="center">
        <Select
          onChange={handleChange}
          _selected={currentSku}
          borderColor={colors.black}
          size={isSm ? 'sm' : 'md'}
          fontSize={isSm && 'small'}
          color={colors.black}
        >
          {sku.map((size) => (
            <option key={size.ecomCode} value={size.ecomCode}>
              {size.packSize < 1 ? size.packSize * 1000 : size.packSize}
              {' '}
              {size.packSize < 1 ? 'GM' : size.packUnit}
            </option>
          ))}
        </Select>
      </Box>
    </Box>
  ) : null;
};

const styles = StyleSheet.create({
  strike: {
    display: 'inline-block',
    textDecoration: 'none',
    position: 'relative',
    ':after': {
      content: "''",
      display: 'block',
      width: '100%',
      height: '55%',
      position: 'absolute',
      top: 0,
      left: 0,
      borderBottom: '1.5px solid',
    },
  },
});

export default ProductCardSelect;
