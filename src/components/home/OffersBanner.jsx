import { Image } from '@chakra-ui/image';
import { Box } from '@chakra-ui/layout';
import useClick from '../../../hooks/useClick';

const OfferBanner = ({ banner }) => {
  const { handleClick } = useClick(banner);

  return (
    <Box
      onClick={handleClick}
      width="100%"
      paddingTop="60%"
      position="relative"
      cursor="pointer"
    >
      <Image
        src={banner.bannerImg}
        position="absolute"
        top="0"
        left="0"
        height="100%"
        width="100%"
        objectFit="cover"
      />
    </Box>
  );
};

export default OfferBanner;
