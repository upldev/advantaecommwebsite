import { Box, Flex, Text } from '@chakra-ui/react';
import React from 'react';
import ReactStars from 'react-rating-stars-component';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { useProductContext } from '../../context/ProductContext';

// Code Review Completed

const ProductCardDetails = () => {
  const { isLg } = useMediaQuery();
  const {
    product: {
      crop, product, avgrating, totalreviews
    },
    inStock,
  } = useProductContext();

  return (
    <Box>
      <Flex
        flexDir="column"
        textTransform="uppercase"
        fontSize={!isLg && 'small'}
      >
        <Text
          fontWeight="bold"
          variant={isLg ? 'mdTextRegular' : 'textRegular'}
          mt={!isLg && '5px'}
        >
          {crop}
        </Text>
        <Text mt={!isLg && '5px'} color={colors.gray}>
          {product}
        </Text>
      </Flex>
      <Box fontSize={isLg ? 'small' : 'x-small'} mt={!isLg && '5px'}>
        {inStock ? (
          <Text color={colors.green}>IN STOCK</Text>
        ) : (
          <Text color={colors.lightRed}>OUT OF STOCK</Text>
        )}
      </Box>
      <Flex alignItems="center" mt={!isLg && '5px'}>
        <ReactStars
          count={5}
          size={isLg ? 18 : 12}
          edit={false}
          activeColor={colors.lightGold}
          value={avgrating}
        />
        <Text color={colors.lightBlue} ml={2} fontSize="xs">
          (
          {totalreviews}
          {' '}
          users)
        </Text>
      </Flex>
    </Box>
  );
};

export default ProductCardDetails;
