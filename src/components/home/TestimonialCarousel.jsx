import { Box } from '@chakra-ui/layout';
import { SwiperSlide } from 'swiper/react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import HomeSwiper from '../shared/HomeSwiper';
import ReviewCard from './ReviewCard';

const TestimonialCarousel = ({ reviews, name }) => {
  const { isLg } = useMediaQuery();

  return (
    <HomeSwiper
      name={name}
      lgSlides={2}
      smSlides={isLg ? 2 : 1.3}
      freeMode={{ sticky: true }}
    >
      {reviews.map((review) => {
        return (
          <SwiperSlide key={review.reviewId}>
            <Box marginBottom={!isLg && '40px'}>
              <ReviewCard review={review} />
            </Box>
          </SwiperSlide>
        );
      })}
    </HomeSwiper>
  );
};

export default TestimonialCarousel;
