import { useSelector } from 'react-redux';
import { selectTrendingProducts } from '../../../store/selectors/home';
import ProductsCarousel from '../shared/ProductsCarousel';
import Section from './Section';

const Trending = () => {
  const trending = useSelector(selectTrendingProducts);

  return (
    <Section
      title="TRENDING PRODUCTS"
      description="Get the best Seasonal purchase that other farmers are buying"
      data={trending}
    >
      <ProductsCarousel products={trending} name="trending" />
    </Section>
  );
};

export default Trending;
