import { useSelector } from 'react-redux';
import { selectBestSellerProducts } from '../../../store/selectors/home';
import ProductsCarousel from '../shared/ProductsCarousel';
import Section from './Section';

const BestSellers = () => {
  const trending = useSelector(selectBestSellerProducts);

  return (
    <Section
      title="BEST SELLER"
      description="Best Sellers Our star products, curated for you"
    >
      <ProductsCarousel name="bestseller" products={trending} />
    </Section>
  );
};

export default BestSellers;
