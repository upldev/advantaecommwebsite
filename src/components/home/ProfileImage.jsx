import { Flex, Image } from '@chakra-ui/react';
import { Storage } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';

const ProfileImage = () => {
  const { isSm } = useMediaQuery();
  const [img, setImg] = useState();
  const profileImage = useSelector((state) => state.user?.profile?.profileImg);

  useEffect(() => {
    (async () => {
      const storageRes = await Storage.get(profileImage, {
        level: 'protected',
      });
      setImg(storageRes);
    })();
  }, [profileImage]);

  return (
    <>
      {!profileImage ? (
        <Flex
          flexShrink={0}
          width={isSm ? 'full' : 'fit-content'}
          justifyContent="center"
        >
          <Image
            boxSize="75"
            objectFit="cover"
            src="./default-profile.png"
            borderRadius="50%"
            alt="Your Profile"
          />
        </Flex>
      ) : (
        <Flex>
          <Image
            boxSize="75"
            objectFit="cover"
            src={img}
            alt="Your Profile"
          />
        </Flex>
      )}
    </>
  );
};

export default ProfileImage;
