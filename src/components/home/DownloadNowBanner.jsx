import { Flex, Image, Text } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

// Code Review Completed

const DownloadNowBanner = () => {
  const { isLg, isXs } = useMediaQuery();
  return (
    <Flex
      flexDirection="column"
      backgroundColor={colors.white}
      mb="5"
      justifyContent="center"
      alignItems="center"
    >
      <Text mt="5" variant="subTitle"> Download Advanta Mobile App Now!! </Text>
      <Text m="2" color={colors.gray} variant="textRegular"> Shopping at your fingertips. Download our app now. </Text>
      <Flex mb="5" justifyContent={!isLg ? 'space-around' : 'center'} width="100%">
        <Image
          className={css(styles.getItOnPlayStore)}
          src="/google-play-badge.png"
          alt="play-store"
          height="4rem"
          width={!isXs ? '10rem' : '40%'}
        />
        <Image
          className={css(styles.getItOnAppStore)}
          src="/app-store.jpg"
          alt="app-store"
          cursor="pointer"
          height="3rem"
          width={!isXs ? '10rem' : '40%'}
        />
      </Flex>
    </Flex>
  );
};
const styles = StyleSheet.create({

  getItOnPlayStore: {
    marginRight: 10
  },
  getItOnAppStore: {
    marginTop: 6
  }
});
export default DownloadNowBanner;
