import {
  Flex, Icon, SimpleGrid, Stack, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { BiSupport } from 'react-icons/bi';
import { FiTruck } from 'react-icons/fi';
import { IoCardOutline, IoPricetagOutline } from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import MobileHomeBanner from './MobileHomeBanner';

// Code Review Completed

const HomeBanner = () => {
  const { isLg } = useMediaQuery();
  return (
    <>
      {isLg && (
        <Stack
          direction="row"
          backgroundColor={colors.white}
          className={css(styles.containerStyle)}
          py="28px"
        >
          <SimpleGrid
            columns={isLg ? 4 : 2}
            justifyContent="space-between"
            width="100%"
            mx={isLg ? 100 : 2}
          >
            <Flex
              alignItems="center"
              justifyContent={isLg ? 'flex-start' : 'center'}
              m={3}
              ml={isLg ? 0 : 3}
            >
              <Icon
                variant="ghost"
                style={{
                  cursor: 'pointer',
                  color: colors.black,
                }}
                as={FiTruck}
                boxSize="26px"
                mr={2}
              />
              <Text
                variant={isLg ? 'mdTextRegular' : 'textRegular'}
                textAlign="center"
                color={colors.black}
                fontSize="20px"
              >
                Direct Shipping
              </Text>
            </Flex>
            <Flex alignItems="center" justifyContent="center" m={3}>
              <Icon
                variant="ghost"
                style={{
                  cursor: 'pointer',
                  color: colors.black,
                }}
                as={IoPricetagOutline}
                boxSize="26px"
                mr={2}
              />
              <Text
                variant={isLg ? 'mdTextRegular' : 'textRegular'}
                textAlign="center"
                color={colors.black}
                fontSize="20px"
              >
                Lowest Prices
              </Text>
            </Flex>
            <Flex alignItems="center" justifyContent="center" m={3}>
              <Icon
                variant="ghost"
                style={{
                  cursor: 'pointer',
                  color: colors.black,
                }}
                as={BiSupport}
                boxSize="26px"
                mr={2}
              />
              <Text
                variant={isLg ? 'mdTextRegular' : 'textRegular'}
                textAlign="center"
                color={colors.black}
                fontSize="20px"
              >
                Online Support
              </Text>
            </Flex>
            <Flex
              alignItems="center"
              justifyContent={isLg ? 'flex-end' : 'center'}
              m={3}
              mr={isLg ? 0 : 3}
            >
              <Icon
                variant="ghost"
                style={{
                  cursor: 'pointer',
                  color: colors.black,
                }}
                as={IoCardOutline}
                boxSize="26px"
                mr={2}
              />
              <Text
                variant={isLg ? 'mdTextRegular' : 'textRegular'}
                textAlign="center"
                color={colors.black}
                fontSize="20px"
              >
                Secure Payment
              </Text>
            </Flex>
          </SimpleGrid>
        </Stack>
      )}

      {!isLg && <MobileHomeBanner />}
    </>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    borderColor: colors.gray,
    justifyContent: 'center',
  },
});

export default HomeBanner;
