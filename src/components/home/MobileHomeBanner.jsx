import {
  Flex,
  IconButton,
  Image,
  SimpleGrid,
  Stack,
  Text
} from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

// Code Review Completed

const HomeBanner = () => {
  const { isLg } = useMediaQuery();
  return (
    <Stack direction="row" my={5} backgroundColor={colors.white} py={3}>
      <SimpleGrid
        columns={isLg ? 4 : 2}
        justifyContent="space-between"
        width="100%"
        mx={isLg ? 100 : 2}
      >
        <Flex
          alignItems="center"
          justifyContent="center"
          m={3}
          ml={isLg ? 0 : 3}
        >
          <IconButton
            variant="ghost"
            // className={css(styles.iconStyles)}
            icon={(
              <Image
                style={{ width: 20, height: 30 * 0.6 }}
                src="/icons/shipping.png"
              />
            )}
            boxSize="1.2rem"
          />
          <Text
            fontSize="x-small"
            textAlign="center"
            color={colors.black}
            fontWeight="bold"
          >
            Shipping and Return
          </Text>
        </Flex>
        <Flex alignItems="center" justifyContent="center" m={3}>
          <IconButton
            variant="ghost"
            // className={css(styles.iconStyles)}
            icon={(
              <Image
                style={{ width: 20, height: 30 * 0.6 }}
                src="/icons/cod.png"
              />
            )}
            boxSize="1.2rem"
          />
          <Text
            fontSize="x-small"
            textAlign="center"
            color={colors.black}
            fontWeight="bold"
          >
            Cash on Delivery
          </Text>
        </Flex>
        <Flex alignItems="center" justifyContent="center" m={3}>
          <IconButton
            variant="ghost"
            // className={css(styles.iconStyles)}
            icon={(
              <Image
                style={{ width: 20, height: 30 * 0.7 }}
                src="/icons/online-support.png"
              />
            )}
            boxSize="1.2rem"
          />
          <Text
            fontSize="x-small"
            textAlign="center"
            color={colors.black}
            fontWeight="bold"
          >
            Online Support 24/7
          </Text>
        </Flex>
        <Flex
          alignItems="center"
          justifyContent={isLg ? 'flex-end' : 'center'}
          m={3}
          mr={isLg ? 0 : 3}
        >
          <IconButton
            variant="ghost"
            // className={css(styles.iconStyles)}
            icon={(
              <Image
                style={{ width: 20, height: 30 * 0.6 }}
                src="/icons/secure-payment.png"
              />
            )}
            boxSize="1.2rem"
          />
          <Text
            fontSize="x-small"
            textAlign="center"
            color={colors.black}
            fontWeight="bold"
          >
            Secure Payment
          </Text>
        </Flex>
      </SimpleGrid>
    </Stack>
  );
};

export default HomeBanner;
