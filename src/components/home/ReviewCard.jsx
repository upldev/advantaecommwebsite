import {
  Avatar, Box, Flex, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Storage } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const ReviewCard = ({ review }) => {
  const { isLg } = useMediaQuery();

  const [img, setImg] = useState();

  useEffect(() => {
    (async () => {
      const storageRes = await Storage.get(review.profileImg, {
        level: 'protected',
        identityId: review.identityId,
      });
      setImg(storageRes);
    })();
  }, [review.identityId, review.profileImg]);

  return (
    <Box mx={isLg && '5'} justifyContent="space-between" flexDirection="column">
      <Flex minHeight={75} m={1} className={css(styles.review)}>
        <Text m={2}>{review.review}</Text>
      </Flex>
      <Flex mt={2} alignItems="center">
        {img && <Avatar m="1" name={review.farmerName} src={img} />}
        <Flex flexDirection="column">
          <Text fontWeight="bold" color={colors.black} variant="textRegular">
            {review.farmerName}
          </Text>
          <Text color={colors.gray} variant="textRegular">
            Our Customer
          </Text>
        </Flex>
      </Flex>
    </Box>
  );
};

const styles = StyleSheet.create({
  review: {
    backgroundColor: colors.light,
    borderRadius: '10px',
    borderWidth: 1,
    borderColor: colors.gray,
    margin: 1,
  },
});

export default ReviewCard;
