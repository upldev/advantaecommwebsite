import { Button, IconButton } from '@chakra-ui/button';
import Icon from '@chakra-ui/icon';
import { AddIcon, MinusIcon } from '@chakra-ui/icons';
import { Box, Flex, Text } from '@chakra-ui/layout';
import { Spinner } from '@chakra-ui/spinner';
import { IoTrashOutline } from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { useProductContext } from '../../context/ProductContext';

// Code Review Completed

const SKUActions = () => {
  const {
    inCart,
    handleAddToCart,
    handleRemoveFromCart,
    handleIncrement,
    handleDecrement,
    isCartUpdating,
    currentSku: sku,
  } = useProductContext();

  const { isLg } = useMediaQuery();

  return (
    <Box pt="10px" mt="1" align="center">
      {!inCart ? (
        <Button
          width="100%"
          fontSize={!isLg && 'x-small'}
          variant={sku.qtyAvailable === 0 ? 'disabledAddToCart' : 'addToCart'}
          isFullWidth
          padding={!isLg && '0'}
          disabled={sku.qtyAvailable === 0}
          size={isLg ? 'md' : 'sm'}
          onClick={() => handleAddToCart(sku)}
        >
          ADD TO CART
        </Button>
      ) : (
        <Flex justifyContent="center">
          <Flex fontWeight="bold" justifyContent="center">
            {inCart?.qty > 1 && (
              <IconButton
                borderRadius="0"
                _hover={{ opacity: 0.5 }}
                onClick={() => handleRemoveFromCart(sku)}
                bgColor={colors.gray}
                color={colors.white}
                mr={2}
                fontSize="sm"
                size={isLg ? 'md' : 'sm'}
              >
                <Icon as={IoTrashOutline} />
              </IconButton>
            )}
            {inCart?.qty > 1 ? (
              <IconButton
                borderRadius="0"
                _hover={{ opacity: 0.5 }}
                backgroundColor={colors.green}
                color={colors.white}
                onClick={() => handleDecrement(sku)}
                fontSize="sm"
                size={isLg ? 'md' : 'sm'}
                disabled={isCartUpdating}
              >
                <Icon as={MinusIcon} boxSize="0.7rem" />
              </IconButton>
            ) : (
              <IconButton
                borderRadius="0"
                _hover={{ opacity: 0.5 }}
                onClick={() => handleRemoveFromCart(sku)}
                bgColor={colors.gray}
                color={colors.white}
                fontSize="sm"
                size={isLg ? 'md' : 'sm'}
              >
                <Icon as={IoTrashOutline} />
              </IconButton>
            )}
            <Flex
              alignItems="center"
              justifyContent="center"
              borderTop="1px"
              borderBottom="1px"
              height="100%"
              width={isLg ? '5vw' : '8vw'}
              borderColor={colors.gray}
            >
              {isCartUpdating ? (
                <Spinner size={isLg ? 'md' : 'sm'} />
              ) : (
                <Text fontSize={!isLg && 'xs'} color={colors.black}>{inCart.qty}</Text>
              )}
            </Flex>
            <IconButton
              size={isLg ? 'md' : 'sm'}
              borderRadius="0"
              _hover={{ opacity: 0.5 }}
              backgroundColor={colors.green}
              color={colors.white}
              disabled={isCartUpdating || inCart?.qty === sku.qtyAvailable}
              onClick={() => handleIncrement(sku)}
              fontSize="sm"
            >
              <Icon as={AddIcon} boxSize="0.7rem" />
            </IconButton>
          </Flex>
        </Flex>
      )}
    </Box>
  );
};

export default SKUActions;
