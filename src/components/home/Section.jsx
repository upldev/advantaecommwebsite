import { Box, Text } from '@chakra-ui/layout';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const Section = ({
  title, description, children, id
}) => {
  const { isLg } = useMediaQuery();
  return (
    <Box
      id={id}
      backgroundColor={colors.white}
      paddingTop={isLg ? '32px' : '25px'}
      paddingX={isLg ? 100 : 2}
    >
      <Box alignItems="center" textAlign="center" justifyContent="center">
        <Text variant="heading" marginBottom="10px" fontSize="25px">
          {title}
        </Text>
        <Text color={colors.gray} fontSize="16px">
          {description}
        </Text>
      </Box>
      <Box>{children}</Box>
    </Box>
  );
};

export default Section;
