import { Flex, Image } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import React from 'react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { productDetailsPage } from '../../../lib/router';
import { useProductContext } from '../../context/ProductContext';

// Code Review Completed

const ProductCardImage = () => {
  const { isLg } = useMediaQuery();

  const {
    product: { materialCode, productImg },
    inStock,
  } = useProductContext();

  const router = useRouter();

  return (
    <Flex
      onClick={() => router.push(productDetailsPage(materialCode))}
      zIndex={1}
      justifyContent="center"
      my={5}
      height={isLg ? '10rem' : '5rem'}
      cursor="pointer"
    >
      <Image
        objectFit="contain"
        src={productImg[0]}
        className={!inStock && css(styles.image)}
      />
    </Flex>
  );
};

const styles = StyleSheet.create({
  image: {
    WebkitFilter: 'grayscale(1)',
    filter: 'grayscale(1)',
  },
});

export default ProductCardImage;
