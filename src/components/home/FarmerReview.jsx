import React from 'react';
import { useSelector } from 'react-redux';
import Section from './Section';
import TestimonialCarousel from './TestimonialCarousel';

// Code Review Completed

const FarmerReview = () => {
  const advantaKisan = useSelector((state) => state.home.advantaKisan);

  return (
    <Section
      title="ADVANTA KISAN"
      description="We have successful products along with success stories. Hear what our Advanta Kisan had to say!"
    >
      <TestimonialCarousel reviews={advantaKisan} />
    </Section>
  );
};

export default FarmerReview;
