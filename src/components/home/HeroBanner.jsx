import { Image } from '@chakra-ui/image';
import { Box } from '@chakra-ui/layout';
import useClick from '../../../hooks/useClick';
import useMediaQuery from '../../../hooks/useMediaQuery';

const HeroBanner = ({ banner }) => {
  const { isLg } = useMediaQuery();
  const { handleClick } = useClick(banner);

  return (
    <Box
      onClick={handleClick}
      width="100%"
      height={isLg ? '33vw' : '50vw'}
      cursor="pointer"
    >
      <Image
        src={isLg ? banner.bannerImg : banner.bannerMobileImg}
        height="100%"
        width="100%"
        objectFit="cover"
      />
    </Box>
  );
};

export default HeroBanner;
