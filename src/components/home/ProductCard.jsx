import { Box } from '@chakra-ui/react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { ProductContext } from '../../context/ProductContext';
import WishListButton from '../shared/WishListButton';
import ProductCartActions from './ProductCardActions';
import ProductCardDetails from './ProductCardDetails';
import ProductCardImage from './ProductCardImage';
import ProductCardSelect from './ProductCardSelect';

// Code Review Completed

const ProductCard = ({ product }) => {
  const { isLg } = useMediaQuery();

  return (
    <ProductContext product={product}>
      <Box
        className="product-card"
        color={colors.gray}
        width="100%"
        borderWidth="1px"
        borderColor={colors.gray}
        padding={isLg ? '15px' : '8px'}
        backgroundColor={colors.white}
        maxH="30rem"
        overflow="hidden"
      >
        <WishListButton />
        <ProductCardImage />
        <ProductCardDetails />
        <ProductCardSelect />
        <ProductCartActions />
      </Box>
    </ProductContext>
  );
};

export default ProductCard;
