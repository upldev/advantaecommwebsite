import { Flex } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

// Code Review Complete

const Progress = ({ progress }) => {
  const { isLg } = useMediaQuery();

  return (
    <Flex justifyContent="center">
      <Flex
        justifyContent="space-between"
        width={isLg ? '30%' : '80%'}
        alignItems="center"
      >
        <div className={css(styles.timeline)}>
          <div
            className={css(styles.timelineProgress)}
            style={{
              width: progress === 1 ? '0%' : progress === 2 ? '50%' : '100%',
            }}
          />
          <div className={css(styles.timelineItems)}>
            <div
              className={css(
                styles.timelineItemContainer,
                progress >= 1 ? styles.timelineItemContainerActive : null
              )}
            >
              <div
                className={css(
                  styles.timelineItem,
                  progress >= 1 ? styles.timelineItemActive : null
                )}
              >
                <div className={css(styles.timelineContent)}>Cart</div>
              </div>
            </div>
            <div
              className={css(
                styles.timelineItemContainer,
                progress >= 2 ? styles.timelineItemContainerActive : null
              )}
            >
              <div
                className={css(
                  styles.timelineItem,
                  progress >= 2 ? styles.timelineItemActive : null
                )}
              >
                <div className={css(styles.timelineContent)}>Address</div>
              </div>
            </div>
            <div
              className={css(
                styles.timelineItemContainer,
                progress >= 3 ? styles.timelineItemContainerActive : null
              )}
            >
              <div
                className={css(
                  styles.timelineItem,
                  progress >= 3 ? styles.timelineItemActive : null
                )}
              >
                <div className={css(styles.timelineContent)}>Payment</div>
              </div>
            </div>
          </div>
        </div>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  timeline: {
    marginTop: '50px',
    marginBottom: '12px',
    height: '4px',
    width: '100%',
    backgroundColor: colors.gray,
  },
  timelineProgress: {
    height: '100%',
    backgroundColor: colors.lightBlue,
  },
  timelineItems: {
    marginLeft: '-8px',
    marginRight: '-8px',
    marginTop: '-16px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  timelineItemContainer: {
    padding: '6px',
    borderWidth: 2,
    borderColor: colors.gray,
    borderRadius: '100%',
  },
  timelineItemContainerActive: {
    borderColor: colors.lightBlue,
  },
  timelineItem: {
    position: 'relative',
    '::before': {
      content: "''",
      width: '12px',
      height: '12px',
      backgroundColor: colors.gray,
      display: 'block',
      borderRadius: '100%',
    },
  },
  timelineContent: {
    position: 'absolute',
    bottom: '30px',
    left: '50%',
    transform: 'translateX(-50%)',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  timelineItemActive: {
    color: colors.lightBlue,
    '::before': {
      backgroundColor: colors.lightBlue,
    },
  },
});

export default Progress;
