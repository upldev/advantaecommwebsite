import { Box, RadioGroup } from '@chakra-ui/react';
import { useState } from 'react';
import CouponRadio from './CouponRadio';

// Code Review Completed

const CouponRadioList = ({ cartCoupons, productCoupons, appliedCoupons }) => {
  const [selected, setSelected] = useState([]);
  return (
    <Box>
      <RadioGroup value={selected}>
        {productCoupons
          && productCoupons.map((coupon) => (
            <CouponRadio
              key={coupon.discountId}
              coupon={coupon}
              isCart={false}
              setSelected={setSelected}
            />
          ))}
        {cartCoupons
          && cartCoupons.map((coupon) => (
            <CouponRadio
              key={coupon.discountId}
              coupon={coupon}
              isCart
              setSelected={setSelected}
            />
          ))}
      </RadioGroup>
    </Box>
  );
};

export default CouponRadioList;
