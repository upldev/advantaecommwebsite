/* eslint-disable max-len */
import { Box } from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { getCoupons } from '../../../store/actions/coupons';
import CouponModal from './CouponModal';
import ReferralForm from './ReferralForm';

// Code Review Completed
// Need to Discuss getCoupons Action with Afrin

const Coupon = () => {
  const dispatch = useDispatch();
  const [meta, setMeta] = useState(null);
  const [cartCoupons, setCartCoupons] = useState([]);
  const [productCoupons, setProductCoupons] = useState([]);
  const { cartItems, cartProducts } = useSelector((state) => state.cart);
  const { isLg } = useMediaQuery();

  useEffect(() => {
    dispatch(getCoupons({ setCartCoupons, setProductCoupons, setMeta }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cartItems, cartProducts]);

  return (
    <Box bgColor={colors.white} flexDir="column" padding="5px">
      {meta && meta.referralActive && <ReferralForm />}
      <CouponModal
        cartCoupons={cartCoupons}
        productCoupons={productCoupons}
        meta={meta}
      />
    </Box>
  );
};

export default Coupon;
