import {
  Badge, Box, Flex, Icon, Radio, Text
} from '@chakra-ui/react';
import { IoPricetags } from 'react-icons/io5';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import {
  radioAddCartCoupon,
  RadioAddProductCoupon
} from '../../../store/actions/coupons';

const CouponRadio = ({ coupon, isCart, setSelected }) => {
  const dispatch = useDispatch();

  const handleChange = () => {
    setSelected(coupon.discountId);
    if (isCart) {
      dispatch(radioAddCartCoupon(coupon));
    } else {
      dispatch(RadioAddProductCoupon(coupon));
    }
  };

  return (
    <>
      <Box py="10px">
        <Radio value={coupon.discountId} onChange={handleChange}>
          {coupon.discount.isFixed ? (
            <Text size="sm" color={colors.advantaBlue}>
              Get
              {' '}
              {`₹${coupon.discount.amount}`}
              {' '}
              discount on
              {' '}
              <Badge mx="10px" colorScheme="purple">
                <Flex alignItems="center">
                  <Icon fontSize="md" mr="5px" as={IoPricetags} />
                  <Text fontSize="md">{coupon.couponName}</Text>
                </Flex>
              </Badge>
            </Text>
          ) : (
            <Text size="sm" color={colors.advantaBlue}>
              Get
              {' '}
              {`${coupon.discount.amount}%`}
              {' '}
              discount on
              {' '}
              <Badge mx="10px" colorScheme="purple">
                <Flex>
                  <Icon fontSize="md" pt={1} as={IoPricetags} />
                  <Text fontSize="md">{coupon.couponName}</Text>
                </Flex>
              </Badge>
            </Text>
          )}
        </Radio>
      </Box>
    </>
  );
};

export default CouponRadio;
