import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  Input, Text,
  Tooltip
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Formik } from 'formik';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import * as yup from 'yup';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { addReferral } from '../../../store/actions/coupons';

const schema = yup.object().shape({
  code: yup
    .string()
    .required('code required')
    .length(6, 'code should be six digit'),
});

const ReferralForm = () => {
  const dispatch = useDispatch();

  const { isLg } = useMediaQuery();

  const [used, setUsed] = useState(false);
  const handleSubmit = (values) => {
    dispatch(addReferral({ code: values.code, setUsed }));
  };

  return (
    <Box className={css(styles.container)}>
      <Box fontWeight="bold" paddingBottom="10px">
        <Text>Enter Referral Code</Text>
      </Box>
      <Formik
        initialValues={{ code: '' }}
        validationSchema={schema}
        onSubmit={handleSubmit}
      >
        {(props) => (
          <FormControl
            mb={2}
            isRequired
            isInvalid={props.touched.code && props.errors.code}
          >
            <Flex>
              <Input
                name="code"
                value={props.values.code}
                onChange={props.handleChange}
                onBlur={props.handleBlur}
                borderRightWidth="0"
                placeholder="Referal Coupon"
              />
              {used ? (
                <Tooltip label="Only one referral discount applicable!">
                  <Button variant="primary">Apply</Button>
                </Tooltip>
              ) : (
                <Button variant="primary" onClick={props.handleSubmit}>
                  Apply
                </Button>
              )}
            </Flex>
            <FormErrorMessage>{props.errors.code}</FormErrorMessage>
          </FormControl>
        )}
      </Formik>
    </Box>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    padding: 20,
  },
});
export default ReferralForm;
