/* eslint-disable max-len */
import { AddIcon, MinusIcon } from '@chakra-ui/icons';
import {
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
  Button,
  Text
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import CouponListItem from './CouponListItem';
import CouponRadioList from './CouponRadioList';

// Code Review Completed

const CouponModal = ({ cartCoupons, productCoupons, meta }) => {
  const appliedCoupons = useSelector((state) => [
    ...state.checkout.appliedCartCoupons.map((item) => item.discountId),
    ...[].concat(
      ...state.checkout.appliedProductCoupons.map((item) => item.discounts.map((i) => i.discountId))
    ),
  ]);
  const [cartDiscounts, setCartDiscounts] = useState(cartCoupons);
  const [productDiscounts, setProductDiscounts] = useState(productCoupons);

  useEffect(() => {
    setCartDiscounts(cartCoupons);
    setProductDiscounts(productCoupons);
  }, [cartCoupons, productCoupons]);

  return (
    <>
      {(cartCoupons.length > 0 || productCoupons.length > 0) && (
        <Accordion defaultIndex={[0]} allowMultiple>
          <AccordionItem border="0">
            {({ isExpanded }) => (
              <>
                <AccordionButton _hover={{ bg: colors.white }}>
                  <Button
                    variant="link"
                    color={colors.advantaBlue}
                    my="1rem"
                    leftIcon={
                      isExpanded ? (
                        <MinusIcon size={14} />
                      ) : (
                        <AddIcon size={14} />
                      )
                    }
                  >
                    <Text
                      color={colors.advantaBlue}
                      fontWeight="bold"
                      paddingRight={2}
                    >
                      View all Available Coupons
                    </Text>
                  </Button>
                </AccordionButton>
                <AccordionPanel borderBottomWidth="0" pb={4}>
                  {meta && meta.multipleApplicable ? (
                    <>
                      {productDiscounts
                        && productDiscounts.map((coupon) => (
                          <CouponListItem
                            coupon={coupon}
                            multiple={meta.multipleApplicable}
                            key={coupon.discountId}
                            appliedCoupons={appliedCoupons}
                            isCartCoupons={false}
                          />
                        ))}
                      {cartDiscounts
                        && cartDiscounts.map((coupon) => (
                          <CouponListItem
                            coupon={coupon}
                            multiple={meta.multipleApplicable}
                            key={coupon.discountId}
                            appliedCoupons={appliedCoupons}
                            isCartCoupons
                          />
                        ))}
                    </>
                  ) : (
                    <CouponRadioList
                      cartCoupons={cartDiscounts}
                      productCoupons={productDiscounts}
                      appliedCoupons={appliedCoupons}
                    />
                  )}
                </AccordionPanel>
              </>
            )}
          </AccordionItem>
        </Accordion>
      )}
    </>
  );
};

export default CouponModal;
