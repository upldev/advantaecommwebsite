import {
  Badge,
  Box,
  Checkbox, Flex,
  Heading,
  Icon,
  Text
} from '@chakra-ui/react';
import { useEffect, useState } from 'react';
import { IoPricetags } from 'react-icons/io5';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import {
  addCartCoupon, addProductCoupon, removeCartCoupon, removeProductCoupon
} from '../../../store/actions/coupons';

// Code Review Completed

const CouponListItem = ({ coupon, appliedCoupons, isCartCoupons }) => {
  const dispatch = useDispatch();
  const [tick, setTick] = useState(false);

  useEffect(() => {
    setTick(() => appliedCoupons.includes(coupon.discountId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [appliedCoupons]);

  const handleTick = (e) => {
    if (e.target.checked) {
      if (isCartCoupons) dispatch(addCartCoupon(coupon));
      else dispatch(addProductCoupon(coupon));
    } else if (isCartCoupons) dispatch(removeCartCoupon(coupon));
    else dispatch(removeProductCoupon(coupon));
  };

  return (
    <>
      <Box p={3}>
        <Checkbox onChange={(e) => handleTick(e)} defaultChecked={tick}>
          {coupon.discount.isFixed ? (
            <Heading size="sm" color={colors.advantaBlue}>
              Get
              {' '}
              {`₹${coupon.discount.amount}`}
              {' '}
              discount on
              {' '}
              <Badge size="lg" p={1} colorScheme="purple">
                <Flex>
                  <Icon fontSize="md" pt={1} as={IoPricetags} />
                  <Text fontSize="md">{coupon.couponName}</Text>
                </Flex>
              </Badge>
            </Heading>
          ) : (
            <Heading size="sm" color={colors.advantaBlue}>
              Get
              {' '}
              {`${coupon.discount.amount}%`}
              {' '}
              discount on
              {' '}
              <Badge size="lg" p={1} colorScheme="purple">
                <Flex>
                  <Icon fontSize="md" pt={1} as={IoPricetags} />
                  <Text fontSize="md">{coupon.couponName}</Text>
                </Flex>
              </Badge>
            </Heading>
          )}
        </Checkbox>
      </Box>
    </>
  );
};

export default CouponListItem;
