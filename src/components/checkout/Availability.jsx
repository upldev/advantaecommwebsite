import {
  Alert,
  AlertDescription,
  AlertIcon,
  Button,
  Flex
} from '@chakra-ui/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { addMultipleToWishList } from '../../../store/actions/cart';
import { selectIsInvalid } from '../../../store/selectors/cart';

// Code Review Complete

const Availability = () => {
  const dispatch = useDispatch();
  const { isInvalid, unavailable } = useSelector(selectIsInvalid);

  const handleMoveToWishList = () => {
    dispatch(addMultipleToWishList(unavailable));
  };

  return (
    isInvalid && (
      <Alert
        mt={5}
        status="error"
        alignItems="center"
        justifyContent="space-between"
      >
        <Flex alignItems="center">
          <AlertIcon />
          <AlertDescription>
            There are items in your cart that are currently unavailable
          </AlertDescription>
        </Flex>
        <Button
          onClick={handleMoveToWishList}
          variant="link"
          color={colors.lightRed}
        >
          Move all to Wishlist
        </Button>
      </Alert>
    )
  );
};

export default Availability;
