import { ChevronRightIcon } from '@chakra-ui/icons';
import {
  Breadcrumb, BreadcrumbItem, BreadcrumbLink, Flex, Icon
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { AiOutlineHome } from 'react-icons/ai';
import { colors } from '../../../constants/colors';

// Code Review Completed

const WishlistBreadCrumb = () => {
  return (
    <Flex mb="5" width="100%" bg={colors.light} boxShadow="lg" pt={5} pb={5}>
      <Flex mx="100">
        <Breadcrumb
          spacing="8px"
          alignItems="center"
          separator={<ChevronRightIcon color={colors.gray} />}
        >
          <BreadcrumbItem>
            <BreadcrumbLink
              href="/"
            >
              <Icon
                variant="ghost"
                as={AiOutlineHome}
                boxSize="1.5rem"
              />
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink
              className={css(styles.breadcrumbStyling)}
            >
              Wishlist
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  breadcrumbStyling: {
    ':hover': {
      textDecoration: 'none',
      color: colors.lightBlue
    }
  }
});
export default WishlistBreadCrumb;
