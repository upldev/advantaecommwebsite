import { Flex, Image, Text } from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const EmptyWishlist = () => {
  const { isLg } = useMediaQuery();
  return (

    <Flex
      justifyContent="center"
      width="100%"
      height={isLg ? '100%' : '65%'}
      bgColor={colors.white}
      alignItems="center"
      flexDirection="column"
    >

      <Image
        src="./EC_image.PNG"
        borderColor={colors.light}
        width={isLg ? '40%' : '50%'}
        height={isLg ? '50%' : '60%'}
      />
      <Text variant={isLg ? 'title' : 'subTitle'} mt="5" textAlign="center" fontWeight="bold" color="gray.600">
        Your wishlist is empty.
      </Text>

    </Flex>

  );
};

export default EmptyWishlist;
