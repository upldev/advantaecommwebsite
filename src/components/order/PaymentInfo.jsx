import {
  Box, Flex, Stack, Text
} from '@chakra-ui/layout';
import { Divider } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { useIndianCurrencyFormatter } from '../../../hooks/useIndianCurrencyFormatter';
import {
  selectTotalDiscount, selectTotalMRP
} from '../../../store/selectors/cart';
import { selectPaymentInfo } from '../../../store/selectors/checkout';

// Code Review Completed

const PaymentInfo = ({ Action, isMobilePayment }) => {
  const totalMRPDiscount = useIndianCurrencyFormatter(
    useSelector(selectTotalDiscount)
  );
  const shipping = useIndianCurrencyFormatter(0);
  const GST = useIndianCurrencyFormatter(0);
  const totalMRP = useSelector(selectTotalMRP);

  const {
    bulkDiscount,
    couponDiscount,
    finalTotal,
    referralDiscount,
    initialPurchase,
  } = useSelector(selectPaymentInfo);

  const taggedBulkDiscount = useIndianCurrencyFormatter(bulkDiscount);
  const taggedCouponDiscount = useIndianCurrencyFormatter(couponDiscount);
  const taggedReferralDiscount = useIndianCurrencyFormatter(referralDiscount);
  const taggedMRPDiscount = useIndianCurrencyFormatter(totalMRPDiscount);
  const taggedMRP = useIndianCurrencyFormatter(totalMRP);
  const taggedFinalTotal = useIndianCurrencyFormatter(finalTotal);

  return (
    <div>
      <Box className={css(styles.container)}>
        {!isMobilePayment && (
          <Box fontWeight="bold" padding="10px" bgColor={colors.lightGray}>
            <Text>Payment Information</Text>
          </Box>
        )}
        <Stack padding="10px">
          <Flex direction="row" justifyContent="space-between">
            <Text>Total MRP</Text>
            <Text>{taggedMRP}</Text>
          </Flex>
          <Flex direction="row" justifyContent="space-between">
            <Text>Discount on MRP</Text>
            <Text>{`-${taggedMRPDiscount}`}</Text>
          </Flex>
          <Flex direction="row" justifyContent="space-between">
            <Text>Coupon Discount</Text>
            <Text color={colors.lightRed}>
              {couponDiscount > 0 ? `-${taggedCouponDiscount}` : '₹0.00'}
            </Text>
          </Flex>
          <Flex direction="row" justifyContent="space-between">
            <Text>Bulk Discount</Text>
            <Text color={colors.lightRed}>
              {bulkDiscount > 0 ? `-${taggedBulkDiscount}` : '₹0.00'}
            </Text>
          </Flex>
          <Flex direction="row" justifyContent="space-between">
            <Text>Referral Discount</Text>
            <Text color={colors.lightRed}>
              {referralDiscount > 0 ? `-${taggedReferralDiscount}` : '₹0.00'}
            </Text>
          </Flex>
          {initialPurchase
            && initialPurchase.map((item) => (
              <Flex
                direction="row"
                key={item.couponName}
                justifyContent="space-between"
              >
                <Text>{item.couponName}</Text>
                - ₹
                {' '}
                {item.discount.toFixed(2)}
              </Flex>
            ))}
          <Flex direction="row" justifyContent="space-between">
            <Text>GST(+)</Text>
            <Text>{GST}</Text>
          </Flex>
          <Flex direction="row" justifyContent="space-between">
            <Text>Shipping</Text>
            <Text>{shipping}</Text>
          </Flex>
          <Divider />
          <Flex direction="row" justifyContent="space-between">
            <Text>Sub Total</Text>
            <Text>{taggedMRP}</Text>
          </Flex>
          <Divider />
          <Flex direction="row" justifyContent="space-between">
            <Text fontWeight="bold">Total amount</Text>
            <Text fontWeight="bold">{taggedFinalTotal}</Text>
          </Flex>
        </Stack>
        {!isMobilePayment && (
          <Flex padding="5" justifyContent="center">
            {Action}
          </Flex>
        )}
      </Box>
    </div>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
  },
});

export default PaymentInfo;
