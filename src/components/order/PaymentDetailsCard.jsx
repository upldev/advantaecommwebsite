import { Flex, Icon, Text } from '@chakra-ui/react';
import React from 'react';
import { BiRupee } from 'react-icons/bi';
import { colors } from '../../../constants/colors';
import { indianCurrencyFormatter } from '../../../constants/IndianCurrencyFormatter';
import useMediaQuery from '../../../hooks/useMediaQuery';

// Code Review Completed

const PaymentDetailsCard = ({ order }) => {
  const { isLg, isMd } = useMediaQuery();
  return (
    <Flex
      borderWidth="1px"
      borderColor={colors.gray}
      width={isLg ? '70%' : '100%'}
      flexDirection="column"
      mb={5}
    >
      <Flex backgroundColor={colors.gray}>
        <Text mx={5} my={2} variant="subTitle" fontWeight="bolder">
          Address/ Payment Details
        </Text>
      </Flex>
      <Flex bgColor={colors.white} flexDirection="column">
        <Flex width="70%" borderWidth="1px" borderColor={colors.gray} m={5} bgColor={colors.light} flexDirection="column">
          <Text fontSize={isMd ? 'sm' : 'md'} m="3" fontWeight="bolder">{order.shipping.name}</Text>
          <Text fontSize={isMd ? 'sm' : 'md'} mx={3} mb="3" color={colors.advantaBlue}>{order.farmerMobile}</Text>
          <Text fontSize={isMd ? 'sm' : 'md'} mx={3} mb="3">
            {`${order.shipping.address}, ${order.shipping.city}, ${order.shipping.state} - ${order.shipping.pincode}`}
          </Text>
        </Flex>
        <Flex m={5}>
          <Flex flexDirection="column">
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Item(s) Subtotal
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Header Discount
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Item(s) Discount
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Shipping Charges
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Order Total
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Net Payment
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              Payment Mode
            </Text>
          </Flex>
          <Flex flexDirection="column">
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mx="2" mb="3">
              :
            </Text>
          </Flex>
          <Flex flexDirection="column">
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              {' '}
              <Icon as={BiRupee} />
              {' '}
              {indianCurrencyFormatter(order.totalEcomPrice)}
              {' '}
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              {' '}
              <Icon as={BiRupee} />
              {' '}
              {indianCurrencyFormatter(order.totalHeaderDiscount)}
              {' '}
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              {' '}
              <Icon as={BiRupee} />
              {' '}
              {indianCurrencyFormatter(order.totalItemDiscount)}
              {' '}
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              {' '}
              <Icon as={BiRupee} />
              {' '}
              {indianCurrencyFormatter(order.shippingCharges)}
              {' '}
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              {' '}
              <Icon as={BiRupee} />
              {' '}
              {indianCurrencyFormatter(
                order.totalEcomPrice - order.totalHeaderDiscount - order.totalItemDiscount
              )}
              {' '}
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray} mb="3">
              {' '}
              <Icon as={BiRupee} />
              {' '}
              {indianCurrencyFormatter(order.totalPayable)}
            </Text>
            <Text fontSize={isMd ? 'sm' : 'md'} color={colors.advantaBlue} fontWeight="bold" mb="3">
              {order.paymentMethod ? order.paymentMethod.toUpperCase() : <p>&#8212;</p> }
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default PaymentDetailsCard;
