import {
  Alert, AlertIcon, AlertTitle, Flex, Icon, Skeleton, Stack, Text
} from '@chakra-ui/react';
import { React } from 'react';
import { BiRupee } from 'react-icons/bi';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import OrderProductImage from '../account/OrderProductImage';

// Code Review Completed

const OrderDetailsProductCard = ({
  product, orderDelivered
}) => {
  const { isMd, isLg } = useMediaQuery();
  return (
    <Flex width="100%" justifyContent="space-between" alignItems="flex-start" p={isMd ? 2 : 5}>
      <Flex minWidth={isMd ? '55%' : '40%'} maxWidth="60%">
        <Flex maxWidth={isLg ? '10%' : '40%'}>
          <OrderProductImage productImg={product.productDetails?.productImg} />
        </Flex>
        {
          product.productDetails ? (
            <Flex ml={3}>
              <Flex flexDirection="column" flexShrink={0}>
                <Text fontSize={isMd ? 'sm' : 'md'} fontWeight="bolder">{product.productDetails?.crop}</Text>
                <Text fontSize={isMd ? 'sm' : 'md'} color={colors.gray}>{product.productDetails?.product}</Text>
              </Flex>
            </Flex>
          ) : (
            <Stack minWidth="100px" ml={5}>
              <Skeleton height="20px" />
              <Skeleton height="20px" />
            </Stack>
          )
        }
      </Flex>
      <Flex bgColor={colors.light} borderWidth="1px" borderColor={colors.gray} flexShrink={0}>
        {
          product.sku.packSize >= 1
            ? (<Text mx={2} my={1} fontWeight="bolder" fontSize={isMd ? 'sm' : 'md'}>{`${product.sku.packSize} ${product.sku.packUnit}`}</Text>)
            : (<Text mx={2} my={1} fontWeight="bolder" fontSize={isMd ? 'sm' : 'md'}>{`${product.sku.packSize * 1000} GM`}</Text>)
        }
      </Flex>
      <Flex flexShrink={0}>
        <Text fontWeight="bolder" pt={isLg && 2}>
          <Icon as={BiRupee} />
          {product.sku.ecomPrice - product.itemDiscount}
        </Text>
      </Flex>
      {
        isLg && orderDelivered && (
          <Flex>
            <Alert m={0} bgColor={colors.white} status="success">
              <AlertIcon mt={0} color={colors.green} />
              <AlertTitle mt={0} color={colors.green} mr={2}>Delivered</AlertTitle>
            </Alert>
          </Flex>
        )
      }
    </Flex>
  );
};

export default OrderDetailsProductCard;
