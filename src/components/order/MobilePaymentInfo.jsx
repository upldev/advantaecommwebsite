import {
  Button, Drawer, DrawerBody, DrawerContent, DrawerHeader,
  DrawerOverlay, Flex, Text, useDisclosure
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { displayUserRazorpay } from '../../../store/actions/checkout';
import {
  selectIsInvalid,
  selectOrderItems
} from '../../../store/selectors/cart';
import PaymentInfo from './PaymentInfo';

// Code Review Completed

const MobilePaymentInfo = () => {

  const { isInvalid } = useSelector(selectIsInvalid);
  const { isPaymentLoading, isOrderProcessing } = useSelector(
    (state) => state.feedback
  );
  const checkout = useSelector((state) => state.checkout);
  const orderItems = useSelector(selectOrderItems);
  const router = useRouter();

  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const handlePayment = () => {
    dispatch(displayUserRazorpay(orderItems, checkout.currentUser, router));
  };
  return (
    <>
      <Text
        ml={2}
        variant="smTextRegular"
        color={colors.gray}
        fontWeight="bold"
        style={{
          cursor: 'pointer',
        }}
        onClick={onOpen}
      >
        View Details
      </Text>
      <Drawer placement="bottom" onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader borderBottomWidth="1px" bgColor={colors.light}>
            <Flex flexDir="row" justifyContent="space-between">
              <Text variant="mdTextRegular">Payment Information</Text>
              <Text variant="mdTextRegular" onClick={onClose} color={colors.gray}>
                Close
              </Text>
            </Flex>
          </DrawerHeader>
          <DrawerBody>
            <PaymentInfo
              Action={(
                <Button
                  isDisabled={isInvalid}
                  onClick={handlePayment}
                  variant="primary"
                  isLoading={isPaymentLoading}
                >
                  PLACE ORDER
                </Button>
                )}
              isMobilePayment
            />
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default MobilePaymentInfo;
