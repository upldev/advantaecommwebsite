import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';
import { Button, HStack } from '@chakra-ui/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getOrders } from '../../../store/actions/orders';
import { selectIsNext, selectIsPrev } from '../../../store/selectors/orders';

const OrdersPagination = () => {
  const dispatch = useDispatch();
  const isPrev = useSelector(selectIsPrev);
  const isNext = useSelector(selectIsNext);

  const handleNext = () => {
    window.scrollTo(0, 0);
    dispatch(getOrders(true));
  };

  const handlePrev = () => {
    window.scrollTo(0, 0);
    dispatch(getOrders(false));
  };

  return (
    <HStack
      width="100%"
      justifyContent="center"
      alignItems="center"
      spacing="10px"
    >
      <Button
        onClick={handlePrev}
        disabled={!isPrev}
        width="150px"
        variant="secondary"
        leftIcon={<ChevronLeftIcon />}
      >
        Previous Page
      </Button>
      <Button
        onClick={handleNext}
        disabled={!isNext}
        width="150px"
        variant="secondary"
        rightIcon={<ChevronRightIcon />}
      >
        Next Page
      </Button>
    </HStack>
  );
};

export default OrdersPagination;
