import { Box, Flex, Text } from '@chakra-ui/layout';
import OrderFilterTag from './OrderFilterTag';

const OrderFilterSection = ({ title, data }) => {
  return (
    <Box paddingY="10px">
      <Text>{title}</Text>
      <Flex direction="row" flexFlow="wrap">
        {data.map((item) => (
          <OrderFilterTag key={item.key} item={item} />
        ))}
      </Flex>
    </Box>
  );
};

export default OrderFilterSection;
