import { Stack } from '@chakra-ui/react';
import { orderTime } from '../../../constants/orderFilter';
import OrderFilterSection from './OrderFilterSection';

// Code Review Completed

const OrderFilterBody = () => {
  return (
    <Stack>
      <OrderFilterSection title="Order Time" data={orderTime} />
    </Stack>
  );
};

export default OrderFilterBody;
