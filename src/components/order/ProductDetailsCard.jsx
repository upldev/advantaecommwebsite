import {
  Button, Divider, Flex, Stack, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Auth, Storage } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import OrderDetailsProductCard from './OrderDetailsProductCard';

// Code Review Completed

const ProductDetailsCard = ({
  order
}) => {
  const { isLg } = useMediaQuery();
  const [invoiceUrl, setInvoiceUrl] = useState();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(async () => {
    const { identityId } = await Auth.currentCredentials();
    const invoicedata = await Storage.get(order.invoiceUrl, {
      level: 'protected',
      identityId
    });
    setInvoiceUrl(invoicedata);
  }, [order.invoiceUrl]);

  return (
    <Flex my={5} borderWidth="1px" borderColor={colors.gray} width={isLg ? '70%' : '100%'} flexDirection="column">
      <Flex backgroundColor={colors.gray} justifyContent="flex-start">
        <Text mx={5} my={2} variant="subTitle" fontWeight="bolder" flexShrink={0}>Product Details</Text>
      </Flex>
      <Stack bgColor={colors.white}>
        {
          order.orderCart.map((product, index) => (
            <>
              <OrderDetailsProductCard
                key={product.sku.ecomCode}
                product={product}
                orderDelivered={order.orderDelivered}
              />
              {
              index !== order.orderCart.length - 1 && (
                <Divider />
              )
            }
            </>
          ))
        }
      </Stack>
      <Flex borderTopWidth="1px" borderColor={colors.gray} bgColor={colors.white}>
        {
          order.invoiceUrl ? (
            <Button m={3} variant="secondary">
              <a className={css(styles.link)} href={invoiceUrl} target="_blank" rel="noreferrer">Download Invoice</a>

            </Button>
          ) : (
            <Text m={3}>Invoice will be available in a while.</Text>
          )
        }
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  link: {
    ':hover': {
      textDecoration: 'none',
      color: colors.white
    }
  }
});

export default ProductDetailsCard;
