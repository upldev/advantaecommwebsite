import { useDisclosure } from '@chakra-ui/hooks';
import {
  Box,
  Button,
  ButtonGroup,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Flex,
  Icon,
  Text
} from '@chakra-ui/react';
import { FiFilter } from 'react-icons/fi';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import {
  applyFilters, resetFilters
} from '../../../store/actions/orders';
import OrderFilterBody from './OrderFilterBody';

// Code Review Completed

const OrderFilterDrawer = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const dispatch = useDispatch();

  const handleApply = () => {
    dispatch(applyFilters());
    onClose();
  };

  const handleReset = () => {
    dispatch(resetFilters());
    onClose();
  };

  return (
    <>
      <Button
        onClick={onOpen}
        variant="secondary"
        paddingX="10px"
        leftIcon={<Icon as={FiFilter} />}
      >
        FILTER
      </Button>
      <Drawer placement="bottom" onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader>
            <Flex width="full" justifyContent="space-between">
              <Text>Filters</Text>
              <Box onClick={handleReset} as="button" color={colors.lightRed}>
                <Text fontSize="sm">Clear Filter</Text>
              </Box>
            </Flex>
          </DrawerHeader>
          <DrawerBody>
            <OrderFilterBody />
          </DrawerBody>
          <DrawerFooter borderTopWidth="1px" borderColor={colors.dividerGray}>
            <ButtonGroup>
              <Button variant="outline_primary" onClick={onClose}>
                Cancel
              </Button>
              <Button variant="outline_primary" onClick={handleApply}>
                Apply
              </Button>
            </ButtonGroup>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default OrderFilterDrawer;
