import {
  Button, Flex, Icon, Text, useDisclosure
} from '@chakra-ui/react';
import React from 'react';
import { BiRupee } from 'react-icons/bi';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import TrackOrderModal from '../account/TrackOrderModal';

// Code Review Completed

const OrderDetailsCard = ({ order }) => {
  const { isLg, isMd } = useMediaQuery();
  // const orders = useSelector(selectOrders);
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Flex
        borderWidth="1px"
        borderColor={colors.gray}
        width={isLg ? '70%' : '100%'}
        flexDirection="column"
      >
        <Flex
          backgroundColor={colors.gray}
          justifyContent="space-between"
          alignItems="center"
        >
          <Text mx={5} my={2} variant="subTitle" fontWeight="bolder">
            Order Details
          </Text>
          <Button
            borderRadius={0}
            backgroundColor={colors.lightBlue}
            onClick={onOpen}
            size="sm"
            mr={3}
          >
            <Text color={colors.white}>Track Order</Text>
          </Button>
        </Flex>
        <Flex bgColor={colors.white}>
          <Flex flexDir="column" m={isMd ? 1 : 5}>
            <Flex flexDirection="row" flexShrink={0}>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mb="3"
                width={isMd ? '110px' : '150px'}
              >
                Order Number
              </Text>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mx={isMd ? 1 : 2}
                mb="3"
              >
                :
              </Text>
              <Text fontSize={isMd ? 'sm' : 'md'} fontWeight="bolder" mb="3">
                {order.orderId}
              </Text>
            </Flex>
            <Flex flexDirection="row" flexShrink={0}>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mb="3"
                minWidth={isMd ? '110px' : '150px'}
              >
                Order Placed
              </Text>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mx={isMd ? 1 : 2}
                mb="3"
              >
                :
              </Text>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                fontWeight="bolder"
                style={{ wordBreak: 'break-word' }}
                mb="3"
              >
                {`${new Date(order.createdAt).toString().substr(0, 21)} | ${
                  order.totalQty
                } item  `}
              </Text>
            </Flex>
            <Flex flexDirection="row" flexShrink={0}>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mb="3"
                width={isMd ? '110px' : '150px'}
              >
                Order Status
              </Text>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mx={isMd ? 1 : 2}
                mb="3"
              >
                :
              </Text>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={order.orderDelivered ? colors.green : colors.black}
                fontWeight="bolder"
                mb="3"
              >
                {order.orderDelivered
                  ? 'Successfully Delivered'
                  : order.orderStatus}
              </Text>
            </Flex>
            <Flex flexDirection="row" flexShrink={0}>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mb="3"
                width={isMd ? '110px' : '150px'}
              >
                Order Total
              </Text>
              <Text
                fontSize={isMd ? 'sm' : 'md'}
                color={colors.gray}
                mx={isMd ? 1 : 2}
                mb="3"
              >
                :
              </Text>
              <Text fontSize={isMd ? 'sm' : 'md'} fontWeight="bolder" mb="3">
                {' '}
                <Icon as={BiRupee} />
                {' '}
                {order.totalPayable}
              </Text>
            </Flex>
          </Flex>
        </Flex>
      </Flex>
      <TrackOrderModal
        isOpen={isOpen}
        order={order}
        onClose={onClose}
        orderId={order.orderId}
        orderPlacedDate={order.orderDate}
        productImg={order.orderCart[0].productDetails?.productImg}
      />
    </>
  );
};

export default OrderDetailsCard;
