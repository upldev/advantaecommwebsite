import { Flex, Image, Text } from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const EmptyOrder = () => {
  const { isMd } = useMediaQuery();

  return (
    <Flex
      justifyContent="center"
      alignItems="center"
      width="100%"
      height={isMd ? '100%' : '65%'}
      bgColor={colors.white}
      flexDirection="column"
      pb={6}
    >
      <Image
        src="/EC_image.PNG"
        borderColor={colors.light}
        width={isMd ? '40%' : '50%'}
      />
      <Text mt="5" textAlign="center" fontWeight="bold" color="gray.600">
        You have not placed any orders.
      </Text>
    </Flex>
  );
};

export default EmptyOrder;
