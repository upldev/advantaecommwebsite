import {
  Icon, Tag, TagLabel, TagRightIcon
} from '@chakra-ui/react';
import { IoAdd } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { toggleOrdersFilter } from '../../../store/actions/orders';
import { selectIsFilterSelected } from '../../../store/selectors/orders';

const OrderFilterTag = ({ item }) => {
  const dispatch = useDispatch();
  const isSelected = useSelector(selectIsFilterSelected(item));

  const handleToggle = () => {
    dispatch(toggleOrdersFilter(item));
  };

  return (
    <Tag
      onClick={handleToggle}
      variant={isSelected ? 'solid' : 'outline'}
      marginTop="10px"
      marginRight="15px"
      size="lg"
      borderRadius="full"
      colorScheme="green"
      cursor="pointer"
    >
      <TagLabel>{item.key}</TagLabel>
      <TagRightIcon>
        <Icon as={IoAdd} fontSize="1.5rem" />
      </TagRightIcon>
    </Tag>
  );
};

export default OrderFilterTag;
