import { Flex, Text } from '@chakra-ui/layout';
import { css, StyleSheet } from 'aphrodite';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import WriteAReviewForm from './WriteAReviewForm';

const WriteAReview = ({ product }) => {
  const user = useSelector((state) => state.user);
  const [writeAReview, setWriteAReview] = useState(false);
  return (
    <>
      {user.logInStatus && (
      <Flex my={3}>
        <Text
          fontWeight="bolder"
          className={css(styles.writeAReview)}
          cursor="pointer"
          onClick={() => setWriteAReview((prev) => !prev)}
          color={colors.green}
        >
          {writeAReview ? 'Hide' : 'Write a Review'}
        </Text>
      </Flex>
      )}
      {writeAReview && <WriteAReviewForm product={product} setWriteAReview={setWriteAReview} /> }
    </>
  );
};

const styles = StyleSheet.create({
  writeAReview: {
    color: colors.green,
    ':hover': {
      opacity: 0.5
    },
    textDecoration: 'underline'
  }
});

export default WriteAReview;
