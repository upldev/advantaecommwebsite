/* eslint-disable max-len */
import { Button } from '@chakra-ui/button';
import {
  Flex, IconButton, Text
} from '@chakra-ui/react';
import { Formik } from 'formik';
import React, { useRef, useState } from 'react';
import { IoCameraOutline } from 'react-icons/io5';
import ReactStars from 'react-rating-stars-component';
import { useDispatch, useSelector } from 'react-redux';
import uuid from 'uuid';
import * as Yup from 'yup';
import { colors } from '../../../constants/colors';
import { saveProductReview } from '../../../store/actions/review';
import FormTextArea from '../shared/FormTextArea';
import ReviewImage from './ReviewImage';

const WriteAReviewForm = ({ product, setWriteAReview }) => {
  const imageRef = useRef();
  const farmer = useSelector((state) => state.user);
  const [formState, setFormState] = useState({
    reviewId: uuid.v4(),
    materialCode: product.materialCode,
    category: product.category,
    crop: product.crop,
    product: product.product,
    farmerMobile: farmer.profile.mobile,
    farmerName: farmer.profile.name,
    rating: product.avgrating,
    review: '',
    isPublic: false,
    advantaKisan: false,
    reviewDate: new Date().toISOString()
  });
  const handleOpenImageSelect = () => {
    imageRef.current.click();
  };
  const dispatch = useDispatch();
  const [imageAddedDeleted, setImageAddedDeleted] = useState(false);

  const reviewSchema = Yup.object({
    rating: Yup.string().required('Required'),
    review: Yup.string().required('Required').matches(/^[a-zA-Z0-9\s,.\\]*$/, 'Cannot enter special characters like $, @..'),
  });
  const [images, setImages] = useState([]);
  const handleSubmit = (values) => {
    dispatch(
      saveProductReview({
        ...formState,
        ...values,
        images
      })
    );
    setWriteAReview(false);
  };
  const handleChange = (e) => {
    const currImage = URL.createObjectURL(e.target.files[0]);
    setImages((prev) => [...prev, { image: currImage, file: e.target.files[0], id: uuid.v4() }]);
    setImageAddedDeleted(true);
  };
  const handleDeleteImage = (id) => {
    const filteretImages = images.filter((item) => id !== item.id);
    setImages(filteretImages);
    setImageAddedDeleted(false);
  };
  return (
    <>
      { formState && formState.farmerName.length > 0
      && (
      <Formik
        initialValues={formState}
        onSubmit={handleSubmit}
        validationSchema={reviewSchema}
      >
        {(props) => (
          <>

            <Text>
              Rating :
              {' '}
            </Text>
            <ReactStars
              count={5}
              isRequired
              value={props.values.rating}
              size={24}
              edit
              isHalf={false}
              onChange={(e) => props.setFieldValue('rating', parseInt(e))}
              emptyIcon={<i className="far fa-star" />}
              fullIcon={<i className="fa fa-star" />}
              activeColor={colors.lightGold}
            />

            <FormTextArea placeholder="Review title" name="review" label="Add comments" isRequired />
            <Flex my="2">
              <IconButton borderRadius="0" borderWidth="1px" borderColor={colors.lightBlue} color={colors.lightBlue} onClick={handleOpenImageSelect} icon={<IoCameraOutline />} />

              <input
                ref={imageRef}
                type="file"
                style={{ display: 'none' }}
                accept="image/*"
                onChange={handleChange}
              />
              {images.length > 0 && images.map((item) => <ReviewImage handleDeleteImage={handleDeleteImage} item={item} key={item.id} />) }
            </Flex>
            { images.length === 0 && <Text color={colors.lightBlue} variant="textRegular"> Add a photo </Text>}
            { images.length > 0 && imageAddedDeleted && <Text color={colors.green} variant="textRegular"> You have successfully edited. </Text>}
            { images.length > 0 && !imageAddedDeleted && <Text color={colors.lightRed} variant="textRegular"> You have successfully removed file. </Text>}
            <Flex mt={5} mb={2}>
              <Button
                type="submit"
                onClick={props.handleSubmit}
                variant="primary"
              >
                Submit
              </Button>
            </Flex>
          </>

        )}
      </Formik>
      )}
    </>
  );
};

export default WriteAReviewForm;
