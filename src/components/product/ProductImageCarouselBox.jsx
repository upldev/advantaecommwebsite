import { Flex, Icon, Image } from '@chakra-ui/react';
import React from 'react';
import { FaRegPlayCircle } from 'react-icons/fa';
import { colors } from '../../../constants/colors';

const ProductImageCarouselBox = ({
  item, index, currShow, setCurrShow
}) => {
  const handleSetCurrShow = () => {
    setCurrShow(item.id);
  };
  return (
    <Flex
      borderWidth="3px"
      height="100%"
      width="100%"
      borderColor={currShow === item.id && colors.lightBlue}
    >
      {item.isImage ? (
        <Image
          backgroundColor={colors.white}
          cursor="pointer"
          onClick={handleSetCurrShow}
          src={item.data}
          padding="15px"
          objectFit="contain"
        />
      ) : (
        <Flex
          backgroundColor={colors.black}
          opacity="0.8"
          cursor="pointer"
          onClick={handleSetCurrShow}
          height="100%"
          width="100%"
          justifyContent="center"
          alignItems="center"
        >
          <Icon color={colors.white} as={FaRegPlayCircle} />
        </Flex>
      )}
    </Flex>
  );
};

export default ProductImageCarouselBox;
