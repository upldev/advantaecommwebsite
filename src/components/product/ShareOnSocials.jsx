import { HStack, Icon, Text } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { FaFacebook, FaTelegram } from 'react-icons/fa';
import { IoLogoWhatsapp } from 'react-icons/io';
import {
  FacebookShareButton,
  TelegramShareButton,
  WhatsappShareButton
} from 'react-share';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const ShareOnSocials = ({ product }) => {
  const BASE_URL = 'https://shopuat.advantaseeds.com/product/';
  const { isLg } = useMediaQuery();
  return (
    <HStack
      spacing="15px"
      alignItems="center"
      justifyContent={isLg ? 'flex-end' : 'center'}
    >
      {isLg && (
        <Text color={colors.green} fontSize="1.25rem" fontWeight="medium">
          Share on
        </Text>
      )}
      <WhatsappShareButton url={`${BASE_URL}${product.materialCode}`}>
        <Icon className={css(styles.icon)} as={IoLogoWhatsapp} />
      </WhatsappShareButton>
      <FacebookShareButton url={`${BASE_URL}${product.materialCode}`}>
        <Icon className={css(styles.icon)} as={FaFacebook} />
      </FacebookShareButton>

      <TelegramShareButton url={`${BASE_URL}${product.materialCode}`}>
        <Icon className={css(styles.icon)} as={FaTelegram} />
      </TelegramShareButton>
      {/* <Tooltip placement="top" label="Copy Link">
        <button
          type="button"
          onClick={() => navigator.clipboard.writeText('www.google.com')}
        >
          <Icon className={css(styles.icon)} as={FaCopy} />
        </button>

      </Tooltip> */}
    </HStack>
  );
};

const styles = StyleSheet.create({
  icon: {
    cursor: 'pointer',
    fontSize: '25px',
    ':hover': {
      color: colors.advantaBlue,
    },
  },
});

export default ShareOnSocials;
