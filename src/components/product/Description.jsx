/* eslint-disable max-len */
import {
  Flex, ListItem, UnorderedList
} from '@chakra-ui/react';
import React from 'react';
import useMediaQuery from '../../../hooks/useMediaQuery';

const Description = ({ product }) => {
  const { isLg } = useMediaQuery();
  return (
    <Flex flexDirection="column" mb={5} mx={isLg ? 100 : 5}>
      <UnorderedList spacing="3">
        { product.specs.map((spec) => (
          <ListItem key={spec.key}>
            {spec.key}
            {' '}
            :
            {' '}
            {spec.value.length > 1

              ? (
                <UnorderedList spacing="1">
                  {spec.value.map((s) => (<ListItem key={s}>{s}</ListItem>)) }

                </UnorderedList>
              )

              : spec.value }

          </ListItem>
        )) }

      </UnorderedList>
    </Flex>
  );
};

export default Description;
