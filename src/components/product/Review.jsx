/* eslint-disable max-len */
import {
  Button, Divider, Flex, Link, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React, { useEffect, useState } from 'react';
import ReactStars from 'react-rating-stars-component';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { LOGIN } from '../../../lib/router';
import { getProductReviews } from '../../../store/actions/review';
import ReviewCard from './ReviewCard';
import WriteAReview from './WriteAReview';

const Review = ({ product }) => {
  const { isLg } = useMediaQuery();
  const user = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [showAllReviews, setShowAllReviews] = useState(false);

  const reviews = useSelector((state) => state.review.productReviews);
  useEffect(() => {
    dispatch(getProductReviews(product.materialCode));
  }, [dispatch, product.materialCode]);
  console.log(reviews);
  return (
    <Flex flexDirection="column" mb={5} mx={isLg ? 100 : 5}>
      <Text mb={3} fontWeight="bolder">
        Customer Reviews
      </Text>
      <Divider />
      <Flex mb={1} alignItems="center">
        <ReactStars
          count={5}
          value={product.avgrating}
          size={isLg ? 24 : 12}
          edit={false}
          isHalf
          emptyIcon={<i className="far fa-star" />}
          fullIcon={<i className="fa fa-star" />}
          activeColor={colors.lightGold}
        />
        <Text mt="1" color={colors.lightBlue} variant="textRegular" ml="2">
          (
          {product.totalreviews}
          {' '}
          Based on Active Users )
        </Text>
      </Flex>
      {user.logInStatus ? <WriteAReview product={product} />
        : (
          <Text>
            {' '}
            Please
            {' '}
            <Link className={css(styles.login)} href={LOGIN}> login </Link>
            {' '}
            to write a review.
            {' '}
          </Text>
        )}
      {reviews && reviews.length > 0 && reviews.length > 2 ? reviews?.slice(0, showAllReviews ? reviews.length : 2).map((review, index) => (
        <ReviewCard key={review.reviewId} review={review} index={index} />
      )) : reviews?.map((review, index) => (
        <ReviewCard key={review.reviewId} review={review} index={index} />
      ))}
      {reviews && reviews.length > 2 && (
      <Flex justifyContent="center">

        <Button onClick={() => setShowAllReviews((prev) => !prev)} variant="primary">{showAllReviews ? 'See Less' : 'See All'}</Button>
      </Flex>
      )}

    </Flex>
  );
};

const styles = StyleSheet.create({
  login: {
    color: colors.green,
    ':hover': {
      opacity: 0.5
    },
    textDecoration: 'none'
  }
});

export default Review;
