import {
  Alert,
  AlertIcon,
  Button,
  Flex,
  Icon,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  useDisclosure
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { BiPencil } from 'react-icons/bi';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import usePincodeServiceability from '../../../hooks/usePincodeServiceability';

const PDPPincode = () => {
  const user = useSelector((state) => state.user);
  const { onOpen, isOpen, onClose } = useDisclosure();

  const [currPincode, setCurrPincode] = useState(
    user.logInStatus && user.hasProfile ? user.profile.billing.pincode : ''
  );

  const { checkPincode } = usePincodeServiceability();

  const handleCheck = async () => {
    await checkPincode(currPincode);
  };

  return (
    <>
      <Flex>
        <Flex alignItems="center">
          <Text whiteSpace="nowrap" fontWeight="bold">
            Deliver to pincode
          </Text>
          <Flex
            cursor="pointer"
            marginLeft="10px"
            alignItems="center"
            onClick={onOpen}
            borderBottomWidth="1px"
            borderBottomColor={colors.advantaBlue}
          >
            <Text color={colors.advantaBlue}>
              {user.profile && user.logInStatus
                ? currPincode
                : 'Check if servicable at your pincode'}
            </Text>
            <Icon
              variant="ghost"
              color={colors.green}
              as={BiPencil}
              marginLeft="5px"
            />
          </Flex>
        </Flex>
      </Flex>
      {user.profile
        && currPincode === user.profile.billing.pincode
        && user.logInStatus
        && user.hasProfile
        && user.userServiceable !== null
        && (user.userServiceable.stat ? (
          <Alert status="success">
            <AlertIcon />
            {user.userServiceable.message}
          </Alert>
        ) : (
          <Alert status="error">
            <AlertIcon />
            {user.userServiceable.message}
          </Alert>
        ))}
      {String(currPincode).length === 6
        && currPincode !== user.profile?.billing.pincode
        && user.check !== null
        && user.check.pincode === currPincode
        && (user.check.servicable ? (
          <Flex mr={3}>
            <Alert ml={0} my={3} status="success">
              <AlertIcon />
              Serviceable in the entered pin code (
              {currPincode}
              ).
            </Alert>
          </Flex>
        ) : (
          <Flex mr={3}>
            <Alert ml={0} my={3} status="error">
              <AlertIcon />
              Unserviceable in the entered pin code (
              {currPincode}
              ).
            </Alert>
          </Flex>
        ))}
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        justifyContent="center"
        p={5}
        size="2xl"
      >
        <ModalOverlay />
        <ModalContent justifyContent="center" mr={5} ml={5}>
          <ModalHeader justifyContent="center" mt={5}>
            <Flex justifyContent="center" mr={3} ml={3}>
              Enter pincode to check if servicable or not
            </Flex>
          </ModalHeader>
          <ModalBody m={3}>
            <Flex width="100%" flexDirection="column">
              <Stack width="100%">
                <Flex m={3} mt="0" ml={0} justifyContent="center">
                  <Input
                    name="pincode"
                    value={currPincode}
                    onChange={(e) => setCurrPincode(e.target.value)}
                    placeholder="Enter your area pincode"
                    type="number"
                  />
                </Flex>
                <Flex m={5} mt="0">
                  <Button
                    size="sm"
                    width="40%"
                    onClick={() => {
                      onClose();
                      handleCheck();
                    }}
                    variant="primary"
                    pt={5}
                    pb={5}
                    disabled={String(currPincode).length !== 6}
                  >
                    Check
                  </Button>
                </Flex>
              </Stack>
            </Flex>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default PDPPincode;
