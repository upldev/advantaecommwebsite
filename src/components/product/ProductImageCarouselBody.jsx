/* eslint-disable max-len */
import { Flex, IconButton, SimpleGrid } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { IoChevronBackOutline, IoChevronForwardOutline } from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ProductImageCarouselBox from './ProductImageCarouselBox';

const ProductImageCarouselBody = ({
  productImageCarouselData,
  currShow,
  setCurrShow,
}) => {
  const {
    isLg, isMd, isSm, isXs
  } = useMediaQuery();
  const dataLength = productImageCarouselData.length;
  const [productIndex, setProductIndex] = useState(0);
  let productShown;
  if (isLg) {
    productShown = dataLength > 4 ? 4 : dataLength;
  }
  if (isMd) {
    productShown = 2;
  }
  if (isSm) {
    productShown = 2;
  }
  if (isXs) {
    productShown = 1;
  }
  useEffect(() => {
    setProductIndex(0);
    setLastIndex(productShown);
  }, [isLg, isMd, isSm, isXs, productShown]);
  const [lastIndex, setLastIndex] = useState(productIndex + productShown);
  const firstFewProducts = productImageCarouselData.slice(
    productIndex,
    productIndex + productShown
  );
  const nextProduct = () => {
    if (lastIndex === dataLength) {
      setProductIndex(0);
      setLastIndex(productShown);
    } else {
      const lastProductIndex = productImageCarouselData.length - 1;
      const resetProductIndex = productIndex === lastProductIndex;
      const index = resetProductIndex ? 0 : productIndex + 1;
      setProductIndex(index);
      setLastIndex(index + productShown);
    }
  };
  const prevProduct = () => {
    if (productIndex === 0) {
      setLastIndex(dataLength);
      setProductIndex(dataLength - productShown);
    } else {
      const lastProductIndex = productImageCarouselData.length - 1;
      const resetProductIndex = productIndex === 0;
      const index = resetProductIndex ? lastProductIndex : productIndex - 1;
      setProductIndex(index);
      setLastIndex(index + productShown);
    }
  };
  return (
    <Flex flexDir="column" marginTop="40px">
      <Flex
        width="100%"
        justifyContent="space-around"
        alignItems="center"
        flexDirection="column"
      >
        <Flex width="100%">
          <SimpleGrid
            width="100%"
            justifyContent="space-between"
            alignItems="center"
            height="100%"
            columns={4}
            spacing={{ base: '3px', lg: '10px' }}
          >
            {firstFewProducts?.map((item, index) => (
              <ProductImageCarouselBox
                key={index}
                currShow={currShow}
                setCurrShow={setCurrShow}
                index={index}
                item={item}
              />
            ))}
          </SimpleGrid>
        </Flex>
        {dataLength > 4 && (
          <Flex justifyContent="center" mt={3}>
            <IconButton
              color={productIndex === 0 ? colors.gray : colors.advantaBlue}
              borderRadius="0"
              onClick={prevProduct}
              borderWidth="1px"
              disabled={productIndex === 0}
              borderColor={
                productIndex === 0 ? colors.light : colors.advantaBlue
              }
              boxSize={{ base: 5, lg: 7 }}
              mr={5}
              icon={<IoChevronBackOutline />}
            />
            <IconButton
              color={
                lastIndex === dataLength ? colors.gray : colors.advantaBlue
              }
              borderRadius="0"
              onClick={nextProduct}
              borderWidth="1px"
              disabled={lastIndex === dataLength}
              borderColor={
                lastIndex === dataLength ? colors.light : colors.advantaBlue
              }
              boxSize={{ base: 5, lg: 7 }}
              icon={<IoChevronForwardOutline />}
            />
          </Flex>
        )}
      </Flex>
    </Flex>
  );
};

export default ProductImageCarouselBody;
