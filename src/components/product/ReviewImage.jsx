import { Flex, Icon, Image } from '@chakra-ui/react';
import React from 'react';
import { IoMdCloseCircle } from 'react-icons/io';

const ReviewImage = ({
  item, handleDeleteImage
}) => {
  const { image, id } = item;
  return (
    <Flex mx="4">
      <Image key={image} src={image} height="75px" width="50px" />
      <Icon cursor="pointer" onClick={() => handleDeleteImage(id)} as={IoMdCloseCircle} />
    </Flex>
  );
};

export default ReviewImage;
