import {
  Box, Flex, Icon, SimpleGrid, Text
} from '@chakra-ui/react';
import React from 'react';
import { FiTruck } from 'react-icons/fi';
import { IoCardOutline, IoPricetagOutline } from 'react-icons/io5';
import { Ri24HoursLine } from 'react-icons/ri';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const Features = () => {
  const { isLg } = useMediaQuery();

  return (
    <Box paddingTop="40px">
      <SimpleGrid columns={2} backgroundColor={colors.white}>
        <Flex justifyContent={!isLg && 'center'} m={3}>
          <Icon
            variant="ghost"
            style={{
              cursor: 'pointer',
              color: colors.black,
            }}
            as={FiTruck}
            mr={2}
            mt={1}
          />
          <Text
            variant={isLg ? 'mdTextRegular' : 'textRegular'}
            color={colors.black}
          >
            Direct Shipping
          </Text>
        </Flex>
        <Flex justifyContent={!isLg && 'center'} m={3}>
          <Icon
            variant="ghost"
            style={{
              cursor: 'pointer',
              color: colors.black,
            }}
            as={IoPricetagOutline}
            mr={2}
            mt={1}
          />
          <Text
            variant={isLg ? 'mdTextRegular' : 'textRegular'}
            color={colors.black}
          >
            Lowest Prices
          </Text>
        </Flex>
        <Flex justifyContent={!isLg && 'center'} m={3}>
          <Icon
            variant="ghost"
            style={{
              cursor: 'pointer',
              color: colors.black,
            }}
            as={Ri24HoursLine}
            mr={2}
            mt={1}
          />
          <Text
            variant={isLg ? 'mdTextRegular' : 'textRegular'}
            color={colors.black}
          >
            Online Support
          </Text>
        </Flex>
        <Flex justifyContent={!isLg && 'center'} m={3}>
          <Icon
            variant="ghost"
            style={{
              cursor: 'pointer',
              color: colors.black,
            }}
            as={IoCardOutline}
            mr={2}
            mt={1}
          />
          <Text
            variant={isLg ? 'mdTextRegular' : 'textRegular'}
            color={colors.black}
          >
            Secure Payment
          </Text>
        </Flex>
      </SimpleGrid>
    </Box>
  );
};

export default Features;
