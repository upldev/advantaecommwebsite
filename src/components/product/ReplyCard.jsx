import { Flex, Text } from '@chakra-ui/react';
import { StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const ReplyCard = ({ item, idx }) => {
  const { isLg } = useMediaQuery();
  const calculatedMargin = idx > 2 ? 6 : idx * 2;
  console.log(idx);
  console.log(item);
  return (
    <Flex mb="2" ml={calculatedMargin} backgroundColor={colors.lightGray} width={'100%' - calculatedMargin} flexDirection="column">
      <Flex m="2" mb="0" justifyContent="space-between">
        <Text variant={!isLg && 'textRegular'} fontWeight="bold">Advanta Seeds</Text>
        <Text variant={!isLg && 'textRegular'} color={colors.gray}>{item.createdAt.substring(0, 10)}</Text>
      </Flex>
      <Text variant={!isLg && 'textRegular'} m="4">
        {' '}
        {item.content}
      </Text>
    </Flex>
  );
};

const styles = StyleSheet.create({
  boxStyle: {
    border: '1px #999 solid',
    borderRadius: '10px',
    textAlign: 'center',
    width: '100px',
    height: '30px',
    color: 'black',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
    position: 'relative',
    left: '100px'
  }
});

export default ReplyCard;
