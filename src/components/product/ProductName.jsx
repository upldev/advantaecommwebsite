import {
  Box,
  Flex,
  IconButton,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Stack,
  Text
} from '@chakra-ui/react';
import React from 'react';
import { IoMdShare } from 'react-icons/io';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ShareOnSocials from './ShareOnSocials';

const ProductName = ({ product }) => {
  const { isLg } = useMediaQuery();

  return (
    <Stack>
      {isLg && <ShareOnSocials product={product} />}
      {isLg ? (
        <Box fontSize="1.35rem" textTransform="uppercase" fontWeight="bold">
          <Text>{product.crop}</Text>
          <Text color={colors.gray}>{product.product}</Text>
        </Box>
      ) : (
        <Flex justifyContent="space-between">
          <Flex flexDirection="column">
            <Text
              textTransform="uppercase"
              variant="subHeading"
              fontWeight="bolder"
            >
              {product.crop}
            </Text>
            <Text
              mb={1}
              color={colors.gray}
              variant="subHeading"
              textTransform="uppercase"
              fontWeight="bolder"
            >
              {product.product}
            </Text>
          </Flex>
          <Flex justifyContent="flex-end">
            <Popover placement="bottom" closeOnBlur={false}>
              <PopoverTrigger>
                <IconButton variant="ghost" icon={<IoMdShare />} />
              </PopoverTrigger>
              <PopoverContent>
                <PopoverHeader
                  textAlign="center"
                  color={colors.green}
                  fontWeight="bolder"
                >
                  Share on
                </PopoverHeader>
                <PopoverArrow />
                <PopoverCloseButton />
                <PopoverBody>
                  <ShareOnSocials product={product} />
                </PopoverBody>
              </PopoverContent>
            </Popover>
          </Flex>
        </Flex>
      )}
    </Stack>
  );
};

export default ProductName;
