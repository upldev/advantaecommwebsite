import {
  Box, Button, Flex, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import { useIndianCurrencyFormatter } from '../../../hooks/useIndianCurrencyFormatter';
import useMediaQuery from '../../../hooks/useMediaQuery';

const SizeAndPrice = ({
  currSelected,
  bagSize = {},
  setCurrSelected,
  setBagSize,
  sku = []
}) => {
  const { isLg } = useMediaQuery();
  const handleBagSizeChange = (e) => {
    setBagSize(sku[parseInt(e.target.id)]);
    setCurrSelected(parseInt(e.target.id));
  };

  const ecomPrice = useIndianCurrencyFormatter(bagSize.ecomPrice);
  const mrp = useIndianCurrencyFormatter(bagSize.MRP);

  return (
    <Box paddingY="20px">
      <Flex alignItems="center" marginBottom="15px">
        <Text mr="10px" fontWeight="bold">
          Size
        </Text>
        <Flex>
          {sku?.map((size, index) => (
            <Button
              mr={2}
              key={size.packSize.toString().concat(size.packUnit)}
              id={index}
              size={isLg ? 'md' : 'sm'}
              onClick={handleBagSizeChange}
              variant={
                currSelected === index ? 'secondary' : 'outline_secondary'
              }
            >
              {size.packSize < 1 ? size.packSize * 1000 : size.packSize}
              {' '}
              {size.packSize < 1 ? 'GM' : size.packUnit}
            </Button>
          ))}
        </Flex>
      </Flex>
      <Flex>
        <Text mr={2} fontWeight="bolder">
          Price
        </Text>
        <Text fontSize="md" color={colors.black} fontWeight="bold">
          {ecomPrice}
        </Text>
        {bagSize.discount > 0 && (
        <>
          <Text
            mx="10px"
            color={colors.gray}
            fontSize="sm"
            className={css(styles.strike)}
          >
            {mrp}
          </Text>
          <Text color={colors.lightRed} fontSize="sm">
            {`(${bagSize.discount}% Off)`}
          </Text>
        </>
        )}
      </Flex>
    </Box>
  );
};

const styles = StyleSheet.create({
  strike: {
    display: 'inline-block',
    textDecoration: 'none',
    position: 'relative',
    ':after': {
      content: "''",
      display: 'block',
      width: '100%',
      height: '50%',
      position: 'absolute',
      top: 0,
      left: 0,
      borderBottom: '1px solid',
    },
  },
});

export default SizeAndPrice;
