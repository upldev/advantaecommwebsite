import {
  Avatar, Divider, Flex, Image, Text
} from '@chakra-ui/react';
import { StyleSheet } from 'aphrodite';
import React from 'react';
import ReactStars from 'react-rating-stars-component';
import { colors } from '../../../constants/colors';
import { monthNumberToName } from '../../../constants/monthNumberToName';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ReplyCard from './ReplyCard';

const ReviewCard = ({ review, index }) => {
  const { isLg } = useMediaQuery();
  const dateToArray = review.reviewDate.substring(0, 10).split('-');
  console.log(review.reply);
  return (
    <Flex flexDirection="column">
      {index >= 0 && <Divider />}
      <Flex>
        <Avatar
          m="5"
          size={isLg ? 'lg' : 'md'}
          name={review.farmerName}
          src={review.profileImg}
        />
        <Flex width="100%" flexDirection="column">
          <Flex m="5" mx={!isLg && 0} flexDirection="column">
            <ReactStars
              count={5}
              value={review.rating}
              size={isLg ? 24 : 12}
              edit={false}
              isHalf
              emptyIcon={<i className="far fa-star" />}
              fullIcon={<i className="fa fa-star" />}
              activeColor={colors.lightGold}
            />
            <Flex my={isLg ? '3' : '1'}>
              <Text
                variant={!isLg && 'textRegular'}
                mr="1"
                fontWeight="bolder"
                color={colors.advantaBlue}
              >
                {review.farmerName}
              </Text>
              <Text variant={!isLg && 'textRegular'}>
                on
                {' '}
                {monthNumberToName(dateToArray[1])}
                {' '}
                {dateToArray[2]}
                ,
                {dateToArray[0]}
              </Text>
            </Flex>
            <Text variant={!isLg && 'textRegular'}>{review.review}</Text>
            <Flex>
              {review.images !== null
                && review.images.map((image, index) => (
                  <Image
                    ml={index > 0 ? '2' : '0'}
                    mr="2"
                    my="2"
                    height="100px"
                    width="75px"
                    key={image}
                    src={image}
                  />
                ))}
            </Flex>
          </Flex>
          {review.reply?.length > 0
            && review.reply.map((item, idx) => (
              <ReplyCard key={item.replyId} idx={idx} item={item} />
            ))}
        </Flex>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  boxStyle: {
    border: '1px #999 solid',
    borderRadius: '10px',
    textAlign: 'center',
    width: '100px',
    height: '30px',
    color: 'black',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center'
  }
});

export default ReviewCard;
