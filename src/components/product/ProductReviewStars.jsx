import { Flex, Text } from '@chakra-ui/react';
import React from 'react';
import ReactStars from 'react-rating-stars-component';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const ProductReviewStars = ({ product }) => {
  const { isLg } = useMediaQuery();
  return (
    <Flex alignItems="center">
      <ReactStars
        count={5}
        value={product.avgrating}
        size={isLg ? 24 : 12}
        edit={false}
        isHalf
        emptyIcon={<i className="far fa-star" />}
        fullIcon={<i className="fa fa-star" />}
        activeColor={colors.lightGold}
      />
      <Text fontSize="0.75rem" color={colors.lightBlue} paddingLeft="10px">
        {`(${product.totalreviews} Based on Active Users)`}
      </Text>
    </Flex>
  );
};

export default ProductReviewStars;
