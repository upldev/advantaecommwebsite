/* eslint-disable max-len */
import { Box, Button, Flex } from '@chakra-ui/react';
import React from 'react';
import { AiFillHeart } from 'react-icons/ai';
import { IoHeartOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  addOnlyToWishlist,
  addToCart,
  removeFromWishlist
} from '../../../store/actions/cart';
import HookUsage from '../shared/HookUsage';

const CartAndWishlist = ({
  product, currSelected, bagSize, sku = []
}) => {
  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const quantity = useSelector((state) => state.cart.cartItems?.filter(
    (x) => x.sku.ecomCode === sku[currSelected]?.ecomCode
  ));

  const inCartDetails = quantity[0];
  const wishlisted = useSelector((state) => state.wishlist.wishlistItems?.find((x) => x === product.materialCode));

  const handleAddToCart = () => {
    dispatch(addToCart(product, 1, bagSize));
  };
  const handleAddToWishlist = () => {
    dispatch(addOnlyToWishlist(product));
  };
  const handleRemoveFromWishlist = () => {
    dispatch(removeFromWishlist(product.materialCode));
  };
  const inCartSku = useSelector((state) => state.cart.cartItems?.find(
    (x) => x.sku.ecomCode === sku[currSelected]?.ecomCode
  ));

  return (
    <Box align="center" flexDirection="column">
      <Flex justify="flex-start" align="center">
        {inCartSku ? (
          <HookUsage
            qty={inCartDetails.qty}
            qtyAvailable={sku[currSelected]?.qtyAvailable}
            product={product}
            currSelected={currSelected}
            sku={sku}
          />
        ) : (
          <Button
            variant="addToCart"
            disabled={bagSize?.qtyAvailable === 0}
            onClick={handleAddToCart}
            size={isLg ? 'md' : 'sm'}
          >
            Add to Cart
          </Button>
        )}

        {wishlisted ? (
          <Button
            ml="10px"
            variant="outline_secondary"
            onClick={handleRemoveFromWishlist}
            leftIcon={<AiFillHeart />}
            size={isLg ? 'md' : 'sm'}
          >
            {' '}
            WISHLISTED
          </Button>
        ) : (
          <Button
            ml="10px"
            variant="outline_secondary"
            onClick={handleAddToWishlist}
            leftIcon={<IoHeartOutline />}
            size={isLg ? 'md' : 'sm'}
          >
            WISHLIST
          </Button>
        )}
      </Flex>
    </Box>
  );
};

export default CartAndWishlist;
