import { Box, Flex, Text } from '@chakra-ui/layout';
import React from 'react';
import { colors } from '../../../constants/colors';

const ProductAvailability = ({ product, currSelected, bagSize }) => {
  console.log(bagSize);
  return (
    <Flex>
      <Text color={colors.black} fontWeight="bold">
        Availability
      </Text>
      <Box paddingLeft="10px">
        {bagSize?.qtyAvailable > 0 ? (
          <Text color={colors.green}>IN STOCK</Text>
        ) : (
          <Text color={colors.lightRed}>OUT OF STOCK</Text>
        )}
      </Box>
    </Flex>
  );
};

export default ProductAvailability;
