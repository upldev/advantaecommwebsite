import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel, Stack,
  Text
} from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import OrderCartProduct from './OrderCartProduct';

// Code Review Completed

const OrdersAccordian = ({ orderPlacedDate, accordianOrders }) => {
  const { isMd } = useMediaQuery();

  return (
    <Accordion allowToggle>
      <AccordionItem>
        {({ isExpanded }) => (
          <>
            <AccordionButton padding="10px">
              <AccordionIcon />
              <Text paddingLeft="10px">
                {isExpanded ? 'View less products' : 'View all products'}
              </Text>
            </AccordionButton>
            <AccordionPanel p={0}>
              {accordianOrders.map((orderProduct) => (
                <Stack
                  borderTopWidth={1}
                  borderColor={colors.lightGray}
                  key={orderProduct.materialCode}
                >
                  <OrderCartProduct
                    orderProduct={orderProduct}
                    orderPlacedDate={orderPlacedDate}
                  />
                </Stack>
              ))}
            </AccordionPanel>
          </>
        )}
      </AccordionItem>
    </Accordion>
  );
};

export default OrdersAccordian;
