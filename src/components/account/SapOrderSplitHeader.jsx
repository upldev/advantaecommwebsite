import { Box, Flex, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { getOrderDetails } from '../../../lib/router';

// Code Review Completed

const SapOrderSplitHeader = ({ orderId }) => {
  const { isLg } = useMediaQuery();
  const router = useRouter();

  console.log({ orderId });

  return (
    <>
      <Flex
        paddingY="10px"
        justifyContent="space-between"
        bgColor={colors.dividerGray}
        alignItems="center"
        paddingX={isLg ? '24px' : '10px'}
      >
        <Box>
          <Text fontSize={!isLg ? 'sm' : 'md'} fontWeight="bold">
            {`Order ID: ${orderId}`}
          </Text>
        </Box>
        <Text
          fontSize={!isLg ? 'sm' : 'md'}
          fontWeight="bold"
          color={colors.lightBlue}
          cursor="pointer"
          onClick={() => {
            router.push(getOrderDetails(orderId));
          }}
        >
          View Details
        </Text>
      </Flex>
    </>
  );
};

export default SapOrderSplitHeader;
