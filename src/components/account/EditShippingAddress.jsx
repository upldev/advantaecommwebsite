import {
  Button,
  Checkbox,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Stack
} from '@chakra-ui/react';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { stateObj } from '../../../constants/cityStates';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { addShippingToProfile } from '../../../store/actions/checkout';
import FormDropDown from '../shared/FormDropDown';
import FormTextArea from '../shared/FormTextArea';
import PincodeInput from '../shared/PincodeInput';

// Code Review Completed

const states = Object.keys(stateObj);

const AddressSchema = Yup.object({
  address: Yup.string()
    .required('Required')
    .max(190, 'Address should not exceed 190 characters'),
  city: Yup.string().required('Required'),
  state: Yup.string().required('Required'),
  pincode: Yup.string()
    .length(6, 'Please enter a valid pincode')
    .required('Required'),
});
const EditShippingAddress = ({
  isOpen, onClose, onOpen, address, user
}) => {
  const dispatch = useDispatch();

  const { isMd } = useMediaQuery();

  const handleSubmit = (values) => {
    const { shipping } = user;

    const filtered = shipping.map((item) => {
      return item.addressId === address.addressId
        ? { ...item, ...values }
        : values.isDefault
          ? { ...item, isDefault: false }
          : item;
    });

    dispatch(addShippingToProfile([...filtered], onClose));
  };

  const [cities, setCities] = useState([]);
  const handleCity = (state) => {
    const selected = stateObj[state];
    setCities(() => selected?.cities);
  };
  useEffect(() => {
    handleCity(address.state);
  }, [address.state]);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Edit Shipping Address</ModalHeader>
        <ModalCloseButton />
        <Formik
          initialValues={address}
          onSubmit={handleSubmit}
          validationSchema={AddressSchema}
        >
          {(props) => (
            <>
              <ModalBody>
                <Stack>
                  <FormTextArea
                    name="address"
                    label="Address"
                    placeholder="Enter your Address"
                    isRequired
                    mb={isMd ? 2 : 0}
                  />

                  <FormControl
                    isRequired
                    isInvalid={props.touched.state && props.errors.state}
                    mb={isMd ? 2 : 0}
                  >
                    <FormLabel>State</FormLabel>
                    <Select
                      onChange={(e) => {
                        props.handleChange(e);
                        handleCity(e.target.value);
                      }}
                      placeholder="Select state"
                      onBlur={props.handleBlur}
                      name="state"
                      defaultValue={address.state}
                    >
                      {states.map((option) => (
                        <option value={option} key={option}>
                          {option}
                        </option>
                      ))}
                    </Select>
                    <FormErrorMessage>{props.errors.state}</FormErrorMessage>
                  </FormControl>
                  <FormDropDown
                    mb={isMd ? 2 : 0}
                    name="city"
                    selectedValue={address.city}
                    label="City"
                    placeholder="Select City"
                    options={cities}
                    isRequired
                  />
                  <PincodeInput />
                </Stack>
              </ModalBody>
              <ModalFooter>
                <Checkbox
                  name="isDefault"
                  defaultChecked={props.values.isDefault}
                  value={props.values.isDefault}
                  onChange={props.handleChange}
                  width="100%"
                >
                  Make this as my Default Address
                </Checkbox>
                <Button
                  disabled={!props.isValid || props.isValidating}
                  mr="10px"
                  variant="primary"
                  onClick={props.handleSubmit}
                >
                  Update
                </Button>
                <Button variant="outline_black" onClick={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </>
          )}
        </Formik>
      </ModalContent>
    </Modal>
  );
};

export default EditShippingAddress;
