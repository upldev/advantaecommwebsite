import {
  Avatar, Button, Flex, FormControl
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { showLoading, showToast } from '../../../store/actions/feedback';

// Code Review Completed

const UserProfileCircle = ({ name, img, setProfileImg }) => {
  const dispatch = useDispatch();
  const {
    values, handleBlur, setFieldValue, errors, touched
  } = useFormikContext();

  const { isMd } = useMediaQuery();

  const handleImageChange = (e) => {
    dispatch(showLoading(true));
    try {
      const file = e.currentTarget.files[0];
      const objUrl = URL.createObjectURL(file);
      setFieldValue(name, {
        imgFile: file,
        imgUrl: objUrl
      });
      if (setProfileImg) setProfileImg(objUrl);
    } catch (error) {
      dispatch(showToast({
        title: 'Failed to add profile',
        description: 'Please try again',
        status: false
      }));
    } finally {
      dispatch(showLoading(false));
    }
  };

  return (
    <Flex width="fit-content">
      <FormControl>
        <Flex justifyContent="center" pb={1}>
          <Avatar
            src={values[name] && values[name].imgUrl ? values[name].imgUrl : img || './add-profile.png'}
            size={isMd ? 'lg' : 'xl'}
            borderWidth={1}
            borderColor={colors.gray}
          />
        </Flex>
        <input
          name={name}
          id="selectedFile"
          type="file"
          style={{ display: 'none' }}
          onChange={(e) => {
            handleImageChange(e);
          }}
          accept="image/*"
          onBlur={handleBlur}
        />
        <Button variant="link" onClick={() => document.getElementById('selectedFile').click()}>add profile image</Button>
      </FormControl>
    </Flex>
  );
};

export default UserProfileCircle;
