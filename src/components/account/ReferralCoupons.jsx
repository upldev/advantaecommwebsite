import {
  Button, Flex, Icon, Input, Stack, Text, useClipboard
} from '@chakra-ui/react';
import { API, Auth, graphqlOperation } from 'aws-amplify';
import React, { useState } from 'react';
import { IoMdClipboard } from 'react-icons/io';
import { useDispatch } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import { createReferrer } from '../../../graphql/mutations';
import { getFarmer, listDiscountData } from '../../../graphql/queries';
import { showToast } from '../../../store/actions/feedback';

const ReferralCoupons = ({ codesGenerated, setCodesGenerated, setReferrals }) => {
  const [result, setResult] = useState('');
  const [isLoading, setLoading] = useState(false);
  const { hasCopied, onCopy } = useClipboard(result);
  const dispatch = useDispatch();

  const generatedCode = async () => {
    try {
      setLoading(true);
      const user = await Auth.currentAuthenticatedUser();
      const { data } = await API.graphql(graphqlOperation(getFarmer, { mobile: user.username }));
      const code = uuidv4().substring(0, 6);
      let discountMeta = await API.graphql(graphqlOperation(listDiscountData));
      // eslint-disable-next-line prefer-destructuring
      discountMeta = discountMeta.data.listDiscountData.items[0];
      await API.graphql(graphqlOperation(createReferrer, {
        input: {
          referralCode: code,
          farmerGenPhone: user.username,
          farmerGenName: data.getFarmer.name,
          farmerGenAmt: discountMeta.refereeDiscount.amount,
          farmerConAmt: discountMeta.referralDiscount.amount,
          farmerGenConsumed: false,
        }
      }));
      setCodesGenerated((prev) => prev + 1);
      setResult(code);
      setReferrals((prev) => [{
        referralCode: code,
        discount: discountMeta.refereeDiscount.amount,
        add: false,
        used: 'Unused'
      }, ...prev]);
    } catch (err) {
      console.error(err);
      dispatch(showToast({
        title: 'Failed to create a referral code.',
        message: 'Please try again.',
        status: false
      }));
    } finally {
      setLoading(false);
    }
  };

  return (
    <Stack mt={6} width="full">
      <Flex>
        <Text>
          Generate code to share with friends
        </Text>
      </Flex>
      <Flex>
        <Input isReadOnly value={result} />
        {
          result && (
          <Button
            onClick={onCopy}
            variant={hasCopied ? 'primary' : 'outline'}
            borderRadius={0}
            colorScheme="blackAlpha"
            ml={1}
          >
            <Icon as={IoMdClipboard} />
          </Button>
          )
        }
        <Button
          flexShrink={0}
          variant="primary"
          onClick={generatedCode}
          isLoading={isLoading}
          p={2}
          ml={1}
          disabled={codesGenerated >= 5}
        >
          Generate Code
        </Button>
      </Flex>
    </Stack>
  );
};

export default ReferralCoupons;
