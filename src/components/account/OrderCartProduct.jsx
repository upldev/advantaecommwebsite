import {
  Box,
  Button,
  Divider,
  Flex,
  Text,
  useDisclosure
} from '@chakra-ui/react';
import router from 'next/router';
import React from 'react';
import { colors } from '../../../constants/colors';
import {
  dateToDay,
  monthNumberToName
} from '../../../constants/monthNumberToName';
import { useIndianCurrencyFormatter } from '../../../hooks/useIndianCurrencyFormatter';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { productDetailsPage } from '../../../lib/router';
import OrderProductImage from './OrderProductImage';
import TrackOrderModal from './TrackOrderModal';

// Code Review Completed
const OrderCartorderProduct = ({
  orderProduct,
  orderPlacedDate,
  orderId,
  firstProduct = false,
  allProduct,
  order
}) => {
  const { isLg, isMd } = useMediaQuery();
  const { onClose, isOpen, onOpen } = useDisclosure();
  const date = new Date(orderPlacedDate);
  const total = useIndianCurrencyFormatter(
    orderProduct.itemDiscount > 0
      ? orderProduct.sku?.ecomPrice - orderProduct.itemDiscount
      : orderProduct.sku?.ecomPrice
  );
  // console.log(allProduct);
  return orderProduct?.productDetails ? (
    <>
      <Box width="100%">
        <Flex
          paddingX={!isLg ? '10px' : '24px'}
          paddingY="15px"
          justifyContent="space-between"
          onClick={() => router.push(productDetailsPage(orderProduct.materialCode))}
          cursor="pointer"
        >
          <Flex width="50%">
            <Box width={isLg ? '20%' : '40%'}>
              <OrderProductImage
                productImg={orderProduct.productDetails?.productImg}
              />
            </Box>
            <Box
              width={isLg ? '80%' : '60%'}
              padding={isLg ? '10px' : '5px'}
              textTransform="capitalize"
              fontSize={isMd ? 'sm' : 'md'}
              textOverflow="ellipsis"
            >
              <Text fontWeight="bold">{orderProduct.productDetails.crop}</Text>
              <Text
                color={colors.gray}
                overflow="hidden"
                whiteSpace="nowrap"
                width={isMd && '80px'}
              >
                {orderProduct.productDetails.product}
              </Text>
            </Box>
          </Flex>
          <Flex flexDirection="column" flexShrink={0}>
            <Text
              borderWidth="1px"
              borderColor={colors.dividerGray}
              bgColor={colors.lighterGray}
              padding={isLg ? '10px' : '5px'}
            >
              {orderProduct.sku.packSize >= 1
                ? `${orderProduct.sku.packSize} ${orderProduct.sku.packUnit}`
                : `${orderProduct.sku.packSize * 1000} GM`}
            </Text>
          </Flex>
          <Flex>
            <Text paddingY={isLg ? '10px' : '5px'} fontWeight="bold">
              {total}
            </Text>
          </Flex>
        </Flex>
        <Divider color={colors.light} width="100%" marginY={3} />
        <Flex
          paddingX="10px"
          pb="10px"
          justifyContent="space-between"
          alignItems="center"
        >
          <Text>
            On
            {' '}
            {`${dateToDay(
              date.getDay()
            )}, ${date.getDate()} ${monthNumberToName(date.getMonth() + 1)}`}
          </Text>
          {firstProduct && (
            <Button
              borderRadius={0}
              backgroundColor={colors.lightBlue}
              onClick={onOpen}
            >
              <Text color={colors.white}>Track Order</Text>
            </Button>
          )}
        </Flex>
      </Box>
      <TrackOrderModal
        isOpen={isOpen}
        onClose={onClose}
        orderId={orderId}
        order={order}
        productImg={orderProduct.productDetails?.productImg}
        orderPlacedDate={orderPlacedDate}
        allProduct={allProduct}
      />
    </>
  ) : null;
};

export default OrderCartorderProduct;
