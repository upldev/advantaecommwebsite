import {
  Box, List, ListItem, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import React from 'react';
import { colors } from '../../../constants/colors';
import {
  CREDITS,
  ORDER_HISTORY,
  USER_ADDRESS,
  USER_PROFILE
} from '../../../lib/router';

// Code Review Completed

const ProfileSideNavbar = ({ type }) => {
  const router = useRouter();
  return (
    <Box className={css(styles.container)}>
      <List>
        <ListItem>
          <Text paddingX="36px" paddingY="25px" fontSize="xl" fontWeight="bold">
            MY ACCOUNT
          </Text>
        </ListItem>
        <ListItem
          className={type === 1 ? css(styles.selected) : css(styles.unselected)}
        >
          <Box
            as="button"
            className={type !== 1 && css(styles.hover)}
            width="full"
            onClick={() => router.push(USER_PROFILE)}
            disabled={type === 1}
          >
            <Text className={css(styles.text)} fontSize="large">
              My Profile
            </Text>
          </Box>
        </ListItem>
        <ListItem
          className={type === 2 ? css(styles.selected) : css(styles.unselected)}
        >
          <Box
            as="button"
            className={type !== 2 && css(styles.hover)}
            width="full"
            onClick={() => router.push(ORDER_HISTORY)}
            disabled={type === 2}
          >
            <Text className={css(styles.text)} fontSize="large">
              Order History
            </Text>
          </Box>
        </ListItem>
        <ListItem
          className={type === 3 ? css(styles.selected) : css(styles.unselected)}
        >
          <Box
            as="button"
            className={type !== 3 && css(styles.hover)}
            width="full"
            onClick={() => router.push(USER_ADDRESS)}
            disabled={type === 3}
          >
            <Text className={css(styles.text)} fontSize="large">
              Address
            </Text>
          </Box>
        </ListItem>
        <ListItem
          className={type === 4 ? css(styles.selected) : css(styles.unselected)}
        >
          <Box
            as="button"
            className={type !== 4 && css(styles.hover)}
            width="full"
            onClick={() => router.push(CREDITS)}
            disabled={type === 4}
          >
            <Text className={css(styles.text)} fontSize="large">
              Invites & Credits
            </Text>
          </Box>
        </ListItem>
      </List>
    </Box>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: '1px',
    borderColor: colors.gray,
    backgroundColor: colors.white,
  },
  unselected: {
    color: colors.gray,
  },
  selected: {
    color: colors.black,
    backgroundColor: colors.dividerGray,
    fontWeight: 'bold',
  },
  text: {
    padding: '10px 36px',
    width: 'full',
    textAlign: 'left',
  },
  hover: {
    color: colors.gray,
    ':hover': {
      color: colors.black,
      backgroundColor: colors.lightGray,
    },
  },
});

export default ProfileSideNavbar;
