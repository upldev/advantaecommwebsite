import { Stack } from '@chakra-ui/react';
import React from 'react';
import { useSelector } from 'react-redux';
import { selectOrders } from '../../../store/selectors/orders';
import EmptyOrder from '../order/EmptyOrder';
import SapOrderSplit from './SapOrderSplit';

// Code Review Completed

const ActiveAndDeliveredOrders = () => {
  const orders = useSelector(selectOrders);

  return orders.length > 0 ? (
    <Stack spacing="30px">
      {orders.map((order) => (
        <SapOrderSplit key={order.orderId} order={order} />
      ))}
    </Stack>
  ) : (
    <Stack alignItems="center" justifyItems="center">
      <EmptyOrder />
    </Stack>
  );
};

export default ActiveAndDeliveredOrders;
