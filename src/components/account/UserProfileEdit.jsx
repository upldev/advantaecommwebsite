import { useDisclosure } from '@chakra-ui/hooks';
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Select,
  SimpleGrid, VStack
} from '@chakra-ui/react';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { stateObj } from '../../../constants/cityStates';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { updateUserProfile } from '../../../store/actions/user';
import Facebook from '../shared/Facebook';
import FormInput from '../shared/FormInput';
import Google from '../shared/Google';
import UserAddressTextArea from './UserAddressTextArea';
import UserProfileSquare from './UserProfileSquare';

// Code Review Completed

const states = Object.keys(stateObj);

const UserProfileSchema = Yup.object({
  name: Yup.string().required('Required').matches(/^[a-zA-Z0-9\s.\\]*$/, 'Cannot enter special characters like $, @..'),
  email: Yup.string().required('Required').email('Invalid Email'),
  billing: Yup.object().shape({
    address: Yup.string()
      .required('Required')
      .max(190, 'Address should not exceed 190 characters'),
    city: Yup.string().required('Required'),
    state: Yup.string().required('Required'),
    pincode: Yup.string()
      .length(6, 'Please enter a valid pincode')
      .required('Required'),
  }),
});

const UserProfileEdit = ({
  formState, img, setProfileImg, setProfile
}) => {
  const { isLg, isMd, isSm } = useMediaQuery();
  const dispatch = useDispatch();
  const [cities, setCities] = useState([]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const isLoading = useSelector((state) => state.feedback.isLoading);

  useEffect(() => {
    handleCity(formState.billing.state);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = (values) => {
    dispatch(
      updateUserProfile({
        farmer: values,
        initialFarmer: formState,
        onClose,
        setProfile,
        setProfileImg,
      })
    );
  };

  const handleCity = (state) => {
    if (state) {
      const selected = stateObj[state];
      setCities(() => selected.cities);
    } else {
      setCities(() => []);
    }
  };

  return (
    <>
      <Flex
        width="full"
        paddingRight={isSm && '10px'}
        justifyContent={isSm ? 'flex-end' : 'flex-start'}
      >
        <Button
          variant="outline_secondary"
          size={isSm ? 'xs' : 'md'}
          onClick={onOpen}
          width={!isSm && '100%'}
          marginTop={!isSm && '10px'}
        >
          Edit
        </Button>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose} size={isMd ? 'md' : 'xl'}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Update Profile</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <VStack>
              <Formik
                initialValues={formState}
                onSubmit={handleSubmit}
                validationSchema={UserProfileSchema}
              >
                {(props) => {
                  return (
                    <>
                      <Flex
                        width={isLg ? 'full' : 'fit-content'}
                        justifyContent="flex-end"
                      >
                        <Facebook />
                        <Google />
                      </Flex>
                      <Flex
                        alignItems={isMd ? 'center' : 'flex-start'}
                        width="full"
                      >
                        <UserProfileSquare name="profileImg" img={img} />
                        <SimpleGrid spacingY="10px" pl={5} width="full">
                          <FormInput name="name" label="Name" isRequired />
                          <FormInput name="email" label="Email" isRequired />
                        </SimpleGrid>
                      </Flex>
                      <Flex width="full" justifyContent="flex-start" pt={3}>
                        <Heading fontSize="lg">Billing Address</Heading>
                      </Flex>
                      <Box width="full">
                        <UserAddressTextArea />
                        <SimpleGrid
                          columns={{ md: 1, lg: 3 }}
                          spacingX="24px"
                          spacingY="0px"
                          mb={isMd ? '' : '10px'}
                          mt={isMd ? '' : '10px'}
                          width="full"
                        >
                          <FormControl
                            isRequired
                            isInvalid={
                              props.touched.billing
                              && props.touched.billing.state
                              && props.errors.billing
                              && props.errors.billing.state
                            }
                            mt={{ base: '10px', lg: '0px' }}
                          >
                            <FormLabel fontSize={isLg ? '18px' : '12px'}>
                              State
                            </FormLabel>
                            <Select
                              size={isLg ? 'md' : 'sm'}
                              defaultValue={props.values.billing.state}
                              onChange={(e) => {
                                props.handleChange(e);
                                handleCity(e.target.value);
                              }}
                              placeholder="Select state"
                              onBlur={props.handleBlur}
                              name="billing.state"
                            >
                              {states.map((option) => (
                                <option value={option} key={option}>
                                  {option}
                                </option>
                              ))}
                            </Select>
                            {props.errors.billing
                              && props.errors.billing.state && (
                                <FormErrorMessage>
                                  {props.errors.billing.state}
                                </FormErrorMessage>
                            )}
                          </FormControl>
                          <FormControl
                            isRequired
                            isInvalid={
                              props.touched.billing
                              && props.touched.billing.city
                              && props.errors.billing
                              && props.errors.billing.city
                            }
                            mt={{ base: '10px', lg: '0px' }}
                          >
                            <FormLabel fontSize={isLg ? '18px' : '12px'}>
                              City
                            </FormLabel>
                            <Select
                              size={isLg ? 'md' : 'sm'}
                              defaultValue={props.values.billing.city}
                              placeholder="Select city"
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                              name="billing.city"
                            >
                              {cities.map((option) => (
                                <option
                                  selected={
                                    option === props.values.billing.city
                                  }
                                  value={option}
                                  key={option}
                                >
                                  {option}
                                </option>
                              ))}
                            </Select>
                            {props.errors.billing
                              && props.errors.billing.city && (
                                <FormErrorMessage>
                                  {props.errors.billing.city}
                                </FormErrorMessage>
                            )}
                          </FormControl>
                          <FormControl
                            isRequired
                            isInvalid={
                              props.touched.billing
                              && props.touched.billing.pincode
                              && props.errors.billing
                              && props.errors.billing.pincode
                            }
                            mb={isMd ? 2 : 0}
                            mt={{ base: '10px', lg: '0px' }}
                          >
                            <FormLabel
                              fontSize={isLg ? '18px' : '12px'}
                              htmlFor="mobile"
                            >
                              Pincode
                            </FormLabel>
                            <Input
                              size={isLg ? 'md' : 'sm'}
                              name="billing.pincode"
                              value={props.values.billing.pincode}
                              onChange={props.handleChange}
                              onBlur={props.handleBlur}
                            />
                            {props.errors.billing
                              && props.errors.billing.pincode && (
                                <FormErrorMessage>
                                  {props.errors.billing.pincode}
                                </FormErrorMessage>
                            )}
                          </FormControl>
                        </SimpleGrid>
                      </Box>
                      <Flex width="100%" justifyContent="space-around">
                        <Button
                          variant="primary"
                          size={isLg ? 'md' : 'sm'}
                          type="submit"
                          onClick={props.handleSubmit}
                          isLoading={isLoading}
                          isFullWidth
                        >
                          Submit
                        </Button>
                      </Flex>
                    </>
                  );
                }}
              </Formik>
            </VStack>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UserProfileEdit;
