import {
  FormControl,
  FormErrorMessage,
  FormLabel, Textarea
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import React from 'react';
import sanitize from 'sanitize-html';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const UserAddressTextArea = () => {
  const { isLg } = useMediaQuery();

  const {
    touched, errors, values, handleBlur, setFieldValue
  } = useFormikContext();

  const handleChange = (e) => {
    const { value } = e.target;
    setFieldValue('billing.address', sanitize(value));
  };

  return (
    <FormControl
      isRequired
      isInvalid={
        touched.billing
        && touched.billing.address
        && errors.billing
        && errors.billing.address
      }
    >
      <FormLabel fontSize={isLg ? '18px' : '12px'}>Address</FormLabel>
      <Textarea
        placeholder="Enter address"
        borderRadius={0}
        borderColor={colors.black}
        name="billing.address"
        size={isLg ? 'md' : 'sm'}
        value={values.billing.address}
        onChange={handleChange}
        onBlur={handleBlur}
        h="25vh"
      />
      {errors.billing && errors.billing.address && (
        <FormErrorMessage>{errors.billing.address}</FormErrorMessage>
      )}
    </FormControl>
  );
};

export default UserAddressTextArea;
