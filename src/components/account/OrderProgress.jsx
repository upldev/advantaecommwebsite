import { Flex } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { colors } from '../../../constants/colors';
import {
  DELIVERED,
  NEW_ORDER,
  OUT_FOR_DELIVERY,
  SHIPPED
} from '../../../constants/orderStatus';
// Code Review Complete

const OrderProgress = ({
  orderDate,
  progress = NEW_ORDER,
  arriving,
  pickupDate,
  deliveryDate,
}) => {

  return (
    <Flex>
      <Flex
        justifyContent="space-between"
        alignItems="center"
        mt={5}
        flexDirection="column"
      >
        <div
          className={css(
            styles.timeline,
            progress === NEW_ORDER
              ? styles.new
              : progress === SHIPPED
                ? styles.shipped
                : progress === OUT_FOR_DELIVERY
                  ? styles.outForDelivery
                  : styles.completed
          )}
        >
          <div className={css(styles.timelineItems)}>
            <div
              className={css(
                styles.timelineItemContainer,
                progress === NEW_ORDER
                  || progress === SHIPPED
                  || progress === OUT_FOR_DELIVERY
                  || progress === DELIVERED
                  ? styles.timelineItemContainerActive
                  : null
              )}
            >
              <div
                className={css(
                  styles.timelineItem,
                  progress >= 1 ? styles.timelineItemActive : null
                )}
              >
                {progress === NEW_ORDER
                || progress === SHIPPED
                || progress === OUT_FOR_DELIVERY
                || progress === DELIVERED ? (
                  <div className={css(styles.checkmark)} />
                  ) : null}
                <div
                  className={css(
                    styles.timelineContent,
                    progress === NEW_ORDER
                      || progress === SHIPPED
                      || progress === OUT_FOR_DELIVERY
                      || progress === DELIVERED
                      ? styles.isCompleted
                      : null
                  )}
                >
                  Ordered,
                  {' '}
                  {new Date(orderDate).toDateString()}
                </div>
              </div>
            </div>
            <div
              className={css(
                styles.timelineItemContainer,
                progress === SHIPPED
                  || progress === OUT_FOR_DELIVERY
                  || progress === DELIVERED
                  ? styles.timelineItemContainerActive
                  : null
              )}
            >
              <div
                className={css(
                  styles.timelineItem,
                  progress >= 2 ? styles.timelineItemActive : null
                )}
              >
                {progress === SHIPPED
                || progress === OUT_FOR_DELIVERY
                || progress === DELIVERED ? (
                  <div className={css(styles.checkmark)} />
                  ) : null}
                <div
                  className={css(
                    styles.timelineContent,
                    progress === SHIPPED
                      || progress === OUT_FOR_DELIVERY
                      || progress === DELIVERED
                      ? styles.isCompleted
                      : null
                  )}
                >
                  Shipped
                  {pickupDate && `, ${pickupDate}`}
                </div>
              </div>
            </div>
            {progress !== DELIVERED && (
              <div
                className={css(
                  styles.timelineItemContainer,
                  progress === OUT_FOR_DELIVERY || progress === DELIVERED
                    ? styles.timelineItemContainerActive
                    : null
                )}
              >
                <div
                  className={css(
                    styles.timelineItem,
                    progress >= 3 ? styles.timelineItemActive : null
                  )}
                >
                  {progress === OUT_FOR_DELIVERY || progress === DELIVERED ? (
                    <div className={css(styles.checkmark)} />
                  ) : null}
                  <div
                    className={css(
                      styles.timelineContent,
                      progress === OUT_FOR_DELIVERY || progress === DELIVERED
                        ? styles.isCompleted
                        : null
                    )}
                  >
                    Out for delivery
                  </div>
                </div>
              </div>
            )}
            <div
              className={css(
                styles.timelineItemContainer,
                progress === DELIVERED
                  ? styles.timelineItemContainerActive
                  : null
              )}
            >
              <div
                className={css(
                  styles.timelineItem,
                  progress >= 3 ? styles.timelineItemActive : null
                )}
              >
                {progress === DELIVERED ? (
                  <div className={css(styles.checkmark)} />
                ) : null}
                <div
                  className={css(
                    styles.timelineContent,
                    progress === DELIVERED ? styles.isCompleted : null
                  )}
                >
                  {progress === DELIVERED
                    ? `Delivered, ${deliveryDate}`
                    : arriving
                      ? `Arriving, ${arriving}`
                      : 'Arriving soon'}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  timeline: {
    marginBottom: '12px',
    height: '330px',
    display: 'flex',
    justifyContent: 'center',
    width: '4px',
    marginLeft: '10px',
    borderWidth: 1,
  },
  new: {
    backgroundColor: colors.gray,
  },
  shipped: {
    background:
      'linear-gradient(top, rgba(19,165,56,1) 0 33%, rgba(157, 157, 156, 1) 33% 66%, rgba(157, 157, 156, 1) 67% 100%)',
  },
  outForDelivery: {
    background:
      'linear-gradient(top, rgba(19,165,56,1) 0 33%, rgba(19,165,56,1) 33% 66%, rgba(157, 157, 156, 1) 67% 100%)',
  },
  completed: {
    backgroundColor: colors.green,
  },
  timelineProgress: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  timelineItems: {
    marginLeft: '-8px',
    marginRight: '-8px',
    marginTop: '-16px',
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  timelineItemContainer: {
    // padding: '6px',
    height: 24,
    width: 24,
    borderWidth: 2,
    borderColor: colors.gray,
    backgroundColor: colors.white,
  },
  checkmark: {
    display: 'inline-block',
    transform: 'rotate(45deg)',
    height: '12px',
    width: '6px',
    marginLeft: 8,
    marginBottom: 4,
    borderBottom: '2px solid white',
    borderRight: '2px solid white',
  },
  timelineItemContainerActive: {
    backgroundColor: colors.green,
    borderColor: colors.green,
  },
  timelineItem: {
    position: 'relative',
    '::before': {
      content: "''",
      width: '2px',
      backgroundColor: colors.green,
      display: 'block',
      borderRadius: '100%',
    },
  },
  timelineContent: {
    position: 'absolute',
    left: '50px',
    // right: '100px',
    // transform: 'translateX(-50%)',
    fontWeight: 'bold',
    marginBottom: 2,
    width: 'max-content',
  },
  isCompleted: {
    bottom: '0px',
  },
  timelineItemActive: {
    color: colors.lightBlue,
    '::before': {
      backgroundColor: colors.lightBlue,
    },
  },
});

export default OrderProgress;
