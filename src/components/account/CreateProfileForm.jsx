/* eslint-disable react/no-children-prop */
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Select,
  SimpleGrid,
  Stack,
  Text
} from '@chakra-ui/react';
import { Auth } from 'aws-amplify';
import { Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import uuid from 'uuid';
import * as Yup from 'yup';
import { stateObj } from '../../../constants/cityStates';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { HOME } from '../../../lib/router';
import { createUserProfile } from '../../../store/actions/user';
import AlertComponent from '../shared/Alert';
import Facebook from '../shared/Facebook';
import FormDropDown from '../shared/FormDropDown';
import FormInput from '../shared/FormInput';
import FormTextArea from '../shared/FormTextArea';
import Google from '../shared/Google';
import UserProfileCircle from './UserProfileCircle';

const states = Object.keys(stateObj);

const schema = Yup.object({
  name: Yup.string()
    .required('Required')
    .matches(
      /^[a-zA-Z0-9\s.\\]*$/,
      'Cannot enter special characters like $, @..'
    ),
  email: Yup.string().required('Required').email('Invalid Email'),
  fb: Yup.string(),
  google: Yup.string(),
  address: Yup.string()
    .required('Required')
    .max(190, 'Address should not exceed 190 characters'),
  city: Yup.string().required('Required'),
  state: Yup.string().required('Required'),
  pincode: Yup.string()
    .matches(/(\d{6})/, 'Pincode should be a number')
    .length(6, 'Pincode should be six digit')
    .required('Required'),
});

// Code Review Completed

const CreateProfileForm = () => {
  const { isLg, isMd } = useMediaQuery();
  const dispatch = useDispatch();
  const [cities, setCities] = useState([]);
  const [mobile, setMobile] = useState();
  const router = useRouter();
  const { query } = router;
  const { redirectBackTo } = query;

  useEffect(() => {
    async function getMobile() {
      try {
        const user = await Auth.currentAuthenticatedUser();
        setMobile(user.username);
      } catch (err) {
        console.error(err);
      }
    }
    getMobile();
  }, []);

  const handleCity = (state) => {
    const selected = stateObj[state];
    setCities(() => selected.cities);
  };

  const formState = {
    profileImg: '',
    name: '',
    email: '',
    fb: '',
    google: '',
    address: '',
    city: '',
    state: '',
    pincode: '',
  };

  const handleCreateProfile = async (values) => {
    const {
      name,
      email,
      fb,
      google,
      address,
      state,
      city,
      pincode,
      profileImg,
    } = values;
    const profile = {
      profileImg,
      name,
      mobile,
      email,
      fb,
      google,
      billing: {
        addressId: uuid.v4(),
        name,
        address,
        state,
        city,
        pincode: parseInt(pincode),
        isDefault: true,
      },
    };
    dispatch(
      createUserProfile({
        profile,
        isRegister: false,
        redirect: redirectBackTo || HOME,
      })
    );
  };

  return (
    <Flex
      flexDir="column"
      justifyContent="center"
      alignItems="center"
      width={isMd && '100%'}
    >
      <Text
        variant={isMd ? 'heading' : 'title'}
        textAlign="center"
        paddingY={isMd ? 5 : 9}
        width="full"
        borderBottomWidth="1px"
        borderColor={colors.gray}
      >
        CREATE PROFILE
      </Text>
      <Stack width={isMd && '100%'} px={isMd && 6}>
        <AlertComponent type="register" />
        <AlertComponent type="otp" />
        <Formik
          initialValues={formState}
          validationSchema={schema}
          onSubmit={handleCreateProfile}
        >
          {(props) => (
            <Form>
              <Flex
                flexDir={isMd ? 'column' : 'row'}
                width="full"
                justifyContent="center"
                pb={3}
                alignItems="center"
              >
                <UserProfileCircle name="profileImg" />
                <Flex
                  mt={isMd && 3}
                  width="full"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Facebook />
                  <Google />
                </Flex>
              </Flex>
              <SimpleGrid
                columns={{ md: 1, lg: 3 }}
                spacingX="24px"
                spacingY={isMd && '10px'}
                width="full"
              >
                <FormInput
                  name="name"
                  label="Name"
                  backgroundColor={colors.white}
                  isRequired
                />
                <FormControl
                  isInvalid={props.touched.mobile && props.errors.mobile}
                  isRequired
                  mb={isLg ? 2 : 0}
                >
                  <FormLabel fontSize={isLg ? '18px' : '12px'}>
                    Phone Number
                  </FormLabel>
                  <Text fontSize={isLg ? '18px' : '12px'} mt={3}>
                    {`+91 ${mobile?.substr(3)}`}
                  </Text>
                </FormControl>
                <FormInput
                  mb={isLg ? 2 : 0}
                  name="email"
                  isRequired
                  backgroundColor={colors.white}
                  label="Email"
                />
              </SimpleGrid>
              <Box width="full" pt={isMd && '10px'}>
                <FormTextArea
                  name="address"
                  label="Address"
                  placeholder="Enter your Address"
                  isRequired
                  borderColor={colors.black}
                  borderRadius={0}
                  backgroundColor={colors.white}
                />
                <SimpleGrid
                  columns={{ md: 1, lg: 3 }}
                  spacingX="24px"
                  spacingY={isMd && '10px'}
                  mb={isLg ? '' : '10px'}
                  mt={isLg ? '' : '10px'}
                >
                  <FormControl
                    isRequired
                    isInvalid={props.touched.state && props.errors.state}
                    mb={isLg ? 2 : 0}
                  >
                    <FormLabel fontSize={isLg ? '18px' : '12px'}>
                      State
                    </FormLabel>
                    <Select
                      onChange={(e) => {
                        props.handleChange(e);
                        handleCity(e.target.value);
                      }}
                      placeholder="Select state"
                      onBlur={props.handleBlur}
                      name="state"
                      size={isLg ? 'md' : 'sm'}
                      backgroundColor={colors.lighterGray}
                    >
                      {states.map((option) => (
                        <option value={option} key={option}>
                          {option}
                        </option>
                      ))}
                    </Select>
                    <FormErrorMessage>{props.errors.state}</FormErrorMessage>
                  </FormControl>
                  <FormDropDown
                    mb={isLg ? 2 : 0}
                    name="city"
                    label="City"
                    placeholder="Select City"
                    options={cities}
                    backgroundColor={colors.lighterGray}
                    isRequired
                  />
                  <FormInput
                    name="pincode"
                    label="Pincode"
                    backgroundColor={colors.white}
                    isRequired
                  />
                </SimpleGrid>
              </Box>
              <Flex width="100%" justifyContent="space-around" py={3}>
                <Button
                  size={isLg ? 'md' : 'sm'}
                  px="16"
                  variant="primary"
                  type="submit"
                  onClick={props.handleSubmit}
                >
                  Submit
                </Button>
              </Flex>
            </Form>
          )}
        </Formik>
      </Stack>
    </Flex>
  );
};

export default CreateProfileForm;
