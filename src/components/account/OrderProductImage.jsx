import { Image } from '@chakra-ui/react';
import React from 'react';
import { useImageFromStorage } from '../../../hooks/useImageFromStorage';

// Code Review Completed

const OrderProductImage = ({ productImg }) => {
  const { url } = useImageFromStorage(productImg);

  return <Image src={url} />;
};

export default OrderProductImage;
