import { Box } from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import OrderCartProduct from './OrderCartProduct';
import OrdersAccordian from './OrdersAccordian';
import SapOrderSplitHeader from './SapOrderSplitHeader';

// Code Review Completed

const SapOrderSplit = ({ order }) => {
  return (
    <Box
      borderColor={colors.dividerGray}
      backgroundColor={colors.white}
      borderWidth={1}
    >
      <SapOrderSplitHeader orderId={order.orderId} orderPlacedDate={order.orderDate} />
      <OrderCartProduct
        order={order}
        orderProduct={order.orderCart[0]}
        orderPlacedDate={order.orderDate}
        orderId={order.orderId}
        allProduct={order.orderCart}
        firstProduct
      />
      {order.orderCart.length > 1 && (
        <OrdersAccordian
          orderPlacedDate={order.orderDate}
          accordianOrders={order.orderCart.slice(1)}
        />
      )}
    </Box>
  );
};

export default SapOrderSplit;
