import {
  Badge,
  Box,
  Button,
  ButtonGroup,
  Flex,
  Radio,
  Stack,
  Text,
  useDisclosure
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { IoCreateOutline, IoTrashOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import usePincodeServiceability from '../../../hooks/usePincodeServiceability';
import { CHECKOUT_SELECT_ADDRESS } from '../../../lib/router';
import {
  addCurrentShipTo,
  addShippingToProfile
} from '../../../store/actions/checkout';
import EditShippingAddress from './EditShippingAddress';

const AddressCard = ({ address, user }) => {
  const router = useRouter();

  const { isLg } = useMediaQuery();

  const { isOpen, onOpen, onClose } = useDisclosure();
  const dispatch = useDispatch();
  const currentShipping = useSelector((state) => state.checkout.currentShipTo);

  const { isDeliverable, isLoading } = usePincodeServiceability(
    address?.pincode || ''
  );

  const handleDelete = () => {
    const {
      createdAt, updatedAt, owner, ...rest
    } = user;
    const filtered = rest.shipping.filter(
      (ship) => ship.addressId !== address.addressId
    );

    dispatch(addShippingToProfile(filtered, onClose));
  };

  const handleDeliver = () => {
    if (router.pathname === CHECKOUT_SELECT_ADDRESS) {
      dispatch(addCurrentShipTo(address));
    }
  };

  return (
    <Stack p="25px" backgroundColor={colors.white}>
      <Flex
        cursor={
          router.pathname === CHECKOUT_SELECT_ADDRESS
            ? isDeliverable && !isLoading
              ? 'pointer'
              : 'not-allowed'
            : 'default'
        }
        onClick={handleDeliver}
        alignItems="start"
        justifyContent="space-between"
      >
        <Flex alignItems="start">
          {router.pathname === CHECKOUT_SELECT_ADDRESS && (
            <Radio
              isChecked={address.addressId === currentShipping.addressId}
              pr="30px"
              pt="1"
              disabled={!isDeliverable}
            />
          )}
          <div>
            <Box>
              <Text pb="1" fontWeight="bold" fontSize="16px">
                {address.name}
              </Text>
              <Text fontSize="14px">{address.address}</Text>
              <Text fontSize="14px">{address.state}</Text>
              <Text fontSize="14px">{address.city}</Text>
              <Text fontSize="14px">{address.pincode}</Text>
            </Box>
            <EditShippingAddress
              isOpen={isOpen}
              onClose={onClose}
              onOpen={onOpen}
              address={address}
              user={user}
            />
          </div>
        </Flex>
        {address.isDefault && <Badge colorScheme="green">Default</Badge>}
        {!isLoading && !isDeliverable && (
          <Badge colorScheme="red">Not Serviceable</Badge>
        )}
      </Flex>
      <ButtonGroup
        pt={isLg ? '40px' : '20px'}
        justifyContent="center"
        spacing="30px"
      >
        <Button
          leftIcon={<IoTrashOutline />}
          onClick={handleDelete}
          variant="outline_black"
        >
          Remove
        </Button>
        <Button
          leftIcon={<IoCreateOutline />}
          onClick={onOpen}
          variant="outline_black"
        >
          Edit
        </Button>
      </ButtonGroup>
    </Stack>
  );
};

export default AddressCard;
