import { Box, Flex, Text } from '@chakra-ui/react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { selectDescription } from '../../../store/selectors/orders';
import OrderFilterDrawer from '../order/OrderFilterDrawer';

// Code Review Completed

const OrdersFilter = () => {
  const { isMd } = useMediaQuery();
  const { isLoading } = useSelector((state) => state.feedback);
  const description = useSelector(selectDescription);

  return (
    <Flex justifyContent="space-between" bgColor={colors.white}>
      <Flex marginX={isMd ? 2 : 8} alignItems="center" flexShrink={0}>
        <Box>
          <Text color={colors.gray}>Showing:</Text>
        </Box>

        {!isLoading && !(description instanceof Promise) && (
          <Box>
            <Text fontWeight="bold" ml="10px">
              {description}
            </Text>
          </Box>
        )}
      </Flex>
      <Flex flexDirection="column">
        <OrderFilterDrawer />
      </Flex>
    </Flex>
  );
};

export default OrdersFilter;
