import {
  Box, Flex, Image, Stack, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Auth, Storage } from 'aws-amplify';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { showToast } from '../../../store/actions/feedback';
import UserProfileEdit from './UserProfileEdit';

// Code Review Completed

const ProfileCard = ({ profile: userProfile }) => {
  const dispatch = useDispatch();
  const { isSm } = useMediaQuery();
  const [profileImg, setProfileImg] = useState('./default-profile.png');
  const [profile, setProfile] = useState(userProfile);

  useEffect(() => {
    async function getProfile() {
      try {
        const { identityId } = await Auth.currentCredentials();
        const img = await Storage.get(profile.profileImg, {
          level: 'protected',
          identityId,
        });
        setProfileImg(img);
      } catch (err) {
        console.error(err);
        dispatch(
          showToast({
            title: 'Failed to fetch profile image!',
            message: 'Please refresh page.',
            status: false,
          })
        );
      }
    }
    if (profile.profileImg) getProfile();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box className={css(styles.container)}>
      <Flex flexDir={isSm ? 'column' : 'row'}>
        <Flex
          flexDir={isSm ? 'row' : 'column'}
          flexShrink={0}
          width={isSm ? 'full' : 'fit-content'}
          justifyContent={isSm && 'space-between'}
        >
          <Image
            boxSize="115px"
            objectFit="cover"
            src={profileImg}
            alt="Your Profile"
          />
          {!isSm && (
            <Box py={2}>
              <UserProfileEdit
                formState={profile}
                img={profileImg}
                setProfileImg={setProfileImg}
                setProfile={setProfile}
              />
            </Box>
          )}
        </Flex>
        {isSm ? (
          <Flex flexDir="column" mx={-5} my={2}>
            <Flex
              alignItems="center"
              backgroundColor="gray.100"
              py={1.5}
              pl={9}
            >
              <Text
                color={colors.gray}
                variant="textRegular"
                flexShrink={0}
                width="120px"
              >
                Username
              </Text>
              <Text variant="textRegular" fontWeight="bold" flexShrink={0}>
                {profile?.name}
              </Text>
            </Flex>
            <Flex
              alignItems="center"
              backgroundColor="gray.100"
              py={1.5}
              mt={1}
              pl={9}
              mb={1}
            >
              <Text
                color={colors.gray}
                variant="textRegular"
                flexShrink={0}
                width="120px"
              >
                Mobile Number
              </Text>
              <Text variant="textRegular" fontWeight="bold" flexShrink={0}>
                {`+91 ${profile?.mobile.substr(3, 13)}`}
              </Text>
              <UserProfileEdit
                formState={profile}
                img={profileImg}
                setProfileImg={setProfileImg}
                setProfile={setProfile}
              />
            </Flex>
            <Flex
              alignItems="center"
              backgroundColor="gray.100"
              py={1.5}
              pl={9}
            >
              <Text
                color={colors.gray}
                variant="textRegular"
                flexShrink={0}
                width="120px"
              >
                Address
              </Text>
              <Text variant="textRegular" fontWeight="bold">
                {`${profile?.billing.address}, ${profile?.billing.city}, ${profile?.billing.state} - ${profile?.billing.pincode}`}
              </Text>
            </Flex>
          </Flex>
        ) : (
          <Stack paddingX="28px">
            <Flex>
              <Text color={colors.gray} width="40%">
                Username
              </Text>
              <Text width="60%">{profile?.name}</Text>
            </Flex>
            <Flex>
              <Text color={colors.gray} width="40%">
                Mobile Number
              </Text>
              <Text width="60%">{`+91 ${profile?.mobile.substr(3, 13)}`}</Text>
            </Flex>
            <Flex>
              <Text color={colors.gray} width="40%">
                Address
              </Text>
              <Text width="60%">{`${profile?.billing.address}, ${profile?.billing.city}, ${profile?.billing.state} - ${profile?.billing.pincode}`}</Text>
            </Flex>
          </Stack>
        )}
      </Flex>
    </Box>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: '1px',
    borderColor: colors.gray,
    backgroundColor: colors.white,
    padding: '20px',
  },
});

export default ProfileCard;
