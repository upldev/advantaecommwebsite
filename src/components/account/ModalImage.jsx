import { Image } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import { useImageFromStorage } from '../../../hooks/useImageFromStorage';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { productDetailsPage } from '../../../lib/router';

const ModalImage = ({ id, productImg }) => {
  const router = useRouter();

  const { isLg } = useMediaQuery();

  const { url } = useImageFromStorage(productImg);

  return (
    <Image
      loading="lazy"
      objectFit="contain"
      cursor="pointer"
      onClick={() => router.push(productDetailsPage(id))}
      height={isLg ? 200 : 120}
      src={url}
      margin={5}
      _first={{ marginLeft: 0 }}
    />
  );
};

export default ModalImage;
