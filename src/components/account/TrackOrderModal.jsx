import {
  Box,
  Divider,
  Flex,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalOverlay,
  Stack,
  Text
} from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import { DELIVERED, OUT_FOR_DELIVERY, SHIPPED } from '../../../constants/orderStatus';
import ModalImage from './ModalImage';
import OrderProgress from './OrderProgress';

const getStatus = (scanResults) => {
  if (!scanResults) {
    return undefined;
  }

  if (scanResults[0].status.includes(DELIVERED)) {
    return DELIVERED;
  } if (scanResults[0].status.includes(OUT_FOR_DELIVERY)) {
    return OUT_FOR_DELIVERY;
  }

  return SHIPPED;
};

const TrackOrderModal = ({
  isOpen, onClose, orderId, allProduct, order
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} size="2xl">
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton />
        <ModalBody mt="8" padding="20px">
          <Stack>
            <Flex justifyContent="center">
              <Flex
                backgroundColor={colors.dividerGray}
                paddingY={2}
                width="100%"
                justifyContent="center"
              >
                <Text fontWeight="bold">
                  OrderId:
                  {' '}
                  {orderId}
                </Text>
              </Flex>
            </Flex>
            <Box padding="1rem">
              <Text fontWeight="bold">Arriving Today</Text>
              <Flex width="100%" overflowX="auto">
                {allProduct?.map((item) => (
                  <ModalImage
                    key={allProduct.materialCode}
                    id={item.materialCode}
                    productImg={item.productDetails.productImg}
                  />
                ))}
              </Flex>
            </Box>
            <Divider />
            <Box padding="1rem">
              <OrderProgress
                orderDate={order?.orderDate}
                arriving={order?.bluedartTrack?.estimatedDeliveryDate}
                progress={getStatus(order?.bluedartTrack?.scanResults)}
                pickupDate={order?.bluedartTrack?.pickupDate}
                deliveryDate={
                  order?.bluedartTrack?.scanResults.find(
                    (item) => item.status.includes(DELIVERED)
                  )?.date
                }
              />
            </Box>
          </Stack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default TrackOrderModal;
