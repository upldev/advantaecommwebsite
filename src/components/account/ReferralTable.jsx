import {
  Flex, Heading, Icon, Stack, Table, Tbody, Td, Text, Th, Thead, Tr
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { BiRupee } from 'react-icons/bi';
import { colors } from '../../../constants/colors';
import { MOBILE_QUERY } from '../../../constants/mediaQueries';
import useMediaQuery from '../../../hooks/useMediaQuery';

// Code Review Completed

const ReferralTable = ({ referrals }) => {
  const { isSm } = useMediaQuery();
  const total = referrals.reduce((sum, item) => {
    if (item.add) return sum + item.discount;
    return sum;
  }, 0);

  return (
    <Stack pt={6}>
      <Flex justifyContent="space-between" pb={2}>
        <Heading fontSize={isSm ? 'md' : 'xl'}>Invites & Credits</Heading>
        <Heading fontSize={isSm ? 'md' : 'xl'}>
          Your Credit(s):
          {' '}
          <Icon as={BiRupee} />
          {total}
        </Heading>
      </Flex>
      <Table variant="simple" size={isSm ? 'sm' : 'lg'}>
        <Thead borderWidth="0.5px" borderColor={colors.gray}>
          <Tr>
            <Th
              className={css(styles.header)}
              borderRightWidth="0.5px"
              borderColor={colors.gray}
            >
              Code
            </Th>
            <Th
              className={css(styles.header)}
              borderRightWidth="0.5px"
              borderColor={colors.gray}
              minWidth="110px"
            >
              Credit On
            </Th>
            <Th
              className={css(styles.header)}
              borderColor={colors.gray}
            >
              Used
            </Th>
          </Tr>
        </Thead>
        <Tbody backgroundColor={colors.white}>
          {
          referrals && referrals.map((item) => (
            <Tr key={item.referralCode}>
              <Td>{item.referralCode}</Td>
              <Td>
                <Icon as={BiRupee} />
                {' '}
                {item.discount}
              </Td>
              <Td>{item.used}</Td>
            </Tr>
          ))
        }
        </Tbody>
      </Table>
      {
        referrals.length === 0 && <Text textAlign="center">No Active Referral codes!</Text>
      }
    </Stack>
  );
};

const styles = StyleSheet.create({
  header: {
    color: colors.gray,
    backgroundColor: colors.light,
    fontWeight: 'normal',
    textTransform: 'none',
    fontSize: '16px',
    [MOBILE_QUERY]: {
      fontSize: '14px'
    },
  }
});

export default ReferralTable;
