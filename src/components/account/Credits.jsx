import {
  Box, Flex, Heading, Icon, Stack
} from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { FiLink } from 'react-icons/fi';
import useMediaQuery from '../../../hooks/useMediaQuery';
import HowToReferral from './HowToReferral';
import ProfileSideNavbar from './ProfileSideNavbar';
import ReferralCoupons from './ReferralCoupons';
import ReferralTable from './ReferralTable';

// Code Review Completed

const Credits = ({ referralCodes, referees }) => {
  const { isMd, isLg } = useMediaQuery();
  const [referrals, setReferrals] = useState([]);
  const [codesGenerated, setCodesGenerated] = useState(0);

  useEffect(() => {
    getReferrals();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getReferrals = async () => {
    try {
      let generatedToday = 0;
      const today = new Date().toString().substr(0, 15);
      const final = referralCodes.map((item) => {
        if (new Date(item.createdAt).toString().substr(0, 15) === today) {
          generatedToday += 1;
        }
        // eslint-disable-next-line consistent-return
        const found = referees.filter(
          (ref) => item.referralCode === ref.referralCode
        );
        if (found.length !== 0) {
          if (item.farmerGenConsumed) {
            return {
              referralCode: item.referralCode,
              discount: item.farmerGenAmt,
              add: false,
              used: `Redeemed (Used by ${found[0].farmerConName})`,
            };
          }
          return {
            referralCode: item.referralCode,
            discount: item.farmerGenAmt,
            add: true,
            used: `Used by ${found[0].farmerConName}`,
          };
        }
        return {
          referralCode: item.referralCode,
          discount: item.farmerGenAmt,
          add: false,
          used: 'Unused',
        };
      });
      setCodesGenerated(generatedToday);
      setReferrals(
        final.sort((a, b) => {
          if (a.used.includes('Unused')) return -1;
          if (a.used.includes('Redeemed')) return 1;
          if (b.used.includes('Unused')) return 1;
          if (b.used.includes('Redeemed')) return -1;
          return 0;
        })
      );
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <Flex
      justifyContent={isMd ? 'center' : 'flex-start'}
      paddingY={isLg ? 12 : '10px'}
      width="full"
      paddingX={isMd ? '15px' : '100px'}
    >
      {isLg && (
      <Box paddingRight="15px" width="30%">
        <ProfileSideNavbar type={4} />
      </Box>
      )}
      <Box paddingLeft={isLg && '15px'} width={isLg ? '70%' : '100%'}>
        <Stack>
          <Heading fontSize="xl">
            <Icon as={FiLink} fontSize="lg" mb={1} me={3} />
            Share Your Code
          </Heading>
          <Flex>
            <ReferralCoupons
              codesGenerated={codesGenerated}
              setCodesGenerated={setCodesGenerated}
              setReferrals={setReferrals}
            />
          </Flex>
          <ReferralTable referrals={referrals} />
          <HowToReferral />
        </Stack>
      </Box>

    </Flex>
  );
};

export default Credits;
