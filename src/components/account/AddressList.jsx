import {
  Box, Flex, HStack, Stack, Text
} from '@chakra-ui/react';
import { useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  selectDefaultAddress,
  selectShippingAddresses
} from '../../../store/selectors/user';
import AddAddress from '../shared/AddAddress';
import AddressCard from './AddressCard';
import ProfileSideNavbar from './ProfileSideNavbar';

// Code Review Completed

const AddressList = () => {
  const { isLg, isMd } = useMediaQuery();
  const { currentUser } = useSelector((state) => state.checkout);
  const defaultAddress = useSelector(selectDefaultAddress);
  const shippingAddress = useSelector(selectShippingAddresses);

  return (
    <Flex
      justifyContent={isMd ? 'center' : 'flex-start'}
      paddingY={isLg ? 12 : '10px'}
      width="full"
      paddingX={isMd ? '15px' : '100px'}
    >
      {isLg && (
        <Box width="30%" paddingRight="15px">
          <ProfileSideNavbar type={3} />
        </Box>
      )}
      <Stack
        paddingLeft={isLg && '15px'}
        spacing="20px"
        width={isLg ? '70%' : '100%'}
      >
        <HStack justifyContent="space-between">
          <Text variant={isLg ? 'heading' : 'subTitle'}>Delivery Address</Text>
          <AddAddress />
        </HStack>
        {Object.keys(defaultAddress).length > 0
        || shippingAddress.length > 0 ? (
          <>
            {Object.keys(defaultAddress).length > 0 && (
              <AddressCard address={defaultAddress} user={currentUser} />
            )}
            {shippingAddress?.map((address) => (
              <AddressCard
                key={address.addressId}
                address={address}
                user={currentUser}
              />
            ))}
          </>
          ) : (
            <Text variant={isLg ? 'mdTextRegular' : 'textRegular'}>
              No Shipping Addresses Available to Select
            </Text>
          )}
      </Stack>
    </Flex>
  );
};

export default AddressList;
