/* eslint-disable react/no-unescaped-entities */
import {
  Divider, Heading, ListItem, OrderedList, Stack
} from '@chakra-ui/layout';
import { colors } from '../../../constants/colors';

const HowToReferral = () => {
  return (
    <Stack width="100%" pt={10}>
      <Heading fontSize="xl">How Does It Work?</Heading>
      <Divider color={colors.gray} />

      <OrderedList pl={5}>
        <ListItem>
          1. Click on 'Generate Code' button to generate the referral code
        </ListItem>
        <ListItem>
          2. Copy the code generated and share it with your friends and family
        </ListItem>
        <ListItem>
          3. Both you and your friend enjoy INR 100 discount on your next purchase.
        </ListItem>
      </OrderedList>
    </Stack>
  );
};

export default HowToReferral;
