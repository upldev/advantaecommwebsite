import {
  Box, Button, Flex, FormControl, Image
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import { useDispatch } from 'react-redux';
import { showLoading, showToast } from '../../../store/actions/feedback';

// Code Review Completed

const UserProfileSquare = ({ name, img }) => {
  const dispatch = useDispatch();
  const {
    values, handleBlur, setFieldValue
  } = useFormikContext();

  const handleImageChange = (e) => {
    dispatch(showLoading(true));
    try {
      const file = e.currentTarget.files[0];
      const objUrl = URL.createObjectURL(file);
      setFieldValue(name, {
        imgFile: file,
        imgUrl: objUrl
      });
    } catch (error) {
      dispatch(showToast({
        title: 'Failed to add profile',
        description: 'Please try again',
        status: false
      }));
    } finally {
      dispatch(showLoading(false));
    }
  };

  return (
    <Flex width="fit-content">
      <FormControl>
        <Box flexShrink={0}>
          <Image
            boxSize="100px"
            objectFit="cover"
            src={values[name] && values[name].imgUrl ? values[name].imgUrl : img || './default-profile.png'}
            alt="Your Profile"
            mb={3}
          />
        </Box>
        <input
          name={name}
          id="selectedFile"
          type="file"
          style={{ display: 'none' }}
          onChange={(e) => {
            handleImageChange(e);
          }}
          accept="image/*"
          onBlur={handleBlur}
        />
        <Button variant="link" onClick={() => document.getElementById('selectedFile').click()}>Change Profile</Button>
      </FormControl>
    </Flex>
  );
};

export default UserProfileSquare;
