import { Flex } from '@chakra-ui/layout';
import { Image, Text } from '@chakra-ui/react';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const SplashScreen = () => {
  const { isLg } = useMediaQuery();
  return (
    <Flex
      flexDirection="column"
      width="100vw"
      height="100vh"
      justifyContent="center"
      alignItems="center"
    >
      <Image width={isLg ? '30%' : '80%'} src="/new-advanta-logo.jpg" />
      <Text
        variant={isLg ? 'mdTextRegular' : 'textRegular'}
        fontWeight="bold"
        color={colors.green}
      >
        Loading... Please Wait
      </Text>
    </Flex>
  );
};

export default SplashScreen;
