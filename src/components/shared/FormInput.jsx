import {
  FormControl, FormErrorMessage, FormLabel, Input
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import sanitize from 'sanitize-html';
import useMediaQuery from '../../../hooks/useMediaQuery';

const FormInput = ({
  isRequired, label, name, ...rest
}) => {
  const { isLg } = useMediaQuery();
  const {
    values, handleBlur, errors, touched, setFieldValue
  } = useFormikContext();

  const handleChange = (e) => {
    const { value } = e.target;
    setFieldValue(name, sanitize(value));
  };

  return (
    <FormControl
      isRequired={isRequired}
      isInvalid={errors[name] && touched[name]}
    >
      <FormLabel fontSize={isLg ? '18px' : '12px'}>{label}</FormLabel>
      <Input
        size={isLg ? 'md' : 'sm'}
        name={name}
        value={values[name]}
        onChange={handleChange}
        onBlur={handleBlur}
        {...rest}
      />
      <FormErrorMessage>{errors[name]}</FormErrorMessage>
    </FormControl>
  );
};

export default FormInput;
