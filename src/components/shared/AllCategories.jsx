import { Flex } from '@chakra-ui/react';
import React from 'react';
import { categories } from '../../../constants/categories';
import useMediaQuery from '../../../hooks/useMediaQuery';
import Section from '../home/Section';
import OurSeedsBox from './OurSeedsBox';

// Code Review Complete

const AllCategories = () => {
  const { isLg } = useMediaQuery();

  return (
    <Section
      id="our-seeds"
      title="Our Seeds"
      description="Explore our innovative and wide variety of seeds"
    >
      <Flex
        direction="row"
        justifyContent={isLg ? 'space-between' : 'space-evenly'}
        flexWrap="wrap"
        marginBottom="32px"
      >
        {categories.map((category) => {
          return (
            <OurSeedsBox margin category={category} key={category.title} />
          );
        })}
      </Flex>
    </Section>
  );
};

export default AllCategories;
