import { IconButton } from '@chakra-ui/button';
import { Flex } from '@chakra-ui/layout';
import { css, StyleSheet } from 'aphrodite';
import { AiFillHeart } from 'react-icons/ai';
import { IoHeartOutline } from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import { useProductContext } from '../../context/ProductContext';

// Code Review Completed

const WishListButton = () => {
  const { isWishlisted, handleAddToWishlist, handleRemoveFromWishlist } = useProductContext();

  return (
    <Flex justifyContent="right" mr={1} mt={1}>
      {isWishlisted ? (
        <IconButton
          bgColor={colors.white}
          className={css(styles.wishlistedBadge)}
          boxShadow="lg"
          onClick={handleRemoveFromWishlist}
          icon={<AiFillHeart size="1.5rem" />}
        />
      ) : (
        <IconButton
          bgColor={colors.white}
          className={css(styles.wishlistBadge)}
          boxShadow="base"
          onClick={handleAddToWishlist}
          icon={<IoHeartOutline size="1.5rem" />}
        />
      )}
    </Flex>
  );
};

const styles = StyleSheet.create({
  wishlistBadge: {
    position: 'absolute',
    color: colors.lightRed,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 5,
    borderRadius: '50%',
  },
  wishlistedBadge: {
    position: 'absolute',
    color: colors.lightRed,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 5,
    borderRadius: '50%',
  },
});

export default WishListButton;
