import { Box } from '@chakra-ui/layout';
import { SwiperSlide } from 'swiper/react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ProductCard from '../home/ProductCard';
import HomeSwiper from './HomeSwiper';

const ProductsCarousel = ({ products, name }) => {
  const { isLg } = useMediaQuery();

  return (
    <HomeSwiper name={name} lgSlides={4} smSlides={2}>
      {products.map((product) => (
        <SwiperSlide key={product.materialCode}>
          <Box marginBottom={!isLg && '45px'}>
            <ProductCard product={product} />
          </Box>
        </SwiperSlide>
      ))}
    </HomeSwiper>
  );
};

export default ProductsCarousel;
