/* eslint-disable react/button-has-type */
import { Button, Flex, Icon } from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import React from 'react';
import { GoogleLogin } from 'react-google-login';
import { TiSocialGooglePlus } from 'react-icons/ti';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { showToast } from '../../../store/actions/feedback';

// Code Review Completed
// Exposed Client ID for Google need to mitigate
// Need to Create Advanta ID for setting up Google connection

const Google = () => {
  const { isLg, isMd } = useMediaQuery();
  const dispatch = useDispatch();
  const { values, setFieldValue } = useFormikContext();

  const success = async (val) => {
    const data = JSON.stringify(val.profileObj);
    if (val.profileObj.name) setFieldValue('name', val.profileObj.name);
    if (val.profileObj.email) setFieldValue('email', val.profileObj.email);
    fetch(val.profileObj.imageUrl, { mode: 'cors' })
      .then(async (response) => {
        const contentType = response.headers.get('content-type');
        const blob = await response.blob();
        const file = new File([blob], 'img.jpg', { contentType, type: 'image/jpeg' });
        const objUrl = URL.createObjectURL(file);
        setFieldValue('profileImg', {
          imgFile: file,
          imgUrl: objUrl
        });
      }).catch((err) => console.error(err));
    setFieldValue('google', data);
  };

  const failure = () => {
    dispatch(showToast({
      title: 'Could not connect to google!',
      message: 'Please try again.',
      status: false
    }));
  };

  return (
    <Flex width={isLg ? 'full' : 'fit-content'}>
      <GoogleLogin
        clientId="880711857135-v2ttidtpn2g07t2o8l0qsr0g9jhgq088.apps.googleusercontent.com"
        render={(renderProps) => (
          <Button
            type="button"
            leftIcon={<Icon as={TiSocialGooglePlus} fontSize="1.5rem" />}
            onClick={renderProps.onClick}
            colorScheme="red"
            variant="solid"
            borderRadius={0}
            borderColor={colors.black}
            size={isLg ? 'md' : 'sm'}
            mx={2}
            width={isMd ? '120px' : 'full'}
          >
            Google
          </Button>
        )}
        buttonText="Login"
        onSuccess={success}
        onFailure={failure}
        cookiePolicy="single_host_origin"
      />
    </Flex>
  );
};

export default Google;
