import { IconButton } from '@chakra-ui/button';
import { Flex, Stack } from '@chakra-ui/layout';
import { IoChevronBackOutline, IoChevronForwardOutline } from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const CarouselFooterButtons = ({ name }) => {
  const { isLg } = useMediaQuery();

  return (
    isLg && (
      <Flex justifyContent="center" my="32px">
        <Stack direction="row" spacing="22px">
          <IconButton
            className={`${name}-prev`}
            color={colors.lightBlue}
            borderRadius="0"
            borderColor={colors.lightBlue}
            borderWidth="1px"
            width="72px"
            icon={<IoChevronBackOutline />}
          />
          <IconButton
            className={`${name}-next`}
            color={colors.lightBlue}
            borderRadius="0"
            borderColor={colors.lightBlue}
            borderWidth="1px"
            width="72px"
            icon={<IoChevronForwardOutline />}
          />
        </Stack>
      </Flex>
    )
  );
};

export default CarouselFooterButtons;
