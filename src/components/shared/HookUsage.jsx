import { AddIcon, MinusIcon } from '@chakra-ui/icons';
import {
  Flex,
  Icon,
  IconButton,
  Spinner,
  Text
} from '@chakra-ui/react';
import { StyleSheet } from 'aphrodite';
import React, { useState } from 'react';
import { IoTrashOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { LG_BREAKPOINT } from '../../../constants/mediaQueries';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  decrementQuantity,
  incrementQuantity,
  removeFromCart
} from '../../../store/actions/cart';

const HookUsage = ({
  qty, product, currSelected, sku = []
}) => {
  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const quantity = useSelector((state) => state.cart.cartItems.filter(
    (x) => x.sku.ecomCode === sku[currSelected]?.ecomCode
  ));

  const inCartDetails = quantity[0];
  const [loadPlusMinus, setPlusMinus] = useState(false);
  // const { getInputProps, getIncrementButtonProps, getDecrementButtonProps } = useNumberInput({
  //   step: 1,
  //   defaultValue: qty,
  //   min: 0,
  //   max: sku[currSelected]?.qtyAvailable + 1,
  // });
  const handleIncrement = () => {
    setPlusMinus(true);
    dispatch(incrementQuantity(sku[currSelected]?.ecomCode, setPlusMinus));
  };
  const handleDecrement = () => {
    setPlusMinus(true);
    dispatch(decrementQuantity(sku[currSelected]?.ecomCode, setPlusMinus));
  };
  const handleRemoveFromCart = () => {
    dispatch(removeFromCart(sku[currSelected]?.ecomCode));
  };
  // const input = getInputProps({ isReadOnly: true });
  return (
    <Flex justifyContent="center">
      <Flex fontWeight="bold" justifyContent="center">
        {inCartDetails?.qty > 1 && (
          <IconButton
            borderRadius="0"
            _hover={{ opacity: 0.5 }}
            onClick={handleRemoveFromCart}
            bgColor={colors.gray}
            color={colors.white}
            mr={2}
            fontSize="sm"
            size={isLg ? 'md' : 'sm'}
          >
            <Icon as={IoTrashOutline} />
          </IconButton>
        )}
        {inCartDetails?.qty > 1 ? (
          <IconButton
            borderRadius="0"
            _hover={{ opacity: 0.5 }}
            backgroundColor={colors.green}
            color={colors.white}
            onClick={handleDecrement}
            fontSize="sm"
            size={isLg ? 'md' : 'sm'}
            disabled={loadPlusMinus}
          >
            <Icon as={MinusIcon} boxSize="1rem" />
          </IconButton>
        ) : (
          <IconButton
            borderRadius="0"
            _hover={{ opacity: 0.5 }}
            onClick={handleRemoveFromCart}
            bgColor={colors.gray}
            color={colors.white}
            fontSize="sm"
            size={isLg ? 'md' : 'sm'}
          >
            <Icon as={IoTrashOutline} />
          </IconButton>
        )}
        <Flex
          alignItems="center"
          justifyContent="center"
          borderTop="1px"
          borderBottom="1px"
          height="100%"
          width={isLg ? '5vw' : '8vw'}
          borderColor={colors.gray}
        >
          {loadPlusMinus ? (
            <Spinner size={isLg ? 'md' : 'sm'} />
          ) : (
            <Text fontSize={!isLg && 'xs'} color={colors.black}>
              {inCartDetails.qty}
            </Text>
          )}
        </Flex>
        <IconButton
          size={isLg ? 'md' : 'sm'}
          borderRadius="0"
          _hover={{ opacity: 0.5 }}
          backgroundColor={colors.green}
          color={colors.white}
          disabled={
            loadPlusMinus
            || inCartDetails?.qty === sku[currSelected]?.qtyAvailable
          }
          onClick={handleIncrement}
          fontSize="sm"
        >
          <Icon as={AddIcon} boxSize="1rem" />
        </IconButton>
      </Flex>
    </Flex>
  );
};
const styles = StyleSheet.create({
  qtyContainer: {
    borderTopWidth: '1px',
    borderBottomWidth: '1px',
    borderColor: colors.black,
    padding: '5px 40px 6px 40px',
    margin: '0',
    width: '5vw',
    [LG_BREAKPOINT]: {
      padding: '3px 0px 1px 0px',
      width: '20vw',
    },
  },
});

export default HookUsage;
