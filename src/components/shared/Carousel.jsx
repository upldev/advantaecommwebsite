import { Box } from '@chakra-ui/layout';
import { useSelector } from 'react-redux';
import { Swiper, SwiperSlide } from 'swiper/react';
import { selectHeroBanners } from '../../../store/selectors/home';
import HeroBanner from '../home/HeroBanner';

// Code Review Completed

const Carousel = () => {
  const hero = useSelector(selectHeroBanners);

  return (
    <Box width="100%">
      <Swiper
        pagination={{ clickable: true }}
        loop
        autoplay={{
          delay: 5000,
          disableOnInteraction: false,
          pauseOnMouseEnter: true,
        }}
      >
        {hero.map((banner) => (
          <SwiperSlide key={banner.bannerId}>
            <HeroBanner banner={banner} />
          </SwiperSlide>
        ))}
      </Swiper>
    </Box>
  );
};

export default Carousel;
