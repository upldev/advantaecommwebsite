/* eslint-disable react/no-unescaped-entities */
import { Text } from '@chakra-ui/layout';
import React from 'react';
import { colors } from '../../../constants/colors';

// Code Review Complete

const AboutUs = () => {
  return (
    <Text mt="3" color={colors.advantaBlue}>
      Our Mission
      We strive to deliver our seeds to every farmer's doorstep in India.
      Provide our farmers with the best quality high performing seeds & agronomic advice to ensure proﬁtability
      Advanced research in plant genetics using most advanced R&D technologies available
      Ensure high crop productivity by testing our Seed technology across a range of environments

    </Text>
  );
};

export default AboutUs;
