import { CheckCircleIcon, WarningIcon } from '@chakra-ui/icons';
import {
  Flex, Icon, Spinner, Text, useToast
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { resetToast } from '../../../store/actions/feedback';

const FeedbackProvider = () => {
  const { isLoading, toast } = useSelector((state) => state.feedback);
  const dispatch = useDispatch();

  const toaster = useToast();

  useEffect(() => {
    if (toast) {
      const { title, message, status } = toast;
      toaster({
        title: title || 'Notification',
        description: message,
        status:
          typeof status === 'boolean' ? (status ? 'success' : 'error') : 'info',
        isClosable: true,
        duration: 2000,
        position: 'bottom-right',
        // eslint-disable-next-line react/display-name
        render: () => (
          <Flex
            borderRadius={3}
            p={3}
            color={colors.white}
            bg={status ? colors.green : colors.lightRed}
            alignItems="center"
          >
            <Flex mr={2}>
              {status ? (
                <Icon as={CheckCircleIcon} />
              ) : (
                <Icon as={WarningIcon} />
              )}
            </Flex>
            <Flex flexDirection="column">
              <Text>{title || 'Notification'}</Text>
              <Text>{message}</Text>
            </Flex>
          </Flex>
        ),
      });
      dispatch(resetToast());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [toast]);

  return (
    <>
      {isLoading ? (
        <div className={css(styles.loadingContainer)}>
          <Spinner size="xl" color={colors.darkGreen} />
        </div>
      ) : null}
    </>
  );
};

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'fixed',
    zIndex: 10,
    top: 0,
    height: '100vh',
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,0.3)',
  },
});

export default FeedbackProvider;
