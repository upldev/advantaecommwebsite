import { Text } from '@chakra-ui/react';
import React from 'react';

const FAQ = ({ faq }) => {
  return (
    <>
      <Text fontWeight="bold" mb={1}>
        {faq.question}
      </Text>
      <Text mb={5}>
        {faq.answer}
      </Text>
    </>
  );
};

export default FAQ;
