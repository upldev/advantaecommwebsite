import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Textarea
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import sanitize from 'sanitize-html';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const FormTextArea = ({
  placeholder, isRequired, label, name, h, ...rest
}) => {
  const { isLg } = useMediaQuery();
  const {
    values, handleBlur, errors, touched, setFieldValue
  } = useFormikContext();

  const handleSanitize = (e) => {
    const { value } = e.target;
    setFieldValue(name, sanitize(value));
  };

  return (
    <FormControl
      isRequired={isRequired}
      isInvalid={errors[name] && touched[name]}
    >
      <FormLabel fontSize={isLg ? '18px' : '12px'}>{label}</FormLabel>
      <Textarea
        placeholder={placeholder}
        name={name}
        size={isLg ? 'md' : 'sm'}
        value={values[name]}
        onChange={handleSanitize}
        onBlur={handleBlur}
        borderRadius={0}
        borderColor={colors.black}
        h={h}
        {...rest}
      />
      <FormErrorMessage>{errors[name]}</FormErrorMessage>
    </FormControl>
  );
};

export default FormTextArea;
