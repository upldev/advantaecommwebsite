import { Box } from '@chakra-ui/layout';
import { Swiper } from 'swiper/react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import CarouselFooterButtons from './CarouselFooterButton';

const HomeSwiper = ({
  name, children, lgSlides, smSlides, ...rest
}) => {
  const { isLg } = useMediaQuery();

  return (
    <Box marginTop="32px">
      <Swiper
        mousewheel={{ forceToAxis: true }}
        pagination={
          !isLg && {
            clickable: true,
          }
        }
        navigation={
          isLg && {
            prevEl: `.${name}-prev`,
            nextEl: `.${name}-next`,
          }
        }
        slidesPerView={smSlides || 2}
        spaceBetween={15}
        breakpoints={
          lgSlides && {
            992: {
              slidesPerView: lgSlides,
            },
          }
        }
        {...rest}
      >
        {children}
      </Swiper>
      <CarouselFooterButtons name={name} />
    </Box>
  );
};

export default HomeSwiper;
