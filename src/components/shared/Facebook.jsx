import { Button, Flex, Icon } from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import { useCallback } from 'react';
import { IoLogoFacebook } from 'react-icons/io5';
import { useDispatch } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { FB_LOGIN } from '../../../lib/router';
import { showToast } from '../../../store/actions/feedback';

let windowObjectReference = null;
let previousUrl = null;

// Code Review Completed

const Facebook = () => {
  const { isLg, isMd } = useMediaQuery();
  const dispatch = useDispatch();
  const { values, setFieldValue } = useFormikContext();

  const dataReceived = (data) => {
    if (data.source === 'DATA_FETCHED') {
      setFieldValue('fb', data.profile);
      const profile = JSON.parse(data.profile);
      setFieldValue('name', profile.name);
      setFieldValue('email', profile.email);
    }
    if (data === 'FETCH_ERROR') {
      dispatch(showToast({
        title: 'Could not connect to facebook!',
        message: 'Please try again.',
        status: false
      }));
    }
    window.removeEventListener('message', receiveMessage);
  };

  const receiveMessage = useCallback(
    (event) => {
      const { data } = event;
      dataReceived(data);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []
  );

  const openSignInWindow = (url, name) => {
    window.removeEventListener('message', receiveMessage); // remove exisiting listener

    // window features
    const strWindowFeatures = 'toolbar=no, menubar=no, width=600, height=600, top=100, left=100';

    if (windowObjectReference === null || windowObjectReference.closed) {
      // not opened or opened window was closed
      windowObjectReference = window.open(url, name, strWindowFeatures);
    } else if (previousUrl !== url) {
      // focus on already opened window on button click
      windowObjectReference = window.open(url, name, strWindowFeatures);
      windowObjectReference.focus();
    } else {
      // focus on window already opened
      windowObjectReference.focus();
    }
    window.addEventListener('message', receiveMessage);
    previousUrl = url;
  };

  const popup = () => {
    openSignInWindow(FB_LOGIN, 'Facebook Login');
  };

  return (
    <Flex width={isLg ? 'full' : 'fit-content'}>
      <Button
        type="button"
        colorScheme="facebook"
        onClick={popup}
        leftIcon={<Icon as={IoLogoFacebook} />}
        size={isLg ? 'md' : 'sm'}
        borderRadius={0}
        mx={2}
        width={isMd ? '120px' : 'full'}
      >
        facebook
      </Button>
    </Flex>
  );
};

export default Facebook;
