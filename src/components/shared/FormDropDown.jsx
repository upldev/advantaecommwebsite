import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Select
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import useMediaQuery from '../../../hooks/useMediaQuery';

const FormDropDown = ({
  isRequired,

  placeholder,
  label,
  options,
  name,
  selectedValue,
  ...rest
}) => {
  const { isLg } = useMediaQuery();
  const {
    values, handleBlur, handleChange, errors, touched
  } = useFormikContext();
  return (
    <FormControl
      isRequired={isRequired}
      isInvalid={errors[name] && touched[name]}
    >
      <FormLabel fontSize={isLg ? '18px' : '12px'}>{label}</FormLabel>
      <Select
        defaultValue={selectedValue}
        onChange={handleChange}
        placeholder={placeholder}
        value={values[name]}
        onBlur={handleBlur}
        name={name}
        size={isLg ? 'md' : 'sm'}
        {...rest}
      >
        {options.map((option) => (
          <option selected={option === selectedValue} value={option} key={option}>
            {option}
          </option>
        ))}
      </Select>
      <FormErrorMessage>{errors[name]}</FormErrorMessage>
    </FormControl>
  );
};

export default FormDropDown;
