import {
  Center, Spinner
} from '@chakra-ui/react';
import { API } from 'aws-amplify';
import React, { useEffect } from 'react';

// Code Review Completed

const PostFacebook = () => {
  const connectToServer = async () => {
    const url = new URL(window.location.href);
    const code = url.searchParams.get('code');
    const res = await API.post('EAMS5TAPI04', '/facebookDetails', {
      body: {
        code
      }
    });
    if (res.success) {
      const profile = JSON.stringify(res.data);
      window.opener.postMessage({
        source: 'DATA_FETCHED',
        profile
      });
      window.close();
    } else {
      window.opener.postMessage('FETCH_ERROR');
    }
  };

  useEffect(() => {
    connectToServer();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Center m="auto">
      <Spinner size="lg" />
    </Center>
  );

};

export default PostFacebook;
