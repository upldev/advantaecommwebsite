import {
  Alert, AlertDescription, AlertIcon, AlertTitle, Box, Stack
} from '@chakra-ui/react';
import React from 'react';
import { useSelector } from 'react-redux';

const AlertComponent = ({ type }) => {
  const alerts = useSelector((state) => state.alert);
  return (

    <Stack spacing={3} my={3}>
      {alerts !== null
    && alerts.length > 0
    && alerts.map((alert) => (
      alert.type === type && (
        <Alert
          key={alert.id}
          status={typeof alert.status === 'boolean'
            ? alert.status
              ? 'success'
              : 'error'
            : 'info'}
        >
          <AlertIcon />
          <Box flex="1">
            <AlertTitle textAlign="justify">{alert.title}</AlertTitle>
            <AlertDescription display="block" textAlign="justify">
              {alert.message}
            </AlertDescription>
          </Box>
        </Alert>
      )
    ))}
    </Stack>
  );
};

export default AlertComponent;
