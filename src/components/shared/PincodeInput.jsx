import { Alert, AlertIcon } from '@chakra-ui/alert';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Spinner } from '@chakra-ui/react';
// import { Input } from '@chakra-ui/input';
import { useFormikContext } from 'formik';
import { useEffect } from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import usePincodeServiceability from '../../../hooks/usePincodeServiceability';
import FormInput from './FormInput';

const PincodeInput = () => {
  const { isMd } = useMediaQuery();

  const {
    touched,
    errors,
    values,
    handleBlur,
    setFieldValue,
    setFieldError,
    setErrors,
  } = useFormikContext();

  const { isDeliverable, isLoading } = usePincodeServiceability(values.pincode);

  useEffect(() => {
    if (!isDeliverable) {
      setErrors({
        ...errors,
        pincode: 'Unserviceable in the entered pincode.',
      });
    } else {
      const { pincode, ...rest } = errors;
      setErrors(rest);
    }
  }, [errors, isDeliverable, setErrors, setFieldError]);

  return (
    <>
      <FormControl
        mb={isMd ? 2 : 0}
        isRequired
        isInvalid={touched.pincode && errors.pincode}
      >
        <FormLabel>Pincode</FormLabel>
        {/* <Input
          name="pincode"
          value={values.pincode}
          size={isLg ? 'md' : 'sm'}
          onBlur={handleBlur}
          onChange={(e) => {
            const { value } = e.target;
            setFieldValue('pincode', value);
          }}
        /> */}
        <FormInput name="pincode" />
      </FormControl>
      {!isLoading && isDeliverable ? (
        <Alert status="success">
          <AlertIcon />
          Serviceable in entered pincode.
        </Alert>
      ) : (
        values.pincode?.length === 6 && (
          <Alert status="error">
            <AlertIcon />
            Unserviceable in entered pincode.
          </Alert>
        )
      )}
      {isLoading && <Spinner color={colors.green} />}
    </>
  );
};

export default PincodeInput;
