import {
  Box, Flex, Image, Text
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { CATEGORY } from '../../../lib/router';

// Code Review Complete
// Routes not working properly need revisit

const OurSeedsBox = ({ category, margin, index }) => {
  const { isLg } = useMediaQuery();
  const router = useRouter();
  return (
    <Box
      cursor="pointer"
      marginTop={isLg ? '32px' : '15px'}
      onClick={() => router.push(`${CATEGORY}?type=${category.link}`, undefined, {
        shallow: true,
      })}
      width={isLg ? '18vw' : '25vw'}
    >
      <Flex
        borderRadius="50%"
        borderWidth="1px"
        borderColor={colors.gray}
        justifyContent="center"
        alignItems="center"
        height={isLg ? '18vw' : '25vw'}
        width={isLg ? '18vw' : '25vw'}
        padding="8px"
      >
        <Image
          src={category.src}
          loading="lazy"
          alt={category.title}
          borderRadius="50%"
          height="100%"
          width="100%"
          borderColor={colors.light}
        />
      </Flex>
      <Text
        variant="subTitle"
        my="10px"
        textTransform="uppercase"
        textAlign="center"
        style={{ color: colors.black }}
        fontSize={{ base: 'xs', lg: 'lg', xl: 'lg' }}
      >
        {category.title}
      </Text>
    </Box>
  );
};

export default OurSeedsBox;
