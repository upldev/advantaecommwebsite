import { Button } from '@chakra-ui/button';
import { Checkbox } from '@chakra-ui/checkbox';
import {
  FormControl,
  FormErrorMessage,
  FormLabel
} from '@chakra-ui/form-control';
import { useDisclosure } from '@chakra-ui/hooks';
import { Stack } from '@chakra-ui/layout';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay
} from '@chakra-ui/modal';
import { Select } from '@chakra-ui/select';
import { Textarea } from '@chakra-ui/textarea';
import { Formik } from 'formik';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { v4 } from 'uuid';
import * as Yup from 'yup';
import { stateObj } from '../../../constants/cityStates';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { addShippingToProfile } from '../../../store/actions/checkout';
import FormDropDown from './FormDropDown';
import FormInput from './FormInput';
import PincodeInput from './PincodeInput';

const states = Object.keys(stateObj);

const AddressSchema = Yup.object({
  address: Yup.string()
    .required('Required')
    .max(190, 'Address should not exceed 190 characters'),
  city: Yup.string().required('Required'),
  state: Yup.string().required('Required'),
  pincode: Yup.string()
    .length(6, 'Please enter a valid pincode')
    .required('Required'),
});

// Code Review Completed
// why is handlePinCheck not used please check

const AddAddress = () => {
  const dispatch = useDispatch();
  const user = useSelector((state) => state.checkout.currentUser);

  const [cities, setCities] = useState([]);

  const { isOpen, onOpen, onClose } = useDisclosure();
  const { isLg, isMd } = useMediaQuery();

  const handleSubmit = (values) => {
    let { shipping } = user;

    if (values.isDefault) {
      shipping = shipping.map((item) => ({ ...item, isDefault: false }));
    }

    shipping.push({ ...values, pincode: parseInt(values.pincode) });

    if (user.shipping) {
      dispatch(addShippingToProfile(shipping));
    } else {
      dispatch(addShippingToProfile(shipping));
    }
    onClose();
  };

  const handleCity = (state) => {
    const selected = stateObj[state];
    setCities(() => selected.cities);
  };

  return (
    <>
      <Button variant="outline_black" onClick={onOpen}>
        Add New Address
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add Address</ModalHeader>
          <ModalCloseButton />
          <Formik
            initialValues={{
              addressId: v4(),
              name: '',
              address: '',
              city: '',
              state: '',
              pincode: '',
              isDefault: false,
            }}
            onSubmit={handleSubmit}
            validationSchema={AddressSchema}
          >
            {(props) => (
              <>
                <ModalBody>
                  <Stack>
                    <FormControl
                      mb={isMd ? 2 : 0}
                      isRequired
                      isInvalid={props.touched.name && props.errors.name}
                    >
                      <FormLabel>Name</FormLabel>
                      {/* <Input
                        name="name"
                        value={props.values.name}
                        size={isLg ? 'md' : 'sm'}
                        onBlur={props.handleBlur}
                        onChange={props.handleChange}
                      /> */}
                      <FormInput
                        name="name"
                      />
                      <FormErrorMessage>{props.errors.name}</FormErrorMessage>
                    </FormControl>
                    <FormControl
                      isInvalid={props.errors.address && props.touched.address}
                      isRequired
                    >
                      <FormLabel>Address</FormLabel>
                      <Textarea
                        placeholder="Enter your Address"
                        name="address"
                        size={isLg ? 'md' : 'sm'}
                        value={props.values.address}
                        onChange={props.handleChange}
                        onBlur={props.handleBlur}
                        borderRadius="0"
                        borderColor={colors.black}
                      />
                    </FormControl>
                    <FormControl
                      isRequired
                      isInvalid={props.touched.state && props.errors.state}
                      mb={isMd ? 2 : 0}
                    >
                      <FormLabel
                        variant={isLg ? 'mdTextRegular' : 'textRegular'}
                      >
                        State
                      </FormLabel>
                      <Select
                        onChange={(e) => {
                          props.handleChange(e);
                          handleCity(e.target.value);
                        }}
                        placeholder="Select state"
                        onBlur={props.handleBlur}
                        name="state"
                        size={isLg ? 'md' : 'sm'}
                      >
                        {states.map((option) => (
                          <option value={option} key={option}>
                            {option}
                          </option>
                        ))}
                      </Select>
                      <FormErrorMessage>{props.errors.state}</FormErrorMessage>
                    </FormControl>
                    <FormControl isRequired>
                      <FormLabel>City</FormLabel>
                      <FormDropDown
                        mb={isMd ? 2 : 0}
                        name="city"
                        placeholder="Select City"
                        options={cities}
                      />
                    </FormControl>
                    <PincodeInput />
                  </Stack>
                </ModalBody>
                <ModalFooter flexDirection="column">
                  <Checkbox
                    name="isDefault"
                    value={props.values.isDefault}
                    onChange={props.handleChange}
                    width="100%"
                  >
                    Make this as my Default Address
                  </Checkbox>
                  <Button
                    disabled={!props.isValid || props.isValidating}
                    mt="3"
                    variant="primary"
                    type="submit"
                    onClick={props.submitForm}
                  >
                    Add New Address
                  </Button>
                </ModalFooter>
              </>
            )}
          </Formik>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AddAddress;
