/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable max-len */
import {
  Flex, Grid, GridItem, Icon, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { IoLogoFacebook, IoLogoLinkedin, IoLogoTwitter } from 'react-icons/io5';
import { categories } from '../../../constants/categories';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  CATEGORY,
  FAQS,
  HOME,
  PRIVACY_POLICY,
  TERMS_AND_CONDITION
} from '../../../lib/router';

const Footer = () => {
  const { isLg } = useMediaQuery();
  const router = useRouter();

  return (
    <footer
      style={{
        color: colors.white,
        backgroundColor: colors.black,
        marginTop: 'auto',
        fontSize: '1rem',
        paddingBottom: !isLg ? '125px' : 0
      }}
    >
      <Grid gap={6} my={5} mx={100} templateColumns="repeat(6, 1fr)">
        <GridItem colSpan={3}>
          <Link href={HOME}>
            <img
              className={css(styles.logo)}
              src="/advanta-bw-logo.png"
              alt="advanta-logo"
            />
          </Link>
          <Flex flexDirection="column">
            <Text my={4} textAlign="left" variant="subTitleWhite">
              {' '}
              ABOUT US
              {' '}
            </Text>
            <Text mb={4} variant="textRegularWhite">
              Advanta Seeds is focused on sustainable agriculture and providing
              farmers with quality seeds. With the intent to deliver convenience
              to our farmers, our e-commerce channel strives to deliver seeds at
              every farmer's doorstep in India.
              {' '}
            </Text>
            {/* <Text mb={4} variant="textRegularWhite">
              Advanta Seeds prides itself on decades of research and deployment of the most advanced
              technologies in traditional plant breeding to deliver high-quality seeds.
              The company has over 60 years of experience in plant genetics research and development.
              {' '}
            </Text> */}
            <Flex>
              <img
                className={css(styles.getItOnPlayStore)}
                src="/google-play-badge.png"
                alt="play-store"
              />
              <img
                className={css(styles.getItOnAppStore)}
                src="/app-store.jpg"
                alt="app-store"
              />
            </Flex>
          </Flex>
        </GridItem>
        <GridItem colSpan={1}>
          <Flex flexDirection="column">
            <Text mb={4} variant="subTitleWhite">
              {' '}
              OUR SEEDS
              {' '}
            </Text>
            {categories
              && categories.length > 0
              && categories.map((category) => {
                return (
                  <Text
                    key={category.title}
                    className={css(styles.linkStyles)}
                    mb={4}
                    variant="textRegularWhite"
                    onClick={() => router.push(
                      `${CATEGORY}?type=${category.link}`,
                      undefined,
                      { shallow: true }
                    )}
                  >
                    {category.title}
                  </Text>
                );
              })}
          </Flex>
        </GridItem>
        <GridItem colSpan={2}>
          <Flex flexDirection="column">
            <Text variant="subTitleWhite"> CONTACT US </Text>
            <Text my={4} variant="textRegularWhite">
              Address: UPL House, 4, Service Rd, Teacher's Colony, Siddharth
              Nagar, Bandra East, Mumbai, Maharashtra 400051
            </Text>
            <Text mb={4} variant="textRegularWhite">
              Telephone:+91 987654321
            </Text>
            <Text mb={4} variant="textRegularWhite">
              Email: customerserviceindia@advantaseeds.com
            </Text>
          </Flex>
          <Flex flexDirection="column">
            <Text my={4} variant="subTitleWhite">
              {' '}
              FOLLOW US
              {' '}
            </Text>
            <Flex>
              <a
                target="_blank"
                className={css(styles.socialMediaLinks)}
                href="https://www.facebook.com/advantaseeds/"
              >
                <Icon mr={4} as={IoLogoFacebook} />
              </a>

              <a
                target="_blank"
                className={css(styles.socialMediaLinks)}
                href="https://twitter.com/advantaseeds"
              >
                <Icon mr={4} as={IoLogoTwitter} />
              </a>

              <a
                target="_blank"
                className={css(styles.socialMediaLinks)}
                href="https://www.linkedin.com/company/advantaseeds/"
              >
                <Icon as={IoLogoLinkedin} />
              </a>
            </Flex>
          </Flex>
        </GridItem>
      </Grid>
      <Flex justifyContent="center" mb={4} flexDirection="column" mx={100}>
        <Flex mb={4} justifyContent="center">
          <Link href={FAQS}>
            <Text className={css(styles.linkStyles)} variant="textRegularWhite">
              FAQ
            </Text>
          </Link>
          <Link href={PRIVACY_POLICY}>
            <Text
              className={css(styles.linkStyles)}
              mx={10}
              variant="textRegularWhite"
            >
              Privacy Policy
            </Text>
          </Link>
          <Link href={TERMS_AND_CONDITION}>
            <Text className={css(styles.linkStyles)} variant="textRegularWhite">
              T&C
            </Text>
          </Link>
        </Flex>
        <Text textAlign="center" variant="textRegularWhite">
          {' '}
          &#169; Copyright 2021, Advanta Seeds (Registered in India), All rights
          reserved.
        </Text>
      </Flex>
    </footer>
  );
};

const styles = StyleSheet.create({
  linkStyles: {
    cursor: 'pointer',
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    }
  },
  socialMediaLinks: {
    fontSize: '1.75rem',
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    },
    marginRight: 10
  },

  divider: {
    height: '100%'
  },
  logo: {
    cursor: 'pointer',
    height: '5rem',
    width: '13rem',
    zIndex: 1
  },
  getItOnPlayStore: {
    cursor: 'pointer',
    height: '4rem',
    width: '10rem',
    marginRight: 10
  },
  getItOnAppStore: {
    cursor: 'pointer',
    height: '3rem',
    width: '10rem',
    marginTop: 6
  }
});
export default Footer;
