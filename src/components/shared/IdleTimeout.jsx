import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure
} from '@chakra-ui/react';
import { useCallback, useEffect, useState } from 'react';
import { useIdleTimer } from 'react-idle-timer';
import { useDispatch } from 'react-redux';
import { userLogout } from '../../../store/actions/user';

const IdleTimeout = () => {
  const dispatch = useDispatch();

  const { isOpen, onOpen, onClose } = useDisclosure();

  const [countDown, setCountDown] = useState(60);

  const handleIdle = () => {
    onOpen();
    setTimeout(() => setCountDown(countDown - 1), 1000);
  };

  const handleLogout = useCallback(() => {
    onClose();
    dispatch(userLogout());
  }, [dispatch, onClose]);

  const { isIdle } = useIdleTimer({
    timeout: 1000 * 60 * 20,
    onIdle: handleIdle,
  });

  useEffect(() => {
    if (countDown <= 0) {
      handleLogout();
    }
  }, [countDown, dispatch, handleLogout]);

  useEffect(() => {
    if (countDown > 0 && isOpen) {
      setTimeout(() => setCountDown(countDown - 1), 1000);
    } else {
      setCountDown(60);
    }
  }, [countDown, isOpen]);

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Idle Prompt</ModalHeader>
          <ModalBody>You have been idle for 20 minutes!</ModalBody>
          <ModalFooter>
            <Button variant="primary" mr={3} onClick={onClose}>
              Continue
            </Button>
            <Button onClick={handleLogout} variant="outline_secondary">
              {`Logout (${countDown}s)`}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default IdleTimeout;
