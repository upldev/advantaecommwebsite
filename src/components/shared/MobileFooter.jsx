/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable max-len */
import { Flex, Icon, Text } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { IoLogoFacebook, IoLogoLinkedin, IoLogoTwitter } from 'react-icons/io5';
import { colors } from '../../../constants/colors';

// Code Review Completed

const MobileFooter = () => {
  return (

    <footer
      className="main-footer"
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: colors.white,
        width: '100%',
        marginTop: 'auto',
        fontSize: '1rem',
        paddingTop: '15px',
        flexDirection: 'column',
        backgroundColor: colors.light
      }}
    >
      <Flex flexDirection="column">
        <Text variant="subTitle" textAlign="center">Follow Us</Text>
        <Flex justifyContent="center">
          <a
            target="_blank"
            className={css(styles.socialMediaLinks)}
            href="https://www.facebook.com/advantaseeds/"
          >
            <Icon mr={4} as={IoLogoFacebook} />
          </a>

          <a
            target="_blank"
            className={css(styles.socialMediaLinks)}
            href="https://twitter.com/advantaseeds"
          >
            <Icon mr={4} as={IoLogoTwitter} />
          </a>

          <a
            target="_blank"
            className={css(styles.socialMediaLinks)}
            href="https://www.linkedin.com/company/advantaseeds/"
          >
            <Icon as={IoLogoLinkedin} />
          </a>
        </Flex>
        <Text my={2} textAlign="center" variant="textRegular">
          {' '}
          &#169; Copyright 2021, Advanta Seeds (Registered in India), All rights
          reserved.
        </Text>
      </Flex>

    </footer>

  );
};

const styles = StyleSheet.create({
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    }
  },
  socialMediaLinks: {
    fontSize: '1.75rem',
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    },
    color: colors.black
  },
  divider: {
    marginLeft: '5px',
    marginRight: '5px'
  }
});
export default MobileFooter;
