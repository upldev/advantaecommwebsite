import {
  Alert,
  AlertIcon,
  Button,
  Flex,
  Input,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalOverlay,
  Stack
} from '@chakra-ui/react';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import usePincodeServiceability from '../../../hooks/usePincodeServiceability';

const PinModal = ({
  isOpen, onClose, currPincode, setCurrPincode
}) => {
  const user = useSelector((state) => state.user);

  const { checkPincode, isLoading } = usePincodeServiceability();

  const handleCheck = async () => {
    await checkPincode(currPincode);
  };

  useEffect(() => {
    (async () => {
      if (
        (typeof currPincode === 'string' && currPincode.length === 6)
        || currPincode.toString().length === 6
      ) {
        await checkPincode(currPincode);
      }
    })();
  }, [checkPincode, currPincode]);

  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      justifyContent="center"
      p={5}
      size="2xl"
    >
      <ModalOverlay />
      <ModalContent justifyContent="center" mr={5} ml={5}>
        <ModalHeader justifyContent="center" mt={5}>
          <Flex justifyContent="center" mr={3} ml={3}>
            Select your city to start shopping
          </Flex>
        </ModalHeader>
        <ModalBody m={3}>
          <Flex width="100%" flexDirection="column">
            <Stack width="100%">
              <Flex mb={3}>
                <Input
                  flex={1}
                  name="pincode"
                  value={currPincode}
                  onChange={(e) => setCurrPincode(e.target.value)}
                  placeholder="Enter your area pincode"
                  type="number"
                />
              </Flex>
              <Flex m={5} mt="0">
                <Button
                  size="sm"
                  flex={1}
                  onClick={handleCheck}
                  variant="primary"
                  pt={5}
                  pb={5}
                  mr={5}
                  disabled={String(currPincode).length !== 6}
                  isLoading={isLoading}
                >
                  Continue
                </Button>
                <Button
                  size="sm"
                  flex={1}
                  variant="outline_secondary"
                  pt={5}
                  pb={5}
                  onClick={onClose}
                >
                  Close
                </Button>
              </Flex>
              {user
                && user.profile
                && user.profile.billing
                && currPincode === user?.profile?.billing?.pincode
                && user.logInStatus
                && user.hasProfile
                && user.userServiceable !== null
                && (user.userServiceable?.stat ? (
                  <Flex>
                    <Alert ml={0} my={3} mr={3} status="success">
                      <AlertIcon />
                      {user.userServiceable.message}
                    </Alert>
                  </Flex>
                ) : (
                  <Flex>
                    <Alert ml={0} my={3} mr={3} status="error">
                      <AlertIcon />
                      {user.userServiceable.message}
                    </Alert>
                  </Flex>
                ))}
              {String(currPincode).length === 6
                && currPincode !== user.profile?.billing?.pincode
                && user.check !== null
                && user.check?.pincode === currPincode
                && (user.check.servicable ? (
                  <Flex mr={3}>
                    <Alert ml={0} my={3} status="success">
                      <AlertIcon />
                      Serviceable in the entered pin code (
                      {currPincode}
                      ).
                    </Alert>
                  </Flex>
                ) : (
                  <Flex mr={3}>
                    <Alert ml={0} my={3} status="error">
                      <AlertIcon />
                      Unserviceable in the entered pin code (
                      {currPincode}
                      ).
                    </Alert>
                  </Flex>
                ))}
            </Stack>
          </Flex>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default PinModal;
