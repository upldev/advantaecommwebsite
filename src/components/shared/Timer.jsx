import {
  Button, Flex, Text
} from '@chakra-ui/react';
import { Auth } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { setAlert } from '../../../store/actions/alert';

const Timer = ({
  phoneNumber, fail, setCognitoUser, setFail
}) => {
  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const [time, setTime] = useState(60);

  useEffect(() => {
    // eslint-disable-next-line consistent-return
    const timerId = setInterval(() => {
      setTime((prevTime) => prevTime - 1);
      if (time === 0) {
        dispatch(setAlert({
          title: 'Timeout!',
          message: 'Please click on retry.',
          status: false,
          type: 'otp'
        }));
      }
    }, 1000);
    return () => clearInterval(timerId);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleRetry = () => {
    Auth.signIn(`+91${phoneNumber}`)
      .then((user) => { setTime(60); setCognitoUser(user); setFail(5); })
      .catch((err) => {
        console.error(err);
        dispatch(setAlert({
          title: 'Error Occured!',
          message: 'Please click on retry.',
          status: false,
          type: 'otp'
        }));
      });
  };

  return (
    <Flex width="full" justifyContent="center" p={3}>
      {
        time < 0 || fail === 0
          ? (
            <Flex m={isLg ? 1 : 0}>
              <Button size={isLg ? 'md' : 'xs'} variant="link" marginY={1} onClick={handleRetry}>
                <Text fontSize={isLg ? 'sm' : 'xs'} p={2} color={colors.black}>
                  Resend OTP
                </Text>
              </Button>
            </Flex>
          )
          : (
            <Flex m={isLg ? 1 : 0}>
              <Text fontSize={isLg ? 'sm' : 'xs'} p={2}>
                Resend OTP in
                {' '}
                <span style={{ fontWeight: 'bold' }}>{`${(Math.floor(time / 60)).toString().padStart(2, '0')}:${(time % 60).toString().padStart(2, '0')}`}</span>
              </Text>
            </Flex>
          )
      }
    </Flex>
  );
};

export default Timer;
