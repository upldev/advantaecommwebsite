import {
  FormControl,
  FormErrorMessage,
  Input,
  InputGroup,
  InputLeftAddon
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const PhoneInput = () => {
  const { isLg } = useMediaQuery();
  const {
    touched, errors, values, handleBlur, handleChange
  } = useFormikContext();

  // const handleChange = (e) => {
  //   const re = /^[0-9\b]+$/;
  //   const { value } = e.target;

  //   if (
  //     value === ''
  //     || (re.test(value) && value?.length <= 10 && value?.length >= 0)
  //   ) {
  //     setFieldValue('phoneNumber', value);
  //   }
  // };

  return (
    <FormControl
      isInvalid={touched.phoneNumber && errors.phoneNumber}
      pb={6}
      width={isLg ? 'full' : '250px'}
      maxWidth="450px"
    >
      <InputGroup size={isLg ? 'md' : 'sm'} alignItems="center">
        <InputLeftAddon
          borderColor={colors.advantaBlue}
          borderRadius={0}
          color={colors.white}
          backgroundColor={colors.advantaBlue}
        >
          +91
        </InputLeftAddon>
        {/* <Input
          name="phoneNumber"
          value={values.phoneNumber}
          backgroundColor={colors.white}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Enter your Mobile Number"
        /> */}
        <Input
          size={isLg ? 'md' : 'sm'}
          name="phoneNumber"
          value={values.phoneNumber}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </InputGroup>
      <FormErrorMessage>{errors.phoneNumber}</FormErrorMessage>
    </FormControl>
  );
};

export default PhoneInput;
