/* eslint-disable react/no-children-prop */
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel, Select,
  SimpleGrid,
  Stack
} from '@chakra-ui/react';
import { Auth } from 'aws-amplify';
import { Form, Formik } from 'formik';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import uuid from 'uuid';
import * as Yup from 'yup';
import { stateObj } from '../../../constants/cityStates';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { setAlert } from '../../../store/actions/alert';
import { showLoading } from '../../../store/actions/feedback';
import { saveRegistered } from '../../../store/actions/user';
import UserProfileCircle from '../account/UserProfileCircle';
import AlertComponent from '../shared/Alert';
import Facebook from '../shared/Facebook';
import FormDropDown from '../shared/FormDropDown';
import FormInput from '../shared/FormInput';
import FormTextArea from '../shared/FormTextArea';
import Google from '../shared/Google';
import RegisterPhoneInput from './RegisterPhoneInput';
// Code Review Completed

const states = Object.keys(stateObj);

const registerSchema = Yup.object({
  name: Yup.string().required('Required').matches(/^[a-zA-Z0-9\s.\\]*$/, 'Cannot enter special characters like $, @..'),
  mobile: Yup.string()
    .required('Required')
    .length(10, 'phone number should be 10 digit'),
  email: Yup.string().required('Required').email('Invalid Email'),
  fb: Yup.string(),
  google: Yup.string(),
  address: Yup.string()
    .required('Required')
    .max(190, 'Address should not exceed 190 characters'),
  city: Yup.string().required('Required'),
  state: Yup.string().required('Required'),
  pincode: Yup.string()
    .matches(/(\d{6})/, 'Pincode should be a number')
    .length(6, 'Pincode should be six digit')
    .required('Required'),
});

const formState = {
  profileImg: '',
  name: '',
  mobile: '',
  email: '',
  fb: '',
  google: '',
  address: '',
  city: '',
  state: '',
  pincode: '',
};

const RegisterForm = ({
  setNewUser, setCognitoUser, setProfile, onOpen
}) => {
  const { isLg, isMd } = useMediaQuery();
  const dispatch = useDispatch();
  const [cities, setCities] = useState([]);

  const handleCity = (state) => {
    const selected = stateObj[state];
    setCities(() => selected.cities);
  };

  const handleRegister = async (values) => {
    dispatch(showLoading(true));
    const phone_number = `+91${values.mobile}`;
    try {
      try {
        await Auth.signUp({
          username: phone_number,
          password: Date.now().toString(),
        });
      } catch (error) {
        console.error(error);
        if (error.code === 'UsernameExistsException') {
          dispatch(
            setAlert({
              title: 'User Already Registered!',
              message: 'Please login in instead.',
              status: false,
              type: 'register',
            })
          );
        } else {
          dispatch(
            setAlert({
              title: 'Error!',
              message: 'Please try again.',
              status: false,
              type: 'register',
            })
          );
        }
        return;
      }
      const user = await Auth.signIn(phone_number);
      if (setNewUser) setNewUser(true);
      setCognitoUser(user);
      const {
        name,
        email,
        fb,
        google,
        address,
        state,
        city,
        pincode,
        profileImg,
      } = values;
      const profile = {
        profileImg,
        name,
        mobile: phone_number,
        email,
        fb,
        google,
        billing: {
          addressId: uuid.v4(),
          name,
          address,
          state,
          city,
          pincode: parseInt(pincode),
          isDefault: true,
        },
      };
      setProfile(profile);
      onOpen();
      dispatch(saveRegistered(profile));
    } catch (err) {
      console.error(err);
      if (err.code === 'UsernameExistsException') {
        dispatch(
          setAlert({
            title: 'User Already Registered!',
            message: 'Please login in instead.',
            status: false,
            type: 'register',
          })
        );
      } else {
        dispatch(
          setAlert({
            title: 'Error!',
            message: 'Please try again.',
            status: false,
            type: 'register',
          })
        );
      }
    } finally {
      dispatch(showLoading(false));
    }
  };

  return (
    <Flex justifyContent="center" alignItems="center" width={isMd && '100%'}>
      <Stack width={isMd && '100%'} px={isMd && 6}>
        <AlertComponent type="register" />
        <Formik
          initialValues={formState}
          validationSchema={registerSchema}
          onSubmit={handleRegister}
        >
          {(props) => (
            <Form>
              <Flex
                flexDir={isMd ? 'column' : 'row'}
                width="full"
                justifyContent="center"
                pb={3}
                alignItems="center"
              >
                <UserProfileCircle name="profileImg" />
                <Flex
                  mt={isMd && 3}
                  width="full"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Facebook />
                  <Google />
                </Flex>
              </Flex>
              <SimpleGrid
                columns={{ md: 1, lg: 3 }}
                spacingX="24px"
                spacingY={isMd && '10px'}
                width="full"
              >
                <FormInput
                  name="name"
                  label="Name"
                  mt={2}
                  backgroundColor={colors.white}
                  isRequired
                />
                <RegisterPhoneInput />
                <FormInput
                  mb={isLg ? 2 : 0}
                  name="email"
                  mt={2}
                  backgroundColor={colors.white}
                  label="Email"
                  isRequired
                />
              </SimpleGrid>
              <Box width="full" pt={isMd && '10px'}>
                <FormTextArea
                  name="address"
                  label="Address"
                  placeholder="Enter your Address"
                  isRequired
                  borderColor={colors.black}
                  borderRadius={0}
                  backgroundColor={colors.white}
                />
                <SimpleGrid
                  columns={{ md: 1, lg: 3 }}
                  spacingX="24px"
                  spacingY={isMd && '10px'}
                  mb={isLg ? '' : '10px'}
                  mt={isLg ? '' : '10px'}
                >
                  <FormControl
                    isRequired
                    isInvalid={props.touched.state && props.errors.state}
                    mb={isLg ? 2 : 0}
                  >
                    <FormLabel fontSize={isLg ? '18px' : '12px'}>
                      State
                    </FormLabel>
                    <Select
                      onChange={(e) => {
                        props.handleChange(e);
                        handleCity(e.target.value);
                      }}
                      placeholder="Select state"
                      onBlur={props.handleBlur}
                      name="state"
                      size={isLg ? 'md' : 'sm'}
                      backgroundColor={colors.lighterGray}
                    >
                      {states.map((option) => (
                        <option value={option} key={option}>
                          {option}
                        </option>
                      ))}
                    </Select>
                    <FormErrorMessage>{props.errors.state}</FormErrorMessage>
                  </FormControl>
                  <FormDropDown
                    mb={isLg ? 2 : 0}
                    name="city"
                    label="City"
                    placeholder="Select City"
                    options={cities}
                    backgroundColor={colors.lighterGray}
                    isRequired
                  />
                  <FormInput
                    name="pincode"
                    label="Pincode"
                    backgroundColor={colors.white}
                    isRequired
                  />
                </SimpleGrid>
              </Box>
              <Flex width="100%" justifyContent="space-around" pt={3}>
                <Button
                  size={isLg ? 'md' : 'sm'}
                  px="16"
                  variant="primary"
                  type="submit"
                  onClick={props.handleSubmit}
                >
                  Get OTP
                </Button>
              </Flex>
            </Form>
          )}
        </Formik>
      </Stack>
    </Flex>
  );
};

export default RegisterForm;
