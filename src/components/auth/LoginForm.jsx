/* eslint-disable react/no-children-prop */
import {
  Button, Divider, Flex, Stack, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Auth } from 'aws-amplify';
import { Formik } from 'formik';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { colors } from '../../../constants/colors';
import { MOBILE_QUERY } from '../../../constants/mediaQueries';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  PRIVACY_POLICY,
  REGISTER,
  TERMS_AND_CONDITION
} from '../../../lib/router';
import { setAlert } from '../../../store/actions/alert';
import { showLoading } from '../../../store/actions/feedback';
import AlertComponent from '../shared/Alert';
import PhoneInput from './PhoneInput';

// Code Review Completed

const validateLogin = Yup.object({
  phoneNumber: Yup.string()
    .required('Required')
    .matches(/(\d{10})/, 'Phone number should be a 10 digit number')
    .length(10, 'phone number should be a 10 digit number'),
});

const LoginForm = ({ setCognitoUser, onOpen }) => {
  const { isLg, isSm } = useMediaQuery();
  const [token, setToken] = useState('');
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLogin = (values) => {
    dispatch(showLoading(true));
    const phone_number = `+91${values.phoneNumber}`;
    Auth.signIn(phone_number)
      .then((user) => {
        setCognitoUser(user);
        onOpen();
      })
      .catch((err) => {
        console.error(err);
        if (err.code === 'UserNotFoundException') {
          dispatch(
            setAlert({
              title: 'User Not Registered!',
              message: 'Please register first to login.',
              status: false,
              type: 'login',
            })
          );
        } else {
          dispatch(
            setAlert({
              title: 'Error!',
              message: 'Please try again.',
              status: false,
              type: 'login',
            })
          );
        }
      })
      .finally(() => dispatch(showLoading(false)));
  };
  return (
    <Flex justifyContent="center" ml={isSm ? 0 : 6}>
      <Stack className={css(styles.loginContainer)}>
        <Text
          fontSize={isSm ? 'lg' : 'sm'}
          mb="1rem"
          fontWeight={isSm && 'bold'}
        >
          {isSm ? 'LOG IN TO YOUR ACCOUNT' : 'Login to your Account'}
        </Text>
        <AlertComponent type="login" />
        <Formik
          initialValues={{ phoneNumber: '' }}
          validationSchema={validateLogin}
          onSubmit={handleLogin}
        >
          {(props) => (
            <>
              <PhoneInput />
              <Button
                size={isLg ? 'md' : 'sm'}
                variant="primary"
                type="submit"
                onClick={props.handleSubmit}
                paddingX={12}
              >
                LOGIN
              </Button>
            </>
          )}
        </Formik>
        <Flex width="full" paddingY={6} maxWidth="450px">
          <Divider color={colors.black} mt={3} />
          <Text flexShrink={0} paddingX={3}>
            Or Register
          </Text>
          <Divider color={colors.black} mt={3} />
        </Flex>
        <Button
          variant="primary"
          size={isLg ? 'md' : 'sm'}
          paddingX={9}
          onClick={() => router.push(REGISTER)}
        >
          REGISTER
        </Button>
        <Text variant="textRegular" pt={12} textAlign="center">
          By continuing, I accept Advanta
          {' '}
          <a className={css(styles.link)} href={TERMS_AND_CONDITION}>
            Terms & Conditions
          </a>
          {' '}
          and
          {' '}
          <a className={css(styles.link)} href={PRIVACY_POLICY}>
            Privacy Policy
          </a>
          .
        </Text>
      </Stack>
    </Flex>
  );
};

const styles = StyleSheet.create({
  loginContainer: {
    width: '500px',
    alignItems: 'center',
    [MOBILE_QUERY]: {
      width: '100%',
      margin: '5px',
    },
  },
  link: {
    color: colors.lightBlue,
    flexShrink: 0,
  },
});

export default LoginForm;
