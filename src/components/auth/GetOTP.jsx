import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Box,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
  PinInput,
  PinInputField,
  Text,
  VStack
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Auth } from 'aws-amplify';
import { Form, Formik } from 'formik';
import md5 from 'md5';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { IoPhonePortraitOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { colors } from '../../../constants/colors';
import { MOBILE_QUERY } from '../../../constants/mediaQueries';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { HOME, PRIVACY_POLICY, TERMS_AND_CONDITION } from '../../../lib/router';
import { setAlert } from '../../../store/actions/alert';
import {
  showLoading,
  showToast
} from '../../../store/actions/feedback';
import {
  createUserProfile,
  userLogIn,
  USER_LOGIN
} from '../../../store/actions/user';
import AlertComponent from '../shared/Alert';
import Timer from '../shared/Timer';

const validateCode = Yup.object({
  code: Yup.string()
    .required('OTP is required')
    .matches(/(\d{6})/, 'Phone number should be a digit')
    .length(6, 'Code should be six digit'),
});

// Code Review Completed

const GetOTP = ({
  isNewUser,
  cognitoUser,
  profile,
  isOpen,
  onClose,
  setCognitoUser,
}) => {
  const { isSm, isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const [retry, setRetry] = useState(false);
  const phoneNumber = cognitoUser ? cognitoUser.username.substring(3) : '';
  const [fail, setFail] = useState(5);
  const isLoading = useSelector((state) => state.feedback.isLoading);
  const router = useRouter();
  const { query } = router;
  const { redirectBackTo } = query;
  const { registeredProfile } = useSelector((state) => state.session);

  const handleVerify = async (values, props) => {
    try {
      if (isNewUser) {
        dispatch(
          createUserProfile({
            cognitoUser,
            profile,
            code: values.code,
            redirect: redirectBackTo || HOME,
            isRegister: true,
            onClose,
          })
        );
        return;
      }
      dispatch(showLoading(true));
      await Auth.sendCustomChallengeAnswer(cognitoUser, md5(values.code));
      await Auth.currentSession();
      onClose();
      if (registeredProfile) {
        // dispatch(setLoader({ isRegistering: true }));
        dispatch(
          createUserProfile({
            profile: registeredProfile,
            isRegister: false,
            redirect: redirectBackTo || HOME,
          })
        );
        // return;
      }
      dispatch(
        showToast({
          title: 'User Logged In!',
          status: true,
        })
      );
      router.push(redirectBackTo || HOME);
      dispatch({ type: USER_LOGIN, payload: { logInStatus: true } });
      dispatch(userLogIn());
    } catch (err) {
      console.error(err);
      if (err.code === 'NotAuthorizedException' || err === 'No current user') {
        dispatch(
          setAlert({
            title: 'OTP entered is incorrect!',
            message: 'Please try again.',
            status: false,
            type: 'otp',
          })
        );
      } else {
        dispatch(
          setAlert({
            title: 'Error!',
            message: 'Please try again.',
            status: false,
            type: 'otp',
          })
        );
      }
    } finally {
      setFail((prev) => prev - 1);
      if (values.code.length === 6) {
        setRetry(true);
      } else {
        setRetry(false);
      }
      props.resetForm({});
      dispatch(showLoading(false));
    }
  };

  return (
    <Modal
      closeOnOverlayClick={false}
      isOpen={isOpen}
      onClose={onClose}
      size={isSm ? 'sm' : 'xl'}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalBody>
          <VStack paddingX={isSm ? 1 : 12} paddingY={3}>
            <Formik
              initialValues={{ code: '' }}
              validationSchema={validateCode}
              onSubmit={handleVerify}
              validateOnChange={false}
              validateOnBlur={false}
            >
              {(props) => (
                <Form>
                  <Flex
                    justifyContent="center"
                    alignItems="center"
                    p={3}
                    flexDir="column"
                  >
                    {isNewUser && (
                      <Box mx={isLg ? -12 : -3} mb={6}>
                        <Alert
                          status="info"
                          variant="subtle"
                          flexDirection="column"
                          alignItems="center"
                          justifyContent="center"
                          textAlign="center"
                        >
                          <AlertIcon box="40px" mr={0} />
                          <Box flex="1">
                            <AlertTitle
                              textAlign="center"
                              fontSize={isLg ? '16px' : '14px'}
                            >
                              Please don&apos;t refresh or close this tab!
                            </AlertTitle>
                            <AlertDescription
                              display="block"
                              textAlign="center"
                              fontSize={isLg ? '14px' : '12px'}
                            >
                              {isLg
                                ? 'Failing to enter OTP requires you to create profile again.'
                                : 'Failing to enter OTP requires you to create profile again.'}
                            </AlertDescription>
                          </Box>
                        </Alert>
                      </Box>
                    )}
                    <Box
                      padding="10px"
                      width="fit-content"
                      borderRadius="50%"
                      backgroundColor="gray.100"
                    >
                      <IoPhonePortraitOutline
                        fontSize="3rem"
                        color={colors.darkGreen}
                      />
                    </Box>
                  </Flex>
                  <Text
                    fontSize="lg"
                    p={3}
                    fontWeight="bold"
                    textAlign="center"
                  >
                    Verify with OTP
                  </Text>
                  <AlertComponent type="otp" />
                  <Flex flexDir="column" justifyContent="center">
                    <FormControl
                      isInvalid={props.touched.code && props.errors.code}
                    >
                      <Flex flexDir="column">
                        <Flex justifyContent="center">
                          <PinInput
                            name="code"
                            value={props.values.code}
                            onChange={(e) => {
                              props.setFieldValue('code', e);
                              props.setErrors({});
                            }}
                            otp
                            size={isSm ? 'sm' : 'md'}
                            placeholder=""
                          >
                            <PinInputField
                              className={css(styles.pin)}
                              autoFocus
                            />
                            <PinInputField className={css(styles.pin)} />
                            <PinInputField className={css(styles.pin)} />
                            <PinInputField className={css(styles.pin)} />
                            <PinInputField className={css(styles.pin)} />
                            <PinInputField className={css(styles.pin)} />
                          </PinInput>
                        </Flex>
                        {props.touched.code
                        && props.errors.code
                        && (retry ? props.values.code.length !== 0 : true) ? (
                          <FormErrorMessage>
                            {props.errors.code}
                          </FormErrorMessage>
                          ) : null}
                      </Flex>
                    </FormControl>
                  </Flex>
                  <Timer
                    phoneNumber={phoneNumber}
                    fail={fail}
                    setCognitoUser={setCognitoUser}
                    setFail={setFail}
                  />
                  <Flex width="full" justifyContent="center">
                    <Button
                      mt={2}
                      variant="primary"
                      onClick={() => {
                        props.handleSubmit();
                      }}
                      size={isSm ? 'sm' : 'md'}
                      isLoading={isLoading}
                      px={9}
                      type="submit"
                    >
                      SUBMIT
                    </Button>
                  </Flex>
                </Form>
              )}
            </Formik>
            <Text variant="textRegular" pt={12} textAlign="center">
              By continuing, I accept Advanta
              {' '}
              <a className={css(styles.link)} href={TERMS_AND_CONDITION}>
                Terms & Conditions
              </a>
              {' '}
              and
              {' '}
              <a className={css(styles.link)} href={PRIVACY_POLICY}>
                Privacy Policy
              </a>
              .
            </Text>
          </VStack>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

const styles = StyleSheet.create({
  loginContainer: {
    margin: '2rem',
    padding: '20px',
    width: '500px',
    alignItems: 'center',
    borderRadius: '10px',
    [MOBILE_QUERY]: {
      width: '100%',
      margin: '5px',
    },
  },
  pin: {
    borderRadius: 0,
    borderColor: colors.black,
    margin: '4px 12px 4px 0px',
  },
  link: {
    color: colors.lightBlue,
  },
});

export default GetOTP;
