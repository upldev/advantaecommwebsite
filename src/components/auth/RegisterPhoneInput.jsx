import {
  FormControl,
  FormLabel,
  InputGroup,
  InputLeftAddon
} from '@chakra-ui/react';
import { useFormikContext } from 'formik';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import FormInput from '../shared/FormInput';

const RegisterPhoneInput = () => {
  const { isLg } = useMediaQuery();
  const { touched, errors } = useFormikContext();

  return (
    <FormControl
      isInvalid={touched.mobile && errors.mobile}
      isRequired
      mb={isLg ? 2 : 0}
    >
      <FormLabel fontSize={isLg ? '18px' : '12px'}>Phone Number</FormLabel>
      <InputGroup size={isLg ? 'md' : 'sm'} alignItems="flex-start">
        <InputLeftAddon
          borderRadius={0}
          backgroundColor={colors.lighterGray}
          borderWidth={1}
          borderColor={colors.black}
          marginTop="8px"
        >
          +91
        </InputLeftAddon>
        <FormInput name="mobile" />
      </InputGroup>
    </FormControl>
  );
};

export default RegisterPhoneInput;
