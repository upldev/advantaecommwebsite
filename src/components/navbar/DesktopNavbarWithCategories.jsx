import { Flex, Stack, Text } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import Link from 'next/link';
import React from 'react';
import { colors } from '../../../constants/colors';
import {
  ABOUT_US, CONTACT_US, FAQS, HOME, OUR_SEEDS
} from '../../../lib/router';
import AllCategory from './AllCategory';
import CartActions from './CartNavActions';
import NavPinServiceability from './NavPinServiceability';
import SearchInput from './SearchInput';
import UserProfileNavActions from './UserProfileNavActions';
import WishlistActions from './WishlistNavActions';

// Code Review Complete

const DesktopNavbar = () => {

  /* useEffect(() => {
    window.addEventListener('beforeunload', () => {
      Auth.signOut({ global: true });
    });
  }, []); */

  return (
    <Stack direction="column" className={`${css(styles.parentcontainer)}`}>
      <Flex direction="column">
        <Flex
          direction="row"
          alignItems="center"
          className={`${css(styles.container)}`}
          justifyContent="space-between"
          spacing="5"
          px="100"
          py="2.5"
        >
          <Link href={HOME}>
            <img
              className={css(styles.logo)}
              src="/advanta2.png"
              alt="advanta-logo2"
            />
          </Link>
          <NavPinServiceability />
          <SearchInput />
          <UserProfileNavActions />
          <WishlistActions />
          <CartActions />
        </Flex>
      </Flex>
      <Flex
        direction="row"
        className={`${css(styles.secondcontainer)}`}
        justifyContent="space-between"
        alignItems="center"
        height="3.5rem"
        paddingX="100"
      >
        <AllCategory />
        <Link href={HOME}>
          <Text
            color={colors.white}
            fontWeight="bold"
            className={`${css(styles.navText)}`}
          >
            HOME
          </Text>
        </Link>
        <Link href={OUR_SEEDS}>
          <Text
            color={colors.white}
            fontWeight="bold"
            className={`${css(styles.navText)}`}
          >
            OUR SEEDS
          </Text>
        </Link>
        <Link href={ABOUT_US}>
          <Text
            color={colors.white}
            fontWeight="bold"
            className={`${css(styles.navText)}`}
          >
            ABOUT US
          </Text>
        </Link>
        <Link href={FAQS}>
          <Text
            color={colors.white}
            fontWeight="bold"
            className={`${css(styles.navText)}`}
          >
            FAQ
          </Text>
        </Link>
        <Link href={CONTACT_US}>
          <Text
            color={colors.white}
            fontWeight="bold"
            className={`${css(styles.navText)}`}
          >
            CONTACT US
          </Text>
        </Link>
      </Flex>
    </Stack>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'sticky',
    top: 0,
    backgroundColor: colors.white,
    borderBottomWidth: '1px',
    borderBottomColor: colors.light,
    zIndex: 13,
  },
  parentcontainer: {
    position: 'sticky',
    top: 0,
    zIndex: 11,
    paddingX: '100px',
    width: '100%'
  },
  secondcontainer: {
    position: 'sticky',
    backgroundColor: colors.darkBlue,
    top: 0,
    zIndex: 12,
    marginTop: 0,
  },
  thirdcontainer: {
    position: 'sticky',
    backgroundColor: colors.white,
    top: 0,
    bottom: 0,
    zIndex: 10,
    marginTop: 0,
  },
  fourthcontainer: {
    position: 'sticky',
    top: 0,
    bottom: 0,
    marginBottom: 0,
    paddingBottom: 0,
    backgroundColor: colors.white,
    borderBottomWidth: '1px',
    borderBottomColor: colors.light,
    zIndex: 13,
  },
  logo: {
    cursor: 'pointer',
    width: '8%',
    zIndex: 1,
  },
  navItem: {
    width: '15%',
    cursor: 'pointer',
    textAlign: 'center',
  },
  navText: {
    color: colors.white,
    ':hover': {
      borderBottomWidth: '2px',
      borderBottomColor: colors.light,
      textDecoration: 'none',
      cursor: 'pointer'
    },
  },
  icon: {
    borderRadius: '50%',
  },
});

export default DesktopNavbar;
