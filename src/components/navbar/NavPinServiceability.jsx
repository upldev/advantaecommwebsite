import { ChevronDownIcon } from '@chakra-ui/icons';
import {
  Flex,
  Icon, Text,
  useDisclosure
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React, { useState } from 'react';
import { IoLocationOutline } from 'react-icons/io5';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import PinModal from '../shared/PinModal';

// Code Review Complete

const NavPinServiceability = () => {
  const user = useSelector((state) => state.user);
  const [currPincode, setCurrPincode] = useState(
    user.logInStatus && user.hasProfile ? user.profile?.billing?.pincode : ''
  );
  const city = useSelector((state) => state.user?.check?.city);

  const { onOpen, isOpen, onClose } = useDisclosure();

  return (
    <>
      <Flex
        onClick={onOpen}
        className={css(styles.pinContainer)}
        alignItems="center"
      >
        <Icon
          color={colors.green}
          onClick={onOpen}
          variant="ghost"
          as={IoLocationOutline}
        />
        {currPincode && city ? (
          <Flex alignItems="center">
            <Text
              fontSize={city.length > 7 ? 'x-small' : 'sm'}
              // mt={1}
              p="1"
            >
              {city}
            </Text>
            <Text
              fontSize="sm"
              // mt="1"
              p="1"
            >
              {currPincode}
            </Text>
          </Flex>
        ) : (
          <Text fontSize="smaller">Enter your pincode</Text>
        )}
        <ChevronDownIcon boxSize="1.5em" />
      </Flex>

      <PinModal
        isOpen={isOpen}
        onClose={onClose}
        currPincode={currPincode}
        setCurrPincode={setCurrPincode}
      />

    </>
  );
};

const styles = StyleSheet.create({
  pinContainer: {
    borderBottomWidth: '1px',
    borderBottomColor: colors.black,
    width: '15%',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '40px',
    cursor: 'pointer',
  },
  city: {
    color: colors.gray,
  },
});

export default NavPinServiceability;
