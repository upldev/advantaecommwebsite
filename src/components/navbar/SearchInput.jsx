import Icon from '@chakra-ui/icon';
import { Flex } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import React from 'react';
import { IoSearchOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import Select, { components } from 'react-select';
import { colors } from '../../../constants/colors';
import { getServices } from '../../../store/actions/home';
import { searchProducts, setPhrase } from '../../../store/actions/search';
import { selectSearchedProducts, selectSearchPhrase } from '../../../store/selectors/search';

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <Icon color="white" as={IoSearchOutline} />
    </components.DropdownIndicator>
  );
};

const SearchInput = () => {
  const router = useRouter();

  const dispatch = useDispatch();

  const mappedProducts = useSelector(selectSearchedProducts);

  const searchPhrase = useSelector(selectSearchPhrase);

  const handleFilter = (searchWord) => {
    if (/^[a-zA-Z0-9\s.\\]*$/.test(searchWord) || searchWord === '') {
      // console.log(searchWord);
      dispatch(setPhrase(searchWord));
      dispatch(getServices(searchWord));
      dispatch(searchProducts());
    }
  };

  const handleClear = () => {
    dispatch(setPhrase(''));
  };

  const handleRedirect = ({ value }) => {
    router.push(value);
  };

  return (
    <Flex width="60%">
      <Select
        options={mappedProducts}
        className={css(styles.search)}
        placeholder="Search product name..."
        inputValue={searchPhrase}
        theme={(theme) => ({
          ...theme,
          borderRadius: 0,
        })}
        onInputChange={handleFilter}
        onChange={handleRedirect}
        components={{ DropdownIndicator }}
        styles={{
          indicatorsContainer: (props) => ({
            ...props,
            backgroundColor: colors.lightBlue,
            cursor: 'pointer'
          }),
          indicatorSeparator: (props) => ({
            ...props,
            backgroundColor: colors.lightBlue,
          }),
          option: (props) => ({
            ...props,
            cursor: 'pointer',
            ':hover': {
              color: colors.green,
              fontWeight: 'bold'
            }
          }),
        }}
      />
    </Flex>
  );
};

const styles = StyleSheet.create({
  search: {
    width: '100%',
    borderColor: colors.black,
    borderRadius: '1px',
    cursor: 'pointer',
  },
});

export default SearchInput;
