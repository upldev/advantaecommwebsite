import {
  Box, Flex, Icon, Popover, PopoverArrow, PopoverBody, PopoverContent, PopoverTrigger, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Auth } from 'aws-amplify';
import { useRouter } from 'next/router';
// import { Link } from 'next/link';
import React, { useEffect } from 'react';
import { IoLogInOutline, IoPersonOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import {
  CREATE_USER_PROFILE,
  CREDITS, FAQS, LOGIN, ORDER_HISTORY, USER_ADDRESS, USER_PROFILE
} from '../../../lib/router';
import {
  sessionActivated,
  sessionExpired,
  setUserProfile,
  userLogout
} from '../../../store/actions/user';

// Code Review Complete

const UserProfileNavActions = () => {
  const user = useSelector((state) => state.user);
  const session = useSelector((state) => state.session.activeSession);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    async function checkUser() {
      try {
        await Auth.currentAuthenticatedUser();
        if (!session) {
          dispatch(sessionActivated()); // update state to logged in
        }
      } catch (error) {
        if (session) dispatch(sessionExpired());
      }
    }
    const timerId = setInterval(() => {
      checkUser();
    }, 5000);
    return () => clearInterval(timerId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session]);

  useEffect(() => {
    if (session) dispatch(setUserProfile());
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      {user.logInStatus
      && (
      <Popover
        autoFocus={false}
        trigger="hover"
      >
        <PopoverTrigger>
          <Flex
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            className={css(styles.profileBox)}
          >
            <button
              type="button"
              className={css(styles.iconButton)}
              onClick={() => {
                const route = user.logInStatus ? USER_PROFILE : LOGIN;
                router.push(route);
              }}
            >
              <Icon
                variant="ghost"
                className={css(styles.icon)}
                style={{
                  cursor: 'pointer',
                }}
                boxSize="1.6rem"
                as={IoPersonOutline}
              />
            </button>
            <Text fontSize="x-small" cursor="pointer">
              Profile
            </Text>
          </Flex>
        </PopoverTrigger>
        <PopoverContent bgColor={colors.light} borderRadius="0" width="40">
          <PopoverArrow bgColor={colors.light} />
          <PopoverBody>
            <Flex flexDirection="column" p={2} width="100%">
              {
                user.hasProfile
                  ? (
                    <>
                      <Box as="button" onClick={() => router.push(USER_PROFILE)} width="full">
                        <Text
                          fontWeight="bold"
                          fontSize="sm"
                          mb={2}
                          className={css(styles.textStyles)}
                          textAlign="left"
                        >
                          Edit Profile
                        </Text>
                      </Box>
                      <Box as="button" onClick={() => router.push(ORDER_HISTORY)} width="full">
                        <Text
                          fontSize="sm"
                          mb={2}
                          className={css(styles.textStyles)}
                          textAlign="left"
                        >
                          Order History
                        </Text>
                      </Box>
                      <Box as="button" onClick={() => router.push(USER_ADDRESS)} width="full">
                        <Text
                          fontSize="sm"
                          mb={2}
                          className={css(styles.textStyles)}
                          textAlign="left"
                        >
                          Manage Address
                        </Text>
                      </Box>
                      <Box as="button" onClick={() => router.push(CREDITS)} width="full">
                        <Text
                          fontSize="sm"
                          mb={2}
                          className={css(styles.textStyles)}
                          textAlign="left"
                        >
                          Referral
                        </Text>
                      </Box>
                    </>
                  ) : (
                    <Box as="button" onClick={() => router.push(CREATE_USER_PROFILE)} width="full">
                      <Text
                        fontSize="sm"
                        mb={2}
                        className={css(styles.textStyles)}
                        textAlign="left"
                      >
                        Create Profile
                      </Text>
                    </Box>
                  )
              }
              <Box as="button" onClick={() => router.push(FAQS)} width="full">
                <Text
                  fontSize="sm"
                  mb={2}
                  className={css(styles.textStyles)}
                  textAlign="left"
                >
                  FAQ&apos;s
                </Text>
              </Box>
              <Box
                as="button"
                onClick={() => {
                  if (user.logInStatus) dispatch(userLogout());
                  else router.push(LOGIN);
                }}
                width="full"
              >
                <Text
                  fontSize="sm"
                  fontWeight="bold"
                  color={colors.lightRed}
                  _hover={{
                    cursor: 'pointer',
                    color: colors.advantaBlue
                  }}
                  textAlign="left"
                >
                  { user.logInStatus ? 'Logout' : 'Log In' }
                </Text>
              </Box>
            </Flex>
          </PopoverBody>
        </PopoverContent>
      </Popover>
      )}
      {!user.logInStatus
      && (
      <Box
        as="button"
        mt="0.5"
        onClick={() => {
          router.push(LOGIN);
        }}
        className={css(styles.profileBox)}
      >
        <Flex flexDirection="column">
          <Icon
            variant="ghost"
            className={css(styles.icon)}
            style={{
              cursor: 'pointer'
            }}
            boxSize="1.7rem"
            as={IoLogInOutline}
          />
          <Text
            fontSize="x-small"
            color={colors.black}
            className={css(styles.textStyles)}
          >
            Login
          </Text>
        </Flex>
      </Box>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  icon: {
    borderRadius: '50%'
  },
  background: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    backgroundColor: colors.green,
    color: colors.white,
    textAlign: 'center',
    zIndex: 5,
    cursor: 'pointer'
  },
  iconButton: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '30px',
    height: '30px',
    border: 'none',
    outline: 'none',
    borderRadius: '50%'
  },
  iconButtonBadge: {
    position: 'absolute',
    top: '-5px',
    right: '-5px',
    width: '17px',
    height: '17px',
    background: colors.lightBlue,
    color: '#ffffff',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    fontSize: '10px'
  },
  profileBox: {
    ':hover': {
      color: colors.green
    }
  },
  textStyles: {
    ':hover': {
      color: colors.green,
      cursor: 'pointer'
    }
  }
});

export default UserProfileNavActions;
