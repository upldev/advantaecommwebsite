import {
  Accordion,
  AccordionButton, AccordionItem, Flex, IconButton, Image, Link, Text,
  useDisclosure
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React, { useRef } from 'react';
import { colors } from '../../../constants/colors';
import { USER_PROFILE } from '../../../lib/router';

const MobileProfile = () => {
  const btnRef = useRef();
  const { onClose } = useDisclosure();

  return (
    <Accordion allowToggle>
      <AccordionItem>
        <Link
          href={USER_PROFILE}
          _hover={{
            textDecoration: 'none'
          }}
        >
          <Flex>
            <IconButton
              variant="ghost"
              ref={btnRef}
              className={css(styles.drawerIcon)}
              icon={(
                <Image
                  style={{ width: 16, height: 30 * 0.6 }}
                  src="/icons/user.png"
                />
              )}
            />
            <AccordionButton
              className={css(styles.homeLink)}
              onClick={() => onClose()}
            >
              <Text
                textAlign="left"
              >
                Profile
              </Text>
            </AccordionButton>
          </Flex>
        </Link>
      </AccordionItem>
    </Accordion>
  );
};

const styles = StyleSheet.create({
  icon: {
    borderRadius: '50%',
    color: colors.green,
    ':hover': {
      color: colors.advantaBlue
    }
  },
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green,
      textDecoration: 'none'
    }
  },
  homeLink: {
    cursor: 'pointer',
    ':hover': {
      color: colors.advantaBlue,
    }
  },
  drawerIcon: {
    borderRadius: '50%',
    color: colors.black,
    ':hover': {
      color: colors.advantaBlue,
      backgroundColor: colors.white
    }
  },
});

export default MobileProfile;
