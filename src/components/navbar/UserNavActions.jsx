import {
  Accordion,
  AccordionButton,
  AccordionItem,
  Button,
  Divider,
  Drawer,
  DrawerBody,
  DrawerCloseButton,
  DrawerContent,
  DrawerFooter,
  // DrawerHeader,
  DrawerOverlay,
  Flex,
  Icon,
  IconButton,
  Image,
  Text,
  useDisclosure
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useRef } from 'react';
import {
  IoArrowBackOutline,
  IoCallOutline,
  IoLocationOutline,
  IoMenuOutline
} from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  CONTACT_US,
  CREDITS,
  FAQS,
  HOME,
  LOGIN,
  ORDER_HISTORY,
  REGISTER, USER_ADDRESS, USER_PROFILE
} from '../../../lib/router';
import { userLogout } from '../../../store/actions/user';
import ProfileImage from '../home/ProfileImage';
import MobileProfile from './MobileProfile';
import NavCropCategories from './NavCropCategories';

// Code Review Completed

const UserActions = () => {
  const router = useRouter();

  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const btnRef = useRef();
  const user = useSelector((state) => state.user);
  const pincode = useSelector((state) => state.user?.profile?.billing?.pincode);
  const city = useSelector((state) => state.user?.profile?.billing?.city);

  const logout = () => {
    dispatch(userLogout());
  };

  return (
    <>
      <Flex flexDirection="row" alignItems="center">
        {router.pathname !== HOME && (
          <IconButton
            variant="ghost"
            className={css(styles.icon)}
            icon={<Icon boxSize="1.25rem" as={IoArrowBackOutline} />}
            onClick={() => router.back()}
          />
        )}
        <IconButton
          variant="ghost"
          ref={btnRef}
          onClick={onOpen}
          className={css(styles.icon)}
          icon={<Icon boxSize="1.7rem" as={IoMenuOutline} />}
        />
      </Flex>
      <Drawer
        isOpen={isOpen}
        placement={isLg ? 'right' : 'left'}
        size={isLg ? 'xs' : 'sm'}
        onClose={onClose}
        finalFocusRef={btnRef}
      >
        <DrawerOverlay />
        <DrawerContent>
          <Flex
            alignItems="center"
            bgColor={colors.light}
            px="5"
            py="3"
            boxShadow="lg"
          >
            <Flex>
              <ProfileImage />
            </Flex>
            <Flex fontWeight="bold" ml={5}>
              {user.logInStatus ? ` ${user.profile?.name}` : ' Guest'}
            </Flex>
          </Flex>
          <DrawerCloseButton />
          <DrawerBody>
            {user.logInStatus && (
              <Flex px="2.5" py="3">
                <Icon
                  variant="ghost"
                  className={css(styles.drawerIcon)}
                  as={IoLocationOutline}
                  boxSize="1.3rem"
                  mr={1.5}
                />
                <Text ml={5}>
                  {pincode}
                  ,
                  {' '}
                  {city}
                </Text>
              </Flex>
            )}
            <Accordion allowToggle>
              <AccordionItem>
                <Link href={HOME}>
                  <Flex>
                    <IconButton
                      variant="ghost"
                      ref={btnRef}
                      className={css(styles.drawerIcon)}
                      icon={(
                        <Image
                          style={{ width: 20, height: 30 * 0.6 }}
                          src="/icons/home.png"
                        />
                      )}
                    />
                    <AccordionButton
                      className={css(styles.homeLink)}
                      onClick={() => onClose()}
                    >
                      <Text textAlign="left">Home</Text>
                    </AccordionButton>
                  </Flex>
                </Link>
              </AccordionItem>
              {user.logInStatus
                && (user.hasProfile ? (
                  <MobileProfile />
                ) : (
                  <AccordionItem>
                    <Flex>
                      <Link href={USER_PROFILE}>
                        <IconButton
                          variant="ghost"
                          ref={btnRef}
                          className={css(styles.drawerIcon)}
                          icon={(
                            <Image
                              style={{ width: 16, height: 30 * 0.6 }}
                              src="/icons/user.png"
                            />
                          )}
                        />
                        <AccordionButton
                          className={css(styles.homeLink)}
                          justifyContent="space-between"
                        >
                          <Text textAlign="left">Create Profile</Text>
                        </AccordionButton>
                      </Link>
                    </Flex>
                  </AccordionItem>
                ))}
              <NavCropCategories onClose={onClose} />
              {user.logInStatus && (
                <>
                  <AccordionItem>
                    <Link href={ORDER_HISTORY}>
                      <Flex>
                        <IconButton
                          variant="ghost"
                          ref={btnRef}
                          className={css(styles.drawerIcon)}
                          icon={(
                            <Image
                              style={{ width: 18, height: 30 * 0.6 }}
                              src="/icons/order-history.png"
                            />
                          )}
                        />
                        <AccordionButton
                          className={css(styles.homeLink)}
                          onClick={() => onClose()}
                        >
                          <Text textAlign="left">Order History</Text>
                        </AccordionButton>
                      </Flex>
                    </Link>
                  </AccordionItem>
                  <AccordionItem>
                    <Link href={USER_ADDRESS}>
                      <Flex>
                        <IconButton
                          variant="ghost"
                          ref={btnRef}
                          className={css(styles.drawerIcon)}
                          icon={(
                            <Image
                              style={{ width: 20, height: 30 * 0.6 }}
                              src="/icons/manage-address.png"
                            />
                          )}
                        />
                        <AccordionButton
                          className={css(styles.homeLink)}
                          onClick={() => onClose()}
                        >
                          <Text textAlign="left">Manage Address</Text>
                        </AccordionButton>
                      </Flex>
                    </Link>
                  </AccordionItem>
                </>
              )}
              {user.logInStatus && (
                <AccordionItem>
                  <Link href={CREDITS}>
                    <Flex>
                      <IconButton
                        variant="ghost"
                        ref={btnRef}
                        className={css(styles.drawerIcon)}
                        icon={(
                          <Image
                            style={{ width: 18, height: 30 * 0.6 }}
                            src="/icons/referrals.png"
                          />
                        )}
                      />
                      <AccordionButton
                        className={css(styles.homeLink)}
                        onClick={() => onClose()}
                      >
                        <Text textAlign="left">Referrals</Text>
                      </AccordionButton>
                    </Flex>
                  </Link>
                </AccordionItem>
              )}
              <AccordionItem>
                <Link href={FAQS}>
                  <Flex>
                    <IconButton
                      variant="ghost"
                      ref={btnRef}
                      className={css(styles.drawerIcon)}
                      icon={(
                        <Image
                          style={{ width: 20, height: 30 * 0.6 }}
                          src="/icons/faqs.png"
                        />
                      )}
                    />
                    <AccordionButton
                      className={css(styles.homeLink)}
                      onClick={() => onClose()}
                    >
                      <Text textAlign="left">FAQs</Text>
                    </AccordionButton>
                  </Flex>
                </Link>
              </AccordionItem>
              <AccordionItem>
                <Link href={CONTACT_US}>
                  <Flex>
                    <IconButton
                      variant="ghost"
                      ref={btnRef}
                      className={css(styles.drawerIcon)}
                      icon={(
                        <Icon
                          style={{ width: 20, height: 30 * 0.6 }}
                          as={IoCallOutline}
                        />
                      )}
                    />
                    <AccordionButton
                      className={css(styles.homeLink)}
                      onClick={() => onClose()}
                    >
                      <Text textAlign="left">Contact Us</Text>
                    </AccordionButton>
                  </Flex>
                </Link>
              </AccordionItem>
            </Accordion>
          </DrawerBody>
          {user.logInStatus ? (
            <DrawerFooter flexDirection="column" alignItems="center">
              <Button onClick={logout} width="100%" variant="primary">
                Logout
              </Button>
            </DrawerFooter>
          ) : (
            <DrawerFooter flexDirection="column" alignItems="center">
              <Link href={LOGIN}>
                <Button width="100%" variant="primary">
                  Login
                </Button>
              </Link>
              <Text mt="1" color="gray.400" fontSize="smaller">
                Please login to continue!
              </Text>
              <Divider mt="2" mb="2" color="gray.300" />
              <Flex
                width="100%"
                justifyContent="space-between"
                alignItems="center"
              >
                <Text>New customer?</Text>
                <Link href={REGISTER}>
                  <Button color={colors.darkGreen} variant="link">
                    Register
                  </Button>
                </Link>
              </Flex>
            </DrawerFooter>
          )}
        </DrawerContent>
      </Drawer>
    </>
  );
};

const styles = StyleSheet.create({
  icon: {
    borderRadius: '50%',
    color: colors.black,
  },
  drawerIcon: {
    borderRadius: '50%',
    color: colors.black,
    ':hover': {
      color: colors.advantaBlue,
      backgroundColor: colors.white,
    },
  },
  container: {
    position: 'sticky',
    top: 0,
  },
  linkStyles: {
    textDecoration: 'none',
    cursor: 'pointer',
    width: '100%',
    ':hover': {
      color: colors.advantaBlue,
    },
  },
  homeLink: {
    cursor: 'pointer',
    ':hover': {
      color: colors.advantaBlue,
    },
  },
  wishlistStyles: {
    cursor: 'pointer',
    ':hover': {
      color: colors.advantaBlue,
    },
  },
});

export default UserActions;
