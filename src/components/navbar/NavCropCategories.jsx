/* eslint-disable jsx-a11y/anchor-is-valid */
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Flex, IconButton, Image, Link,
  Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import router from 'next/router';
import React, { useRef } from 'react';
import { colors } from '../../../constants/colors';
import { CATEGORY } from '../../../lib/router';

const NavCropCategories = ({ onClose }) => {
  const btnRef = useRef();

  return (
    <>
      <Accordion allowToggle>
        <AccordionItem>
          <Flex>
            <IconButton
              variant="ghost"
              ref={btnRef}
              className={css(styles.icon)}
              icon={(
                <Image
                  style={{ width: 18, height: 30 * 0.6 }}
                  src="/icons/categories.png"
                />
              )}
            />
            <AccordionButton
              className={css(styles.homeLink)}
              justifyContent="space-between"
              // onClick={() => {
              //   router.push(CATEGORY);
              //   onClose();
              // }}
            >
              <Text textAlign="left">
                Categories
              </Text>
              <AccordionIcon />
            </AccordionButton>
          </Flex>

          <AccordionPanel pb={4}>
            <Flex flexDirection="column" mx="10">
              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Field Corn'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Field Corn
              </Link>

              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Rice'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Rice
              </Link>
              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Okra'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Okra
              </Link>
              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Tomato'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Tomato
              </Link>
              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Gourds'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Gourds
              </Link>
              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Cauliflower and Cabbage'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Cauliflower, Cabbage
              </Link>
              <Link
                onClick={() => { onClose(); router.push(`${CATEGORY}?type=${'Other Vegetables'}`, undefined, { shallow: true }); }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Other Vegetables
              </Link>
              <Link
                onClick={() => {
                  router.push(`${CATEGORY}?type=${'Forages'}`, undefined, { shallow: true });
                  onClose();
                }}
                className={css(styles.linkStyles)}
                my={2}
              >
                Forages
              </Link>
            </Flex>
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </>
  );
};

const styles = StyleSheet.create({
  icon: {
    borderRadius: '50%',
    color: colors.green,
    ':hover': {
      color: colors.advantaBlue
    }
  },
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    }
  },
  homeLink: {
    cursor: 'pointer',
    ':hover': {
      color: colors.advantaBlue
    }
  }
});

export default NavCropCategories;
