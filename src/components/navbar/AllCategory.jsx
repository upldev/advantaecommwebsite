/* eslint-disable jsx-a11y/anchor-is-valid */
import { ChevronDownIcon } from '@chakra-ui/icons';
import {
  Button, Icon, Link, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import router from 'next/router';
import React from 'react';
import { GiHamburgerMenu } from 'react-icons/gi';
import { colors } from '../../../constants/colors';
import { CATEGORY } from '../../../lib/router';

const AllCategory = () => {
  return (
    <div className={`dropdown ${css(styles.dropdown)}`}>
      <Button
        className="dropbtn"
        variant="primary"
        onClick={() => router.push(CATEGORY)}
        width="100%"
        leftIcon={<Icon as={GiHamburgerMenu} boxSize="1.4rem" />}
        rightIcon={<ChevronDownIcon boxSize="2rem" />}
        height="100%"
        overflow="hidden"
      >
        <Text
          className={css(styles.linkStyles)}
          fontWeight="bold"
          fontSize="lg"
        >
          ALL CATEGORY
        </Text>
      </Button>
      <div className="dropdown-content">
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Field Corn'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Field Corn
          </Text>
        </Link>
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Rice'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Rice
          </Text>
        </Link>
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Okra'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Okra
          </Text>
        </Link>
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Tomato'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Tomato
          </Text>
        </Link>
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Gourds'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Gourds
          </Text>
        </Link>
        <Link
          onClick={() => router.push(
            `${CATEGORY}?type=${'Cauliflower and Cabbage'}`,
            undefined,
            { shallow: true }
          )}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Cauliflower and Cabbage
          </Text>
        </Link>
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Other Vegetables'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Other Vegetables
          </Text>
        </Link>
        <Link
          onClick={() => router.push(`${CATEGORY}?type=${'Forages'}`, undefined, {
            shallow: true,
          })}
          className={css(styles.linkContent)}
        >
          <Text className={css(styles.linkContent)} fontSize="lg">
            Forages
          </Text>
        </Link>
      </div>
    </div>
  );
};

const styles = StyleSheet.create({
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.white,
    },
  },
  linkContent: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green,
    },
  },
  dropdown: {
    width: '25%',
    height: '100%',
  },
});

export default AllCategory;
