/* eslint-disable import/no-unresolved */
import {
  ButtonGroup, Flex, Icon, Image, Link
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
// import Link from 'next/link';
import { useRouter } from 'next/router';
import {
  // IoCartOutline,
  IoSearchOutline
} from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import { CATEGORY, HOME } from '../../../lib/router';
import CartActions from './CartNavActions';
// import SearchInput from './SearchInput';
import UserActions from './UserNavActions';
import WishlistActions from './WishlistNavActions';
// Code Review Completed

const MobileNavbar = () => {
  const router = useRouter();

  return (
    <>
      <Flex
        minWidth="100vw"
        height="10%"
        alignItems="center"
        justifyContent="center"
        className={`${css(styles.containerTop)} row`}
      >
        <Flex
          className="col-12 col-lg-4"
          justifyContent="space-between"
          alignItems="center"
          width="100vw"
          px="5"
        >
          <Flex>
            <UserActions />
            <Image
              objectFit="contain"
              className={css(styles.homeIcon)}
              width="80px"
              height="80px"
              src="/advanta2.png"
              onClick={() => router.push(HOME)}
              ml={3}
            />
          </Flex>
          <Flex>
            <ButtonGroup variant="ghost" alignItems="center" spacing="1rem">
              <Link href={CATEGORY} backgroundColor={colors.white}>
                <Icon
                  variant="ghost"
                  className={css(styles.icon)}
                  style={{
                    cursor: 'pointer',
                  }}
                  boxSize="1.6rem"
                  width="100%"
                  as={IoSearchOutline}
                />
              </Link>
              <WishlistActions />
              <CartActions />
            </ButtonGroup>
          </Flex>
        </Flex>
      </Flex>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.white,
    padding: '1 rem',
    borderTopWidth: '2.5px',
    borderTopColor: colors.light,
    zIndex: 10,
    width: '100vw',
    marginTop: 20,
  },
  navLinks: {
    cursor: 'pointer',
    color: colors.gray,
    fontSize: '1.25rem',
    ':hover': {
      color: colors.darkGreen,
    },
  },
  icon: {
    borderRadius: '0%',
    ':hover': {
      backgroundColor: colors.white,
      color: colors.green,
    },
  },
  homeIcon: {
    cursor: 'pointer',
  },
  containerTop: {
    position: 'sticky',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.white,
    zIndex: 10,
    width: '100vw',
    borderBottomColor: colors.light,
    borderBottomWidth: '2.5px',
    boxShadow: `0px 1px ${colors.dividerGray}`,
  },
  background: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    backgroundColor: colors.green,
    color: colors.white,
    textAlign: 'center',
    zIndex: 1,
    cursor: 'pointer',
  },
  iconButton: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '30px',
    height: '30px',
    border: 'none',
    outline: 'none',
    borderRadius: '50%',
    backgroundColor: colors.white,
  },
  iconButtonBadge: {
    position: 'absolute',
    top: '-3px',
    right: '-3px',
    width: '17px',
    height: '17px',
    background: colors.green,
    color: '#ffffff',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    fontSize: '11px',
  },
  logo: {
    cursor: 'pointer',
    width: '20%',
    zIndex: 1,
  },
});

export default MobileNavbar;
