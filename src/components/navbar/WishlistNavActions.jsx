import {
  Flex,
  Icon, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import Link from 'next/link';
import { IoHeartOutline } from 'react-icons/io5';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { WISHLIST } from '../../../lib/router';

// Code Review Complete

const WishlistActions = () => {
  const { isLg } = useMediaQuery();
  const getWishlistCount = useSelector((state) => state.wishlist.wishlistItems?.length);

  return (
    <>
      {isLg && (
      <Link href={WISHLIST}>
        <Flex
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          className={css(styles.wishlistBox)}
        >
          <button type="button" className={css(styles.iconButton)}>
            <Icon
              variant="ghost"
              className={css(styles.icon)}
              style={{
                cursor: 'pointer',
              }}
              boxSize="1.6rem"
              as={IoHeartOutline}
            />
            { getWishlistCount > 0
          && <span className={css(styles.iconButtonBadge)}>{ getWishlistCount }</span> }
          </button>
          <Text fontSize="x-small" cursor="pointer">
            Wishlist
          </Text>
        </Flex>
      </Link>
      )}
      {!isLg
      && (
      <Link href={WISHLIST}>
        <Flex
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          className={css(styles.wishlistBox)}
        >
          <button type="button" className={css(styles.iconButton)}>
            <Icon
              variant="ghost"
              className={css(styles.icon)}
              style={{
                cursor: 'pointer',

              }}
              boxSize="1.6rem"
              as={IoHeartOutline}
            />
            { getWishlistCount > 0
        && <span className={css(styles.iconButtonBadge)}>{ getWishlistCount }</span> }
          </button>
        </Flex>
      </Link>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  icon: {
    borderRadius: '50%'
  },
  background: {
    width: 20,
    height: 20,
    borderRadius: '50%',
    backgroundColor: colors.green,
    color: colors.white,
    textAlign: 'center',
    zIndex: 5,
    cursor: 'pointer'
  },
  iconButton: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '30px',
    height: '30px',
    border: 'none',
    outline: 'none',
    borderRadius: '50%'
  },
  iconButtonBadge: {
    position: 'absolute',
    top: '-5px',
    right: '-5px',
    width: '17px',
    height: '17px',
    background: colors.white,
    color: colors.black,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    fontSize: '10px',
    borderWidth: '2px',
    borderColor: colors.green
  },
  wishlistBox: {
    ':hover': {
      color: colors.green
    }
  }
});

export default WishlistActions;
