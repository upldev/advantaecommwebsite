import { Box, Flex, Text } from '@chakra-ui/react';
import React from 'react';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { resetFilters } from '../../../store/actions/filter';
import BrandTypeFilter from './BrandTypeFilter';
import FilterPinServiceability from './FilterPinServiceability';
import PriceRangeFilter from './PriceRangeFilter';
import SeedProductsFilter from './SeedProductsFilter';
import SeedTypesFilter from './SeedTypeFilter';

const Filter = () => {
  const dispatch = useDispatch();

  const handleReset = () => {
    dispatch(resetFilters());
  };

  const { isLg } = useMediaQuery();

  return (
    <Box width="25%" paddingRight="15px">
      <Box
        flexDirection="column"
        bgColor={colors.white}
        boxShadow="lg"
        height="-webkit-fit-content"
      >
        {isLg && <FilterPinServiceability />}
        {isLg && (
          <Flex
            width="100%"
            bgColor={colors.gray}
            mt={3}
            pt={3}
            pb={3}
            justifyContent="space-between"
          >
            <Flex>
              <Text fontWeight="bold" fontSize="sm" ml={5}>
                Filter By
              </Text>
            </Flex>
            <Flex>
              <Text
                fontWeight="bold"
                fontSize="sm"
                color={colors.lightRed}
                mr={3}
                borderBottomWidth="1px"
                borderBottomColor={colors.lightRed}
                cursor="pointer"
                onClick={handleReset}
              >
                Clear All
              </Text>
            </Flex>
          </Flex>
        )}
        <Flex width="100%" flexDirection="column">
          <Text m={isLg ? 5 : 0} my={3} fontWeight="bold" fontSize="lg">
            Seeds Product
          </Text>
          <Flex
            flexDirection="column"
            height={isLg ? '240' : 'unset'}
            overflowY={isLg ? 'scroll' : 'none'}
          >
            <SeedProductsFilter />
          </Flex>
        </Flex>
        <Flex flexDirection="column">
          <Text m={5} my={3} fontWeight="bold" fontSize="lg">
            Seeds Type
          </Text>
          <Flex width="100%" flexDirection="column">
            <SeedTypesFilter />
          </Flex>
        </Flex>
        <Flex flexDirection="column">
          <Text m={5} my={3} fontWeight="bold" fontSize="lg">
            Brand Type
          </Text>
          <Flex
            width="100%"
            flexDirection="column"
            height="240"
            overflowY="scroll"
          >
            <BrandTypeFilter />
          </Flex>
        </Flex>
        <Flex flexDirection="column" mb={isLg ? 0 : 5}>
          <Text m={isLg ? 5 : 0} my={3} fontWeight="bold" fontSize="lg">
            Price
          </Text>
          <PriceRangeFilter />
        </Flex>
      </Box>
    </Box>
  );
};

export default Filter;
