import {
  Drawer,
  DrawerContent,
  DrawerOverlay,
  Flex,
  Icon,
  Stack,
  Text,
  useDisclosure
} from '@chakra-ui/react';
import React, { useEffect } from 'react';
import { BsArrowUpDown } from 'react-icons/bs';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { sortBy } from '../../../constants/sortBy';
import { selectSort } from '../../../store/selectors/filter';
import MobileSortSelect from './MobileSortSelect';

const MobileSort = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const sort = useSelector(selectSort);

  useEffect(() => {
    onClose();
  }, [onClose, sort]);

  return (
    <>
      <Flex
        onClick={onOpen}
        bgColor={colors.white}
        fontWeight="bold"
        height="100%"
        alignItems="center"
        justifyContent="center"
        width="50vw"
      >
        <Icon mr="8px" as={BsArrowUpDown} />
        <Text>SORT</Text>
      </Flex>
      <Drawer isOpen={isOpen} placement="bottom" onClose={onClose}>
        <DrawerOverlay />
        <DrawerContent bgColor={colors.white} autoFocus={false}>
          <Stack>
            {sortBy.map(({ name, label }) => (
              <MobileSortSelect key={name} label={label} name={name} />
            ))}
          </Stack>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default MobileSort;
