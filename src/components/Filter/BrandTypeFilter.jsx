import { Box } from '@chakra-ui/layout';
import { useSelector } from 'react-redux';
import { selectBrandTypes } from '../../../store/selectors/filter';
import BrandTypeCheckbox from './BrandTypeCheckbox';

const BrandTypeFilter = () => {
  const brandTypes = useSelector(selectBrandTypes);

  return (
    <Box>
      {brandTypes?.map(({ name, quantity, isChecked }) => (
        <BrandTypeCheckbox
          key={name}
          name={name}
          quantity={quantity}
          isChecked={isChecked}
        />
      ))}
    </Box>
  );
};

export default BrandTypeFilter;
