import React from 'react';
import { useDispatch } from 'react-redux';
import { setSeedTypes } from '../../../store/actions/filter';
import FilterCheckbox from './FilterCheckbox';

const SeedTypeCheckbox = ({ name, quantity, isChecked }) => {
  const dispatch = useDispatch();

  const handleChange = () => {
    dispatch(setSeedTypes(name));
  };

  return (
    <FilterCheckbox
      name={name}
      handleChange={handleChange}
      isChecked={isChecked}
      quantity={quantity}
    />
  );
};

export default SeedTypeCheckbox;
