import { Box, Select } from '@chakra-ui/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { sortBy } from '../../../constants/sortBy';
import { setSortFilter } from '../../../store/actions/filter';
import { selectSort } from '../../../store/selectors/filter';

const SortBy = () => {
  const dispatch = useDispatch();

  const sort = useSelector(selectSort);

  const handleChange = (e) => {
    const id = e.target.value;
    dispatch(setSortFilter(id));
  };

  return (
    <Box>
      <Select
        placeholder="Default Sorting"
        value={sort}
        onChange={handleChange}
      >
        {sortBy.map(({ name, label }) => (
          <option key={name} value={name}>
            {label}
          </option>
        ))}
      </Select>
    </Box>
  );
};

export default SortBy;
