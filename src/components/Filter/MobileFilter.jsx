import { Flex } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import FilterDrawer from './FilterDrawer';
import MobileSort from './MobileSort';

const MobileFilter = () => {
  return (
    <Flex className={`${css(styles.container)} row`}>
      <MobileSort />
      <FilterDrawer />
    </Flex>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'sticky',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.white,
    boxShadow: `0 2px 5px ${colors.gray}`,
    zIndex: 10,
    width: '100vw',
    height: '10vh',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
});

export default MobileFilter;
