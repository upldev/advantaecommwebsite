import {
  Box, Center,
  Divider,
  Drawer,
  DrawerContent,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  Flex, Icon, Text,
  useDisclosure
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { FiFilter } from 'react-icons/fi';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import { resetFilters } from '../../../store/actions/filter';
import MobileFilterBody from './MobileFilterBody';

const FilterDrawer = () => {
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleReset = () => {
    onClose();
    dispatch(resetFilters());
  };

  return (
    <>
      <Flex
        onClick={onOpen}
        bgColor={colors.white}
        fontWeight="bold"
        height="100%"
        alignItems="center"
        justifyContent="center"
        width="50vw"
      >
        <Icon mr="8px" as={FiFilter} />
        Filter
      </Flex>
      <Drawer isOpen={isOpen} placement="left" onClose={onClose} size="full">
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader
            px="16px"
            boxShadow={`0 2px 5px ${colors.gray}`}
            zIndex={12}
          >
            <Flex alignItems="center" justifyContent="space-between">
              <Text>Filters</Text>
              <Text
                fontWeight="bold"
                fontSize="sm"
                color={colors.lightRed}
                cursor="pointer"
                onClick={handleReset}
              >
                Clear All
              </Text>
            </Flex>
          </DrawerHeader>
          <MobileFilterBody />
          <DrawerFooter className={`${css(styles.container)}`}>
            <Box className={css(styles.btn)} onClick={onClose}>
              CLOSE
            </Box>
            <Center height="80%">
              <Divider orientation="vertical" />
            </Center>
            <Box
              className={css(styles.btn)}
              onClick={onClose}
              color={colors.green}
            >
              APPLY
            </Box>
          </DrawerFooter>
        </DrawerContent>
      </Drawer>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'fixed',
    bottom: 0,
    backgroundColor: colors.white,
    zIndex: 10,
    width: '100vw',
    boxShadow: `0 2px 5px ${colors.gray}`,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  btn: {
    width: '48%',
    borderRadius: 0,
    backgroundColor: colors.white,
    justifyContent: 'center',
    textAlign: 'center',
    height: '100%'
  },
});

export default FilterDrawer;
