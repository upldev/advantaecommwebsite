import { Box } from '@chakra-ui/layout';
import { useSelector } from 'react-redux';
import { selectPriceRange } from '../../../store/selectors/filter';
import PriceRangeCheckbox from './PriceRange';

const PriceRangeFilter = () => {
  const priceRange = useSelector(selectPriceRange);

  return (
    <Box>
      {priceRange.map(({
        name, label, isChecked, quantity
      }) => (
        <PriceRangeCheckbox
          key={name}
          label={label}
          isChecked={isChecked}
          quantity={quantity}
          name={name}
        />
      ))}
    </Box>
  );
};

export default PriceRangeFilter;
