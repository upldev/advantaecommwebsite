import {
  Tab, TabList, TabPanel, TabPanels, Tabs
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React, { useState } from 'react';
import { colors } from '../../../constants/colors';
import BrandTypeFilter from './BrandTypeFilter';
import PriceRangeFilter from './PriceRangeFilter';
import SeedProductsFilter from './SeedProductsFilter';
import SeedTypesFilter from './SeedTypeFilter';

const MobileFilterBody = () => {
  const [index, setIndex] = useState(0);

  return (
    <Tabs
      orientation="vertical"
      variant="enclosed"
      width="100vw"
      height="100%"
      index={index}
      onChange={(idx) => setIndex(idx)}
    >
      <TabList width="50%" bgColor={colors.light}>
        <Tab
          className={css(styles.tabListStyles, index === 0 && styles.active)}
        >
          Seeds Product
        </Tab>
        <Tab
          className={css(styles.tabListStyles, index === 1 && styles.active)}
        >
          Seeds Type
        </Tab>
        <Tab
          className={css(styles.tabListStyles, index === 2 && styles.active)}
        >
          Brand Type
        </Tab>
        <Tab
          className={css(styles.tabListStyles, index === 3 && styles.active)}
        >
          Price
        </Tab>
      </TabList>
      <TabPanels>
        <TabPanel className={css(styles.panel)}>
          <SeedProductsFilter />
        </TabPanel>
        <TabPanel className={css(styles.panel)}>
          <SeedTypesFilter />
        </TabPanel>
        <TabPanel className={css(styles.panel)}>
          <BrandTypeFilter />
        </TabPanel>
        <TabPanel className={css(styles.panel)}>
          <PriceRangeFilter />
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
};

const styles = StyleSheet.create({
  tabListStyles: {
    borderBottomColor: colors.gray,
    borderBottomWidth: '1px',
    borderRadius: 0,
    color: colors.gray,
    textAlign: 'left',
    justifyContent: 'flex-start'
  },
  active: {
    color: colors.green,
    fontWeight: 'bold',
    backgroundColor: colors.white,
    boxShadow: `0 2px 5px ${colors.gray}`,
  },
  panel: {
    padding: '8px 0',
    overflowY: 'scroll'
  }
});

export default MobileFilterBody;
