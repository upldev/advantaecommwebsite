import { Box } from '@chakra-ui/layout';
import { useSelector } from 'react-redux';
import { selectSeedProducts } from '../../../store/selectors/filter';
import SeedProductsCheckbox from './SeedsProductCheckbox';

const SeedProductsFilter = () => {
  const seedProducts = useSelector(selectSeedProducts);

  return (
    <Box>
      {seedProducts?.map(({ name, quantity, isChecked }) => {
        return (
          <SeedProductsCheckbox
            key={name}
            name={name}
            quantity={quantity}
            isChecked={isChecked}
          />
        );
      })}
    </Box>
  );
};

export default SeedProductsFilter;
