import React from 'react';
import { useDispatch } from 'react-redux';
import { setSeedsProducts } from '../../../store/actions/filter';
import FilterCheckbox from './FilterCheckbox';

const SeedProductsCheckbox = ({ name, quantity, isChecked }) => {
  const dispatch = useDispatch();

  const handleChange = () => {
    dispatch(setSeedsProducts(name));
  };

  return (
    <FilterCheckbox
      name={name}
      handleChange={handleChange}
      isChecked={isChecked}
      quantity={quantity}
    />
  );
};

export default SeedProductsCheckbox;
