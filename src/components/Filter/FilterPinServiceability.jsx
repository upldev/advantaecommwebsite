import {
  Flex,
  Icon, Text,
  useDisclosure
} from '@chakra-ui/react';
import { StyleSheet } from 'aphrodite';
import React, { useState } from 'react';
import { BiPencil } from 'react-icons/bi';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import PinModal from '../shared/PinModal';

const FilterPinServiceability = () => {
  const user = useSelector((state) => state.user);
  const [currPincode, setCurrPincode] = useState(
    user.logInStatus && user.hasProfile ? user.profile.billing.pincode : ''
  );
  const city = useSelector((state) => state.user?.check?.city);

  const { onOpen, isOpen, onClose } = useDisclosure();

  return (
    <>
      <Flex onClick={onOpen} justifyContent="center" pt="1em" cursor="pointer">
        {/* <Text fontSize="sm" fontWeight="bold" width="80" ml={5}>
          Deliver to Pincode:
        </Text> */}
        <Flex
          mr={2}
          borderBottomWidth="1px"
          borderBottomColor={colors.black}
          alignItems="center"
        >
          {currPincode && city ? (
            //   <Input
            //     size="xs"
            //     justifyContent="right"
            //     alignContent="right"
            //     readOnly
            //     color={colors.black}
            //     border="0"
            //     value={currPincode}
            //     type="number"
            //   />
            // ) : (
            //   <Text fontSize="xs">Enter your pincode</Text>
            // )}
            <Flex>
              <Text fontSize="sm" mt={1} p="1">
                {city}
              </Text>
              <Text fontSize="sm" mt="1" p="1">
                {currPincode}
              </Text>
            </Flex>
          ) : (
            <Text>Enter your pincode</Text>
          )}
          <Icon
            color={colors.green}
            onClick={onOpen}
            variant="ghost"
            // mt={1}
            ml={1}
            as={BiPencil}
          />
        </Flex>
      </Flex>

      <PinModal
        isOpen={isOpen}
        onClose={onClose}
        currPincode={currPincode}
        setCurrPincode={setCurrPincode}
      />

    </>
  );
};

const styles = StyleSheet.create({
  pinContainer: {
    borderBottomWidth: '1px',
    borderBottomColor: colors.black,
    width: '15%',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '40px',
    cursor: 'pointer',
  },
  city: {
    color: colors.gray,
  },
});

export default FilterPinServiceability;
