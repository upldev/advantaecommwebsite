import { Flex, Text } from '@chakra-ui/layout';
import { css, StyleSheet } from 'aphrodite';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { setSortFilter } from '../../../store/actions/filter';
import { selectSort } from '../../../store/selectors/filter';

const MobileSortSelect = ({ name, label }) => {
  const dispatch = useDispatch();

  const sort = useSelector(selectSort);

  const handleChange = () => {
    dispatch(setSortFilter(name));
  };

  return (
    <Flex
      className={sort === name && css(styles.active)}
      p="8px 16px"
      onClick={handleChange}
      value="bestSellers"
    >
      <Text>{label}</Text>
    </Flex>
  );
};

const styles = StyleSheet.create({
  active: {
    backgroundColor: colors.green,
    color: colors.white,
    fontWeight: 'bold',
  },
});

export default MobileSortSelect;
