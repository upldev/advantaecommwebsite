import { Box } from '@chakra-ui/layout';
import { useSelector } from 'react-redux';
import { selectSeedTypes } from '../../../store/selectors/filter';
import SeedTypeCheckbox from './SeedsTypeCheckbox';

const SeedTypesFilter = () => {
  const seedTypes = useSelector(selectSeedTypes);

  return (
    <Box>
      {seedTypes?.map(({ name, quantity, isChecked }) => {
        return (
          <SeedTypeCheckbox
            key={name}
            name={name}
            quantity={quantity}
            isChecked={isChecked}
          />
        );
      })}
    </Box>
  );
};

export default SeedTypesFilter;
