import { Checkbox } from '@chakra-ui/checkbox';
import { Badge, Flex } from '@chakra-ui/layout';

const FilterCheckbox = ({
  isChecked, handleChange, name, quantity, label
}) => {
  return (
    <Flex
      width="100%"
      mb={5}
      px="16px"
      justifyContent="space-between"
      alignItems="center"
    >
      <Checkbox
        onChange={handleChange}
        isChecked={isChecked}
        width="100%"
        isDisabled={quantity === 0}
      >
        {label || name}
      </Checkbox>
      <Badge
        fontSize="md"
        ml={3}
        colorScheme={isChecked ? 'green' : 'blackAlpha'}
      >
        {quantity}
      </Badge>
    </Flex>
  );
};

export default FilterCheckbox;
