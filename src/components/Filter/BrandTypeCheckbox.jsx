import React from 'react';
import { useDispatch } from 'react-redux';
import { setBrandTypes } from '../../../store/actions/filter';
import FilterCheckbox from './FilterCheckbox';

const BrandTypeCheckbox = ({ name, quantity, isChecked }) => {
  const dispatch = useDispatch();

  const handleChange = () => {
    dispatch(setBrandTypes(name));
  };

  return (
    <FilterCheckbox
      name={name}
      handleChange={handleChange}
      isChecked={isChecked}
      quantity={quantity}
    />
  );
};

export default BrandTypeCheckbox;
