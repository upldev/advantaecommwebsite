import React from 'react';
import { useDispatch } from 'react-redux';
import { setPriceRange } from '../../../store/actions/filter';
import FilterCheckbox from './FilterCheckbox';

const PriceRangeCheckbox = ({
  name, quantity, isChecked, label
}) => {
  const dispatch = useDispatch();

  const handleChange = () => {
    dispatch(setPriceRange(name));
  };

  return (
    <FilterCheckbox
      handleChange={handleChange}
      name={name}
      quantity={quantity}
      isChecked={isChecked}
      label={label}
    />
  );
};

export default PriceRangeCheckbox;
