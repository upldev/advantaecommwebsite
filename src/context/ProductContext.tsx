import React, {
  createContext,
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useProductImages } from '../../hooks/useProductImages';
import { useSKU } from '../../hooks/useSKU';
import {
  addOnlyToWishlist,
  addToCart,
  decrementQuantity,
  incrementQuantity,
  removeFromCart,
  removeFromWishlist
} from '../../store/actions/cart';
import { selectSkuInCart } from '../../store/selectors/cart';
import { selectIsWishlisted } from '../../store/selectors/wishlist';
import { Product, SKU } from '../../types/Product';

interface ProductContextProps {
  children: FunctionComponent;
  product: Product;
}

interface ReduxState {
  feedback: any;
}

interface ProductHook {
  product: Product;
  handleAddToWishlist: Function;
  handleRemoveFromWishlist: Function;
  handleAddToCart: Function;
  handleIncrement: Function;
  handleDecrement: Function;
  handleRemoveFromCart: Function;
  handleCurrentSkuChange: Function;
  isCartUpdating: boolean;
  inCart: boolean;
  currentSku: SKU;
  inStock: boolean;
  isWishlisted: boolean;
}

export const Context = createContext(null);

export const ProductContext: FunctionComponent<ProductContextProps> = ({
  children,
  product,
}) => {
  const { sku, isLoading } = useSKU(product.materialCode);
  const urls = useProductImages(product.productImg);

  const [currentSku, setCurrentSku] = useState<SKU>(product.sku[0]);

  const dispatch = useDispatch();
  const { isCartUpdating } = useSelector((state: ReduxState) => state.feedback);
  const inCart = useSelector(selectSkuInCart(currentSku.ecomCode));
  const isWishlisted = useSelector(selectIsWishlisted(product.materialCode));

  useEffect(() => {
    if (sku.length > 0) {
      setCurrentSku(sku[0]);
    }
  }, [sku]);

  const handleCurrentSkuChange = useCallback((sku_: SKU) => {
    setCurrentSku(sku_);
  }, []);

  const handleAddToWishlist = useCallback(() => {
    dispatch(addOnlyToWishlist(product));
  }, [dispatch, product]);

  const handleRemoveFromWishlist = useCallback(() => {
    dispatch(removeFromWishlist(product.materialCode));
  }, [dispatch, product.materialCode]);

  const handleAddToCart = useCallback(
    (sku_: SKU) => {
      dispatch(addToCart(product, 1, sku_));
    },
    [dispatch, product]
  );

  const handleIncrement = useCallback(
    (sku_: SKU) => {
      dispatch(incrementQuantity(sku_.ecomCode));
    },
    [dispatch]
  );

  const handleDecrement = useCallback(
    (sku_: SKU) => {
      dispatch(decrementQuantity(sku_.ecomCode));
    },
    [dispatch]
  );

  const handleRemoveFromCart = useCallback(
    (sku_) => {
      dispatch(removeFromCart(sku_.ecomCode));
    },
    [dispatch]
  );

  return (
    <Context.Provider
      value={{
        product: {
          ...product,
          sku: sku.length < product.sku.length ? product.sku : sku,
          productImg: urls || [],
        },
        handleAddToWishlist,
        handleRemoveFromWishlist,
        handleAddToCart,
        handleIncrement,
        handleDecrement,
        handleRemoveFromCart,
        handleCurrentSkuChange,
        isCartUpdating,
        inCart,
        currentSku,
        inStock: currentSku.qtyAvailable > 0,
        isWishlisted,
      }}
    >
      {!isLoading && sku.length > 0 && children}
    </Context.Provider>
  );
};

export const useProductContext = () => {
  return useContext<ProductHook>(Context);
};
