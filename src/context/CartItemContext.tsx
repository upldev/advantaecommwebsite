import React, {
  createContext,
  FunctionComponent,
  useCallback,
  useContext
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useIndianCurrencyFormatter } from '../../hooks/useIndianCurrencyFormatter';
import { useProductImages } from '../../hooks/useProductImages';
import {
  addToWishlist,
  decrementQuantity,
  incrementQuantity,
  removeFromCart
} from '../../store/actions/cart';
import { selectItemQuantity } from '../../store/selectors/cart';
import { selectIsWishlisted } from '../../store/selectors/wishlist';
import { CartItemProduct } from '../../types/CartItemProduct';

interface CartContextProps {
  children: FunctionComponent;
  product: CartItemProduct;
  isCartCheckout: boolean;
}

interface ReduxState {
  feedback: any;
}

interface CartItemHook {
  product: CartItemProduct;
  handleAddToWishlist: Function;
  handleIncrement: Function;
  handleDecrement: Function;
  handleRemoveFromCart: Function;
  isCartUpdating: boolean;
  inStock: boolean;
  isWishlisted: boolean;
  price: number;
  isCartCheckout: boolean;
}

export const Context = createContext(null);

export const CartItemContext: FunctionComponent<CartContextProps> = ({
  children,
  product,
  isCartCheckout,
}) => {
  const urls = useProductImages(product.productImg);
  const dispatch = useDispatch();
  const { isCartUpdating } = useSelector((state: ReduxState) => state.feedback);
  const isWishlisted = useSelector(selectIsWishlisted(product.materialCode));
  const qty = useSelector(selectItemQuantity(product.sku.ecomCode));
  const price = useIndianCurrencyFormatter(product.sku.ecomPrice * qty);

  const handleAddToWishlist = useCallback(() => {
    dispatch(addToWishlist(product));
  }, [dispatch, product]);

  const handleIncrement = useCallback(() => {
    dispatch(incrementQuantity(product.sku.ecomCode));
  }, [dispatch, product.sku.ecomCode]);

  const handleDecrement = useCallback(() => {
    dispatch(decrementQuantity(product.sku.ecomCode));
  }, [dispatch, product.sku.ecomCode]);

  const handleRemoveFromCart = useCallback(() => {
    dispatch(removeFromCart(product.sku.ecomCode));
  }, [dispatch, product.sku.ecomCode]);

  return (
    <Context.Provider
      value={{
        product: { ...product, qty, productImg: urls },
        handleAddToWishlist,
        handleIncrement,
        handleDecrement,
        handleRemoveFromCart,
        isCartUpdating,
        inStock: product.sku.qtyAvailable > 0,
        isWishlisted,
        price,
        isCartCheckout,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useCartItemContext = () => {
  return useContext<CartItemHook>(Context);
};
