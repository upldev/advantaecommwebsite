import {
  Button, Flex
} from '@chakra-ui/react';
import React, { useState } from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import Description from '../../components/product/Description';
import Review from '../../components/product/Review';

const PDPMiddleSection = ({ product }) => {
  const { isLg } = useMediaQuery();
  const sections = ['DESCRIPTION', 'REVIEW'];
  const [currentSection, setCurrentSection] = useState(0);
  return (
    <Flex mb={5} width="100%" flexDir="column" backgroundColor={colors.white}>
      <Flex justifyContent={!isLg && 'center'} my={5} mx={isLg ? 100 : 5}>
        { sections.map((section, index) => (
          <Button size={isLg ? 'md' : 'sm'} mr={5} id={index} onClick={(e) => setCurrentSection(parseInt(e.target.id))} key={section} variant={index === currentSection ? 'secondary' : 'outline_secondary'}>{section}</Button>
        )) }
      </Flex>
      {currentSection === 0 && <Description product={product} /> }
      {currentSection === 1 && <Review product={product} /> }
    </Flex>
  );
};

export default PDPMiddleSection;
