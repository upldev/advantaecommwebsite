import { Flex } from '@chakra-ui/layout';
import React, { useEffect, useState } from 'react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ProductImage from './ProductImage';
import ProductImageMobile from './ProductImageMobile';

const ProductCarousel = ({ product }) => {
  const { isLg } = useMediaQuery();
  const [productImageCarouselData, setProductImageCarouselData] = useState([]);

  useEffect(() => {
    const { productImg, productVideo, testimonialVideo } = product;
    const temp = productImg.map((image) => ({
      data: image,
      isImage: true,
      id: image,
    }));
    if (productVideo !== null && productVideo !== '') {
      temp.push({ data: productVideo, isImage: false, id: productVideo });
    }
    if (testimonialVideo !== null && testimonialVideo !== '') {
      temp.push({
        data: testimonialVideo,
        isImage: false,
        id: testimonialVideo,
      });
    }
    setProductImageCarouselData(temp);
  }, [product]);

  return (
    <Flex>
      {isLg && productImageCarouselData.length > 0 && (
        <ProductImage productImageCarouselData={productImageCarouselData} />
      )}

      {!isLg && productImageCarouselData.length > 0 && (
        <ProductImageMobile
          product={product}
          productImageCarouselData={productImageCarouselData}
        />
      )}
    </Flex>
  );
};

export default ProductCarousel;
