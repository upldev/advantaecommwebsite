import { useSelector } from 'react-redux';
import Section from '../../components/home/Section';
import ProductsCarousel from '../../components/shared/ProductsCarousel';

const RecommendedProducts = () => {
  const similarProducts = useSelector(
    (state) => state.product.productDetails.similarProducts
  );

  return similarProducts && similarProducts.length > 0 ? (
    <Section
      title="SIMILAR PRODUCTS"
    >
      <ProductsCarousel name="similar-products" products={similarProducts} />
    </Section>
  ) : null;
};

export default RecommendedProducts;
