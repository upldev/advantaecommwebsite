/* eslint-disable max-len */
import { AspectRatio, Flex } from '@chakra-ui/react';
import { StyleSheet } from 'aphrodite';
import React, { useEffect, useState } from 'react';
import Zoom from 'react-img-zoom';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ProductImageCarouselBody from '../../components/product/ProductImageCarouselBody';

const ProductImage = ({ productImageCarouselData }) => {
  const { isLg } = useMediaQuery();
  const [currShow, setCurrShow] = useState(productImageCarouselData[0].id);
  useEffect(() => {
    setCurrShow(productImageCarouselData[0].id);
  }, [productImageCarouselData]);
  const currShownImage = productImageCarouselData.filter(
    (item) => item.id === currShow
  );

  return (
    <Flex flexDirection="column" width="100%">
      {currShownImage[0]?.isImage ? (
        <Flex
          cursor="zoom-in"
          backgroundColor={colors.white}
          width="100%"
          justifyContent="center"
          padding="40px"
        >
          <Zoom
            img={currShownImage[0]?.data}
            zoomScale={3}
            width={475}
            height={400}
          />
        </Flex>
      ) : (
        <AspectRatio height="60vh" width={isLg ? '100%' : '50vw'}>
          <iframe
            title="video"
            src={currShownImage[0]?.data.replace('watch?v=', 'embed/')}
          />
        </AspectRatio>
      )}
      {productImageCarouselData.length > 1 && (
        <ProductImageCarouselBody
          currShow={currShow}
          setCurrShow={setCurrShow}
          productImageCarouselData={productImageCarouselData}
        />
      )}
    </Flex>
  );
};

const styles = StyleSheet.create({
  slider: {
    position: 'relative',
    width: '100%',
    height: '100%',
    boxSizing: 'border-box',
    margin: 0,
    padding: 0,
    display: 'flex',
    overflow: 'hidden',
    background: 'white',
    border: '1px solid black',
  },
  slide: {
    minWidth: '100%',
    height: '100%',
    transition: 0.5,
    overflow: 'hidden',
  },
  goLeft: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '2rem',
    color: colors.white,
    height: '100%',
    border: 'none',
    outline: 'none',
    cursor: 'pointer',
    zIndex: 0,
    ':hover': {
      backgroundImage:
        'linear-gradient(to right,rgba(0,0,0,0.5), rgba(0, 0, 0, .256))',
    },
  },
  goRight: {
    position: 'absolute',
    top: 0,
    color: colors.white,
    right: 0,
    width: '2rem',
    height: '100%',
    border: 'none',
    outline: 'none',
    cursor: 'pointer',
    ':hover': {
      background:
        'linear-gradient(to left,rgba(0,0,0,0.5), rgba(0, 0, 0, .256))',
    },
  },
  image: {
    display: 'block',
    width: '100%',
    pointerEvents: 'none',
    opacity: 0,
  },
  figure: {
    width: '100%',
    cursor: 'zoom-in',
    backgroundRepeat: 'no-repeat',
  },
});

export default ProductImage;
