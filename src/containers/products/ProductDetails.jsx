import { Box, Stack } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { useSKU } from '../../../hooks/useSKU';
import CartAndWishlist from '../../components/product/CartAndWishlist';
import Features from '../../components/product/Features';
import PDPPincode from '../../components/product/PDPPincode';
import ProductAvailability from '../../components/product/ProductAvailability';
import ProductName from '../../components/product/ProductName';
import ProductReviewStars from '../../components/product/ProductReviewStars';
import SizeAndPrice from '../../components/product/SizeAndPrice';

const ProductDetails = ({ product }) => {
  const { isLg } = useMediaQuery();
  const [currSelected, setCurrSelected] = useState(0);

  const { sku } = useSKU(product.materialCode);
  console.log(product);
  const [bagSize, setBagSize] = useState(product.sku[0]);

  useEffect(() => {
    setBagSize(sku[0]);
  }, [sku]);

  return (
    <Box>
      <Stack m={!isLg && 5} spacing="15px" justifyContent={!isLg && 'center'}>
        <ProductName product={product} />
        <ProductReviewStars product={product} />
        <ProductAvailability product={product} bagSize={bagSize} currSelected={currSelected} />
        <PDPPincode />
        <SizeAndPrice
          currSelected={currSelected}
          bagSize={bagSize}
          setCurrSelected={setCurrSelected}
          setBagSize={setBagSize}
          product={product}
          sku={sku}
        />
        <CartAndWishlist
          currSelected={currSelected}
          bagSize={bagSize}
          product={product}
          sku={sku}
        />
      </Stack>
      <Features />
    </Box>
  );
};

export default ProductDetails;
