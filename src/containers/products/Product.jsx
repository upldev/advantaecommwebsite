import {
  Box, Flex, Spinner, Stack
} from '@chakra-ui/react';
import { Storage } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import PDPBreadCrumb from './PDPBreadCrumb';
import PDPMiddleSection from './PDPMiddleSection';
import ProductCarousel from './ProductCarousel';
import ProductDetails from './ProductDetails';
import RecommendedProducts from './RecommendedProducts';

const ProductContainer = ({ product }) => {
  const { isLg } = useMediaQuery();

  const dispatch = useDispatch();
  const [productImgs, setProductImgs] = useState();

  useEffect(() => {
    async function getProductImgs() {
      const res = await Promise.all(
        product?.productImg?.map(async (img) => {
          const imgUrl = await Storage.get(img);
          return imgUrl;
        })
      );
      setProductImgs(res);
    }
    getProductImgs();
  }, [dispatch, product.productImg, product.similarProducts]);

  return (
    <>
      {isLg && <PDPBreadCrumb product={product} />}
      <Stack spacing="40px" paddingY={isLg && '40px'}>
        <Flex flexDirection={!isLg && 'column'} paddingX={isLg && '100px'}>
          <Box width={!isLg ? '100%' : '40%'} paddingRight={isLg && '15px'}>
            {productImgs && productImgs.length > 0 ? (
              <ProductCarousel
                product={{ ...product, productImg: productImgs }}
              />
            ) : (
              <Flex
                py={{ base: 0, lg: 3 }}
                alignItems="center"
                justifyContent="center"
              >
                <Spinner color={colors.green} />
              </Flex>
            )}
          </Box>
          <Box width={isLg ? '60%' : '100%'} paddingLeft={isLg && '15px'}>
            <ProductDetails product={{ ...product, productImg: productImgs }} />
          </Box>
        </Flex>
        <PDPMiddleSection product={{ ...product, productImg: productImgs }} />
        <RecommendedProducts />
      </Stack>
    </>
  );
};

export default ProductContainer;
