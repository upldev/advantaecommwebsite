/* eslint-disable consistent-return */
/* eslint-disable no-unused-expressions */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { AspectRatio, Flex, Image } from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React, { useEffect, useRef, useState } from 'react';
import { AiFillHeart } from 'react-icons/ai';
import { IoHeartOutline } from 'react-icons/io5';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import {
  addOnlyToWishlist,
  removeFromWishlist
} from '../../../store/actions/cart';

const ProductImageMobile = ({ product, productImageCarouselData }) => {
  const goDirectTo = (e) => {
    setCurrent(parseInt(e.target.id));
  };
  const dispatch = useDispatch();
  const [current, setCurrent] = useState(0);
  const [delay, setDelay] = useState(5000);
  useEffect(() => {
    setCurrent(0);
    setLength(productImageCarouselData.length);
  }, [productImageCarouselData]);
  const [length, setLength] = useState(productImageCarouselData.length);
  useInterval(() => {
    current === length - 1 ? setCurrent(0) : setCurrent(current + 1);
  }, delay);
  const handleAddToWishlist = () => {
    dispatch(addOnlyToWishlist(product));
  };
  const handleRemoveFromWishlist = () => {
    dispatch(removeFromWishlist(product.materialCode));
  };

  const wishlisted = useSelector((state) => {
    return state.wishlist.wishlistItems?.find(
      (x) => x === product.materialCode
    );
  });

  return (
    <Flex
      width="100%"
      backgroundColor={colors.white}
      flexDirection="column"
      justifyContent="center"
      position="relative"
    >
      <Flex width="100%" justifyContent="center">
        {productImageCarouselData[current]?.isImage && (
          <>
            <Flex justifyContent="center">
              <Image
                objectFit="contain"
                height="40vh"
                width="100%"
                m={5}
                src={productImageCarouselData[current].data}
              />
            </Flex>
          </>
        )}
        {!productImageCarouselData[current]?.isImage && (
          <>
            <AspectRatio m={5} height="40vh" width="100%">
              <iframe
                title="video"
                src={productImageCarouselData[current].data.replace(
                  'watch?v=',
                  'embed/'
                )}
              />
            </AspectRatio>
          </>
        )}
      </Flex>
      <Flex mx={100} my={5} justifyContent="center">
        {Array(length)
          .fill()
          .map((item, index) => {
            return (
              <>
                {index === current ? (
                  <span
                    onClick={goDirectTo}
                    id={index}
                    key={index}
                    className={css(styles.greenDot)}
                  />
                ) : (
                  <span
                    onClick={goDirectTo}
                    id={index}
                    key={index}
                    className={css(styles.grayDot)}
                  />
                )}
              </>
            );
          })}
      </Flex>
      <Flex
        position="absolute"
        top="15px"
        right="15px"
        height="50px"
        width="50px"
        borderRadius="50%"
        boxShadow="xl"
        backgroundColor={colors.white}
        alignItems="center"
        justifyContent="center"
      >
        {wishlisted ? (
          <AiFillHeart
            size="2rem"
            color={colors.lightRed}
            onClick={handleRemoveFromWishlist}
          />
        ) : (
          <IoHeartOutline
            size="2rem"
            color={colors.lightRed}
            onClick={handleAddToWishlist}
          />
        )}
      </Flex>
    </Flex>
  );
};
function useInterval(callback, delay) {
  const savedCallback = useRef();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}
const styles = StyleSheet.create({
  grayDot: {
    height: '10px',
    width: '10px',
    backgroundColor: colors.gray,
    borderRadius: '50%',
    display: 'inline-block',
    marginRight: 5,
    ':hover': {
      cursor: 'pointer',
    },
  },
  greenDot: {
    height: '10px',
    width: '20px',
    backgroundColor: colors.green,
    borderRadius: '15px',
    display: 'inline-block',
    marginRight: 5,
    ':hover': {
      cursor: 'pointer',
    },
  },
  wishlistBadge: {
    position: 'absolute',
    color: colors.lightRed,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
  },
  wishlistedBadge: {
    position: 'absolute',
    color: colors.lightRed,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
  },

  image: {
    display: 'block',
    width: '100%',
    pointerEvents: 'none',
    opacity: 0,
  },
  figure: {
    width: '100%',
    cursor: 'zoom-in',
    backgroundRepeat: 'no-repeat',
  },
});
export default ProductImageMobile;
