import { ChevronRightIcon } from '@chakra-ui/icons';
import {
  Breadcrumb, BreadcrumbItem, BreadcrumbLink, Flex, Icon, Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import React from 'react';
import { AiOutlineHome } from 'react-icons/ai';
import { colors } from '../../../constants/colors';
import { CATEGORY } from '../../../lib/router';

const PDPBreadCrumb = ({ product }) => {
  const router = useRouter();
  const handleRedirect = () => {
    const cauliflowerAndCabbage = 'Cauliflower and Cabbage';
    if (product.filterCategory === 'Cauliflower & Cabbage') {
      router.push(`${CATEGORY}?type=${cauliflowerAndCabbage}`, undefined, { shallow: true });

    } else {
      router.push(`${CATEGORY}?type=${product.filterCategory}`, undefined, { shallow: true });
    }
  };
  return (
    <Flex bg={colors.white} pt={5} pb={5}>
      <Flex mx="100">
        <Breadcrumb
          spacing="8px"
          alignItems="center"
          separator={<ChevronRightIcon color={colors.gray} />}
        >
          <BreadcrumbItem>
            <BreadcrumbLink
              href="/"
            >
              <Icon
                variant="ghost"
                as={AiOutlineHome}
                boxSize="1.5rem"
              />
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <BreadcrumbLink
              onClick={handleRedirect}
              className={css(styles.breadcrumbStyling)}
            >
              <Text>
                {product.crop}
              </Text>
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink
              className={css(styles.breadcrumbStyling)}
            >
              {product.product}
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  breadcrumbStyling: {
    ':hover': {
      textDecoration: 'none',
      color: colors.lightBlue
    }
  }
});

export default PDPBreadCrumb;
