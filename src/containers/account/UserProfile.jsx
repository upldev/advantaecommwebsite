import { Box, Flex } from '@chakra-ui/react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ProfileCard from '../../components/account/ProfileCard';
import ProfileSideNavbar from '../../components/account/ProfileSideNavbar';

// Code Review Completed

const UserProfileContainer = ({ profile }) => {
  const { isMd, isSm } = useMediaQuery();
  return (
    <Flex
      alignItems="center"
      justifyContent={isMd ? 'center' : 'flex-start'}
      paddingY={isMd ? 0 : 12}
      paddingX={isSm ? '15px' : '100px'}
    >
      <Flex>
        {!isSm && (
          <Box width="30%" paddingRight="15px">
            <ProfileSideNavbar type={1} />
          </Box>
        )}
        <Box paddingLeft="15px" width={isSm ? '100%' : '70%'}>
          <ProfileCard profile={profile} />
        </Box>
      </Flex>
    </Flex>
  );
};

export default UserProfileContainer;
