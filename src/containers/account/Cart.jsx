import {
  Box, Button, Divider, Flex, Stack
} from '@chakra-ui/react';
import { API, Auth, graphqlOperation } from 'aws-amplify';
import router from 'next/router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import { getFarmer } from '../../../graphql/queries';
import useMediaQuery from '../../../hooks/useMediaQuery';
import {
  CHECKOUT_SELECT_ADDRESS,
  CREATE_USER_PROFILE,
  redirectBackTo
} from '../../../lib/router';
import { getCartProducts } from '../../../store/actions/cart';
import { showToast } from '../../../store/actions/feedback';
import { getSimilarProducts } from '../../../store/actions/product';
import {
  selectIsInvalid
} from '../../../store/selectors/cart';
import CartHeader from '../../components/cart/CartHeader';
import EmptyCart from '../../components/cart/EmptyCart';
import MobileCheckout from '../../components/cart/MobileCheckout';
import ReturningCustomer from '../../components/cart/ReturningCustomer';
import Progress from '../../components/checkout/Progress';
import HomeBanner from '../../components/home/HomeBanner';
import Trending from '../../components/home/Trending';
import PaymentInfo from '../../components/order/PaymentInfo';
import CartBody from './CartBody';
// Code Review Complete
// Need to change logic for Similar Products

const CartContainer = ({ isCartCheckout }) => {
  const dispatch = useDispatch();

  const user = useSelector((state) => state.user);

  const { cartItems, cartProducts } = useSelector((state) => state.cart);
  const { isInvalid } = useSelector(selectIsInvalid);
  const { isLg } = useMediaQuery();

  useEffect(() => {
    dispatch(getCartProducts());
    if (cartItems.length > 0) {
      cartProducts.map((item) => dispatch(getSimilarProducts(item?.similarProducts)));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, cartItems.length]);

  const handleCheckout = async () => {
    if (user.logInStatus) {
      if (!isCartCheckout) {
        if (!user.hasProfile) {
          const cognitoUser = await Auth.currentAuthenticatedUser();
          const data = await API.graphql(
            graphqlOperation(getFarmer, {
              mobile: cognitoUser.username,
            })
          );
          if (data.data.getFarmer === null) {
            dispatch(
              showToast({
                title: 'Profile Not Created!',
                message: 'Please create a profile',
                status: false,
              })
            );
            router.push(CREATE_USER_PROFILE);
            return;
          }
        }
        router.push(CHECKOUT_SELECT_ADDRESS);
      } else {
        router.reload();
      }
    } else {
      router.push(redirectBackTo(CHECKOUT_SELECT_ADDRESS), undefined, {
        shallow: true,
      });
    }
  };

  return (
    <Stack spacing="40px" paddingY="40px">
      {cartItems.length > 0 ? (
        <>
          <Box backgroundColor={colors.light}>
            <Progress progress={1} />
          </Box>
          <Divider marginBottom="30px" alignSelf="center" color={colors.gray} />
          <Flex
            paddingX={isLg ? 100 : 2}
            flexDirection={isLg ? 'row' : 'column'}
            justifyContent="space-between"
            width="100%"
          >
            <Box width={isLg && '65%'} pr={isLg && '30px'}>
              <ReturningCustomer />
              <CartHeader />
              <CartBody isCartCheckout={isCartCheckout} />
            </Box>
            <Box width={isLg && '35%'}>
              <PaymentInfo
                Action={(
                  <Button onClick={handleCheckout} disabled={isInvalid} variant="primary">
                    PROCEED TO CHECKOUT
                  </Button>
                )}
              />
            </Box>
          </Flex>
          <HomeBanner />
        </>
      ) : (
        <Flex justifyContent="center" mt={20} mb={20}>
          <EmptyCart />
        </Flex>
      )}
      <Trending />
      {!isLg && cartItems.length > 0 && (
        <MobileCheckout isCartCheckout={isCartCheckout} />
      )}
    </Stack>
  );
};

export default CartContainer;
