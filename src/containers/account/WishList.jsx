import { Flex, SimpleGrid, VStack } from '@chakra-ui/react';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { getWishlistProducts } from '../../../store/actions/cart';
import ProductCard from '../../components/home/ProductCard';
import EmptyWishlist from '../../components/wishlist/EmptyWishlist';
import WishlistBreadCrumb from '../../components/wishlist/WishlistBreadCrumb';

// Code Review Completed

const WishListContainer = () => {
  const { isLg, isXs } = useMediaQuery();
  const dispatch = useDispatch();
  const wishlist = useSelector((state) => state.wishlist);
  const { wishlistItems, wishlistProducts } = wishlist;

  useEffect(() => {
    if (wishlistItems?.length > 0) {
      dispatch(getWishlistProducts(wishlistItems));
    }
  }, [dispatch, wishlistItems]);

  return (
    <>
      {isLg && <WishlistBreadCrumb />}
      <VStack minHeight="100vh" mx={isLg ? 100 : 2} my="30px" spacing={5}>
        {wishlistItems?.length === 0 && <EmptyWishlist isWishlist />}
        <SimpleGrid
          justifyContent="space-between"
          width="100%"
          spacing={isLg ? '40px' : '15px'}
          columns={isLg ? 4 : 2}
        >
          {wishlistItems?.length > 0
            && wishlistProducts?.length > 0
            && wishlistProducts?.map((item) => (
              <ProductCard key={item.materialCode} product={item} />
            ))}
        </SimpleGrid>
        <Flex mb={10} />
      </VStack>
    </>
  );
};

export default WishListContainer;
