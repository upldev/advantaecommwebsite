import { Flex, Text, VStack } from '@chakra-ui/react';
import React from 'react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import CartCheckout from '../../components/cart/CartCheckout';
import CartItem from '../../components/cart/CartItem';

// Code Review Completed

const CartBody = ({ isCartCheckout }) => {
  const { cartItems, cartProducts } = useSelector((state) => state.cart);

  const { isLg } = useMediaQuery();

  return (
    <VStack
      spacing={0}
      borderTop={!isLg && '1px'}
      borderTopColor={colors.gray}
    >
      {cartItems.length > 0 && (
        <>
          {isLg && (
            <Flex
              width="100%"
              fontWeight="bold"
              borderWidth="1px"
              p="14px"
              borderColor={colors.gray}
              bgColor={colors.light}
            >
              <Text width="50%" variant="smTextRegular" align="left">
                Product
              </Text>
              <Text width="10%" textAlign="center" variant="smTextRegular">
                Size
              </Text>
              <Text width="30%" textAlign="center" variant="smTextRegular">
                Quantity
              </Text>
              <Text width="10%" textAlign="center" variant="smTextRegular">
                Total
              </Text>
            </Flex>
          )}
        </>
      )}
      {cartItems.length > 0
        && cartProducts.length > 0
        && cartProducts?.map((item) => (
          <CartItem
            key={item.sku.ecomCode}
            product={item}
            isCartCheckout={isCartCheckout}
          />
        ))}
      {cartItems.length > 0 && <CartCheckout isCartCheckout={isCartCheckout} />}
    </VStack>
  );
};

export default CartBody;
