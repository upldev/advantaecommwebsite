import { Box, Flex, Stack } from '@chakra-ui/react';
import React from 'react';
import { useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import ActiveAndDeliveredOrders from '../../components/account/ActiveAndDeliveredOrders';
import OrdersFilter from '../../components/account/OrdersFilter';
import ProfileSideNavbar from '../../components/account/ProfileSideNavbar';
import OrdersPagination from '../../components/order/OrdersPagination';

// Code Review Completed

const OrderHistoryContainer = () => {
  const { isLg } = useMediaQuery();
  const ordersLength = useSelector((state) => state.orders.orders);

  return (
    <Flex paddingY={isLg ? 12 : '15px'} paddingX={!isLg ? '15px' : '100px'}>
      {isLg && (
        <Box width="30%" paddingRight="15px">
          <ProfileSideNavbar type={2} />
        </Box>
      )}
      <Box width={isLg ? '70%' : '100%'} paddingLeft={isLg && '15px'}>
        <Stack width="100%" spacing="30px">
          {ordersLength.length !== 0 && <OrdersFilter />}
          <ActiveAndDeliveredOrders />
          <OrdersPagination />
        </Stack>
      </Box>
    </Flex>
  );
};

export default OrderHistoryContainer;
