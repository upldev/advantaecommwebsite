import { Flex, SimpleGrid, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { selectProducts } from '../../../store/selectors/product';
import Filter from '../../components/Filter/Filter';
import MobileFilter from '../../components/Filter/MobileFilter';
import SortBy from '../../components/Filter/SortBy';
import ProductCard from '../../components/home/ProductCard';
import Pagination from '../../components/pagination/Pagination';

// Code Review Completed

const CategoryContainer = () => {
  const products = useSelector(selectProducts);
  const filteredProducts = useSelector(selectProducts);
  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage, setProductsPerPage] = useState(12);
  const indexOfLastProduct = currentPage * productsPerPage;
  const indexOfFirstProduct = indexOfLastProduct - productsPerPage;

  const setCurrentProducts = () => {
    const productsToShow = products.length;
    const currentProducts = products.slice(
      indexOfFirstProduct,
      indexOfLastProduct
    );
    return { currentProducts, productsToShow };
  };

  const { isLg, isXs } = useMediaQuery();

  const { currentProducts, productsToShow } = setCurrentProducts();

  return (
    <>
      <Flex py={isLg ? '40px' : '15px'} justifyContent="space-between" px={isLg ? '100px' : '15px'}>
        {isLg && <Filter />}
        <Flex
          width={isLg ? '75%' : '100%'}
          flexDirection="column"
          justifyContent="space-between"
          alignItems="center"
          pl={isLg && '15px'}
        >
          <Flex width="100%" flexDirection="column">
            {isLg && (
              <Flex justifyContent="space-between" alignItems="center">
                {products.length > 0 ? (
                  <Flex>
                    <Text fontWeight="bold" mr={1}>
                      Filtered Results
                    </Text>
                    <Text>
                      (
                      {products?.length}
                      {' '}
                      products)
                    </Text>
                  </Flex>
                ) : (
                  <Text>No results</Text>
                )}
                <Flex justifyContent="space-between" alignItems="center">
                  <Text fontWeight="bold" pr="2">
                    Sort By:
                  </Text>
                  <SortBy />
                </Flex>
              </Flex>
            )}
            <SimpleGrid
              my={isLg ? '5' : '1.5'}
              justifyContent="space-between"
              width="100%"
              spacing={isLg ? '30px' : '15px'}
              columns={isLg ? 3 : isXs ? 1 : 2}
            >
              {(isLg ? currentProducts : products)?.map((product) => (
                <ProductCard key={product.materialCode} product={product} />
              ))}
            </SimpleGrid>
          </Flex>
          {currentProducts?.length === 0 && (
            <Text>No available products for applied filters</Text>
          )}
          {productsToShow > 12 && isLg && (
            <Pagination
              productsPerPage={productsPerPage}
              setCurrentPage={setCurrentPage}
              currentPage={currentPage}
              totalProducts={filteredProducts?.length}
            />
          )}
        </Flex>
      </Flex>
      {!isLg && <MobileFilter />}
    </>
  );
};

export default CategoryContainer;
