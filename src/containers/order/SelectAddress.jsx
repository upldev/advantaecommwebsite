import {
  Alert,
  AlertIcon,
  AlertTitle,
  Box,
  Button,
  Divider,
  Flex,
  Stack,
  Text
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import usePincodeServiceability from '../../../hooks/usePincodeServiceability';
import { CHECKOUT_ORDER_SUMMARY } from '../../../lib/router';
import { getUser } from '../../../store/actions/checkout';
import { selectIsInvalid } from '../../../store/selectors/cart';
import {
  selectDefaultAddress,
  selectShippingAddresses
} from '../../../store/selectors/user';
import AddressCard from '../../components/account/AddressCard';
import MobileCheckout from '../../components/cart/MobileCheckout';
import Progress from '../../components/checkout/Progress';
import PaymentInfo from '../../components/order/PaymentInfo';
import AddAddress from '../../components/shared/AddAddress';
// Code Review Completed

const SelectAddressContainer = () => {
  const router = useRouter();

  const dispatch = useDispatch();
  const { currentUser, currentShipTo } = useSelector((state) => state.checkout);
  const defaultAddress = useSelector(selectDefaultAddress);
  const shippingAddress = useSelector(selectShippingAddresses);
  const { isInvalid } = useSelector(selectIsInvalid);

  const { isDeliverable, isLoading } = usePincodeServiceability(
    currentShipTo?.pincode || ''
  );

  const { isLg } = useMediaQuery();

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  const handleNext = () => router.push(`/${CHECKOUT_ORDER_SUMMARY}`);

  return (
    <Stack spacing="40px" paddingY="40px">
      <Box>
        <Progress progress={2} />
      </Box>
      <Divider alignSelf="center" color={colors.gray} />
      <Flex
        paddingX={isLg ? 100 : 2}
        flexDirection={isLg ? 'row' : 'column'}
        justifyContent="space-between"
        width="100%"
      >
        <Box width={isLg && '65%'} pr={isLg && '30px'} pb={!isLg && '30px'}>
          <Flex justifyContent="space-between">
            <Text variant={isLg ? 'heading' : 'subTitle'}>
              Select a delivery address
            </Text>
            <AddAddress />
          </Flex>
          {!isDeliverable && !isLoading && (
            <Alert status="error" marginTop="30px">
              <AlertIcon />
              <AlertTitle>Cannot deliver to the selected pincode</AlertTitle>
            </Alert>
          )}
          <Stack spacing="30px" marginTop="30px">
            {Object.keys(defaultAddress).length > 0
            || shippingAddress.length > 0 ? (
              <>
                {Object.keys(defaultAddress).length > 0 && (
                  <AddressCard address={defaultAddress} user={currentUser} />
                )}
                {shippingAddress?.map((address) => (
                  <AddressCard
                    key={address.addressId}
                    address={address}
                    user={currentUser}
                  />
                ))}
              </>
              ) : (
                <Text variant={isLg ? 'mdTextRegular' : 'textRegular'}>
                  No Shipping Addresses Available to Select
                </Text>
              )}
          </Stack>
        </Box>
        <Box width={isLg && '35%'}>
          <PaymentInfo
            Action={(
              <Button
                onClick={handleNext}
                variant="primary"
                isDisabled={
                  Object.keys(currentShipTo || {}).length === 0
                  || !currentShipTo?.addressId
                  || isInvalid
                  || !isDeliverable
                }
                isLoading={isLoading}
              >
                PLACE ORDER
              </Button>
            )}
          />
        </Box>
      </Flex>
      {!isLg && (
        <MobileCheckout
          isSelectAddress
          addressSelected={Object.keys(currentShipTo || {}).length === 0}
        />
      )}
    </Stack>
  );
};

export default SelectAddressContainer;
