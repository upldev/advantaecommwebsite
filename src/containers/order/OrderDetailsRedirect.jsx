import {
  Flex, Text
} from '@chakra-ui/react';
import { API } from 'aws-amplify';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { colors } from '../../../constants/colors';
import { getMinProductDetails } from '../../../graphql/customQueries';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { showLoading, showToast } from '../../../store/actions/feedback';
import OrderDetailsCard from '../../components/order/OrderDetailsCard';
import PaymentDetailsCard from '../../components/order/PaymentDetailsCard';
import ProductDetailsCard from '../../components/order/ProductDetailsCard';

// Code Review Completed

const OrderDetailsContainer = ({ latestOrder }) => {
  const { isLg } = useMediaQuery();
  const [order, setOrder] = useState(latestOrder);
  const dispatch = useDispatch();

  useEffect(() => {
    async function getOrder(order) {
      dispatch(showLoading(true));
      try {
        if (order) {
          const orderCart = await Promise.all(order.orderCart.map(async (orderItem) => {
            const { data } = await API.graphql({
              query: getMinProductDetails,
              variables: {
                materialCode: orderItem.materialCode
              },
              authMode: 'API_KEY'
            });
            const productImg = data.getProduct.productImg[0];
            const { crop, product } = data.getProduct;
            return { ...orderItem, productDetails: { crop, product, productImg } };
          }));
          setOrder({ ...order, orderCart });
          return;
        }
        dispatch(showToast({
          title: 'Order not found!',
          status: false
        }));
      } catch (err) {
        console.error(err);
        dispatch(showToast({
          title: 'Error Occured!',
          message: 'Please try again',
          status: false
        }));
      } finally {
        dispatch(showLoading(false));
      }
    }
    getOrder(latestOrder);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Flex flexDirection="column" mx={isLg ? 100 : 3} bgColor={colors.light}>
      <Text mt={10} mb={5} variant="subTitle" fontWeight="bold"> Order Details </Text>
      <OrderDetailsCard order={order} />
      <ProductDetailsCard order={order} />
      <PaymentDetailsCard order={order} />
    </Flex>
  );
};

export default OrderDetailsContainer;
