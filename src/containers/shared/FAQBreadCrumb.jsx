import { ChevronRightIcon } from '@chakra-ui/icons';
import {
  Breadcrumb, BreadcrumbItem, BreadcrumbLink, Flex, Icon
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { AiOutlineHome } from 'react-icons/ai';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

const FAQBreadCrumb = () => {
  const { isLg } = useMediaQuery();
  return (
    <Flex bg={colors.white} pt={5} pb={5}>
      <Flex mx={isLg ? '100' : '2'}>
        <Breadcrumb
          spacing="8px"
          alignItems="center"
          separator={<ChevronRightIcon color={colors.gray} />}
        >
          <BreadcrumbItem>
            <BreadcrumbLink
              href="/"
            >
              <Icon
                variant="ghost"
                as={AiOutlineHome}
                boxSize="1.5rem"
              />
            </BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink
              className={css(styles.breadcrumbStyling)}
            >
              FAQ
            </BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </Flex>
    </Flex>
  );
};

const styles = StyleSheet.create({
  breadcrumbStyling: {
    ':hover': {
      textDecoration: 'none',
      color: colors.lightBlue
    }
  }
});

export default FAQBreadCrumb;
