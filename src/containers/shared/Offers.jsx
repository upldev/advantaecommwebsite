import {
  Box, ListItem, Text, UnorderedList
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';

// Code Review Completed
// Need to remove whole code

const OffersContainer = () => {
  const { isLg } = useMediaQuery();

  return (
    <Box
      className={css(styles.container)}
    >
      <Box fontWeight="bold" paddingBottom="10px">
        <Text>Available Offers</Text>
      </Box>

      <UnorderedList>
        <ListItem>Integer molestie lorem at massa</ListItem>
        <ListItem>Facilisis in pretium nisl aliquet</ListItem>
      </UnorderedList>
    </Box>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    padding: 20
  },
});
export default OffersContainer;
