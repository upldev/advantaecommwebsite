import {
  Center,
  Divider,
  Flex,
  Heading,
  Icon,
  Text,
  VStack
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { FaPhoneAlt } from 'react-icons/fa';
import {
  IoLogoFacebook,
  IoLogoLinkedin,
  IoLogoTwitter,
  IoNavigate
} from 'react-icons/io5';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import Email from './Email';
import ReportAnIssueContainer from './ReportAnIssue';

// Code Review Completed
// Remove Chatbot, Add Contact details card

const ContactUsContainer = () => {
  const { isLg } = useMediaQuery();

  return (
    <VStack bgColor="gray.50" justifyContent="center" alignItems="center">
      <Flex
        backgroundImage="url(/smart-agriculture.jpg)"
        justifyContent="center"
        alignItems="center"
        width="100vw"
        height="50vh"
        backgroundSize="cover"
      >
        <Heading color={colors.white}>CONTACT US</Heading>
      </Flex>
      {isLg ? (
        <Flex
          className={css(styles.desktopContainer)}
          flexDirection={isLg ? 'row' : 'column'}
          boxShadow="lg"
        >
          <Flex>
            <ReportAnIssueContainer />
          </Flex>
          <Center
            height="90vh"
            backgroundColor={colors.gray}
            width="1px"
            my={5}
          />
          <Flex flexDirection="column" justifyContent="left" px="50">
            <Flex className={css(styles.container)} my={5}>
              <Flex>
                <a
                  target="_blank"
                  className={css(styles.socialMediaLinks)}
                  href="https://www.facebook.com/advantaseeds/"
                  rel="noreferrer"
                >
                  <Icon mr={4} as={IoLogoFacebook} />
                </a>
                <a
                  target="_blank"
                  className={css(styles.socialMediaLinks)}
                  href="https://twitter.com/advantaseeds"
                  rel="noreferrer"
                >
                  <Icon mr={4} as={IoLogoTwitter} />
                </a>
                <a
                  target="_blank"
                  className={css(styles.socialMediaLinks)}
                  href="https://www.linkedin.com/company/advantaseeds/"
                  rel="noreferrer"
                >
                  <Icon as={IoLogoLinkedin} />
                </a>
              </Flex>
            </Flex>
            <Email />
            <Flex alignItems="center" my={5}>
              <Icon as={FaPhoneAlt} mr={3} />
              <Text fontWeight="bold">98745 63210 / 98745 63210</Text>
            </Flex>
            <Flex alignItems="center" my={5}>
              <Icon as={IoNavigate} mr={3} />
              <Text fontWeight="bold">New Delhi, India</Text>
            </Flex>
          </Flex>
        </Flex>
      ) : (
        <Flex flexDirection={isLg ? 'row' : 'column'} p={3}>
          <Flex flexDirection="column" justifyContent="left" px="50" mb={3}>
            <Flex className={css(styles.container)} my={5}>
              <Flex>
                <a
                  target="_blank"
                  className={css(styles.socialMediaLinks)}
                  href="https://www.facebook.com/advantaseeds/"
                  rel="noreferrer"
                >
                  <Icon mr={4} as={IoLogoFacebook} />
                </a>
                <a
                  target="_blank"
                  className={css(styles.socialMediaLinks)}
                  href="https://twitter.com/advantaseeds"
                  rel="noreferrer"
                >
                  <Icon mr={4} as={IoLogoTwitter} />
                </a>
                <a
                  target="_blank"
                  className={css(styles.socialMediaLinks)}
                  href="https://www.linkedin.com/company/advantaseeds/"
                  rel="noreferrer"
                >
                  <Icon as={IoLogoLinkedin} />
                </a>
              </Flex>
            </Flex>
            <Email />
            <Flex alignItems="center" my={5}>
              <Icon as={FaPhoneAlt} mr={3} />
              <Text fontWeight="bold">98745 63210 / 98745 63210</Text>
            </Flex>
            <Flex alignItems="center" my={5}>
              <Icon as={IoNavigate} mr={3} />
              <Text fontWeight="bold">New Delhi, India</Text>
            </Flex>
          </Flex>
          <Divider mb={3} color={colors.gray} />
          <Flex backgroundColor={colors.white} p={3}>
            <ReportAnIssueContainer />
          </Flex>
        </Flex>
      )}
    </VStack>
  );
};

const styles = StyleSheet.create({
  desktopContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    marginTop: -50,
    marginBottom: 50,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  icons: {
    backgroundColor: colors.white,
    ':hover': {
      backgroundColor: colors.white,
      color: colors.green,
    },
  },
  socialMediaLinks: {
    fontSize: '1.75rem',
    textDecoration: 'none',
    ':hover': {
      color: colors.green,
    },
    marginRight: 10,
  },
});

export default ContactUsContainer;
