import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Link,
  Text,
  VStack
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { CONTACT_US, redirectBackTo } from '../../../lib/router';
import {
  createAnIssue,
  newIssue,
  reportAnIssue
} from '../../../store/actions/reportAnIssue';
import FormDropDown from '../../components/shared/FormDropDown';
import FormTextArea from '../../components/shared/FormTextArea';

const options = [
  'Broken Seeds',
  'Germination Related',
  'Product Performance Related',
  'Late Delivery',
  'Packaging Damaged',
  'Payment Related',
  'Website Related',
  'Order Status Enquiry',
  'Other',
];

const ReportAnIssueSchema = Yup.object({
  complaintId: Yup.string().required('Required'),
  farmerMobile: Yup.string().required('Required'),
  complaintType: Yup.string().required('Required'),
  complaintDesc: Yup.string()
    .required('Required')
    .matches(
      /^[a-zA-Z0-9\s,.\\]*$/,
      'Cannot enter special characters like $, @..'
    ),
});

const ReportAnIssueContainer = () => {
  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const [load, setLoad] = useState(false);
  const formState = useSelector((state) => state.reportAnIssue);
  const isLoggedIn = useSelector((state) => state.user.logInStatus);

  useEffect(() => {
    dispatch(createAnIssue());
  }, [dispatch]);

  useEffect(() => {
    if (formState.complaintId) {
      setLoad(true);
    }
  }, [formState]);

  function handleSubmit(values) {
    dispatch(reportAnIssue(values));
    setLoad(false);
    dispatch(newIssue());
  }

  return isLoggedIn ? (
    <VStack
      // backgroundColor={isLg ? 'white' : 'gray.50'}
      backgroundColor={colors.white}
      borderRadius={{ base: '0px', lg: '5' }}
      p={isLg && '20px'}
      width="100%"
    >
      <Text
        variant={isLg ? 'heading' : 'subTitle'}
        mt={isLg ? '10' : '0'}
        textAlign="center"
        color={colors.advantaBlue}
      >
        REPORT ISSUE
      </Text>
      {load && (
        <Formik
          initialValues={formState}
          onSubmit={handleSubmit}
          validationSchema={ReportAnIssueSchema}
        >
          {(props) => (
            <>
              <Flex
                width="100%"
                flexDir="column"
                align="center"
                justify="center"
              >
                <Flex flexDirection="column">
                  <FormControl mt="3">
                    <FormLabel fontSize={isLg ? '18' : '12'}>
                      Phone Number
                    </FormLabel>
                    <Text
                      variant={isLg ? 'mdTextRegular' : 'textRegular'}
                      fontWeight="bold"
                    >
                      {props.values.farmerMobile}
                    </Text>
                  </FormControl>
                  <Flex mt={3}>
                    <FormDropDown
                      name="complaintType"
                      label="Type Of Issue"
                      options={options}
                      placeholder="Select An Issue"
                      isRequired
                      error={props.errors}
                    />
                  </Flex>
                  <Flex mt={3}>
                    <FormTextArea
                      h="28vh"
                      name="complaintDesc"
                      label="Message"
                      isRequired
                      placeholder="Please Mention Details"
                    />
                  </Flex>
                </Flex>
                <Button
                  size={isLg ? 'md' : 'sm'}
                  my={isLg ? 5 : 2}
                  variant="primary"
                  type="submit"
                  onClick={props.handleSubmit}
                >
                  Submit Request
                </Button>
              </Flex>
            </>
          )}
        </Formik>
      )}
    </VStack>
  ) : (
    <Flex
      px={5}
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      textAlign="center"
    >
      <Text fontSize="xl" textAlign="center">
        Please
        {' '}
        <Link
          className={css(styles.linkStyles)}
          color={colors.darkGreen}
          href={redirectBackTo(CONTACT_US)}
        >
          Login
        </Link>
        {' '}
        to report an issue.
      </Text>
    </Flex>
  );
};

const styles = StyleSheet.create({
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green,
    },
    cursor: 'pointer',
    fontWeight: 'bold',
    color: colors.darkGreen,
    whiteSpace: 'nowrap',
  },
  buttonStyle: {
    backgroundColor: colors.green,
    color: colors.white,
    ':hover': {
      textDecoration: 'none',
    },
  },
});

export default ReportAnIssueContainer;
