import { Flex, Icon, Text } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { AiOutlineMail } from 'react-icons/ai';

const NAME = 'info';
const DOMAIN = 'advantaseeds.com';

const Email = () => {
  const [email, setEmail] = useState('');

  useEffect(() => {
    setEmail(`${NAME}@${DOMAIN}`);
  }, []);

  const handleClick = () => {
    window.open(`mailto:${email}`);
  };

  return (
    <Flex onClick={handleClick} cursor="pointer" alignItems="center" my={5}>
      <Icon as={AiOutlineMail} mr={3} />
      <Text fontWeight="bold">customerserviceindia@advantaseeds.com</Text>
    </Flex>
  );
};

export default Email;
