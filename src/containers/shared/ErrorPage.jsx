import { Button, Flex, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { HOME } from '../../../lib/router';

// Code Review Completed

const ErrorPageContainer = () => {
  const { isLg } = useMediaQuery();
  const router = useRouter();
  return (
    <Flex
      minH={isLg ? '80vh' : '100vh'}
      backgroundColor={colors.light}
      borderColor={colors.gray}
      justifyContent="center"
      alignItems="center"
    >
      <Flex
        height="min-content"
        width={isLg ? '25vw' : '75vw'}
        m="5"
        justifyContent="center"
        alignItem="center"
        backgroundColor={colors.white}
        borderRadius="25"
        flexDirection="column"
      >
        <Text my="5" textAlign="center" variant="title">
          404
        </Text>
        <Text mb="5" textAlign="center" variant="subTitle">
          PAGE NOT FOUND
        </Text>
        <Text
          mx="2"
          mb="5"
          textAlign="center"
          color={colors.gray}
          variant="textRegular"
        >
          The link you clicked may be broken the page may have been removed or
          renamed.
        </Text>
        <Flex mb="5" justifyContent="center">
          <Button onClick={() => router.push(HOME)} variant="primary">
            {' '}
            GO TO HOME
            {' '}
          </Button>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default ErrorPageContainer;
