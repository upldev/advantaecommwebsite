/* eslint-disable react/no-unescaped-entities */
import {
  Flex, Image, SimpleGrid, Text, VStack
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import AboutUs from '../../components/shared/AboutUs';

// Code Review Complete

const AboutUsContainer = ({ setData, data }) => {
  const { isLg } = useMediaQuery();
  return (
    <>
      <Flex
        backgroundImage="url(/sunny-meadow.jpg)"
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
        textAlign="center"
        paddingX={isLg ? '100' : '5'}
        paddingY={isLg ? '50' : '3'}
      >
        <Text
          fontWeight="bold"
          fontSize="xx-large"
          // color={colors.white}
          mb={5}
          pt={isLg ? 'unset' : '10'}
        >
          ABOUT US
        </Text>
        <Text
          // color={colors.white}
          paddingX="20"
          pb={10}
        >
          Advanta Seeds is focused on sustainable agriculture and providing
          farmers with quality seeds. With the intent to deliver convenience to
          our farmers, our e-commerce channel strives to deliver seeds at every
          farmer's doorstep in India.
        </Text>
      </Flex>
      <SimpleGrid
        columns={isLg ? '2' : '1'}
        width="full"
        backgroundColor={colors.light}
        paddingX={isLg ? '100' : '50'}
        py={isLg ? '5' : 'unset'}
        justifyContent="center"
        alignItems="center"
        backgroundSize="cover"
        backgroundAttachment="fixed"
      >
        <VStack>
          <Text fontSize="xx-large" className={css(styles.textStyles)}>
            OUR MISSION
          </Text>
          <Flex mx={isLg ? '100' : '5'}>
            <Flex flexDirection="column" width="100%">
              <AboutUs />
            </Flex>
          </Flex>
          <Flex mb={5} />
        </VStack>
        <VStack justifyContent="center" alignItems="center">
          <Flex
            mx={isLg ? '100' : '5'}
            justifyContent="center"
            alignItems="center"
          >
            <Image
              src="/young-farmer.jpg"
              alt="young-farmer"
              mr={4}
              className={css(styles.smallImages)}
            />
            <Image
              src="/cultivated-field.jpg"
              alt="cultivated-field"
              boxSize="50%"
              className={css(styles.smallImages)}
            />
          </Flex>
          <Flex
            mx={isLg ? '100' : '5'}
            justifyContent="center"
            alignItems="center"
          >
            <Image
              src="tea-garden.jpg"
              alt="tea-garden"
              className={css(styles.largeImage)}
            />
          </Flex>
        </VStack>
      </SimpleGrid>
      <Flex className={css(styles.container)} mb={5}>
        <Text
          className={css(styles.textStyles)}
          fontSize="xx-large"
          paddingX={isLg ? '100' : '5'}
          paddingY="3"
        >
          MEET OUR LEADERS
        </Text>
        {/* <Flex
          flexDirection={isLg ? 'row' : 'column'}
          width="full"
          paddingX={isLg ? '100' : '5'}
          justifyContent="center"
          alignItems="center"
          my={2}
        >
          <Flex flexDirection="column" justifyContent="center" alignItems="center">
            <Image src="/bd-1.jpeg" alt="Leader1" boxSize="25%" />
            <Text color={colors.green} fontWeight="bold" paddingY={2}>
              Mr. BHUPEN
            </Text>
            <Text color={colors.advantaBlue} fontSize="sm">
              CEO, Advanta
            </Text>
          </Flex>
          <Flex flexDirection="column" justifyContent="center" alignItems="center">
            <Image src="/prashant-sir.jpg" alt="Leader2" boxSize="31%" />
            <Text color={colors.green} fontWeight="bold" paddingY={2}>
              Mr. BHUPEN
            </Text>
            <Text color={colors.advantaBlue} fontSize="sm">
              CEO, Advanta
            </Text>
          </Flex>
        </Flex> */}
        <SimpleGrid
          columns={isLg ? '2' : '1'}
          width="full"
          paddingX={isLg ? '100' : '5'}
          justifyContent="center"
          alignItems="center"
          my={2}
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
          >
            <Image src="/bd-1.jpeg" alt="Leader1" boxSize="25%" />
            <Text color={colors.green} fontWeight="bold" paddingY={2}>
              Mr. BHUPEN
            </Text>
            <Text color={colors.advantaBlue} fontSize="sm">
              CEO, Advanta
            </Text>
          </Flex>
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
          >
            <Image src="/prashant-sir.jpg" alt="Leader2" boxSize="25%" />
            <Text color={colors.green} fontWeight="bold" paddingY={2}>
              Mr. BHUPEN
            </Text>
            <Text color={colors.advantaBlue} fontSize="sm">
              CEO, Advanta
            </Text>
          </Flex>
        </SimpleGrid>
      </Flex>
      {isLg && (
        <Flex
          mt={5}
          mb={10}
          className={css(styles.container)}
          paddingX={isLg ? '100' : '5'}
          paddingY="5"
        >
          <Text fontWeight="extrabold" fontSize="x-large" textAlign="center">
            Download Advanta Mobile App Now!!
          </Text>
          <Text
            fontSize="sm"
            color={colors.gray}
            paddingX={isLg ? '100' : '0'}
            textAlign="center"
            my={5}
          >
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestiae
            impedit omnis recusandae eius architecto ex itaque, necessitatibus
            odit error ut?
          </Text>
          <Flex justifyContent="center" alignItems="center">
            <Image
              src="/google-play-badge.png"
              alt="google-play-badge"
              className={css(styles.getItOnPlayStore)}
            />
            <Image
              src="/app-store.jpg"
              alt="app-store-badge"
              className={css(styles.getItOnAppStore)}
            />
          </Flex>
        </Flex>
      )}
    </>
  );
};
const styles = StyleSheet.create({
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    },
    cursor: 'pointer',
    fontWeight: 'bold',
    color: colors.darkGreen,
    whiteSpace: 'nowrap'
  },
  textStyles: {
    color: colors.darkBlue,
    fontWeight: 'bolder',
    textAlign: 'center'
  },
  container: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white
  },
  getItOnPlayStore: {
    cursor: 'pointer',
    height: '4rem',
    width: '10rem',
    marginRight: 10
  },
  getItOnAppStore: {
    cursor: 'pointer',
    height: '3rem',
    width: '10rem'
  },
  smallImages: {
    height: '7rem',
    width: '9rem'
  },
  largeImage: {
    height: '10rem',
    width: '19rem'
  }
});

export default AboutUsContainer;
