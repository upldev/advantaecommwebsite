/* eslint-disable max-len */
import {
  ChevronRightIcon
} from '@chakra-ui/icons';
import {
  Box, Breadcrumb, BreadcrumbItem, BreadcrumbLink, Flex, Heading, Text, VStack
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { HOME } from '../../../lib/router';

// Code Review Completed

const CustomerComplaintPolicyContainer = () => {
  const { isLg } = useMediaQuery();
  return (
    <VStack mt={10} mb={10} spacing={10}>
      <Box
        backgroundImage='url("https://image.freepik.com/free-photo/healthy-vegetables-marble-kitchen-countertop-banner-background_8087-2231.jpg")'
        // bgPosition="center"
        width="100%"
        height="100%"
        p="6%"
      >
        <Breadcrumb
          spacing="8px"
          separator={<ChevronRightIcon color="black" />}
          alignContent="center"
          justifyContent="center"
          ml="35%"
          mt={10}
          fontSize="x-large"
        >
          <BreadcrumbItem>
            <BreadcrumbLink href={HOME} className={css(styles.linkStyles)}>Home</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem isCurrentPage>
            <BreadcrumbLink href="#" className={css(styles.linkStyles)}>Customer Complaint Policy</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </Box>
      <Heading textAlign="center" width="90%">Customer Complaint Policy</Heading>

      <Text textAlign="center" width="90%" fontSize="xl">Please find below the latest Advanta Customer Complaint Policy.</Text>

      <VStack spacing={10} width={isLg ? '70%' : '80%'}>
        <Text fontSize="lg">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi provident doloribus eos
          distinctio accusantium quibusdam facere! Mollitia quas officiis illo in laboriosam. Provident,
          aut minima, eum rem obcaecati ut culpa aliquid nostrum quod at voluptas sed quasi error officia
          totam nesciunt ducimus officiis omnis dicta suscipit in corrupti esse quia.
        </Text>
        <Flex flexDirection="column">
          <Text fontWeight="bold" mb={5} fontSize="xl">1. Lorem, ipsum dolor</Text>
          <Text fontSize="lg">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi provident doloribus eos
            distinctio accusantium quibusdam facere! Mollitia quas officiis illo in laboriosam. Provident,
            aut minima, eum rem obcaecati ut culpa aliquid nostrum quod at voluptas sed quasi error officia
            totam nesciunt ducimus officiis omnis dicta suscipit in corrupti esse quia.
          </Text>
        </Flex>
        <Flex flexDirection="column">
          <Text fontWeight="bold" mb={5} fontSize="xl">2. Lorem, ipsum dolor</Text>
          <Text fontSize="lg">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi provident doloribus eos
            distinctio accusantium quibusdam facere! Mollitia quas officiis illo in laboriosam. Provident,
            aut minima, eum rem obcaecati ut culpa aliquid nostrum quod at voluptas sed quasi error officia
            totam nesciunt ducimus officiis omnis dicta suscipit in corrupti esse quia.
          </Text>
        </Flex>
        <Flex flexDirection="column">
          <Text fontWeight="bold" mb={5} fontSize="xl">3. Lorem, ipsum dolor</Text>
          <Text fontSize="lg">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi provident doloribus eos
            distinctio accusantium quibusdam facere! Mollitia quas officiis illo in laboriosam. Provident,
            aut minima, eum rem obcaecati ut culpa aliquid nostrum quod at voluptas sed quasi error officia
            totam nesciunt ducimus officiis omnis dicta suscipit in corrupti esse quia.
          </Text>
        </Flex>
      </VStack>
    </VStack>
  );
};
const styles = StyleSheet.create({
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    }
  }
});

export default CustomerComplaintPolicyContainer;
