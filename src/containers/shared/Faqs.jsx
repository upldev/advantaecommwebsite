/* eslint-disable no-console */
import {
  Flex, VStack
} from '@chakra-ui/react';
import { StyleSheet } from 'aphrodite';
import React from 'react';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import FAQ from '../../components/shared/FAQ';
import FAQBreadCrumb from './FAQBreadCrumb';

// Code Review Completed

const FaqsContainer = ({
  data, faqs, setData
}) => {
  const { isLg } = useMediaQuery();
  return (
    <>
      {isLg && <FAQBreadCrumb />}
      <VStack mt="10" backgroundColor={colors.light} spacing={10}>

        <Flex mx={isLg ? '100' : '5'}>
          <Flex flexDirection="column" width="100%">
            {faqs && faqs.length > 0 && faqs.map((faq) => {
              return (
                <FAQ key={faq.id} faq={faq} />
              );
            })}
          </Flex>
        </Flex>
        <Flex mb={5} />
      </VStack>
    </>
  );
};
const styles = StyleSheet.create({
  linkStyles: {
    textDecoration: 'none',
    ':hover': {
      color: colors.green
    },
    cursor: 'pointer',
    fontWeight: 'bold',
    color: colors.darkGreen,
    whiteSpace: 'nowrap'
  }
});
export default FaqsContainer;
