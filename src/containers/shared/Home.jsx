import { Stack } from '@chakra-ui/layout';
import React from 'react';
import useMediaQuery from '../../../hooks/useMediaQuery';
import Advertisement from '../../components/home/Advertisement';
import BestSellers from '../../components/home/BestSellers';
import DownloadNowBanner from '../../components/home/DownloadNowBanner';
import FarmerReview from '../../components/home/FarmerReview';
import HomeBanner from '../../components/home/HomeBanner';
import Trending from '../../components/home/Trending';
import AllCategories from '../../components/shared/AllCategories';
import Carousel from '../../components/shared/Carousel';

// Code Review Completed

const HomeContainer = () => {
  const { isLg } = useMediaQuery();

  return (
    <Stack spacing={isLg ? '40px' : '30px'} flexDirection="column">
      <Carousel />
      <HomeBanner />
      <Trending />
      <AllCategories />
      <Advertisement />
      <BestSellers />
      <FarmerReview />
      <DownloadNowBanner />
    </Stack>
  );
};

export default HomeContainer;
