import {
  Container, Flex, Heading, VStack
} from '@chakra-ui/react';
import { API, Auth, graphqlOperation } from 'aws-amplify';
import React from 'react';
import { useDispatch } from 'react-redux';
import ScratchCard from 'react-scratchcard';
import { updateFarmer } from '../../../graphql/mutations';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { showToast } from '../../../store/actions/feedback';

// Code Review Completed

const ScratchCardContainer = ({ discount, }) => {
  const { isSm } = useMediaQuery();
  const dispatch = useDispatch();

  const settings = {
    width: isSm ? 280 : 800,
    height: 300,
    image: '../scratch-card.jpg',
    finishPercent: 50,
    onComplete: async () => {
      try {
        const user = await Auth.currentAuthenticatedUser();
        await API.graphql(graphqlOperation(updateFarmer, {
          input: {
            mobile: user.username,
            secondPurchaseDiscount: discount
          }
        }));
        dispatch(showToast({
          title: 'Discount Added Successfully!',
          status: true
        }));
      } catch (err) {
        console.error(err);
        dispatch(showToast({
          title: 'Failed to add Discount!',
          message: 'Please try again!',
          status: false
        }));
      }
    }
  };

  return (
    <Flex width="full" paddingY={12} alignItems="center" justifyContent="center">
      <VStack>
        {
          isSm
            ? (
              <Heading textAlign="center" fontSize="lg">
                Scratch to Avail Discount
                <br />
                on Second Purchase!
              </Heading>
            )
            : <Heading textAlign="center">Scratch to Avail Discount on Second Purchase!</Heading>
        }
        <ScratchCard {...settings}>
          <Flex width="full" height="300" alignItems="center" justifyContent="center" backgroundColor="purple.100">
            <Container textAlign="center" color="purple">
              {
                discount.isFixed
                  ? <Heading p={3}>{`₹${discount.amount} OFF!`}</Heading>
                  : <Heading p={3}>{`${discount.amount}% OFF!`}</Heading>
              }
              <Heading fontSize="2xl">Make Second Purchase Now!</Heading>
            </Container>
          </Flex>
        </ScratchCard>
      </VStack>
    </Flex>
  );
};

export default ScratchCardContainer;
