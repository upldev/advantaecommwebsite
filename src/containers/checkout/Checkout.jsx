import {
  Box,
  Button,
  Divider,
  Flex,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
  Spinner,
  Stack,
  Text
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { getCartProducts } from '../../../store/actions/cart';
import { displayUserRazorpay } from '../../../store/actions/checkout';
import {
  selectIsInvalid,
  selectOrderItems
} from '../../../store/selectors/cart';
import EmptyCart from '../../components/cart/EmptyCart';
import MobileCheckout from '../../components/cart/MobileCheckout';
import Coupon from '../../components/checkout/Coupon';
import Progress from '../../components/checkout/Progress';
import HomeBanner from '../../components/home/HomeBanner';
// import MobileTrending from '../../components/home/MobileTrending';
// import Trending from '../../components/home/Trending';
import PaymentInfo from '../../components/order/PaymentInfo';
import CartBody from '../account/CartBody';

// Code Review Completed

const CheckoutC = ({ isCartCheckout }) => {
  const router = useRouter();
  const { isLg } = useMediaQuery();
  const dispatch = useDispatch();
  const { isInvalid } = useSelector(selectIsInvalid);
  const { isPaymentLoading, isOrderProcessing } = useSelector(
    (state) => state.feedback
  );
  const { cartItems } = useSelector((state) => state.cart);
  const checkout = useSelector((state) => state.checkout);
  const orderItems = useSelector(selectOrderItems);

  useEffect(() => {
    dispatch(getCartProducts());
  }, [dispatch]);

  const handlePayment = () => {
    dispatch(displayUserRazorpay(orderItems, checkout.currentUser, router));
  };

  return (
    <>
      {cartItems.length > 0 && (
        <Stack spacing="40px" paddingY="40px">
          <Box backgroundColor={colors.light}>
            <Progress progress={3} />
          </Box>
          <Divider marginBottom="30px" alignSelf="center" color={colors.gray} />
          <Box paddingX={isLg ? 100 : 2}>
            <Flex
              flexDirection={isLg ? 'row' : 'column'}
              justifyContent="space-between"
              width="100%"
            >
              <Box width={isLg && '65%'} pr={isLg && '30px'}>
                <CartBody isCartCheckout />
              </Box>
              <Box width={isLg && '35%'}>
                <Coupon />
                <PaymentInfo
                  Action={(
                    <Button
                      isDisabled={isInvalid}
                      onClick={handlePayment}
                      variant="primary"
                      isLoading={isPaymentLoading}
                    >
                      PLACE ORDER
                    </Button>
                  )}
                />
              </Box>
            </Flex>
          </Box>
          <HomeBanner />
          {/* <Trending /> */}
        </Stack>
      )}
      {cartItems.length === 0 && (
        <>
          <Flex justifyContent="center" mt={isLg ? 20 : 2} mb={isLg ? 20 : 2}>
            <EmptyCart />
          </Flex>
          {/* {isLg ? <Trending /> : <MobileTrending />} */}
        </>
      )}
      {!isLg && cartItems.length > 0 && (
        <MobileCheckout isCartCheckout={isCartCheckout} />
      )}
      <Modal size="xs" isOpen={isOrderProcessing}>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>
            <Stack
              height="30vh"
              justifyContent="space-evenly"
              alignItems="center"
            >
              <Image src="/advanta2.png" height="80px" />
              <Spinner color={colors.green} thickness="2px" />
              <Text fontWeight="bold" color={colors.green}>
                Your Order is Processing
              </Text>
            </Stack>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CheckoutC;
