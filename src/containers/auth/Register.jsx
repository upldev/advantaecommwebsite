import { useDisclosure } from '@chakra-ui/hooks';
import {
  Button,
  Divider,
  Flex,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
  Spinner,
  Stack,
  Text
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { LOGIN } from '../../../lib/router';
import GetOTP from '../../components/auth/GetOTP';
import RegisterForm from '../../components/auth/RegisterForm';

// Code Review Completed

const RegisterContainer = () => {
  const [cognitoUser, setCognitoUser] = useState(null);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [profile, setProfile] = useState(null);
  const router = useRouter();
  const { isMd } = useMediaQuery();
  const { isRegistering } = useSelector((state) => state.feedback);

  return (
    <Flex
      alignItems="center"
      justifyContent="center"
      py={isMd ? 3 : 6}
      flexDir="column"
    >
      <Text
        variant={isMd ? 'heading' : 'title'}
        textAlign="center"
        paddingY={isMd ? 1 : 3}
        width="full"
      >
        REGISTER
      </Text>
      <Text color={colors.gray} mb={3}>
        Login instead
      </Text>
      <Button
        variant="primary"
        size={isMd ? 'md' : 'sm'}
        paddingX={9}
        onClick={() => router.push(LOGIN)}
      >
        LOGIN
      </Button>
      <Flex width={isMd ? 'full' : '90%'} pt={6} pb={3}>
        <Divider color={colors.gray} mt={3} />
        <Text flexShrink={0} paddingX={3}>
          Or
        </Text>
        <Divider color={colors.gray} mt={3} />
      </Flex>
      <GetOTP
        isNewUser
        cognitoUser={cognitoUser}
        setCognitoUser={setCognitoUser}
        profile={profile}
        isOpen={isOpen}
        onClose={onClose}
      />
      <RegisterForm
        setCognitoUser={setCognitoUser}
        setProfile={setProfile}
        onOpen={onOpen}
      />
      <Modal size="xs" isOpen={isRegistering}>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>
            <Stack
              height="30vh"
              justifyContent="space-evenly"
              alignItems="center"
            >
              <Image src="/advanta2.png" height="80px" />
              <Spinner color={colors.green} thickness="2px" />
              <Text fontWeight="bold" color={colors.green}>
                Your Profile is getting registered
              </Text>
            </Stack>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

export default RegisterContainer;
