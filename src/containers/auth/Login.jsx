import { useDisclosure } from '@chakra-ui/hooks';
import { ChevronRightIcon } from '@chakra-ui/icons';
import {
  Box,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
  Icon,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalOverlay,
  Spinner,
  Stack,
  Text
} from '@chakra-ui/react';
import { css, StyleSheet } from 'aphrodite';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { AiOutlineHome } from 'react-icons/ai';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../../../constants/colors';
import useMediaQuery from '../../../hooks/useMediaQuery';
import { showToast } from '../../../store/actions/feedback';
import GetOTP from '../../components/auth/GetOTP';
import LoginForm from '../../components/auth/LoginForm';

// Code Review Completed

const LoginContainer = () => {
  const { isMd } = useMediaQuery();
  const [cognitoUser, setCognitoUser] = useState(null);
  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const { query } = router;
  const { redirectBackTo } = query;
  const { isRegistering } = useSelector((state) => state.feedback);

  useEffect(() => {
    if (redirectBackTo) {
      dispatch(
        showToast({
          title: 'Login!',
          message: 'Please login to continue.',
          status: false,
        })
      );
    }
  }, [dispatch, redirectBackTo]);
  return (
    <Stack width="full">
      {isMd ? (
        <Flex
          width="full"
          justifyContent="center"
          alignItems="center"
          px={3}
          pt={12}
          pb={3}
        >
          <Image src="./advanta-bw-login.png" width="200px" />
        </Flex>
      ) : (
        <>
          <Flex bgColor={colors.light} boxShadow="md" pt="3" pb="3" px="100">
            <Breadcrumb
              spacing="8px"
              separator={<ChevronRightIcon boxSize="1.6rem" />}
            >
              <BreadcrumbItem>
                <BreadcrumbLink href="/">
                  <Icon
                    variant="ghost"
                    as={AiOutlineHome}
                    boxSize="1.2rem"
                    mb="1"
                  />
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbItem isCurrentPage>
                <BreadcrumbLink className={css(styles.breadcrumbStyling)}>
                  Login Page
                </BreadcrumbLink>
              </BreadcrumbItem>
            </Breadcrumb>
          </Flex>
          <Text
            variant={isMd ? 'subtitle' : 'title'}
            textAlign="center"
            paddingY={6}
            borderBottomWidth="1px"
            borderColor={colors.gray}
            width="full"
          >
            LOG IN TO YOUR ACCOUNT
          </Text>
        </>
      )}
      <Flex
        justifyContent={isMd ? 'center' : 'flex-start'}
        paddingX={isMd ? 3 : '100px'}
        paddingY={isMd ? 3 : 12}
      >
        {!isMd && (
          <Box>
            <img
              src="/login-img.png"
              alt="login"
              style={{ minWidth: '40vw' }}
            />
          </Box>
        )}
        <GetOTP
          isNewUser={false}
          cognitoUser={cognitoUser}
          setCognitoUser={setCognitoUser}
          isOpen={isOpen}
          onClose={onClose}
        />
        <LoginForm setCognitoUser={setCognitoUser} onOpen={onOpen} />
      </Flex>
      <Modal size="xs" isOpen={isRegistering}>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>
            <Stack
              height="30vh"
              justifyContent="space-evenly"
              alignItems="center"
            >
              <Image src="/advanta2.png" height="80px" />
              <Spinner color={colors.green} thickness="2px" />
              <Text fontWeight="bold" color={colors.green}>
                Your Profile is getting registered
              </Text>
            </Stack>
          </ModalBody>
        </ModalContent>
      </Modal>
    </Stack>
  );
};

const styles = StyleSheet.create({
  breadcrumbStyling: {
    ':hover': {
      textDecoration: 'none',
      color: colors.lightBlue,
    },
  },
});

export default LoginContainer;
