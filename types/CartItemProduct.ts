import { SKU } from './Product';

export interface CartItemProduct {
  materialCode: string;
  crop: string;
  product: string;
  category: string;
  productImg: string;
  similarProducts: string;
  avgrating: number;
  totalreviews: number;
  sku: SKU;
  qty: number;
}
