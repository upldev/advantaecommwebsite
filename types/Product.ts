export interface ProductSpec {
  key: string;
  value: string;
}

export interface SKU {
  sapSkuCode: string;
  ecomCode: string;
  packSize: number;
  packUnit: string;
  MRP: number;
  discount: number;
  ecomPrice: number;
  qtyAvailable: number;
  isTrending: number;
  isBestSeller: number;
  minStockLevel: string;
  maxStockLevel: string;
}

export interface Product {
  materialCode: string;
  materialGroup: string;
  prodHier: string;
  filterCategory: string;
  category: string;
  crop: string;
  product: string;
  productImg: string;
  productVideo: string;
  testimonialVideo: string;
  sku: SKU[];
  specs: ProductSpec[];
  similarProducts: string;
  avgrating: number;
  totalreviews: number;
  isTrending: boolean;
  isBestSeller: boolean;
  isPublic: boolean;
}
