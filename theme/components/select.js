import { colors } from '../../constants/colors';
import Input from './input';

const baseStyle = {
  field: {
    ...Input.baseStyle.field,
    bg: colors.white,
    appearance: 'none',
    paddingBottom: '1px',
    lineHeight: 'normal',
    borderRadius: 0,
    borderColor: colors.black,
    colors: colors.black,
    '> option, > optgroup': {
      bg: colors.white,
    },
  },
  icon: {
    width: '1.5rem',
    height: '100%',
    insetEnd: '0.5rem',
    position: 'relative',
    color: 'currentColor',
    fontSize: '1.25rem',
    _disabled: {
      opacity: 0.5,
    },
  },
};

const sizes = {
  ...Input.sizes,
  xs: {
    ...Input.sizes.xs,
    icon: { insetEnd: '0.25rem' },
  },
};

export default {
  baseStyle,
  sizes,
  variants: Input.variants,
  defaultProps: Input.defaultProps,
};
