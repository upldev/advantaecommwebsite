import { colors } from '../../constants/colors';

export const textStyles = {
  baseStyle: {},
  sizes: {},
  variants: {
    title: {
      fontSize: '36px',
      fontWeight: 'bold'
    },
    heading: {
      fontSize: '20px',
      fontWeight: 'bold',
      color: colors.black
    },
    subTitle: {
      fontSize: '18px',
      fontWeight: 'bold',
      color: colors.black
    },
    headingWhite: {
      fontSize: '20px',
      fontWeight: 'bold',
      color: colors.white
    },
    subTitleWhite: {
      fontSize: '18px',
      fontWeight: 'bold',
      color: colors.white
    },
    mdTextRegular: {
      fontSize: '18px',
      color: colors.black
    },
    mdPriceOff: {
      fontSize: '16px',
      color: colors.gray
    },
    textRegular: {
      fontSize: '12px',
      color: colors.black
    },
    textRegularWhite: {
      fontSize: '14px',
      color: colors.white
    },
    mobilePriceOff: {
      fontSize: '11px',
      color: colors.gray
    },
    numberRegular: {
      fontSize: '13px',
      color: colors.black
    },
    emaphasizeTextRegular: {
      fontSize: '16px',
      fontWeight: 'bold',
      color: colors.black
    },
    default: {
      fontSize: '16px',
      color: colors.black
    },

  },
  defaultProps: {
    fontSize: '16px',
    color: colors.black
  },
};
