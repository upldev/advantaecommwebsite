import { colors } from '../../constants/colors';

export const dividerStyles = {
  baseStyle: {
    color: colors.lightGray,
    paddingTop: '1px',
  },
};
