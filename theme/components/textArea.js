import Input from './input';

const baseStyle = {
  ...Input.baseStyle.field,
  paddingY: '8px',
  minHeight: '80px',
  lineHeight: 'short',
  verticalAlign: 'top',
};

export default { baseStyle, ...Input };
