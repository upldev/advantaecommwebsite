import { darken, whiten } from '@chakra-ui/theme-tools';
import { colors } from '../../constants/colors';

export const buttonStyles = {
  baseStyle: {},
  sizes: {
    md: {
      px: '18px',
    },
  },
  variants: {
    primary: {
      bg: colors.green,
      color: colors.white,
      _hover: {
        bg: whiten(colors.green, 20),
        _disabled: {
          opacity: 0.4,
          bg: colors.green,
        },
      },
      borderRadius: 0,
    },
    addToCart: {
      bg: colors.green,
      color: colors.white,
      _hover: {
        bg: whiten(colors.lightGold, 20),
        color: colors.advantaBlue,
      },
      borderRadius: 0,
    },
    disabledAddToCart: {
      bg: colors.gray,
      color: colors.white,
      borderColor: colors.gray,
      borderWidth: '1px',
      _hover: {
        color: colors.gray,
        bg: colors.white,
      },
      borderRadius: 0,
    },
    primary_disabled: {
      bg: colors.darkGreen,
      color: colors.white,
      borderColor: colors.darkGreen,
      borderWidth: '1px',
      _hover: {
        color: colors.darkGreen,
        bg: colors.white,
      },
      borderRadius: 0,
    },
    outline_primary: {
      bg: colors.white,
      color: colors.green,
      borderWidth: '1px',
      borderColor: colors.green,
      _hover: {
        bg: colors.green,
        color: colors.white,
      },
      borderRadius: 0,
    },
    primaryOutline: {
      bg: colors.white,
      color: colors.green,
      borderWidth: '1px',
      borderColor: colors.green,
      _hover: {
        bg: colors.green,
        color: colors.white,
      },
      borderRadius: 0,
    },
    greyOutline: {
      bg: colors.white,
      color: colors.gray,
      borderWidth: '1px',
      borderColor: colors.gray,
    },
    secondary: {
      bg: colors.lightBlue,
      color: colors.white,
      _hover: {
        opacity: 0.8,
        _disabled: {
          opacity: 0.4,
          bg: colors.lightBlue,
        }
      },
      borderRadius: 0,
    },
    outline_secondary: {
      bg: colors.white,
      color: colors.lightBlue,
      borderWidth: '1px',
      borderColor: colors.lightBlue,
      _hover: {
        bg: colors.lightBlue,
        color: colors.white,
      },
      borderRadius: 0,
    },
    danger: {
      bg: colors.lightRed,
      color: colors.white,
      _hover: {
        bg: whiten(colors.lightRed, 20),
      },
      borderRadius: 0,
    },
    google: {
      bg: colors.white,
      color: colors.gray,
      _hover: {
        bg: whiten(colors.gray, 80),
      },
    },
    checkout: {
      bg: colors.lightGold,
      color: colors.black,
      _hover: {
        bg: whiten(colors.lightGold, 30),
      },
      borderRadius: 0,
    },
    outline_black: {
      bg: colors.white,
      color: colors.black,
      borderWidth: '1px',
      borderColor: colors.black,
      _hover: {
        bg: colors.black,
        color: colors.white,
      },
      borderRadius: 0,
    },
    gray: {
      bg: colors.gray,
      color: colors.white,
      _hover: {
        bg: whiten(colors.gray, 30),
      },
      borderRadius: 0,
    },
    default: {
      bg: colors.white,
      color: colors.black,
      _hover: {
        bg: darken(colors.white, 80),
        color: whiten(colors.black, 80),
      },
      borderRadius: 0,
    },
  },
};
