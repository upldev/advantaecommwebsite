import { extendTheme } from '@chakra-ui/react';
import { colors } from '../constants/colors';
import { accordionStyles as Accordion } from './components/accordion';
import { buttonStyles as Button } from './components/button';
import Checkbox from './components/checkbox';
import { dividerStyles as Divider } from './components/divider';
import Image from './components/image';
import Input from './components/input';
import Modal from './components/modal';
import Radio from './components/radio';
import Select from './components/select';
import { textStyles as Text } from './components/text';
import TextArea from './components/textArea';

export const theme = extendTheme({
  colors: {
    primary: colors.darkGreen,
    secondary: colors.advantaBlue,
    danger: colors.lightRed,
    checkout: colors.lightGold,
  },
  components: {
    Button,
    Divider,
    Accordion,
    Text,
    Input,
    Select,
    Modal,
    Checkbox,
    Radio,
    Image,
    TextArea
  },
  fonts: {
    body: 'Noto Sans',
    heading: 'Noto Sans',
  },
});
