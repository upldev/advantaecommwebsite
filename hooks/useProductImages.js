import { useEffect, useState } from 'react';
import { getImages } from '../lib/utils/getImages';

export const useProductImages = (images) => {
  const [urls, setUrls] = useState();

  useEffect(() => {
    (async () => {
      const fetchedUrls = await getImages(images);
      setUrls(fetchedUrls);
    })();
  }, [images]);

  return urls || [];
};
