import { useCallback } from 'react';

export const useTextEscape = () => {
  const escape = useCallback((input) => {
    return input?.replace(/[^a-zA-Z0-9\s.,@]/g, '');
  }, []);

  return { escape };
};
