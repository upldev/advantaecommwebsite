import { Auth } from 'aws-amplify';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  sessionActivated,
  sessionExpired,
  setUserProfile
} from '../store/actions/user';

export const useSessionExpiry = () => {
  const dispatch = useDispatch();
  const session = useSelector((state) => state.session.activeSession);

  useEffect(() => {
    async function checkUser() {
      try {
        await Auth.currentAuthenticatedUser();
        if (!session) {
          dispatch(sessionActivated()); // update state to logged in
        }
      } catch (error) {
        if (session) dispatch(sessionExpired());
      }
    }
    const timerId = setInterval(() => {
      checkUser();
    }, 5000);
    return () => clearInterval(timerId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session]);

  useEffect(() => {
    if (session) dispatch(setUserProfile());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // useEffect(() => {
  //   if (session) {
  //     window.addEventListener('beforeunload', handleLogout);
  //   }

  //   return () => window.removeEventListener('beforeunload', handleLogout);
  // }, [dispatch, handleLogout, session]);

  // const handleLogout = useCallback(
  //   (event) => {
  //     // event.preventDefault();
  //     // event.returnValue = '';
  //     // dispatch(userLogout());
  //   },
  //   [dispatch]
  // );
};
