import API from '@aws-amplify/api';
import { useEffect, useState } from 'react';
import { getProductSku } from '../graphql/customQueries';

export const useSKU = (materialCode) => {
  const [sku, setSku] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const getSku = async () => {
      try {
        const { data } = await API.graphql({
          query: getProductSku,
          variables: {
            materialCode,
          },
          authMode: 'API_KEY',
        });

        const sortedSku = data.getProduct.sku.sort(
          (a, b) => b.qtyAvailable - a.qtyAvailable
        );

        setSku(sortedSku);
      } catch (e) {
        console.error(e);
      } finally {
        setIsLoading(false);
      }
    };

    getSku();
  }, [materialCode]);

  return { sku, isLoading };
};
