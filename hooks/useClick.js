import API from '@aws-amplify/api';

const useClick = (banner) => {
  const handleClick = async () => {
    window.open(banner.redirectUrl, '_blank', 'noreferrer');

    try {
      await API.post('EAMS5TAPI05', '/incBannerClick', {
        body: {
          bannerId: banner.bannerId,
        },
        authMode: 'AWS_IAM',
      });
    } catch (error) {
      console.error(error);
    }
  };

  return { handleClick };
};

export default useClick;
