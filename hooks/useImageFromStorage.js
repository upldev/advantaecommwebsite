import Storage from '@aws-amplify/storage';
import { useEffect, useState } from 'react';

export const useImageFromStorage = (image) => {
  const [url, setUrl] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      try {
        const imgUrl = await Storage.get(image);
        setUrl(imgUrl);
      } catch (error) {
        console.error(error);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [image]);

  return { url, isLoading };
};
