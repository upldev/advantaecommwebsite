import { useEffect, useState } from 'react';

export const usePackSize = (input) => {
  const [size, setSize] = useState('');

  useEffect(() => {
    if (input < 1.00) {
      setSize(`${input * 1000} GM`);
    } else {
      setSize(`${input} KG`);
    }
  }, [input]);

  return size;
};
