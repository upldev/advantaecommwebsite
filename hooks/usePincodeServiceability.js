import API from '@aws-amplify/api';
import { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { PIN_SERVICEABLE } from '../store/actions/user';

const usePincodeServiceability = (input = '') => {
  const dispatch = useDispatch();
  const [isDeliverable, setIsDeliverable] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    (async () => {
      if (
        (typeof input === 'string' && input.length !== 6)
        || input.toString().length !== 6
      ) {
        return;
      }

      setIsLoading(true);

      try {
        const data = await API.post('EAMS5TAPI05', '/checkPincode', {
          body: {
            pincode: input,
          },
        });

        if (!data.servicable) {
          setIsDeliverable(false);
        } else {
          setIsDeliverable(true);
        }
      } catch (error) {
        console.error(error);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [input]);

  const checkPincode = useCallback(async (pincode) => {
    setIsLoading(true);

    const updateStore = (city, servicable) => {
      dispatch({
        type: PIN_SERVICEABLE,
        payload: {
          check: {
            pincode,
            city,
            servicable,
          },
        },
      });
    };

    try {
      const data = await API.post('EAMS5TAPI05', '/checkPincode', {
        body: {
          pincode,
        },
      });

      if (!data.servicable) {
        updateStore(data.desc, data.servicable);
        setIsDeliverable(false);
      } else {
        updateStore(data.desc, data.servicable);
        setIsDeliverable(true);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }, [dispatch]);

  return { isDeliverable, checkPincode, isLoading };
};

export default usePincodeServiceability;
