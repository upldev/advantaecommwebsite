import { useEffect, useState } from 'react';

export const useIndianCurrencyFormatter = (input = 0) => {
  const [result, setResult] = useState('');

  useEffect(() => {
    if (!input && input !== 0) return;

    setResult(
      input.toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
    );
  }, [input]);

  return result;
};
