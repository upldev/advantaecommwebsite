/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateGlobalLocks = /* GraphQL */ `
  subscription OnCreateGlobalLocks {
    onCreateGlobalLocks {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGlobalLocks = /* GraphQL */ `
  subscription OnUpdateGlobalLocks {
    onUpdateGlobalLocks {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGlobalLocks = /* GraphQL */ `
  subscription OnDeleteGlobalLocks {
    onDeleteGlobalLocks {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const onCreateIntruder = /* GraphQL */ `
  subscription OnCreateIntruder {
    onCreateIntruder {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateIntruder = /* GraphQL */ `
  subscription OnUpdateIntruder {
    onUpdateIntruder {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteIntruder = /* GraphQL */ `
  subscription OnDeleteIntruder {
    onDeleteIntruder {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const onCreateFarmer = /* GraphQL */ `
  subscription OnCreateFarmer($owner: String) {
    onCreateFarmer(owner: $owner) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateFarmer = /* GraphQL */ `
  subscription OnUpdateFarmer($owner: String) {
    onUpdateFarmer(owner: $owner) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteFarmer = /* GraphQL */ `
  subscription OnDeleteFarmer($owner: String) {
    onDeleteFarmer(owner: $owner) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateReferrer = /* GraphQL */ `
  subscription OnCreateReferrer($owner: String) {
    onCreateReferrer(owner: $owner) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateReferrer = /* GraphQL */ `
  subscription OnUpdateReferrer($owner: String) {
    onUpdateReferrer(owner: $owner) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteReferrer = /* GraphQL */ `
  subscription OnDeleteReferrer($owner: String) {
    onDeleteReferrer(owner: $owner) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateReferee = /* GraphQL */ `
  subscription OnCreateReferee($owner: String) {
    onCreateReferee(owner: $owner) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateReferee = /* GraphQL */ `
  subscription OnUpdateReferee($owner: String) {
    onUpdateReferee(owner: $owner) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteReferee = /* GraphQL */ `
  subscription OnDeleteReferee($owner: String) {
    onDeleteReferee(owner: $owner) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateSAPStock = /* GraphQL */ `
  subscription OnCreateSAPStock {
    onCreateSAPStock {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSAPStock = /* GraphQL */ `
  subscription OnUpdateSAPStock {
    onUpdateSAPStock {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSAPStock = /* GraphQL */ `
  subscription OnDeleteSAPStock {
    onDeleteSAPStock {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePackaging = /* GraphQL */ `
  subscription OnCreatePackaging {
    onCreatePackaging {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const onUpdatePackaging = /* GraphQL */ `
  subscription OnUpdatePackaging {
    onUpdatePackaging {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const onDeletePackaging = /* GraphQL */ `
  subscription OnDeletePackaging {
    onDeletePackaging {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const onCreatePayment = /* GraphQL */ `
  subscription OnCreatePayment($owner: String) {
    onCreatePayment(owner: $owner) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdatePayment = /* GraphQL */ `
  subscription OnUpdatePayment($owner: String) {
    onUpdatePayment(owner: $owner) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeletePayment = /* GraphQL */ `
  subscription OnDeletePayment($owner: String) {
    onDeletePayment(owner: $owner) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateOrderSequenceNumber = /* GraphQL */ `
  subscription OnCreateOrderSequenceNumber {
    onCreateOrderSequenceNumber {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateOrderSequenceNumber = /* GraphQL */ `
  subscription OnUpdateOrderSequenceNumber {
    onUpdateOrderSequenceNumber {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteOrderSequenceNumber = /* GraphQL */ `
  subscription OnDeleteOrderSequenceNumber {
    onDeleteOrderSequenceNumber {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const onCreateOrder = /* GraphQL */ `
  subscription OnCreateOrder($owner: String) {
    onCreateOrder(owner: $owner) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const onUpdateOrder = /* GraphQL */ `
  subscription OnUpdateOrder($owner: String) {
    onUpdateOrder(owner: $owner) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const onDeleteOrder = /* GraphQL */ `
  subscription OnDeleteOrder($owner: String) {
    onDeleteOrder(owner: $owner) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const onCreateComplaint = /* GraphQL */ `
  subscription OnCreateComplaint($owner: String) {
    onCreateComplaint(owner: $owner) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateComplaint = /* GraphQL */ `
  subscription OnUpdateComplaint($owner: String) {
    onUpdateComplaint(owner: $owner) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteComplaint = /* GraphQL */ `
  subscription OnDeleteComplaint($owner: String) {
    onDeleteComplaint(owner: $owner) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateDiscountData = /* GraphQL */ `
  subscription OnCreateDiscountData {
    onCreateDiscountData {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDiscountData = /* GraphQL */ `
  subscription OnUpdateDiscountData {
    onUpdateDiscountData {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDiscountData = /* GraphQL */ `
  subscription OnDeleteDiscountData {
    onDeleteDiscountData {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateProductDiscount = /* GraphQL */ `
  subscription OnCreateProductDiscount {
    onCreateProductDiscount {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateProductDiscount = /* GraphQL */ `
  subscription OnUpdateProductDiscount {
    onUpdateProductDiscount {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteProductDiscount = /* GraphQL */ `
  subscription OnDeleteProductDiscount {
    onDeleteProductDiscount {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const onCreateCartDiscount = /* GraphQL */ `
  subscription OnCreateCartDiscount {
    onCreateCartDiscount {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateCartDiscount = /* GraphQL */ `
  subscription OnUpdateCartDiscount {
    onUpdateCartDiscount {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteCartDiscount = /* GraphQL */ `
  subscription OnDeleteCartDiscount {
    onDeleteCartDiscount {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDiscountApplied = /* GraphQL */ `
  subscription OnCreateDiscountApplied($owner: String) {
    onCreateDiscountApplied(owner: $owner) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateDiscountApplied = /* GraphQL */ `
  subscription OnUpdateDiscountApplied($owner: String) {
    onUpdateDiscountApplied(owner: $owner) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteDiscountApplied = /* GraphQL */ `
  subscription OnDeleteDiscountApplied($owner: String) {
    onDeleteDiscountApplied(owner: $owner) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateBanner = /* GraphQL */ `
  subscription OnCreateBanner {
    onCreateBanner {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateBanner = /* GraphQL */ `
  subscription OnUpdateBanner {
    onUpdateBanner {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteBanner = /* GraphQL */ `
  subscription OnDeleteBanner {
    onDeleteBanner {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const onCreateProductReview = /* GraphQL */ `
  subscription OnCreateProductReview($owner: String) {
    onCreateProductReview(owner: $owner) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateProductReview = /* GraphQL */ `
  subscription OnUpdateProductReview($owner: String) {
    onUpdateProductReview(owner: $owner) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteProductReview = /* GraphQL */ `
  subscription OnDeleteProductReview($owner: String) {
    onDeleteProductReview(owner: $owner) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateProduct = /* GraphQL */ `
  subscription OnCreateProduct {
    onCreateProduct {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateProduct = /* GraphQL */ `
  subscription OnUpdateProduct {
    onUpdateProduct {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteProduct = /* GraphQL */ `
  subscription OnDeleteProduct {
    onDeleteProduct {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const onCreateFaq = /* GraphQL */ `
  subscription OnCreateFaq {
    onCreateFaq {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateFaq = /* GraphQL */ `
  subscription OnUpdateFaq {
    onUpdateFaq {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteFaq = /* GraphQL */ `
  subscription OnDeleteFaq {
    onDeleteFaq {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
