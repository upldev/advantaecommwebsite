/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createGlobalLocks = /* GraphQL */ `
  mutation CreateGlobalLocks(
    $input: CreateGlobalLocksInput!
    $condition: ModelGlobalLocksConditionInput
  ) {
    createGlobalLocks(input: $input, condition: $condition) {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const updateGlobalLocks = /* GraphQL */ `
  mutation UpdateGlobalLocks(
    $input: UpdateGlobalLocksInput!
    $condition: ModelGlobalLocksConditionInput
  ) {
    updateGlobalLocks(input: $input, condition: $condition) {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const deleteGlobalLocks = /* GraphQL */ `
  mutation DeleteGlobalLocks(
    $input: DeleteGlobalLocksInput!
    $condition: ModelGlobalLocksConditionInput
  ) {
    deleteGlobalLocks(input: $input, condition: $condition) {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const createBanner = /* GraphQL */ `
  mutation CreateBanner(
    $input: CreateBannerInput!
    $condition: ModelBannerConditionInput
  ) {
    createBanner(input: $input, condition: $condition) {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const updateBanner = /* GraphQL */ `
  mutation UpdateBanner(
    $input: UpdateBannerInput!
    $condition: ModelBannerConditionInput
  ) {
    updateBanner(input: $input, condition: $condition) {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const deleteBanner = /* GraphQL */ `
  mutation DeleteBanner(
    $input: DeleteBannerInput!
    $condition: ModelBannerConditionInput
  ) {
    deleteBanner(input: $input, condition: $condition) {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const createIntruder = /* GraphQL */ `
  mutation CreateIntruder(
    $input: CreateIntruderInput!
    $condition: ModelIntruderConditionInput
  ) {
    createIntruder(input: $input, condition: $condition) {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const updateIntruder = /* GraphQL */ `
  mutation UpdateIntruder(
    $input: UpdateIntruderInput!
    $condition: ModelIntruderConditionInput
  ) {
    updateIntruder(input: $input, condition: $condition) {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const deleteIntruder = /* GraphQL */ `
  mutation DeleteIntruder(
    $input: DeleteIntruderInput!
    $condition: ModelIntruderConditionInput
  ) {
    deleteIntruder(input: $input, condition: $condition) {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const createFarmer = /* GraphQL */ `
  mutation CreateFarmer(
    $input: CreateFarmerInput!
    $condition: ModelFarmerConditionInput
  ) {
    createFarmer(input: $input, condition: $condition) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateFarmer = /* GraphQL */ `
  mutation UpdateFarmer(
    $input: UpdateFarmerInput!
    $condition: ModelFarmerConditionInput
  ) {
    updateFarmer(input: $input, condition: $condition) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteFarmer = /* GraphQL */ `
  mutation DeleteFarmer(
    $input: DeleteFarmerInput!
    $condition: ModelFarmerConditionInput
  ) {
    deleteFarmer(input: $input, condition: $condition) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createReferrer = /* GraphQL */ `
  mutation CreateReferrer(
    $input: CreateReferrerInput!
    $condition: ModelReferrerConditionInput
  ) {
    createReferrer(input: $input, condition: $condition) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateReferrer = /* GraphQL */ `
  mutation UpdateReferrer(
    $input: UpdateReferrerInput!
    $condition: ModelReferrerConditionInput
  ) {
    updateReferrer(input: $input, condition: $condition) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteReferrer = /* GraphQL */ `
  mutation DeleteReferrer(
    $input: DeleteReferrerInput!
    $condition: ModelReferrerConditionInput
  ) {
    deleteReferrer(input: $input, condition: $condition) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createReferee = /* GraphQL */ `
  mutation CreateReferee(
    $input: CreateRefereeInput!
    $condition: ModelRefereeConditionInput
  ) {
    createReferee(input: $input, condition: $condition) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateReferee = /* GraphQL */ `
  mutation UpdateReferee(
    $input: UpdateRefereeInput!
    $condition: ModelRefereeConditionInput
  ) {
    updateReferee(input: $input, condition: $condition) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteReferee = /* GraphQL */ `
  mutation DeleteReferee(
    $input: DeleteRefereeInput!
    $condition: ModelRefereeConditionInput
  ) {
    deleteReferee(input: $input, condition: $condition) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createProductReview = /* GraphQL */ `
  mutation CreateProductReview(
    $input: CreateProductReviewInput!
    $condition: ModelProductReviewConditionInput
  ) {
    createProductReview(input: $input, condition: $condition) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateProductReview = /* GraphQL */ `
  mutation UpdateProductReview(
    $input: UpdateProductReviewInput!
    $condition: ModelProductReviewConditionInput
  ) {
    updateProductReview(input: $input, condition: $condition) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteProductReview = /* GraphQL */ `
  mutation DeleteProductReview(
    $input: DeleteProductReviewInput!
    $condition: ModelProductReviewConditionInput
  ) {
    deleteProductReview(input: $input, condition: $condition) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createSAPStock = /* GraphQL */ `
  mutation CreateSAPStock(
    $input: CreateSAPStockInput!
    $condition: ModelSAPStockConditionInput
  ) {
    createSAPStock(input: $input, condition: $condition) {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const updateSAPStock = /* GraphQL */ `
  mutation UpdateSAPStock(
    $input: UpdateSAPStockInput!
    $condition: ModelSAPStockConditionInput
  ) {
    updateSAPStock(input: $input, condition: $condition) {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const deleteSAPStock = /* GraphQL */ `
  mutation DeleteSAPStock(
    $input: DeleteSAPStockInput!
    $condition: ModelSAPStockConditionInput
  ) {
    deleteSAPStock(input: $input, condition: $condition) {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const createProduct = /* GraphQL */ `
  mutation CreateProduct(
    $input: CreateProductInput!
    $condition: ModelProductConditionInput
  ) {
    createProduct(input: $input, condition: $condition) {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const updateProduct = /* GraphQL */ `
  mutation UpdateProduct(
    $input: UpdateProductInput!
    $condition: ModelProductConditionInput
  ) {
    updateProduct(input: $input, condition: $condition) {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const deleteProduct = /* GraphQL */ `
  mutation DeleteProduct(
    $input: DeleteProductInput!
    $condition: ModelProductConditionInput
  ) {
    deleteProduct(input: $input, condition: $condition) {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const createPackaging = /* GraphQL */ `
  mutation CreatePackaging(
    $input: CreatePackagingInput!
    $condition: ModelPackagingConditionInput
  ) {
    createPackaging(input: $input, condition: $condition) {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const updatePackaging = /* GraphQL */ `
  mutation UpdatePackaging(
    $input: UpdatePackagingInput!
    $condition: ModelPackagingConditionInput
  ) {
    updatePackaging(input: $input, condition: $condition) {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const deletePackaging = /* GraphQL */ `
  mutation DeletePackaging(
    $input: DeletePackagingInput!
    $condition: ModelPackagingConditionInput
  ) {
    deletePackaging(input: $input, condition: $condition) {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const createPayment = /* GraphQL */ `
  mutation CreatePayment(
    $input: CreatePaymentInput!
    $condition: ModelPaymentConditionInput
  ) {
    createPayment(input: $input, condition: $condition) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updatePayment = /* GraphQL */ `
  mutation UpdatePayment(
    $input: UpdatePaymentInput!
    $condition: ModelPaymentConditionInput
  ) {
    updatePayment(input: $input, condition: $condition) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deletePayment = /* GraphQL */ `
  mutation DeletePayment(
    $input: DeletePaymentInput!
    $condition: ModelPaymentConditionInput
  ) {
    deletePayment(input: $input, condition: $condition) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createOrderSequenceNumber = /* GraphQL */ `
  mutation CreateOrderSequenceNumber(
    $input: CreateOrderSequenceNumberInput!
    $condition: ModelOrderSequenceNumberConditionInput
  ) {
    createOrderSequenceNumber(input: $input, condition: $condition) {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const updateOrderSequenceNumber = /* GraphQL */ `
  mutation UpdateOrderSequenceNumber(
    $input: UpdateOrderSequenceNumberInput!
    $condition: ModelOrderSequenceNumberConditionInput
  ) {
    updateOrderSequenceNumber(input: $input, condition: $condition) {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const deleteOrderSequenceNumber = /* GraphQL */ `
  mutation DeleteOrderSequenceNumber(
    $input: DeleteOrderSequenceNumberInput!
    $condition: ModelOrderSequenceNumberConditionInput
  ) {
    deleteOrderSequenceNumber(input: $input, condition: $condition) {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const createOrder = /* GraphQL */ `
  mutation CreateOrder(
    $input: CreateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    createOrder(input: $input, condition: $condition) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const updateOrder = /* GraphQL */ `
  mutation UpdateOrder(
    $input: UpdateOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    updateOrder(input: $input, condition: $condition) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const deleteOrder = /* GraphQL */ `
  mutation DeleteOrder(
    $input: DeleteOrderInput!
    $condition: ModelOrderConditionInput
  ) {
    deleteOrder(input: $input, condition: $condition) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const createComplaint = /* GraphQL */ `
  mutation CreateComplaint(
    $input: CreateComplaintInput!
    $condition: ModelComplaintConditionInput
  ) {
    createComplaint(input: $input, condition: $condition) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateComplaint = /* GraphQL */ `
  mutation UpdateComplaint(
    $input: UpdateComplaintInput!
    $condition: ModelComplaintConditionInput
  ) {
    updateComplaint(input: $input, condition: $condition) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteComplaint = /* GraphQL */ `
  mutation DeleteComplaint(
    $input: DeleteComplaintInput!
    $condition: ModelComplaintConditionInput
  ) {
    deleteComplaint(input: $input, condition: $condition) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createFaq = /* GraphQL */ `
  mutation CreateFaq(
    $input: CreateFaqInput!
    $condition: ModelFaqConditionInput
  ) {
    createFaq(input: $input, condition: $condition) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const updateFaq = /* GraphQL */ `
  mutation UpdateFaq(
    $input: UpdateFaqInput!
    $condition: ModelFaqConditionInput
  ) {
    updateFaq(input: $input, condition: $condition) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const deleteFaq = /* GraphQL */ `
  mutation DeleteFaq(
    $input: DeleteFaqInput!
    $condition: ModelFaqConditionInput
  ) {
    deleteFaq(input: $input, condition: $condition) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const createDiscountData = /* GraphQL */ `
  mutation CreateDiscountData(
    $input: CreateDiscountDataInput!
    $condition: ModelDiscountDataConditionInput
  ) {
    createDiscountData(input: $input, condition: $condition) {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateDiscountData = /* GraphQL */ `
  mutation UpdateDiscountData(
    $input: UpdateDiscountDataInput!
    $condition: ModelDiscountDataConditionInput
  ) {
    updateDiscountData(input: $input, condition: $condition) {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteDiscountData = /* GraphQL */ `
  mutation DeleteDiscountData(
    $input: DeleteDiscountDataInput!
    $condition: ModelDiscountDataConditionInput
  ) {
    deleteDiscountData(input: $input, condition: $condition) {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const createProductDiscount = /* GraphQL */ `
  mutation CreateProductDiscount(
    $input: CreateProductDiscountInput!
    $condition: ModelProductDiscountConditionInput
  ) {
    createProductDiscount(input: $input, condition: $condition) {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const updateProductDiscount = /* GraphQL */ `
  mutation UpdateProductDiscount(
    $input: UpdateProductDiscountInput!
    $condition: ModelProductDiscountConditionInput
  ) {
    updateProductDiscount(input: $input, condition: $condition) {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const deleteProductDiscount = /* GraphQL */ `
  mutation DeleteProductDiscount(
    $input: DeleteProductDiscountInput!
    $condition: ModelProductDiscountConditionInput
  ) {
    deleteProductDiscount(input: $input, condition: $condition) {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const createCartDiscount = /* GraphQL */ `
  mutation CreateCartDiscount(
    $input: CreateCartDiscountInput!
    $condition: ModelCartDiscountConditionInput
  ) {
    createCartDiscount(input: $input, condition: $condition) {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const updateCartDiscount = /* GraphQL */ `
  mutation UpdateCartDiscount(
    $input: UpdateCartDiscountInput!
    $condition: ModelCartDiscountConditionInput
  ) {
    updateCartDiscount(input: $input, condition: $condition) {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const deleteCartDiscount = /* GraphQL */ `
  mutation DeleteCartDiscount(
    $input: DeleteCartDiscountInput!
    $condition: ModelCartDiscountConditionInput
  ) {
    deleteCartDiscount(input: $input, condition: $condition) {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const createDiscountApplied = /* GraphQL */ `
  mutation CreateDiscountApplied(
    $input: CreateDiscountAppliedInput!
    $condition: ModelDiscountAppliedConditionInput
  ) {
    createDiscountApplied(input: $input, condition: $condition) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateDiscountApplied = /* GraphQL */ `
  mutation UpdateDiscountApplied(
    $input: UpdateDiscountAppliedInput!
    $condition: ModelDiscountAppliedConditionInput
  ) {
    updateDiscountApplied(input: $input, condition: $condition) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteDiscountApplied = /* GraphQL */ `
  mutation DeleteDiscountApplied(
    $input: DeleteDiscountAppliedInput!
    $condition: ModelDiscountAppliedConditionInput
  ) {
    deleteDiscountApplied(input: $input, condition: $condition) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
