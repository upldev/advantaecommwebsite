export const getMinProductDetails = /* GraphQL */ `
  query GetProduct($materialCode: String!) {
    getProduct(materialCode: $materialCode) {
      materialCode
      crop
      product
      category
      productImg
      similarProducts
      avgrating
      totalreviews
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      createdAt
      updatedAt
    }
  }
`;

export const listMinDiscountsApplied = /* GraphQL */ `
  query ListDiscountApplieds(
    $discountApplyId: String
    $filter: ModelDiscountAppliedFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listDiscountApplieds(
      discountApplyId: $discountApplyId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        discountId
        orderDate
      }
    }
  }
`;

export const getProductSku = /* GraphQL */ `
  query GetProduct($materialCode: String!) {
    getProduct(materialCode: $materialCode) {
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
    }
  }
`;
