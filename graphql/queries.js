/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getGlobalLocks = /* GraphQL */ `
  query GetGlobalLocks($lockId: String!) {
    getGlobalLocks(lockId: $lockId) {
      lockId
      sapCustCreateLock
      sapCustUpdateLock
      sapPaymentLock
      sapOrderLock
      bluedartOrderLock
      createdAt
      updatedAt
    }
  }
`;
export const listGlobalLocks = /* GraphQL */ `
  query ListGlobalLocks(
    $lockId: String
    $filter: ModelGlobalLocksFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listGlobalLocks(
      lockId: $lockId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        lockId
        sapCustCreateLock
        sapCustUpdateLock
        sapPaymentLock
        sapOrderLock
        bluedartOrderLock
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getIntruder = /* GraphQL */ `
  query GetIntruder($mobile: AWSPhone!) {
    getIntruder(mobile: $mobile) {
      mobile
      lastLogin
      numberOfHits
      createdAt
      updatedAt
    }
  }
`;
export const listIntruders = /* GraphQL */ `
  query ListIntruders(
    $mobile: AWSPhone
    $filter: ModelIntruderFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listIntruders(
      mobile: $mobile
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        mobile
        lastLogin
        numberOfHits
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getFarmer = /* GraphQL */ `
  query GetFarmer($mobile: AWSPhone!) {
    getFarmer(mobile: $mobile) {
      identityId
      profileImg
      name
      mobile
      billing {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      email
      fb
      google
      customerSAPCode
      sapUpdate
      sapCreateLock
      firstPurchase
      secondPurchase
      secondPurchaseDiscount {
        amount
        isFixed
      }
      cart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      wishList
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listFarmers = /* GraphQL */ `
  query ListFarmers(
    $mobile: AWSPhone
    $filter: ModelFarmerFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listFarmers(
      mobile: $mobile
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        identityId
        profileImg
        name
        mobile
        billing {
          addressId
          name
          address
          state
          city
          pincode
          isDefault
        }
        shipping {
          addressId
          name
          address
          state
          city
          pincode
          isDefault
        }
        email
        fb
        google
        customerSAPCode
        sapUpdate
        sapCreateLock
        firstPurchase
        secondPurchase
        secondPurchaseDiscount {
          amount
          isFixed
        }
        cart {
          materialCode
          product
          sku {
            sapSkuCode
            ecomCode
            packSize
            packUnit
            MRP
            discount
            ecomPrice
            qtyAvailable
            isTrending
            isBestSeller
            minStockLevel
            maxStockLevel
          }
          qty
          itemDiscount
        }
        wishList
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getReferrer = /* GraphQL */ `
  query GetReferrer($referralCode: String!) {
    getReferrer(referralCode: $referralCode) {
      referralCode
      farmerGenPhone
      farmerGenName
      farmerGenAmt
      farmerConAmt
      farmerGenConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listReferrers = /* GraphQL */ `
  query ListReferrers(
    $referralCode: String
    $filter: ModelReferrerFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listReferrers(
      referralCode: $referralCode
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        referralCode
        farmerGenPhone
        farmerGenName
        farmerGenAmt
        farmerConAmt
        farmerGenConsumed
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getReferee = /* GraphQL */ `
  query GetReferee($referralCode: String!) {
    getReferee(referralCode: $referralCode) {
      referralCode
      farmerConPhone
      farmerConName
      farmerConConsumed
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listReferees = /* GraphQL */ `
  query ListReferees(
    $referralCode: String
    $filter: ModelRefereeFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listReferees(
      referralCode: $referralCode
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        referralCode
        farmerConPhone
        farmerConName
        farmerConConsumed
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getSAPStock = /* GraphQL */ `
  query GetSAPStock($sapStockCode: String!) {
    getSAPStock(sapStockCode: $sapStockCode) {
      sapStockCode
      sapSkuCode
      plantCode
      qtyAvailable
      packUnit
      packSize
      netSkuWeight
      grossSkuWeight
      salesUOM
      conversionFactor
      createdAt
      updatedAt
    }
  }
`;
export const listSAPStocks = /* GraphQL */ `
  query ListSAPStocks(
    $sapStockCode: String
    $filter: ModelSAPStockFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listSAPStocks(
      sapStockCode: $sapStockCode
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        sapStockCode
        sapSkuCode
        plantCode
        qtyAvailable
        packUnit
        packSize
        netSkuWeight
        grossSkuWeight
        salesUOM
        conversionFactor
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPackaging = /* GraphQL */ `
  query GetPackaging($packageId: String!) {
    getPackaging(packageId: $packageId) {
      packageId
      sku
      particulars
      minCapacityInKg
      maxCapacityInKg
      lengthInMM
      widthInMM
      heightInMM
      weightInKg
      createdAt
      updatedAt
    }
  }
`;
export const listPackagings = /* GraphQL */ `
  query ListPackagings(
    $packageId: String
    $filter: ModelPackagingFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listPackagings(
      packageId: $packageId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        packageId
        sku
        particulars
        minCapacityInKg
        maxCapacityInKg
        lengthInMM
        widthInMM
        heightInMM
        weightInKg
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getPayment = /* GraphQL */ `
  query GetPayment($ecomPaymentId: String!) {
    getPayment(ecomPaymentId: $ecomPaymentId) {
      ecomPaymentId
      ecomOrderId
      amount
      sapPaymentId
      sapLock
      farmerMobile
      rzpPaymentId
      rzpOrderId
      isMobile
      paymentMethod
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listPayments = /* GraphQL */ `
  query ListPayments(
    $ecomPaymentId: String
    $filter: ModelPaymentFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listPayments(
      ecomPaymentId: $ecomPaymentId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        ecomPaymentId
        ecomOrderId
        amount
        sapPaymentId
        sapLock
        farmerMobile
        rzpPaymentId
        rzpOrderId
        isMobile
        paymentMethod
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getOrderSequenceNumber = /* GraphQL */ `
  query GetOrderSequenceNumber($tableId: String!) {
    getOrderSequenceNumber(tableId: $tableId) {
      tableId
      masterNumber
      lastMasterUpdated
      fcNumber
      lastFcUpdated
      vcNumber
      lastVcUpdated
      frNumber
      lastFrUpdated
      createdAt
      updatedAt
    }
  }
`;
export const listOrderSequenceNumbers = /* GraphQL */ `
  query ListOrderSequenceNumbers(
    $tableId: String
    $filter: ModelOrderSequenceNumberFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listOrderSequenceNumbers(
      tableId: $tableId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        tableId
        masterNumber
        lastMasterUpdated
        fcNumber
        lastFcUpdated
        vcNumber
        lastVcUpdated
        frNumber
        lastFrUpdated
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getOrder = /* GraphQL */ `
  query GetOrder($orderId: String!) {
    getOrder(orderId: $orderId) {
      orderId
      masterOrderId
      farmerMobile
      orderDate
      shipping {
        addressId
        name
        address
        state
        city
        pincode
        isDefault
      }
      profitCenter
      plantCode
      division
      orderCart {
        materialCode
        product
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        qty
        itemDiscount
      }
      totalPayable
      totalQty
      totalItemDiscount
      totalHeaderDiscount
      totalEcomPrice
      orderStatus
      orderDelivered
      invoiceUrl
      bluedartAwbNo
      bluedartTrack {
        pickupDate
        pickupTime
        estimatedDeliveryDate
        statusDate
        statusTime
        scanResults {
          status
          date
          time
          location
        }
      }
      sapOrderId
      sapTriggerLock
      shippingCharges
      createdAt
      isMobile
      isOrderDetailMail
      is24Mail
      is48Mail
      updatedAt
      owner
    }
  }
`;
export const listOrders = /* GraphQL */ `
  query ListOrders(
    $orderId: String
    $filter: ModelOrderFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listOrders(
      orderId: $orderId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        orderId
        masterOrderId
        farmerMobile
        orderDate
        shipping {
          addressId
          name
          address
          state
          city
          pincode
          isDefault
        }
        profitCenter
        plantCode
        division
        orderCart {
          materialCode
          product
          sku {
            sapSkuCode
            ecomCode
            packSize
            packUnit
            MRP
            discount
            ecomPrice
            qtyAvailable
            isTrending
            isBestSeller
            minStockLevel
            maxStockLevel
          }
          qty
          itemDiscount
        }
        totalPayable
        totalQty
        totalItemDiscount
        totalHeaderDiscount
        totalEcomPrice
        orderStatus
        orderDelivered
        invoiceUrl
        bluedartAwbNo
        bluedartTrack {
          pickupDate
          pickupTime
          estimatedDeliveryDate
          statusDate
          statusTime
          scanResults {
            status
            date
            time
            location
          }
        }
        sapOrderId
        sapTriggerLock
        shippingCharges
        createdAt
        isMobile
        isOrderDetailMail
        is24Mail
        is48Mail
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getComplaint = /* GraphQL */ `
  query GetComplaint($complaintId: String!) {
    getComplaint(complaintId: $complaintId) {
      complaintId
      farmerMobile
      farmerName
      complaintDate
      complaintResolvedDate
      complaintType
      complaintDesc
      isClarification
      status
      remarks
      isMail
      is24Mail
      is48Mail
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listComplaints = /* GraphQL */ `
  query ListComplaints(
    $complaintId: String
    $filter: ModelComplaintFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listComplaints(
      complaintId: $complaintId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        complaintId
        farmerMobile
        farmerName
        complaintDate
        complaintResolvedDate
        complaintType
        complaintDesc
        isClarification
        status
        remarks
        isMail
        is24Mail
        is48Mail
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getDiscountData = /* GraphQL */ `
  query GetDiscountData($firstPurchaseDiscountId: String!) {
    getDiscountData(firstPurchaseDiscountId: $firstPurchaseDiscountId) {
      multipleApplicable
      autoApply
      firstPurchase {
        amount
        isFixed
      }
      secondPurchase {
        amount
        isFixed
      }
      firstPurchaseDiscountId
      secondPurchaseDiscountId
      firstPurchaseActive
      secondPurchaseActive
      cartValueBasedActive
      cartQtyBasedActive
      productQtyBasedActive
      referralActive
      refereeDiscount {
        amount
        isFixed
      }
      referralDiscount {
        amount
        isFixed
      }
      createdAt
      updatedAt
    }
  }
`;
export const listDiscountData = /* GraphQL */ `
  query ListDiscountData(
    $firstPurchaseDiscountId: String
    $filter: ModelDiscountDataFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listDiscountData(
      firstPurchaseDiscountId: $firstPurchaseDiscountId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        multipleApplicable
        autoApply
        firstPurchase {
          amount
          isFixed
        }
        secondPurchase {
          amount
          isFixed
        }
        firstPurchaseDiscountId
        secondPurchaseDiscountId
        firstPurchaseActive
        secondPurchaseActive
        cartValueBasedActive
        cartQtyBasedActive
        productQtyBasedActive
        referralActive
        refereeDiscount {
          amount
          isFixed
        }
        referralDiscount {
          amount
          isFixed
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getProductDiscount = /* GraphQL */ `
  query GetProductDiscount($discountId: String!) {
    getProductDiscount(discountId: $discountId) {
      discountId
      couponName
      materialCode
      sku
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isQtyBased
      qty
      createdAt
      updatedAt
    }
  }
`;
export const listProductDiscounts = /* GraphQL */ `
  query ListProductDiscounts(
    $discountId: String
    $filter: ModelProductDiscountFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listProductDiscounts(
      discountId: $discountId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        discountId
        couponName
        materialCode
        sku
        startDate
        endDate
        isActive
        discount {
          amount
          isFixed
        }
        availFreq {
          times
          period
        }
        isQtyBased
        qty
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getCartDiscount = /* GraphQL */ `
  query GetCartDiscount($discountId: String!) {
    getCartDiscount(discountId: $discountId) {
      discountId
      couponName
      startDate
      endDate
      isActive
      discount {
        amount
        isFixed
      }
      availFreq {
        times
        period
      }
      isCartQtyBased
      isCartValueBased
      qty
      lessThan
      greaterThan
      createdAt
      updatedAt
    }
  }
`;
export const listCartDiscounts = /* GraphQL */ `
  query ListCartDiscounts(
    $discountId: String
    $filter: ModelCartDiscountFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listCartDiscounts(
      discountId: $discountId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        discountId
        couponName
        startDate
        endDate
        isActive
        discount {
          amount
          isFixed
        }
        availFreq {
          times
          period
        }
        isCartQtyBased
        isCartValueBased
        qty
        lessThan
        greaterThan
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDiscountApplied = /* GraphQL */ `
  query GetDiscountApplied($discountApplyId: String!) {
    getDiscountApplied(discountApplyId: $discountApplyId) {
      discountApplyId
      orderId
      farmerMobile
      discountId
      discount {
        amount
        isFixed
      }
      orderDate
      isMobile
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listDiscountApplieds = /* GraphQL */ `
  query ListDiscountApplieds(
    $discountApplyId: String
    $filter: ModelDiscountAppliedFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listDiscountApplieds(
      discountApplyId: $discountApplyId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        discountApplyId
        orderId
        farmerMobile
        discountId
        discount {
          amount
          isFixed
        }
        orderDate
        isMobile
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const sapStockBySku = /* GraphQL */ `
  query SapStockBySku(
    $sapSkuCode: String!
    $sortDirection: ModelSortDirection
    $filter: ModelSAPStockFilterInput
    $limit: Int
    $nextToken: String
  ) {
    sapStockBySku(
      sapSkuCode: $sapSkuCode
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        sapStockCode
        sapSkuCode
        plantCode
        qtyAvailable
        packUnit
        packSize
        netSkuWeight
        grossSkuWeight
        salesUOM
        conversionFactor
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const orderByFarmer = /* GraphQL */ `
  query OrderByFarmer(
    $farmerMobile: String!
    $orderDate: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelOrderFilterInput
    $limit: Int
    $nextToken: String
  ) {
    orderByFarmer(
      farmerMobile: $farmerMobile
      orderDate: $orderDate
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        orderId
        masterOrderId
        farmerMobile
        orderDate
        shipping {
          addressId
          name
          address
          state
          city
          pincode
          isDefault
        }
        profitCenter
        plantCode
        division
        orderCart {
          materialCode
          product
          sku {
            sapSkuCode
            ecomCode
            packSize
            packUnit
            MRP
            discount
            ecomPrice
            qtyAvailable
            isTrending
            isBestSeller
            minStockLevel
            maxStockLevel
          }
          qty
          itemDiscount
        }
        totalPayable
        totalQty
        totalItemDiscount
        totalHeaderDiscount
        totalEcomPrice
        orderStatus
        orderDelivered
        invoiceUrl
        bluedartAwbNo
        bluedartTrack {
          pickupDate
          pickupTime
          estimatedDeliveryDate
          statusDate
          statusTime
          scanResults {
            status
            date
            time
            location
          }
        }
        sapOrderId
        sapTriggerLock
        shippingCharges
        createdAt
        isMobile
        isOrderDetailMail
        is24Mail
        is48Mail
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getBanner = /* GraphQL */ `
  query GetBanner($bannerId: String!) {
    getBanner(bannerId: $bannerId) {
      bannerId
      bannerImg
      bannerMobileImg
      bannerType
      clicks
      redirectUrl
      isActive
      createdAt
      updatedAt
    }
  }
`;
export const listBanners = /* GraphQL */ `
  query ListBanners(
    $bannerId: String
    $filter: ModelBannerFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listBanners(
      bannerId: $bannerId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        bannerId
        bannerImg
        bannerMobileImg
        bannerType
        clicks
        redirectUrl
        isActive
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getProductReview = /* GraphQL */ `
  query GetProductReview($reviewId: String!) {
    getProductReview(reviewId: $reviewId) {
      reviewId
      images
      profileImg
      identityId
      materialCode
      category
      crop
      product
      farmerMobile
      farmerName
      reviewDate
      advantaKisan
      rating
      review
      isModerated
      isPublic
      reply {
        replyId
        content
        createdAt
      }
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listProductReviews = /* GraphQL */ `
  query ListProductReviews(
    $reviewId: String
    $filter: ModelProductReviewFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listProductReviews(
      reviewId: $reviewId
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        reviewId
        images
        profileImg
        identityId
        materialCode
        category
        crop
        product
        farmerMobile
        farmerName
        reviewDate
        advantaKisan
        rating
        review
        isModerated
        isPublic
        reply {
          replyId
          content
          createdAt
        }
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getProduct = /* GraphQL */ `
  query GetProduct($materialCode: String!) {
    getProduct(materialCode: $materialCode) {
      materialCode
      materialGroup
      prodHier
      filterCategory
      category
      crop
      product
      productImg
      productVideo
      testimonialVideo
      sku {
        sapSkuCode
        ecomCode
        packSize
        packUnit
        MRP
        discount
        ecomPrice
        qtyAvailable
        isTrending
        isBestSeller
        minStockLevel
        maxStockLevel
      }
      specs {
        key
        value
      }
      similarProducts
      avgrating
      totalreviews
      isTrending
      isBestSeller
      isPublic
      createdAt
      updatedAt
    }
  }
`;
export const listProducts = /* GraphQL */ `
  query ListProducts(
    $materialCode: String
    $filter: ModelProductFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listProducts(
      materialCode: $materialCode
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        materialCode
        materialGroup
        prodHier
        filterCategory
        category
        crop
        product
        productImg
        productVideo
        testimonialVideo
        sku {
          sapSkuCode
          ecomCode
          packSize
          packUnit
          MRP
          discount
          ecomPrice
          qtyAvailable
          isTrending
          isBestSeller
          minStockLevel
          maxStockLevel
        }
        specs {
          key
          value
        }
        similarProducts
        avgrating
        totalreviews
        isTrending
        isBestSeller
        isPublic
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getFaq = /* GraphQL */ `
  query GetFaq($id: ID!) {
    getFaq(id: $id) {
      id
      question
      answer
      createdAt
      updatedAt
    }
  }
`;
export const listFaqs = /* GraphQL */ `
  query ListFaqs(
    $filter: ModelFaqFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listFaqs(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        question
        answer
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
