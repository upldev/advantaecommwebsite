import { useMediaQuery } from '@chakra-ui/media-query';

const withMediaQueries = (WrappedComponent) => {
  const MediaQueriesHOC = (props) => {
    const [isXs] = useMediaQuery('(max-width: 299.98px)');
    const [isSm] = useMediaQuery('(max-width: 575.98px)');
    const [isMd] = useMediaQuery('(max-width: 991.98px)');
    const [isLg] = useMediaQuery('(min-width: 992px)');

    return <WrappedComponent {...props} isXs={isXs} isSm={isSm} isMd={isMd} isLg={isLg} />;
  };

  return MediaQueriesHOC;
};

export default withMediaQueries;
