export const CROSS_FILTER_CROP = 'CROSS_FILTER_CROP';
export const RESET_FILTER_CROP = 'RESET_FILTER_CROP';
export const TICK_CROP = 'CROSS_FILTER_CROP';
export const SET_CROP = 'SET_CROP';

export const toggleCropFilter = (id) => (dispatch) => {
  dispatch({
    type: TICK_CROP,
    payload: id,
  });
};

export const crossFilter = (id) => (dispatch) => {
  dispatch({
    type: CROSS_FILTER_CROP,
    payload: id
  });
};
