export const SHOW_TOAST = 'SHOW_TOAST';
export const SHOW_LOADING = 'TOGGLE_LOADING';
export const RESET_TOAST = 'RESET_TOAST';
export const SET_LOADER = 'SET_LOADER';

export const showLoading = (visible) => ({
  type: SHOW_LOADING,
  payload: visible,
});

export const setLoader = (loader) => ({
  type: SET_LOADER,
  payload: loader,
});

export const showToast = ({ message, title, status }) => ({
  type: SHOW_TOAST,
  payload: { message, title, status },
});

export const resetToast = () => ({ type: RESET_TOAST, payload: null });
