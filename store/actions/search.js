import API from '@aws-amplify/api';
import { listProducts } from '../../graphql/queries';
import { formatProducts } from '../../lib/utils/formatProducts';

export const SET_PHRASE = 'SET_PHRASE';
export const SET_FILTERED_PRODUCTS = 'SET_FILTERED_PRODUCTS';
export const SET_SEARCH_LOADING = 'SET_SEARCH_LOADING';

export const setPhrase = (phrase) => (dispatch) => {
  dispatch({ type: SET_PHRASE, payload: phrase });
  dispatch(searchProducts());
};

export const searchProducts = () => async (dispatch, getState) => {
  const {
    search: { phrase },
  } = getState();

  dispatch(setSearchLoading(true));

  const phraseFilters = phrase !== ''
    ? [
      { category: { contains: phrase } },
      { category: { contains: phrase.toUpperCase() } },
      { category: { contains: phrase.toLowerCase() } },
      {
        category: { contains: phrase[0].toUpperCase() + phrase.slice(1) },
      },
      { crop: { contains: phrase } },
      { crop: { contains: phrase.toUpperCase() } },
      { crop: { contains: phrase.toLowerCase() } },
      { crop: { contains: phrase[0].toUpperCase() + phrase.slice(1) } },
      { product: { contains: phrase } },
      { product: { contains: phrase.toUpperCase() } },
      { product: { contains: phrase.toLowerCase() } },
      {
        product: { contains: phrase[0].toUpperCase() + phrase.slice(1) },
      },
      { filterCategory: { contains: phrase } },
      { filterCategory: { contains: phrase.toUpperCase() } },
      { filterCategory: { contains: phrase.toLowerCase() } },
      {
        filterCategory: {
          contains: phrase[0].toUpperCase() + phrase.slice(1),
        },
      },
    ]
    : [];

  try {
    const result = await API.graphql({
      query: listProducts,
      authMode: 'API_KEY',
      variables: {
        filter:
          phraseFilters.length > 0
            ? { and: [{ isPublic: { eq: true } }, { or: [...phraseFilters] }] }
            : { isPublic: { eq: true } },
      },
    });

    const formatedProducts = await Promise.all(
      result.data.listProducts.items.map(async (item) => {
        const product = await formatProducts(item);
        return product;
      })
    );

    dispatch({
      type: SET_FILTERED_PRODUCTS,
      payload: formatedProducts,
    });
  } catch (error) {
    console.error(error);
  } finally {
    dispatch(setSearchLoading(false));
  }
};

export const setSearchLoading = (isSearching) => ({
  type: SET_SEARCH_LOADING,
  payload: isSearching,
});
