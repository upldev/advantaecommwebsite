import { setCatFilteredProducts } from './product';

export const TICK_CAT = 'TICK_CAT';
export const CROSS_FILTER_CAT = 'CROSS_FILTER_CAT';
export const RESET_FILTER_CAT = 'RESET_FILTER_CAT';
export const SET_CATEGORIES = 'SET_CATEGORIES';

export const toggleCatFilter = (id) => (dispatch, getState) => {
  const {
    filteredProducts,
    products,
    subcategoryFiltered,
    categoryFiltered,
    priceFiltered,
  } = getState().product;
  const { filter, subCategory, category } = getState();
  const priceFilters = filter.priceRange;
  const scatFilters = Object.entries(subCategory).filter((i) => i[1]).length;
  const catFilters = Object.entries(category).filter((i) => i[1]).length;
  if (category[id]) {
    // remove filter
    if (catFilters - 1 === 0) {
      let filtering = [];
      if (scatFilters && priceFilters) {
        filtering = intersection(subcategoryFiltered, priceFiltered);
      } else if (!scatFilters && priceFilters) {
        filtering = priceFiltered;
      } else if (!priceFilters && scatFilters) {
        filtering = subcategoryFiltered;
      } else {
        filtering = products;
      }
      dispatch(
        setCatFilteredProducts({
          filteredProducts: filtering,
          categoryFiltered: [],
        })
      );
    } else {
      const catFiltered = categoryFiltered.filter(
        (item) => id !== item.category
      );
      const filtered = filteredProducts.filter((item) => id !== item.category);
      dispatch(
        setCatFilteredProducts({
          filteredProducts: filtered,
          categoryFiltered: catFiltered,
        })
      );
    }
  } else {
    // add subcategory
    let catFiltered = [];
    if (catFilters) {
      catFiltered = products.filter((item) => id === item.category);
      catFiltered = [...new Set([...catFiltered, ...categoryFiltered])];
    } else {
      catFiltered = products.filter((item) => id === item.category);
    }
    let filtering = [];
    if (scatFilters && priceFilters) {
      filtering = intersection(catFiltered, subcategoryFiltered);
      filtering = intersection(filtering, priceFiltered);
    } else if (!scatFilters && priceFilters) {
      filtering = intersection(catFiltered, priceFiltered);
    } else if (!priceFilters && scatFilters) {
      filtering = intersection(catFiltered, subcategoryFiltered);
    } else {
      filtering = catFiltered;
    }
    dispatch(
      setCatFilteredProducts({
        filteredProducts: filtering,
        categoryFiltered: catFiltered,
      })
    );
  }
  dispatch({
    type: TICK_CAT,
    payload: id,
  });
};

export const crossCatFilter = (id) => (dispatch) => {
  dispatch({
    type: CROSS_FILTER_CAT,
    payload: id,
  });
};

export const resetCatFilters = () => (dispatch, getState) => {
  const { category } = getState();
  Object.keys(category).forEach((v) => {
    category[v] = false;
  });
  dispatch({
    type: RESET_FILTER_CAT,
    payload: category,
  });
};

export const setCategories = () => (dispatch, getState) => {
  const { availableCategories } = getState().product;
  const { category } = getState();
  const categories = {};
  availableCategories.forEach((cat) => {
    categories[cat[0]] = category && category[cat[0]] ? category[cat[0]] : false;
  });
  dispatch({
    type: SET_CATEGORIES,
    payload: categories,
  });
};

const intersection = (products1, products2) => products1.filter((product1) => products2.some(
  (product2) => product1.materialCode === product2.materialCode
));
