import { setScatFilteredProducts } from './product';

export const TICK_SCAT = 'TICK_SCAT';
export const CROSS_FILTER_SCAT = 'CROSS_FILTER_SCAT';
export const RESET_FILTER_SCAT = 'RESET_FILTER_SCAT';
export const SET_SUBCATEGORIES = 'SET_SUBCATEGORIES';

export const toggleSCatFilter = (id) => (dispatch, getState) => {
  const {
    filteredProducts, products, subcategoryFiltered, categoryFiltered, priceFiltered
  } = getState().product;
  const { filter, subCategory, category } = getState();
  const priceFilters = filter.priceRange;
  const scatFilters = Object.entries(subCategory).filter((i) => i[1]).length;
  const catFilters = Object.entries(category).filter((i) => i[1]).length;
  if (subCategory[id]) { // remove filter
    if (scatFilters - 1 === 0) {
      let filtering = [];
      if (catFilters && priceFilters) {
        filtering = intersection(categoryFiltered, priceFiltered);
      } else if (!catFilters && priceFilters) {
        filtering = priceFiltered;
      } else if (!priceFilters && catFilters) {
        filtering = categoryFiltered;
      } else {
        filtering = products;
      }
      dispatch(setScatFilteredProducts({
        filteredProducts: filtering,
        subcategoryFiltered: []
      }));
    } else {
      const scatFiltered = subcategoryFiltered.filter((item) => id !== item.crop);
      const filtered = filteredProducts.filter((item) => id !== item.crop);
      dispatch(setScatFilteredProducts({
        filteredProducts: filtered,
        subcategoryFiltered: scatFiltered
      }));
    }
  } else { // add subcategory
    let scatFiltered = [];
    if (scatFilters) {
      scatFiltered = products.filter((item) => id === item.crop);
      scatFiltered = [...new Set([...scatFiltered, ...subcategoryFiltered])];
    } else {
      scatFiltered = products.filter((item) => id === item.crop);
    }
    let filtering = [];
    if (catFilters && priceFilters) {
      filtering = intersection(scatFiltered, categoryFiltered);
      filtering = intersection(filtering, priceFiltered);
    } else if (!catFilters && priceFilters) {
      filtering = intersection(scatFiltered, priceFiltered);
    } else if (!priceFilters && catFilters) {
      filtering = intersection(scatFiltered, categoryFiltered);
    } else {
      filtering = scatFiltered;
    }
    dispatch(setScatFilteredProducts({
      filteredProducts: filtering,
      subcategoryFiltered: scatFiltered
    }));
  }
  dispatch({
    type: TICK_SCAT,
    payload: id,
  });
};

export const crossFilter = (id) => (dispatch) => {
  dispatch({
    type: CROSS_FILTER_SCAT,
    payload: id
  });
};

export const resetSCatFilters = () => (dispatch, getState) => {
  const { subCategory } = getState();
  Object.keys(subCategory).forEach((v) => { subCategory[v] = false; });
  dispatch({
    type: RESET_FILTER_SCAT,
    payload: subCategory
  });
};

export const setSubcategories = () => (dispatch, getState) => {
  const { availableSubcategories } = getState().product;
  const { subCategory } = getState();
  const subcategories = {};
  availableSubcategories.forEach((cat) => {
    subcategories[cat[0]] = subCategory && subCategory[cat[0]] ? subCategory[cat[0]] : false;
  });
  dispatch({
    type: SET_SUBCATEGORIES,
    payload: subcategories
  });
};

const intersection = (products1, products2) => (
  products1.filter((product1) => products2.some((product2) => product1.materialCode
  === product2.materialCode))
);
