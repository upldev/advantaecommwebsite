import {
  API, Auth, graphqlOperation, Storage
} from 'aws-amplify';
import md5 from 'md5';
import Router from 'next/router';
import uuid from 'uuid';
import { createFarmer, updateFarmer } from '../../graphql/mutations';
import { getFarmer, getProduct } from '../../graphql/queries';
import { CREATE_USER_PROFILE, HOME } from '../../lib/router';
import { setAlert } from './alert';
import { logInCartUpdate, resetCart, resetWishlist } from './cart';
import { resetCheckout } from './checkout';
import { setLoader, showLoading, showToast } from './feedback';
import { loginWishListUpdate } from './wishlist';

export const USER_REGISTER = 'USER_REGISTER';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';
export const SET_USER_PROFILE = 'SET_USER_PROFILE';
export const UPDATE_USER_PROFILE = 'UPDATE_USER_PROFILE';

export const PIN_SERVICEABLE = 'PIN_SERVICEABLE';

export const ADD_ORDERS = 'ADD_ORDERS';
export const SET_ORDER_FILTER = 'SET_ORDER_FILTER';
export const RESET_ORDER_FILTER = 'RESET_ORDER_FILTER';

export const ACTIVATE_SESSION = 'ACTIVATE_SESSION';
export const SAVE_REGISTRATION_DATA = 'SAVE_REGISTRATION_DATA';
export const CLEAR_REGISTRATION_DATA = 'CLEAR_REGISTRATION_DATA';

export const saveRegistered = (profile) => ({
  type: SAVE_REGISTRATION_DATA,
  payload: profile,
});

export const sessionExpired = () => ({
  type: USER_LOGOUT,
});

export const sessionActivated = () => ({
  type: ACTIVATE_SESSION,
});

export const userLogIn = () => async (dispatch, getState) => {
  const {
    logInStatus, profile, cart, wishList
  } = await getCurrentUser();
  if (logInStatus) {
    if (profile.name === '') {
      dispatch({
        type: USER_LOGIN,
        payload: {
          logInStatus,
          hasProfile: false,
          profile: {
            name: '',
          },
        },
      });
      dispatch(
        showToast({
          title: 'Profile not created!',
          message: 'Please create a profile',
          status: false,
        })
      );
      Router.push(CREATE_USER_PROFILE);
      return;
    }
    const userServiceable = await checkUserServiceble(profile);
    dispatch({
      type: USER_LOGIN,
      payload: {
        logInStatus,
        hasProfile: true,
        profile,
        userServiceable,
        check: userServiceable,
      },
    });
    try {
      let updateCloudCart = true;
      let updateCloudWishList = true;
      if (cart.length > 0) {
        dispatch(logInCartUpdate(cart)); // cloud cart items added to local cart
        updateCloudCart = false;
      }

      if (wishList.length > 0) {
        dispatch(loginWishListUpdate(wishList)); // cloud wish list items added to local wish list
        updateCloudWishList = false;
      }

      if (updateCloudWishList || updateCloudCart) {
        const { cartItems } = getState().cart;
        const { wishlistItems } = getState().wishlist;
        if (
          (cartItems && cartItems.length > 0)
          || (wishlistItems && wishlistItems.length > 0)
        ) {
          const { username } = await Auth.currentAuthenticatedUser();

          const input = {
            mobile: username,
            ...(updateCloudCart && { cart: cartItems }),
            ...(updateCloudWishList && { wishList: wishlistItems }),
          };

          await API.graphql({
            query: updateFarmer,
            variables: {
              input,
            },
          });
        }
      }
    } catch (error) {
      console.error(error);
    }
  } else {
    dispatch({
      type: USER_LOGIN,
      payload: {
        logInStatus: false,
        profile: null,
      },
    });
  }
};

export const userLogout = () => async (dispatch) => {
  dispatch(showLoading(true));
  try {
    await Auth.signOut();
    dispatch({
      type: USER_LOGOUT,
    });
    dispatch(resetCart());
    dispatch(resetCheckout());
    dispatch(resetWishlist());
    dispatch(resetOrderFilter());

    Router.push(HOME);
    dispatch(
      showToast({
        title: 'Logged out successfully!',
        status: true,
      })
    );
  } catch (error) {
    console.error(error);
    dispatch(
      showToast({
        title: 'Error',
        message: `${error.code}`,
        status: false,
      })
    );
  } finally {
    dispatch(showLoading(false));
  }
};

export const setUserProfile = () => async (dispatch) => {
  const { logInStatus, profile } = await getCurrentUser();
  if (logInStatus) {
    if (profile.name === '') {
      dispatch({
        type: USER_LOGIN,
        payload: {
          logInStatus,
          hasProfile: false,
          profile: {
            name: '',
          },
        },
      });
      dispatch(
        showToast({
          title: 'Profile not created!',
          message: 'Please create a profile',
          status: false,
        })
      );
      Router.push(CREATE_USER_PROFILE);
      return;
    }
    const userServiceable = await checkUserServiceble(profile);
    dispatch({
      type: SET_USER_PROFILE,
      payload: {
        logInStatus,
        hasProfile: true,
        profile,
        userServiceable,
        check: userServiceable,
      },
    });
  }
};

export const createUserProfile = ({
  cognitoUser, profile, code, redirect, isRegister, onClose
}) => async (dispatch, getState) => {
  const { profileImg } = profile;
  if (isRegister) {
    try {
      await Auth.sendCustomChallengeAnswer(cognitoUser, md5(code));
      await Auth.currentSession();
      if (onClose) onClose();
      dispatch(setLoader({ isRegistering: true }));
    } catch (err) {
      if (
        err.code === 'NotAuthorizedException'
          || err === 'No current user'
      ) {
        dispatch(
          setAlert({
            title: 'OTP entered is incorrect!',
            message: 'Please try again.',
            status: false,
            type: 'otp',
          })
        );
      } else {
        dispatch(
          setAlert({
            title: 'Error occured!',
            message: 'Please try again.',
            status: false,
            type: 'otp',
          })
        );
      }
      dispatch(showLoading(false));
      return;
    }
  } else {
    dispatch(showLoading(true));
  }
  let imgKey;
  if (profileImg !== '' && profileImg.imgFile instanceof File) {
    try {
      const imgName = `${uuid.v4()}${profileImg.imgFile.name.substr(
        profileImg.imgFile.name.lastIndexOf('.')
      )}`;
      imgKey = await Storage.put(imgName, profileImg.imgFile, {
        level: 'protected',
        contentType: profileImg.imgFile.type,
        contentDisposition: 'inline',
      });
    } catch (err) {
      console.error(err);
      dispatch(
        showToast({
          title: 'Failed to add profile image.',
          description: 'Retry later by going to profile.',
          status: false,
        })
      );
    }
  }
  try {
    const { cartItems: cart } = getState().cart;
    const { wishlistItems: wishList } = getState().wishlist;
    const { identityId } = await Auth.currentCredentials();
    const {
      name, email, mobile, billing, fb, google
    } = profile;
    const farmerData = {
      profileImg: imgKey ? imgKey.key : '',
      identityId,
      name,
      email,
      mobile,
      fb,
      google,
      billing,
      shipping: [billing],
      cart,
      wishList,
      firstPurchase: true,
      secondPurchase: false,
      customerSAPCode: null,
    };
    await API.graphql(graphqlOperation(createFarmer, { input: farmerData }));
    if (redirect) Router.push(redirect);
    const userServiceable = await checkUserServiceble({
      ...profile,
      shipping: [],
    });
    dispatch({
      type: USER_REGISTER,
      payload: {
        profile: {
          name,
          email,
          mobile,
          billing,
          shipping: [],
          profileImg: imgKey ? imgKey.key : '',
        },
        userServiceable,
        check: userServiceable,
      },
    });
    dispatch(
      showToast({
        title: 'Profile Created!',
        status: true,
      })
    );
    dispatch({ type: CLEAR_REGISTRATION_DATA });
  } catch (error) {
    console.error(error);
    dispatch(
      setAlert({
        title: 'Error occured!',
        message: 'Please try again.',
        status: false,
        type: 'otp',
      })
    );
  } finally {
    dispatch(showLoading(false));
    dispatch(setLoader({ isRegistering: false }));
  }
};

export const updateUserProfile = ({
  farmer, initialFarmer, onClose, setProfileImg, setProfile
}) => async (dispatch) => {
  dispatch(showLoading(true));
  const { profileImg } = farmer;
  let imgKey;
  if (
    profileImg !== ''
      && profileImg !== null
      && profileImg.imgFile
      && profileImg.imgFile instanceof File
  ) {
    try {
      const imgName = `${uuid.v4()}${profileImg.imgFile.name.substr(
        profileImg.imgFile.name.lastIndexOf('.')
      )}`;
      imgKey = await Storage.put(imgName, profileImg.imgFile, {
        level: 'protected',
        contentType: profileImg.imgFile.type,
        contentDisposition: 'inline',
      });

      setProfileImg(profileImg.imgUrl);
    } catch (err) {
      dispatch(
        showToast({
          title: 'Failed to add profile image.',
          description: 'Retry later by going to profile.',
          status: false,
        })
      );
    }
  }
  try {
    const {
      name, email, mobile, billing, fb, google
    } = farmer;
    const { identityId } = await Auth.currentCredentials();
    console.log(identityId);
    const farmerData = {
      identityId,
      profileImg: imgKey ? imgKey.key : initialFarmer.profileImg,
      name,
      email,
      mobile,
      fb,
      google,
      billing: { ...billing, pincode: parseInt(billing.pincode), name },
      customerSAPCode: initialFarmer.customerSAPCode,
      sapUpdate: true
    };
    await API.graphql({
      query: updateFarmer,
      variables: { input: farmerData },
    });
    dispatch({
      type: UPDATE_USER_PROFILE,
      payload: {
        name,
        email,
        billing,
        profileImg: imgKey ? imgKey.key : '',
      },
    });
    setProfile(farmer);
    if (onClose) onClose();
    dispatch(
      showToast({
        title: 'Updated Profile',
        status: true,
      })
    );
  } catch (error) {
    console.log(error);
    dispatch(
      showToast({
        title: 'Error Occured',
        message: 'Please try again.',
        status: false,
      })
    );
  } finally {
    dispatch(showLoading(false));
  }
};

export const updateUser = (values) => ({
  type: UPDATE_USER_PROFILE,
  payload: values,
});

const getCurrentUser = async () => {
  try {
    const user = await Auth.currentAuthenticatedUser();
    const { phone_number } = user.attributes;
    const farmer = await API.graphql(
      graphqlOperation(getFarmer, { mobile: phone_number })
    );
    if (farmer.data.getFarmer) {
      const {
        name,
        mobile,
        email,
        billing,
        shipping,
        customerSAPCode,
        profileImg,
        cart,
        wishList,
      } = farmer.data.getFarmer;
      return {
        logInStatus: true,
        profile: {
          name,
          mobile,
          email,
          billing,
          shipping,
          customerSAPCode,
          profileImg,
        },
        cart,
        wishList,
      };
    }
    return {
      logInStatus: true,
      profile: {
        name: '',
      },
    };
  } catch (error) {
    console.error(error);
  }
  return {
    logInStatus: false,
  };
};

// eslint-disable-next-line consistent-return
const checkUserServiceble = async (profile) => {
  try {
    const { billing, shipping } = profile;
    const { pincode: billingPin } = billing;
    const defaultShip = shipping.filter((address) => address.isDefault);

    const shippingPin = defaultShip && defaultShip.length > 0 ? defaultShip[0].pincode : null;

    if (shippingPin) {
      const data = await API.post('EAMS5TAPI05', '/checkPincode', {
        body: {
          pincode: parseInt(shippingPin),
        },
      });

      const shippingStat = data.servicable;
      const city = data.desc;
      const message = shippingStat
        ? `Serviceable in your area. (Shipping address PIN: ${shippingPin})`
        : `Not serviceable in your area. (Shipping address PIN: ${shippingPin})`;
      return {
        message,
        city,
        stat: shippingStat,
        pincode: shippingPin,
      };
    }

    const data = await API.post('EAMS5TAPI05', '/checkPincode', {
      body: {
        pincode: parseInt(shippingPin),
      },
    });

    const billingStat = data.servicable;
    const city = data.desc;
    const message = billingStat
      ? `Serviceable in your area. (Billing address PIN: ${billingPin})`
      : `Not serviceable in your area. (Billing address PIN: ${billingPin})`;
    return {
      message,
      city,
      stat: billingStat,
      pincode: billingPin,
    };
  } catch (error) {
    console.error(error);
  }
};

export const pinServiceable = (pincode) => async (dispatch) => {
  try {
    const data = await API.post('EAMS5TAPI05', '/checkPincode', {
      body: {
        pincode,
      },
    });

    if (!data.servicable) {
      dispatch({
        type: PIN_SERVICEABLE,
        payload: {
          check: {
            pincode,
            city: '',
            servicable: false,
          },
        },
      });
    } else {
      dispatch({
        type: PIN_SERVICEABLE,
        payload: {
          check: {
            pincode,
            city: data.desc,
            servicable: data.servicable,
          },
        },
      });
    }
  } catch (error) {
    dispatch(
      showToast({
        title: 'Error occured',
        message: 'Enter pincode again.',
        status: false,
      })
    );
  }
};

export const addOrders = (orders) => async (dispatch) => {
  dispatch(showLoading(true));
  try {
    const orderStatusFilter = [];
    const orderDateFilter = ['Last 30 days'];
    const final = await Promise.all(
      orders.map(async (item) => {
        if (!orderStatusFilter.includes(item.orderStatus.toLowerCase())) {
          orderStatusFilter.push(item.orderStatus.toLowerCase());
        }
        if (!orderDateFilter.includes(item.orderDate.substr(0, 4))) {
          orderDateFilter.push(item.orderDate.substr(0, 4));
        }
        const orderCart = await Promise.all(
          item.orderCart.map(async (orderItem) => {
            const { data } = await API.graphql({
              query: getProduct,
              variables: {
                materialCode: orderItem.materialCode,
              },
              authMode: 'API_KEY',
            });
            const productImg = await Storage.get(data.getProduct.productImg[0]);
            const {
              crop, product, materialCode, category, avgrating
            } = data.getProduct;
            return {
              ...orderItem,
              productDetails: {
                crop,
                product,
                productImg,
                materialCode,
                category,
                avgrating,
              },
            };
          })
        );
        return { ...item, orderCart };
      })
    );
    dispatch({
      type: ADD_ORDERS,
      payload: {
        orders: final,
        orderStatusFilter,
        orderDateFilter,
      },
    });
  } catch (err) {
    dispatch(
      showToast({
        title: 'Failed to get product details.',
        message: 'Please try again!',
        status: false,
      })
    );
  } finally {
    dispatch(showLoading(false));
  }
};

export const setOrderFilter = ({
  appliedOrderStatusFilter,
  appliedOrderDateFilter,
}) => ({
  type: SET_ORDER_FILTER,
  payload: {
    appliedOrderDateFilter,
    appliedOrderStatusFilter,
  },
});

export const resetOrderFilter = () => ({
  type: RESET_ORDER_FILTER,
});
