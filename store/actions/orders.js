import { API, Auth } from 'aws-amplify';
import { getProduct, orderByFarmer } from '../../graphql/queries';
import { showLoading, showToast } from './feedback';

export const SET_ORDERS = 'SET_ORDERS';
export const PUSH_TOKEN = 'PUSH_TOKEN';
export const POP_TOKEN = 'POP_TOKEN';
export const TOGGLE_ORDERS_FILTER = 'SET_ORDERS_FILTERS';
export const RESET_ORDERS_FILTERS = 'RESET_ORDERS_FILTERS';
export const RESET_TOKENS = 'RESET_ORDERS_NEXT_TOKENS';
export const PUT_ORDER_COUNT = 'PUT_ORDER_COUNT';
export const getOrders = (next = true) => async (dispatch, getState) => {
  dispatch(showLoading(true));

  try {
    const {
      orders: { tokens, filters },
    } = getState();

    const filter = filters.length > 0
      ? {
        or: filters.map(({ field, value }) => ({ [field]: value })),
      }
      : null;

    const farmerMobile = (await Auth.currentAuthenticatedUser()).attributes
      .phone_number;

    const { length } = tokens;

    const orders = await API.graphql({
      query: orderByFarmer,
      variables: {
        farmerMobile,
        limit: 5,
        nextToken: tokens[length - 1],
        sortDirection: 'DESC',
        filter,
      },
    });

    const formatted = await Promise.all(
      orders.data.orderByFarmer.items.map(async (order) => {
        const orderCart = await Promise.all(
          order.orderCart.map(async (orderItem) => {
            const { data } = await API.graphql({
              query: getProduct,
              variables: {
                materialCode: orderItem.materialCode,
              },
              authMode: 'API_KEY',
            });

            const {
              crop, product, materialCode, category, avgrating
            } = data.getProduct;

            return {
              ...orderItem,
              productDetails: {
                crop,
                product,
                productImg: data.getProduct.productImg[0],
                materialCode,
                category,
                avgrating,
              },
            };
          })
        );

        return { ...order, orderCart };
      })
    );

    dispatch(setOrders(formatted));

    if (next) {
      dispatch(pushToken(orders.data.orderByFarmer.nextToken));
    } else {
      dispatch(popToken());
    }
  } catch (error) {
    console.error(error);
    dispatch(
      showToast({
        title: 'Failed to get orders.',
        message: 'Please try again!',
        status: false,
      })
    );
  } finally {
    dispatch(showLoading(false));
  }
};

export const setOrders = (orders) => ({ type: SET_ORDERS, payload: orders });

export const pushToken = (token) => ({ type: PUSH_TOKEN, payload: token });

export const popToken = () => ({ type: POP_TOKEN });

export const resetTokens = () => ({ type: RESET_TOKENS });

export const applyFilters = () => (dispatch) => {
  dispatch(resetTokens());
  dispatch(getOrders());
};

export const toggleOrdersFilter = (filter) => ({
  type: TOGGLE_ORDERS_FILTER,
  payload: filter,
});

export const resetFilters = () => (dispatch) => {
  dispatch({
    type: RESET_ORDERS_FILTERS,
  });
  dispatch(getOrders());
};
