import { showLoading } from './feedback';
import { sortAlphabetically } from './product';

export const TICK = 'TICK';
export const SET_SORT_FILTER = 'SORT_FILTER';
export const ALPHABETICAL_SORT_FILTER = 'ALPHABETICAL_SORT_FILTER';
export const SET_PRICE_RANGE = 'PRICE_RANGE_FILTER';
export const RESET_FILTERS = 'RESET_FILTERS';
export const CROSS_FILTER = 'CROSS_FILTER';
export const SET_SEEDS_PRODUCTS = 'SET_SEEDS_PRODUCTS';
export const SET_SEED_TYPES = 'SET_SEED_TYPES';
export const SET_BRAND_TYPES = 'SET_BRAND_TYPES';

export const toggleFilter = (id) => (dispatch) => {
  dispatch({
    type: TICK,
    payload: id,
  });
};

export const alphabeticalSortFilter = (id) => (dispatch) => {
  dispatch(showLoading(true));
  dispatch(sortAlphabetically(id));
  dispatch({
    type: ALPHABETICAL_SORT_FILTER,
    payload: id,
  });
  dispatch(showLoading(false));
};

export const crossFilter = (id) => (dispatch) => {
  dispatch({
    type: CROSS_FILTER,
    payload: id,
  });
};

export const setSeedsProducts = (name) => {
  return {
    type: SET_SEEDS_PRODUCTS,
    payload: name,
  };
};

export const setSeedTypes = (name) => {
  return {
    type: SET_SEED_TYPES,
    payload: name,
  };
};

export const setBrandTypes = (name) => {
  return {
    type: SET_BRAND_TYPES,
    payload: name,
  };
};

export const setPriceRange = (name) => {
  return {
    type: SET_PRICE_RANGE,
    payload: name,
  };
};

export const setSortFilter = (value) => ({
  type: SET_SORT_FILTER,
  payload: value,
});

export const resetFilters = () => ({ type: RESET_FILTERS, payload: null });
