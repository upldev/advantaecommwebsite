import { showToast } from './feedback';

export const ADD_TO_WISHLIST = 'ADD_TO_WISHLIST';
export const REMOVE_FROM_WISHLIST = 'REMOVE_FROM_WISHLIST';
export const RESET_WISHLIST = 'RESET_WISHLIST';
export const LOGIN_WISHLIST_UPDATE = 'LOGIN_WISHLIST_UPDATE';

export const addToWishlist = (product) => (dispatch) => {
  dispatch({
    type: ADD_TO_WISHLIST,
    payload: {
      materialCode: product.materialCode,
      productImg: product.productImg,
      product: product.product,
      description: product.description,
      ecomPrice: product.ecomPrice,
      crop: product.crop,
      category: product.category,
      packSize: product.packSize,
      qtyAvailable: product.qtyAvailable,
      uniqueCode: product.bagSize.ecomCode
    },
  });
  dispatch(
    showToast({
      title: 'Added',
      message: `${product.product} added to wishlist!`,
      status: true,
    })
  );
};

export const removeFromWishlist = (materialCode) => (dispatch) => {
  dispatch({
    type: REMOVE_FROM_WISHLIST,
    payload: materialCode,
  });
};

export const resetWishlist = () => (dispatch) => {
  dispatch({
    type: RESET_WISHLIST,
  });
};

export const loginWishListUpdate = (payload) => (dispatch) => {
  dispatch({
    type: LOGIN_WISHLIST_UPDATE,
    payload
  });
};
