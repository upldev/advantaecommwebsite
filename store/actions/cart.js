/* eslint-disable max-len */
import { API, Auth, Storage } from 'aws-amplify';
import { getMinProductDetails } from '../../graphql/customQueries';
import { updateFarmer } from '../../graphql/mutations';
import { getFarmer } from '../../graphql/queries';
import { setLoader, showLoading, showToast } from './feedback';

export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_FROM_CART = 'REMOVE_FROM_CART';
export const INCREMENT_QUANTITY = 'INCREMENT_QUANTITY';
export const DECREMENT_QUANTITY = 'DECREMENT_QUANTITY';
export const CART_RESET = 'CART_RESET';
export const SET_CART_PRODUCTS = 'SET_CART_PRODUCTS';
export const BAG_SIZE_CHANGE = 'BAG_SIZE_CHANGE';
export const ADD_TO_WISHLIST = 'ADD_TO_WISHLIST';
export const SET_WISHLIST_PRODUCTS = 'SET_WISHLIST_PRODUCTS';
export const ADD_ONLY_TO_WISHLIST = 'ADD_ONLY_TO_WISHLIST';
export const REMOVE_FROM_WISHLIST = 'REMOVE_FROM_WISHLIST';
export const RESET_WISHLIST = 'RESET_WISHLIST';

// backend
export const FETCH_BOTH = 'FETCH_CART_WISHLIST';
export const UPDATE_TO_BACKEND = 'UPDATE_TO_BACKEND';
export const LISTENER_UPDATE_CART = 'LISTENER_UPDATE_CART';
export const LISTENER_UPDATE_WISHLIST = 'LISTENER_UPDATE_WISHLIST';
export const LOGIN_UPDATE = 'LOGIN_UPDATE';
export const FETCH_UPDATES = 'FETCH_UPDATES';

export const addToCart = (product, qty, bagSize) => (dispatch) => {
  dispatch({
    type: ADD_TO_CART,
    payload: {
      materialCode: product.materialCode,
      qty,
      sku: bagSize,
      product: product.product,
    },
  });
  dispatch(
    showToast({
      title: 'Added',
      message: `${product.product} added to cart!`,
      status: true,
    })
  );
  dispatch(updateBoth());
};

export const removeFromCart = (materialCode) => (dispatch) => {
  dispatch({
    type: REMOVE_FROM_CART,
    payload: materialCode,
  });
  dispatch(updateCart());
};

export const incrementQuantity = (materialCode, setPlusMinus) => (dispatch) => {
  dispatch({
    type: INCREMENT_QUANTITY,
    payload: materialCode,
  });
  dispatch(updateCart(setPlusMinus));
};

export const decrementQuantity = (materialCode, setPlusMinus) => (dispatch) => {
  dispatch({
    type: DECREMENT_QUANTITY,

    payload: materialCode,
  });
  dispatch(updateCart(setPlusMinus));
};

export const getCartProducts = () => async (dispatch, getState) => {
  const { cart } = getState();

  try {
    const cartProducts = await Promise.all(
      cart.cartItems.map(async (cartItem) => {
        const { data: product } = await API.graphql({
          query: getMinProductDetails,
          variables: {
            materialCode: cartItem.materialCode,
          },
          authMode: 'API_KEY',
        });
        return {
          ...product.getProduct,
          sku: product.getProduct.sku.find(
            ({ ecomCode }) => ecomCode === cartItem.sku.ecomCode
          ),
          qty: cartItem.qty,
        };
      })
    );

    dispatch({ type: SET_CART_PRODUCTS, payload: cartProducts });
  } catch (error) {
    console.error(error);
  } finally {
    // dispatch(setProductLoader({ isSimilarProductLoading: false }));
  }
};

const formatProducts = async (products) => {
  const formatedProducts = await Promise.all(
    products.map(async (product) => {
      const imgUrls = await Promise.all(
        product.productImg.map(getImageFromStorage)
      );

      return {
        ...product,
        productImg: imgUrls,
      };
    })
  );

  return formatedProducts;
};

const getImageFromStorage = async (img) => {
  const imgUrl = await Storage.get(img);
  return imgUrl;
};

export const getWishlistProducts = () => async (dispatch, getState) => {
  const { wishlist } = getState();

  try {
    const wishlistProducts = await Promise.all(
      wishlist.wishlistItems.map(async (wishlistItem) => {
        const { data: product } = await API.graphql({
          query: getMinProductDetails,
          variables: {
            materialCode: wishlistItem,
          },
          authMode: 'API_KEY',
        });

        return { ...product.getProduct };
      })
    );

    // const formatedProducts = await formatProducts(wishlistProducts);

    dispatch({ type: SET_WISHLIST_PRODUCTS, payload: wishlistProducts });
  } catch (error) {
    console.error({ error });
  } finally {
    // dispatch(setProductLoader({ isSimilarProductLoading: false }));
  }
};

export const resetCart = () => (dispatch) => {
  dispatch({
    type: CART_RESET,
  });
  dispatch(updateCart());
};

export const addOnlyToWishlist = (product) => (dispatch) => {
  dispatch({
    type: ADD_ONLY_TO_WISHLIST,
    payload: product.materialCode,
  });
  dispatch(
    showToast({
      title: 'Added',
      message: `${product.product} added to wishlist!`,
      status: true,
    })
  );
  dispatch(updateWishList());
};

export const addMultipleToWishList = (products) => (dispatch) => {
  products.forEach((product) => {
    dispatch({
      type: ADD_TO_WISHLIST,
      payload: {
        materialCode: product.materialCode,
        skuCode: product.sku.ecomCode,
      },
    });
  });
};

export const addToWishlist = (product) => (dispatch) => {
  dispatch({
    type: ADD_TO_WISHLIST,
    payload: {
      materialCode: product.materialCode,
      skuCode: product.sku.ecomCode,
    },
  });
  dispatch(
    showToast({
      title: 'Added',
      message: `${product.product} added to wishlist!`,
      status: true,
    })
  );
  dispatch(updateBoth());
};

export const removeFromWishlist = (materialCode) => (dispatch) => {
  dispatch({
    type: REMOVE_FROM_WISHLIST,
    payload: materialCode,
  });
  dispatch(updateWishList());
};

export const resetWishlist = () => (dispatch) => {
  dispatch({
    type: RESET_WISHLIST,
  });
  dispatch(updateWishList());
};

export const updateCart = (setPlusMinus) => async (dispatch, getState) => {
  // local changes to db
  dispatch(setLoader({ isCartUpdating: true }));
  try {
    const { cartItems } = getState().cart;
    const user = await Auth.currentAuthenticatedUser();
    await API.graphql({
      query: updateFarmer,
      variables: { input: { mobile: user.username, cart: cartItems } },
    });
    dispatch({
      type: UPDATE_TO_BACKEND,
    });
  } catch (error) {
    // do nothing
  } finally {
    dispatch(setLoader({ isCartUpdating: false }));
    if (setPlusMinus) {
      setPlusMinus(false);
    }
  }
};

export const updateWishList = () => async (dispatch, getState) => {
  // local changes to db
  dispatch(showLoading(true));
  try {
    const { wishlistItems: wishList } = getState().wishlist;
    const user = await Auth.currentAuthenticatedUser();
    await API.graphql({
      query: updateFarmer,
      variables: { input: { mobile: user.username, wishList } },
    });
    dispatch({
      type: UPDATE_TO_BACKEND,
    });
  } catch (error) {
    // do nothing
  } finally {
    dispatch(showLoading(false));
  }
};

export const updateBoth = () => async (dispatch, getState) => {
  // simultaneously update cart & wishlist
  dispatch(showLoading(true));
  try {
    const { wishlistItems: wishList } = getState().wishlist;
    const { cartItems: cart } = getState().cart;
    const user = await Auth.currentAuthenticatedUser();
    await API.graphql({
      query: updateFarmer,
      variables: { input: { mobile: user.username, cart, wishList } },
    });
    dispatch({
      type: UPDATE_TO_BACKEND,
    });
  } catch (error) {
    // do nothing
  } finally {
    dispatch(showLoading(false));
  }
};

export const listenerCartUpdate = (
  // update cart based on subscription
  updatedCart
) => async (dispatch, getState) => {
  try {
    const { isUpdating } = getState().cart;
    if (!isUpdating) {
      if (updatedCart) {
        const cartProducts = await Promise.all(
          updatedCart.map(async (cartItem) => {
            const { data: product } = await API.graphql({
              query: getMinProductDetails,
              variables: {
                materialCode: cartItem.materialCode,
              },
              authMode: 'API_KEY',
            });
            return {
              ...product.getProduct,
              sku: product.getProduct.sku.find(
                ({ ecomCode }) => ecomCode === cartItem.sku.ecomCode
              ),
            };
          })
        );

        const formatedProducts = await formatProducts(cartProducts);
        dispatch({
          type: LISTENER_UPDATE_CART,
          payload: {
            cartItems: updatedCart,
            cartProducts: formatedProducts,
          },
        });
      }
    } else {
      dispatch({
        type: LISTENER_UPDATE_CART,
        payload: false,
      });
    }
  } catch (err) {
    console.error(err);
  }
};

export const listenerWishListUpdate = (
  // update wish list based on subscription
  updatedWishList
) => async (dispatch, getState) => {
  const { isUpdating } = getState().wishlist;
  if (!isUpdating && updatedWishList && updatedWishList.length > 0) {
    dispatch({
      type: LISTENER_UPDATE_WISHLIST,
      payload: updatedWishList,
    });
  } else {
    dispatch({
      type: LISTENER_UPDATE_WISHLIST,
      payload: false,
    });
  }
};

export const fetchUpdates = () => async (dispatch) => {
  dispatch(showLoading(true));
  try {
    const user = await Auth.currentAuthenticatedUser();
    const { data } = await API.graphql({
      query: getFarmer,
      variables: {
        mobile: user.username,
      },
    });
    const { cart, wishList } = data.getFarmer;
    dispatch({
      type: FETCH_UPDATES,
      payload: {
        cart,
        wishList,
      },
    });
  } catch (error) {
    console.error(error);
  } finally {
    dispatch(showLoading(false));
  }
};

export const logInCartUpdate = (payload) => async (dispatch) => {
  dispatch({
    type: LOGIN_UPDATE,
    payload
  });
};
