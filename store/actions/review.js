import {
  API, Auth, graphqlOperation, Storage
} from 'aws-amplify';
import { createProductReview } from '../../graphql/mutations';
import { listProductReviews } from '../../graphql/queries';
import { showToast } from './feedback';

export const ADD_PRODUCT_REVIEWS = 'ADD_PRODUCT_REVIEWS';
export const CLEAR_PRODUCT_REVIEWS = 'CLEAR_PRODUCT_REVIEWS';

export const getProductReviews = (materialCode) => async (dispatch) => {
  try {
    dispatch({
      type: CLEAR_PRODUCT_REVIEWS,
    });
    const productReviews = await API.graphql({
      query: listProductReviews,
      authMode: 'API_KEY',
      variables: {
        filter: {
          materialCode: { eq: materialCode },
          isModerated: { eq: true },
          isPublic: { eq: true },
        }
      }
    });

    const formattedReviews = await Promise.all(
      productReviews.data.listProductReviews.items.map(async (item) => {
        if (item.images) {
          const images_download = await Promise.all(item.images.map(async (img) => {
            const imgres = await Storage.get(img, {
              level: 'protected',
              identityId: item.identityId
            });
            return imgres;
          }));

          const profileImg = await Storage.get(item.profileImg, {
            level: 'protected',
            identityId: item.identityId
          });
          return {
            ...item,
            images: images_download,
            profileImg: item.profileImg !== '' ? profileImg : item.profileImg
          };
        }
        return null;
      })
    );
    dispatch({
      type: ADD_PRODUCT_REVIEWS,
      payload: formattedReviews
    });
  } catch (e) {
    console.error(e);
    dispatch({
      type: CLEAR_PRODUCT_REVIEWS,
    });
  }
};

export const saveProductReview = (review) => async (dispatch, getState) => {
  try {
    const { profile } = getState().user;
    const { images, ...rest } = review;

    let isContent = true;
    let isExt = true;

    images.forEach((img) => {
      if (!img.file.type.includes('image')) {
        isContent = false;
      }
      if (img.file.name.split('.').length > 2) { isExt = false; }
    });

    if (isContent && isExt) {
      const afterSavingImages = await Promise.all(images.map(async (curr) => {
        const imageres = await Storage.put(curr.file.name, curr.file, {
          level: 'protected',
          contentType: curr.file.type,
          contentDisposition: 'inline'
        });
        return imageres.key;
      }));
      const res = await API.graphql(graphqlOperation(createProductReview, {
        input: {
          ...rest,
          images: afterSavingImages,
          identityId: (await Auth.currentCredentials()).identityId,
          profileImg: profile.profileImg
        }
      }));
      dispatch(
        showToast({
          title: 'Review Add Success',
          message: 'Our team is reviewing your review and it will be activated in 1-2 days',
          status: true,
        })
      );
    } else {
      if (!isContent) {
        dispatch(
          showToast({
            title: 'Please upload only images!',
            status: false,
          })
        );
      }
      if (!isExt) {
        dispatch(
          showToast({
            title: 'File name or extension has a problem',
            status: false,
          })
        );
      }
    }
  } catch (error) {
    console.error(error);
    dispatch(
      showToast({
        title: 'Unable to add Review',
        status: false,
      })
    );
  }
};
