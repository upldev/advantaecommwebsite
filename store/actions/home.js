import { API, Storage } from 'aws-amplify';
import { services } from '../../constants/services';
import { listProducts } from '../../graphql/queries';
import { showLoading } from './feedback';

export const SET_BEST_SELLER = 'SET_BEST_SELLER';
export const SET_TRENDING = 'SET_TRENDING';
export const SET_FILTERED_PRODUCTS = 'SET_FILTERED_PRODUCTS';
export const GET_SERVICES = 'GET_SERVICES';

export const setProducts = (products, action) => async (dispatch) => {
  dispatch(showLoading(true));
  try {
    let productsFormatted = [];
    await Promise.all(
      products.map(async (product) => {
        const imgUrls = await Promise.all(
          product.productImg.map(async (img) => {
            const imgUrl = await Storage.get(img);
            return imgUrl;
          })
        );
        const newProduct = { ...product, productImg: imgUrls };
        productsFormatted = [...productsFormatted, newProduct];
      })
    );
    dispatch({
      type: action,
      payload: productsFormatted,
    });

    dispatch(showLoading(false));
  } catch (e) {
    console.error(e);
    dispatch(showLoading(false));
  }
};

export const setBestSeller = (products) => async (dispatch) => {
  dispatch(setProducts(products, SET_BEST_SELLER));
};

export const setTrending = (products) => async (dispatch) => {
  dispatch(setProducts(products, SET_TRENDING));
};

export const getFilteredProducts = (phrase) => async (dispatch) => {
  dispatch(showLoading(true));

  dispatch(getServices(phrase));
  try {
    const result = await API.graphql({
      query: listProducts,
      authMode: 'API_KEY',
      variables: {
        filter: {
          and: [
            { isPublic: { eq: true } },
            {
              or: [
                { category: { contains: phrase } },
                { category: { contains: phrase.toUpperCase() } },
                { category: { contains: phrase.toLowerCase() } },
                {
                  category: {
                    contains: phrase[0].toUpperCase() + phrase.slice(1),
                  },
                },
                { crop: { contains: phrase } },
                { crop: { contains: phrase.toUpperCase() } },
                { crop: { contains: phrase.toLowerCase() } },
                {
                  crop: { contains: phrase[0].toUpperCase() + phrase.slice(1) },
                },
                { product: { contains: phrase } },
                { product: { contains: phrase.toUpperCase() } },
                { product: { contains: phrase.toLowerCase() } },
                {
                  product: {
                    contains: phrase[0].toUpperCase() + phrase.slice(1),
                  },
                },
                { filterCategory: { contains: phrase } },
                { filterCategory: { contains: phrase.toUpperCase() } },
                { filterCategory: { contains: phrase.toLowerCase() } },
                {
                  filterCategory: {
                    contains: phrase[0].toUpperCase() + phrase.slice(1),
                  },
                },
              ],
            },
          ],
        },
      },
    });

    // console.log(result.data.listProducts.items);

    dispatch({
      type: SET_FILTERED_PRODUCTS,
      payload:
        phrase !== ''
          ? {
            searchFiltered: result.data.listProducts.items,
            searchInput: phrase,
          }
          : [],
    });
  } catch (e) {
    console.error(e);
    dispatch({
      type: SET_FILTERED_PRODUCTS,
      payload:
        phrase !== ''
          ? {
            searchFiltered: [],
            searchInput: phrase,
          }
          : [],
    });
  } finally {
    dispatch(showLoading(false));
  }
};

export const getServices = (phrase) => async (dispatch) => {
  const filteredData = services.filter(({ key }) => key.includes(phrase.toUpperCase()));

  dispatch({
    type: GET_SERVICES,
    payload: filteredData,
  });
};
