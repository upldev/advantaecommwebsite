/* eslint-disable import/no-cycle */
import { API, Storage } from 'aws-amplify';
import { getProduct } from '../../graphql/queries';

export const SAVE_PRODUCTS = 'SAVE_PRODUCTS';
export const SET_CURRENT_PRODUCT = 'SET_CURRENT_PRODUCT';

// filter
export const SORT_PRODUCTS = 'SORT_PRODUCTS';
export const SORT_PRODUCTS_PRICERANGE = 'SORT_PRODUCTS_PRICERANGE';
export const SET_CAT_FILTERED_PRODUCTS = 'SET_CAT_FILTERED_PRODUCTS';
export const SET_SCAT_FILTERED_PRODUCTS = 'SET_SCAT_FILTERED_PRODUCTS';
export const RESET_FILTER_DATA = 'RESET_FILTER_DATA';
export const SORT_ALPHABETICALLY = 'SORT_ALPHABETICALLY';

// categories
export const AVAILABLE_CATEGORIES = 'AVAILABLE_CATEGORIES';
export const AVAILABLE_SUBCATEGORIES = 'AVAILABLE_SUBCATEGORIES';
export const SET_PRODUCT_LIST_LOADER = 'SET_PRODUCT_LIST_LOADER';
export const SET_PRODUCT_DETAILS = 'SET_PRODUCT_DETAILS';
export const SET_SIMILAR_PRODUCTS = 'SET_SIMILAR_PRODUCTS';

export const setCurrent = (product) => ({
  type: SET_CURRENT_PRODUCT,
  payload: product,
});

export const sortProducts = (id) => async (dispatch, getState) => {
  const { products, filteredProducts } = getState().product;
  const { filter, category, subCategory } = getState();
  const otherFilters = Object.entries(filter).filter((i) => i[1]).length >= 1;
  const catFilter = Object.entries(category).filter((i) => i[1]).length >= 1;
  const scatFilter = Object.entries(subCategory).filter((i) => i[1]) >= 1;
  const filtering = otherFilters || catFilter || scatFilter || filteredProducts.length > 0
    ? filteredProducts
    : [...products];
  if (id === 'lowToHigh') {
    filtering.sort(
      (product1, product2) => product1.sku[0].ecomPrice - product2.sku[0].ecomPrice
    );
  } else {
    filtering.sort(
      (product1, product2) => product2.sku[0].ecomPrice - product1.sku[0].ecomPrice
    );
  }
  dispatch({
    type: SORT_PRODUCTS,
    payload: filtering,
  });
};

export const sortAlphabetically = (id) => async (dispatch, getState) => {
  const { products, filteredProducts } = getState().product;
  const { filter, category, subCategory } = getState();
  const otherFilters = Object.entries(filter).filter((i) => i[1]).length >= 1;
  const catFilter = Object.entries(category).filter((i) => i[1]).length >= 1;
  const scatFilter = Object.entries(subCategory).filter((i) => i[1]) >= 1;
  const filtering = otherFilters || catFilter || scatFilter || filteredProducts.length > 0
    ? filteredProducts
    : [...products];
  if (id === 'AtoZ') {
    filtering.sort((a, b) => {
      if (a.product < b.product) {
        return -1;
      }
      if (a.product > b.product) {
        return 1;
      }
      return 0;
    });
  } else {
    filtering.sort((a, b) => {
      if (a.product < b.product) {
        return 1;
      }
      if (a.product > b.product) {
        return -1;
      }
      return 0;
    });
  }
  dispatch({
    type: SORT_ALPHABETICALLY,
    payload: filtering,
  });
};

export const sortByPriceRange = (filter) => async (dispatch, getState) => {
  const { products, categoryFiltered, subcategoryFiltered } = getState().product;
  const { category } = getState();
  const { subCategory } = getState();
  const catFilter = Object.entries(category).filter((i) => i[1]).length >= 1;
  const scatFilter = Object.entries(subCategory).filter((i) => i[1]).length >= 1;
  let priceFiltered = products;
  if (filter === 'thousandAndBelow') {
    priceFiltered = priceFiltered.filter(
      (product) => product.sku[0].ecomPrice < 1000
    );
  } else if (filter === 'oneToFiveThousand') {
    priceFiltered = priceFiltered.filter(
      (product) => product.sku[0].ecomPrice > 1000 && product.sku[0].ecomPrice < 5000
    );
  } else if (filter === 'fiveThousandAndAbove') {
    priceFiltered = priceFiltered.filter(
      (product) => product.sku[0].ecomPrice > 5000
    );
  }
  let filteredProducts = [];
  if (catFilter && scatFilter) {
    filteredProducts = intersection(priceFiltered, categoryFiltered);
    filteredProducts = intersection(filteredProducts, subcategoryFiltered);
  } else if (!catFilter && scatFilter) {
    filteredProducts = intersection(priceFiltered, subcategoryFiltered);
  } else if (!scatFilter && catFilter) {
    filteredProducts = intersection(priceFiltered, categoryFiltered);
  } else {
    filteredProducts = priceFiltered;
  }
  dispatch({
    type: SORT_PRODUCTS_PRICERANGE,
    payload: {
      filteredProducts,
      priceFiltered,
    },
  });
};

export const setCatFilteredProducts = ({ categoryFiltered, filteredProducts }) => (dispatch, getState) => {
  const { filter } = getState();
  dispatch({
    type: SET_CAT_FILTERED_PRODUCTS,
    payload: {
      categoryFiltered,
      filteredProducts,
    },
  });
  if (filter.sort) {
    dispatch(sortProducts(filter.sort));
  }
  if (filter.alphabeticalSort) {
    dispatch(sortAlphabetically(filter.alphabeticalSort));
  }
};

export const setScatFilteredProducts = ({ subcategoryFiltered, filteredProducts }) => (dispatch, getState) => {
  const { filter } = getState();
  dispatch({
    type: SET_SCAT_FILTERED_PRODUCTS,
    payload: {
      subcategoryFiltered,
      filteredProducts,
    },
  });
  if (filter.sort) {
    dispatch(sortProducts(filter.sort));
  }
  if (filter.alphabeticalSort) {
    dispatch(sortAlphabetically(filter.alphabeticalSort));
  }
};

export const resetFilterData = () => (dispatch) => {
  dispatch({
    type: RESET_FILTER_DATA,
  });
};

export const getProductDetails = (materialCode) => async (dispatch) => {
  dispatch(setProductLoader({ isProductDetailsLoading: true }));

  try {
    const { data } = await API.graphql({
      query: getProduct,
      variables: {
        materialCode,
      },
      authMode: 'API_KEY',
    });

    const productDetails = await formatProduct(data.getProduct);

    dispatch({
      type: SET_PRODUCT_DETAILS,
      payload: productDetails,
    });
  } catch (error) {
    console.error({ error });
  } finally {
    dispatch(setProductLoader({ isProductDetailsLoading: false }));
  }
};
export const setProductLoader = (isLoading) => ({
  type: SET_PRODUCT_LIST_LOADER,
  payload: isLoading,
});
export const getSimilarProducts = (materialCodes) => async (dispatch) => {
  dispatch(setProductLoader({ isSimilarProductLoading: true }));

  try {
    const similarProducts = await Promise.all(
      materialCodes.map(async (materialCode) => {
        const { data: product } = await API.graphql({
          query: getProduct,
          variables: {
            materialCode,
          },
          authMode: 'API_KEY',
        });

        return product.getProduct;
      })
    );

    dispatch({ type: SET_SIMILAR_PRODUCTS, payload: similarProducts });
  } catch (error) {
    console.error({ error });
  } finally {
    dispatch(setProductLoader({ isSimilarProductLoading: false }));
  }
};

const formatProduct = async (product) => {
  const imgUrls = await Promise.all(
    product.productImg.map(getImageFromStorage)
  );

  return {
    ...product,
    productImg: imgUrls,
    // reviews: JSON.parse(product.reviews)
  };
};
const getImageFromStorage = async (img) => {
  const imgUrl = await Storage.get(img);
  return imgUrl;
};
const intersection = (products1, products2) => products1.filter((product1) => products2.some(
  (product2) => product1.materialCode === product2.materialCode
));
