import { API, Auth, graphqlOperation } from 'aws-amplify';
import Router from 'next/router';
import { colors } from '../../constants/colors';
import { updateFarmer } from '../../graphql/mutations';
import { getFarmer } from '../../graphql/queries';
import { CREATE_USER_PROFILE, ORDER_HISTORY } from '../../lib/router';
import { setLoader, showLoading, showToast } from './feedback';
import { updateUser } from './user';

export const ADD_LOCAL_GUEST = 'ADD_LOCAL_GUEST';
export const ADD_CURRENT_LOGGED_USER = 'ADD_CURRENT_LOGGED_USER';
export const ADD_CURRENT_SHIP_TO = 'ADD_CURRENT_SHIP_TO';
export const RESET_CHECKOUT = 'RESET_CHECKOUT';
export const REMOVE_CURRENT_SHIP_TO = 'REMOVE_CURRENT_SHIP_TO';

export const resetCheckout = () => ({
  type: RESET_CHECKOUT,
});

export const addCurrentShipTo = (shipTo) => ({
  type: ADD_CURRENT_SHIP_TO,
  payload: shipTo,
});

export const removeCurrentShipTo = () => ({
  type: REMOVE_CURRENT_SHIP_TO,
});

export const addLocalGuest = (guest) => ({
  type: ADD_LOCAL_GUEST,
  payload: guest,
});

export const getUser = () => async (dispatch) => {
  dispatch(showLoading(true));
  try {
    const phone = (await Auth.currentAuthenticatedUser()).attributes
      .phone_number;
    const user = await API.graphql(
      graphqlOperation(getFarmer, {
        mobile: phone,
      })
    );
    if (user.data.getFarmer === null) {
      dispatch(
        showToast({
          title: 'Profile not created!',
          message: 'Please create a profile',
          status: false,
        })
      );
      Router.push(CREATE_USER_PROFILE);
    }
    dispatch({
      type: ADD_CURRENT_LOGGED_USER,
      payload: user.data.getFarmer,
    });

    const currentShipTo = user.data.getFarmer?.shipping?.find(
      (item) => item.isDefault
    );

    if (currentShipTo) {
      dispatch(addCurrentShipTo(currentShipTo));
    }
  } catch (e) {
    console.error(e);
  } finally {
    dispatch(showLoading(false));
  }
};

export const addShippingToProfile = (shipping, onClose) => async (dispatch, getState) => {
  const { mobile } = getState().checkout.currentUser;

  dispatch(showLoading(true));
  try {
    const res = await API.graphql({
      query: updateFarmer,
      variables: {
        input: {
          mobile,
          shipping,
        },
      },
    });

    dispatch({
      type: ADD_CURRENT_LOGGED_USER,
      payload: res.data.updateFarmer,
    });

    dispatch(updateUser(res.data.updateFarmer));

    const {
      address, state, city, pincode
    } = res.data.updateFarmer;

    dispatch(
      addCurrentShipTo({
        address,
        state,
        city,
        pincode,
      })
    );

    if (onClose) {
      onClose();
    }
  } catch (e) {
    console.error(e);
  } finally {
    dispatch(showLoading(false));
  }
};

export const razorpayInit = () => async (dispatch) => {
  const checkoutUrl = 'https://checkout.razorpay.com/v1/checkout.js';

  const res = new Promise((resolve) => {
    const script = document.createElement('script');
    script.src = checkoutUrl;
    script.onload = () => {
      resolve(true);
    };
    script.onerror = () => {
      resolve(false);
    };
    document.body.appendChild(script);
  });

  if (!res) {
    console.error('Razorpay SDK failed to load. Are you online?');
  }
};

export const displayUserRazorpay = (orderInfo, userInfo, router) => async (dispatch, getState) => {
  dispatch(setLoader({ isPaymentLoading: true }));
  const checkoutState = getState().checkout;
  const { appliedCartCoupons, appliedProductCoupons, otherDiscounts } = checkoutState;

  const isStock = await API.post('EAMS5TAPI05', '/checkStockAvail', {
    body: {
      cartItems: orderInfo,
    },
  });

  if (!isStock.success) {
    router.reload();
  } else {
    const result = await API.post('EAMS5TAPI03', '/createOrder', {
      body: {
        orderInfo,
        coupons: {
          appliedCartCoupons,
          appliedProductCoupons,
          otherDiscounts,
        },
      },
    }).catch(console.error);

    console.log(result);

    if (!result.success) {
      alert('Server error. Are you online?');
      return;
    }

    // Getting the order details back
    const { amount, id: order_id, currency } = result.order;

    const options = {
      key: result.key, // Enter the Key ID generated from the Dashboard
      amount: amount.toString(),
      currency,
      name: 'Advanta Seeds',
      description: 'Test Transaction',
      order_id,
      async handler(response) {
        dispatch(setLoader({ isPaymentLoading: false }));
        const data = {
          orderCreationId: order_id,
          razorpayPaymentId: response.razorpay_payment_id,
          razorpayOrderId: response.razorpay_order_id,
          razorpaySignature: response.razorpay_signature,
          amount,
          orderInfo,
          userInfo,
          coupons: {
            appliedCartCoupons,
            appliedProductCoupons,
            otherDiscounts,
          },
          shipping: checkoutState.currentShipTo,
          isMobile: false
        };
        try {
          dispatch(setLoader({ isOrderProcessing: true }));
          const verify = await API.post(
            'EAMS5TAPI03',
            '/verifyPaymentWithOrder',
            {
              body: data,
            }
          ).catch(console.error);
          if (verify.success === true) {
            dispatch(
              showToast({ title: 'Order Placed Successfully', status: true })
            );
            router.replace(`${ORDER_HISTORY}/?fromCheckout=true`, undefined, {
              shallow: true,
            });
          } else {
            dispatch(
              showToast({
                title: 'Order Failure Please try again',
                status: false,
              })
            );
          }
          dispatch(setLoader({ isOrderProcessing: false }));
          dispatch(setLoader({ isPaymentLoading: false }));
        } catch (e) {
          console.error(e);
          dispatch(setLoader({ isPaymentLoading: false }));
          dispatch(showToast({ title: 'Payment Failure', status: false }));
        }
      },
      modal: {
        ondismiss() {
          dispatch(setLoader({ isPaymentLoading: false }));
          dispatch(showToast({ title: 'Please pay to place order' }));
        },
      },
      prefill: {
        name: userInfo.name,
        email: userInfo.email,
        contact: userInfo.mobile,
      },
      notes: {
        address: userInfo.billing.address,
      },
      theme: {
        color: colors.darkGreen,
      },
    };

    const paymentObject = new window.Razorpay(options);
    paymentObject.open();
  }
};
