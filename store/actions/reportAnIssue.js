/* eslint-disable no-console */
import { API, Auth, graphqlOperation } from 'aws-amplify';
import { v4 as uuidv4 } from 'uuid';
import {
  createComplaint
} from '../../graphql/mutations';
import { showLoading, showToast } from './feedback';

export const REPORT_AN_ISSUE = 'REPORT_AN_ISSUE';
export const CREATE_AN_ISSUE = 'CREATE_AN_ISSUE';
export const NEW_ISSUE = 'NEW_ISSUE';

export const createAnIssue = () => async (dispatch) => {
  dispatch(showLoading(true));
  try {
    const user = await Auth.currentAuthenticatedUser();
    const complaintId = uuidv4();
    dispatch({
      type: CREATE_AN_ISSUE,
      payload: {
        complaintId,
        farmerMobile: user.attributes.phone_number,
      }
    });
  } catch (error) {
    if (error !== 'The user is not authenticated') {
      dispatch(
        showToast({
          title: 'Error',
          message: `${error}`,
          status: false,
        })
      );
    }
  } finally {
    dispatch(showLoading(false));
  }
};

export const reportAnIssue = (complaintDetails) => async (dispatch, getState) => {

  const { user } = getState();
  dispatch(showLoading(true));
  try {
    const {
      complaintId, farmerMobile, complaintType, complaintDesc
    } = complaintDetails;
    const date = new Date();
    const complaint = {
      complaintId,
      farmerMobile,
      farmerName: user.profile.name,
      complaintDate: date.toISOString(),
      complaintType,
      complaintDesc,
      isClarification: false,
      status: 'unresolved'
    };
    await API.graphql(graphqlOperation(createComplaint, { input: complaint }));
    dispatch(showToast({
      title: 'Issue Reported',
      status: true
    }));
  } catch (error) {
    console.error(error);
    dispatch(
      showToast({
        title: 'Issue Create Fail',
        message: 'Unable to Create Issue',
        status: false,
      })
    );
  } finally {
    dispatch(showLoading(false));
  }
};

export const newIssue = () => async (dispatch) => {
  dispatch(showLoading(true));
  const newComplaintId = uuidv4();
  dispatch({
    type: NEW_ISSUE,
    payload: {
      newComplaintId
    }
  });
  dispatch(showLoading(false));
};
