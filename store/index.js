import { useMemo } from 'react';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import alertReducer from './reducers/alert';
import cartReducer from './reducers/cart';
import categoryReducer from './reducers/category';
import checkoutReducer from './reducers/checkout';
import cropReducer from './reducers/cropCategory';
import feedbackReducer from './reducers/feedback';
import filterReducer from './reducers/filter';
import homeReducer from './reducers/home';
import ordersReducer from './reducers/orders';
import productReducer from './reducers/product';
import reportAnIssueReducer from './reducers/reportAnIssue';
import reviewReducer from './reducers/review';
import { searchReducer } from './reducers/search';
import sessionReducer from './reducers/session';
import subCategoryReducer from './reducers/subCategory';
import userReducer from './reducers/user';
import wishlistReducer from './reducers/wishlist';

let store;

const persistedReducer = persistCombineReducers(
  {
    key: 'primary',
    storage,
    whitelist: ['cart', 'wishlist', 'checkout', 'session'],
  },
  {
    cart: cartReducer,
    wishlist: wishlistReducer,
    feedback: feedbackReducer,
    review: reviewReducer,
    reportAnIssue: reportAnIssueReducer,
    user: userReducer,
    filter: filterReducer,
    category: categoryReducer,
    subCategory: subCategoryReducer,
    product: productReducer,
    home: homeReducer,
    alert: alertReducer,
    checkout: checkoutReducer,
    cropCategory: cropReducer,
    session: sessionReducer,
    orders: ordersReducer,
    search: searchReducer,
  }
);

const makeStore = (initialState = {}) => createStore(
  persistedReducer,
  initialState,
  process.env.NODE_ENV === 'production'
    ? applyMiddleware(thunk)
    : composeWithDevTools(applyMiddleware(thunk))
);

export const initializeStore = (preloadedState) => {
  let _store = store ?? makeStore(preloadedState);

  if (preloadedState && store) {
    _store = makeStore({
      ...store.getState(),
      ...preloadedState,
    });
    store = undefined;
  }

  if (typeof window === 'undefined') return _store;

  if (!store) store = _store;

  return _store;
};

export const useStore = (initialState) => {
  return useMemo(() => initializeStore(initialState), [initialState]);
};
