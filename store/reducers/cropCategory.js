/* eslint-disable no-console */
import {
  CROSS_FILTER_CROP, RESET_FILTER_CROP, SET_CROP, TICK_CROP
} from '../actions/cropCategory';

const initialValue = {
  'Field Corn': false,
  Tomato: false,
  Gourds: false,
  Rice: false,
  Okra: false,
  'Cauliflower & Cabbage': false,
  // 'Other Vegetables': false,
  Peas: false,
  Chilli: false,
  'Kitchen Garden': false,
  Forages: false
};

const cropReducer = (state = initialValue, action) => {
  const { type, payload } = action;
  switch (type) {
    case TICK_CROP:
      const item = payload;
      const toggledFilter = !state[item];
      return { ...state, [payload]: toggledFilter };
    case CROSS_FILTER_CROP:
      return { ...state, [payload]: false };
    case RESET_FILTER_CROP:
      return initialValue;
    case SET_CROP:
      return payload;
    default:
      return state;
  }
};

export default cropReducer;
