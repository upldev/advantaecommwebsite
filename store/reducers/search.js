import {
  SET_FILTERED_PRODUCTS,
  SET_PHRASE,
  SET_SEARCH_LOADING
} from '../actions/search';

const initialState = {
  phrase: '',
  filteredProducts: [],
  isSearching: false,
};

export const searchReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_PHRASE:
      return {
        ...state,
        phrase: payload,
      };
    case SET_FILTERED_PRODUCTS:
      return {
        ...state,
        filteredProducts: payload,
      };
    case SET_SEARCH_LOADING:
      return {
        ...state,
        isSearching: payload,
      };
    default:
      return state;
  }
};
