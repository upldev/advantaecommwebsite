import {
  GET_SERVICES,
  SET_BEST_SELLER,
  SET_FILTERED_PRODUCTS,
  SET_TRENDING
} from '../actions/home';

const initialState = {
  bestseller: [],
  trending: [],
  filtered: [],
  searchInput: '',
  filteredServices: [],
};

const homeReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_BEST_SELLER:
      return {
        ...state,
        bestseller: payload
      };
    case SET_TRENDING:
      return {
        ...state,
        trending: payload
      };
    case SET_FILTERED_PRODUCTS:
      return payload.searchInput ? {
        ...state,
        ...payload
      } : {
        ...state,
        filtered: payload
      };
    case GET_SERVICES:
      return {
        ...state,
        filteredServices: payload
      };
    default:
      return state;
  }
};

export default homeReducer;
