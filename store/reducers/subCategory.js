/* eslint-disable no-console */
import {
  CROSS_FILTER_SCAT, RESET_FILTER_SCAT, SET_SUBCATEGORIES, TICK_SCAT
} from '../actions/subCategory';

const initialValue = {};

const subCategoryReducer = (state = initialValue, action) => {
  const { type, payload } = action;
  switch (type) {
    case TICK_SCAT:
      const item = payload;
      const toggledFilter = !state[item];
      return { ...state, [payload]: toggledFilter };
    case CROSS_FILTER_SCAT:
      return { ...state, [payload]: false };
    case RESET_FILTER_SCAT:
      return payload;
    case SET_SUBCATEGORIES:
      return payload;
    default:
      return state;
  }
};

export default subCategoryReducer;
