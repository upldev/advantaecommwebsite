import {
  RESET_TOAST,
  SET_LOADER,
  SHOW_LOADING,
  SHOW_TOAST
} from '../actions/feedback';

const initialState = {
  isLoading: false,
  isPaymentLoading: false,
  isOrderProcessing: false,
  isCartUpdating: false
};

const feedbackReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SHOW_LOADING:
      return { ...state, isLoading: payload };
    case SHOW_TOAST:
      return { ...state, toast: payload };
    case SET_LOADER:
      return {
        ...state,
        ...payload,
      };
    case RESET_TOAST:
      return { ...state, toast: payload };
    default:
      return state;
  }
};

export default feedbackReducer;
