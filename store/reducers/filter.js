import { PRICE_RANGE } from '../../constants/priceRange';
import {
  ALPHABETICAL_SORT_FILTER,
  CROSS_FILTER,
  RESET_FILTERS,
  SET_BRAND_TYPES,
  SET_PRICE_RANGE,
  SET_SEEDS_PRODUCTS,
  SET_SEED_TYPES,
  SET_SORT_FILTER,
  TICK
} from '../actions/filter';

const initialValue = {
  sort: '',
  priceRange: [...PRICE_RANGE],
  alphabeticalSort: false,
  seedProducts: [],
  seedTypes: [],
  brandTypes: [],
};

const filterReducer = (state = initialValue, action) => {
  const { type, payload } = action;
  switch (type) {
    case TICK:
      const item = payload;
      const toggledFilter = !state[item];
      return { ...state, [payload]: toggledFilter };
    case ALPHABETICAL_SORT_FILTER:
      const alphabeticalSortValue = payload;
      return { ...state, alphabeticalSort: alphabeticalSortValue };
    case CROSS_FILTER:
      return { ...state, [payload]: false };
    case SET_SORT_FILTER:
      return { ...state, sort: payload };
    case SET_PRICE_RANGE:
      return {
        ...state,
        priceRange: state.priceRange.map((range) => (range.name === payload
          ? { ...range, isChecked: !range.isChecked }
          : range)),
      };
    case SET_SEEDS_PRODUCTS:
      return {
        ...state,
        seedProducts: state.seedProducts.map((seedProduct) => {
          return seedProduct.name === payload
            ? { ...seedProduct, isChecked: !seedProduct.isChecked }
            : seedProduct;
        }),
      };
    case SET_SEED_TYPES:
      return {
        ...state,
        seedTypes: state.seedTypes.map((seedType) => {
          return seedType.name === payload
            ? { ...seedType, isChecked: !seedType.isChecked }
            : seedType;
        }),
      };
    case SET_BRAND_TYPES:
      return {
        ...state,
        brandTypes: state.brandTypes.map((brandType) => {
          return brandType.name === payload
            ? { ...brandType, isChecked: !brandType.isChecked }
            : brandType;
        }),
      };
    case RESET_FILTERS:
      return {
        ...initialValue,
        priceRange: state.priceRange.map((range) => ({
          ...range,
          isChecked: false,
        })),
        seedProducts: state.seedProducts.map((seedProduct) => ({
          ...seedProduct,
          isChecked: false,
        })),
        seedTypes: state.seedTypes.map((seedType) => ({
          ...seedType,
          isChecked: false,
        })),
        brandTypes: state.brandTypes.map((brandType) => ({
          ...brandType,
          isChecked: false,
        })),
      };
    default:
      return state;
  }
};

export default filterReducer;
