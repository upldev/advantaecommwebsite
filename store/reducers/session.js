import {
  ACTIVATE_SESSION,
  CLEAR_REGISTRATION_DATA,
  SAVE_REGISTRATION_DATA,
  USER_LOGIN,
  USER_LOGOUT,
  USER_REGISTER
} from '../actions/user';

// active session => user authenticated & session not expired

const sessionReducer = (state = { activeSession: true }, action) => {
  const { type, payload } = action;
  switch (type) {
    case ACTIVATE_SESSION:
    case USER_LOGIN:
    case USER_REGISTER:
      return {
        ...state,
        activeSession: true
      };
    case USER_LOGOUT:
      return {
        ...state,
        activeSession: false
      };
    case SAVE_REGISTRATION_DATA:
    // save registered profile in case profile fails and user tries log in
      // on log in create profile with the saved data
      return {
        ...state,
        registeredProfile: payload
      };
    case CLEAR_REGISTRATION_DATA:
      return {
        ...state,
        registeredProfile: null
      };
    default:
      return state;
  }
};

export default sessionReducer;
