/* eslint-disable max-len */
import {
  ADD_TO_CART, ADD_TO_WISHLIST, BAG_SIZE_CHANGE, CART_RESET,
  DECREMENT_QUANTITY, FETCH_UPDATES, INCREMENT_QUANTITY, LISTENER_UPDATE_CART,
  LOGIN_UPDATE,
  REMOVE_FROM_CART, SET_CART_PRODUCTS, UPDATE_TO_BACKEND
} from '../actions/cart';
import { USER_LOGIN, USER_LOGOUT } from '../actions/user';

const cartReducer = (state = { cartItems: [], cartProducts: [], isUpdating: false }, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_TO_CART:
      const item = payload;
      const existItem = state.cartItems.find((x) => x.sku.ecomCode === item.sku.ecomCode);
      if (existItem) {
        return {
          ...state,
          cartItems: state.cartItems.map((x) => {
            return x.sku.ecomCode === existItem.sku.ecomCode ? { ...x, qty: x.qty + item.qty } : x;
          }),
        };
      }
      return {
        ...state,
        cartItems: [...state.cartItems, item],
      };
    case ADD_TO_WISHLIST:
      const removeCurrId = payload.skuCode;
      return {
        ...state,
        cartItems: state.cartItems.filter((x) => x.sku.ecomCode !== removeCurrId),
        cartProducts: state.cartProducts.filter((x) => x.sku.ecomCode !== removeCurrId),
      };
    case REMOVE_FROM_CART:
      const removeId = payload;
      return {
        ...state,
        cartItems: state.cartItems.filter((x) => x.sku.ecomCode !== removeId),
        cartProducts: state.cartProducts.filter((x) => x.sku.ecomCode !== removeId),
      };
    case INCREMENT_QUANTITY:
      const incrementId = payload;
      return {
        ...state,
        cartItems: state.cartItems.map((x) => (x.sku.ecomCode === incrementId ? { ...x, qty: x.qty + 1 } : x)),
      };
    case SET_CART_PRODUCTS:
      return {
        ...state,
        cartProducts: payload,
      };
    case DECREMENT_QUANTITY:
      const decrementId = payload;
      return {
        ...state,
        cartItems: state.cartItems.map((x) => (x.sku.ecomCode === decrementId ? { ...x, qty: x.qty - 1 } : x)),
      };
    case BAG_SIZE_CHANGE:
      const data = payload;
      return {
        ...state,
        cartItems: state.cartItems.map((x) => (x.materialCode === data.materialCode ? { ...x, sku: data.sku, indexOfAddedSKU: data.indexOfAddedSKU } : x)),
      };
    case CART_RESET:
      return {
        ...state,
        cartItems: [],
        cartProducts: [],
        isUpdating: false
      };
    case LISTENER_UPDATE_CART:
      if (payload) {
        return {
          ...state,
          ...payload
        };
      }
      return {
        ...state,
        isUpdating: false
      };
    case UPDATE_TO_BACKEND:
      return {
        ...state,
        isUpdating: true
      };
    case USER_LOGIN:
      return {
        ...state,
        isUpdating: true
      };
    case FETCH_UPDATES:
      return {
        ...state,
        cartItems: payload.cart
      };
    case USER_LOGOUT:
      return { cartItems: [], cartProducts: [], isUpdating: false };
    case LOGIN_UPDATE:
      return {
        ...state,
        cartItems: [...payload]
      };
    default:
      return state;
  }
};

export default cartReducer;
