import {
  ADD_ORDERS,
  PIN_SERVICEABLE,
  RESET_ORDER_FILTER,
  SET_ORDER_FILTER,
  SET_USER_PROFILE,
  UPDATE_USER_PROFILE, USER_LOGIN,
  USER_LOGOUT, USER_REGISTER
} from '../actions/user';

const initialState = {
  logInStatus: false,
  hasProfile: false,
  profile: null,
  userServiceable: null,
  check: null,
  orders: [],
  currentOrder: {},
  orderStatusFilter: [],
  orderDateFilter: [],
  appliedOrderStatusFilter: [],
  appliedOrderDateFilter: []
};

const userReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case USER_LOGIN:
      return {
        ...state,
        ...payload
      };
    case SET_USER_PROFILE:
      return {
        ...state,
        ...payload
      };
    case USER_LOGOUT:
      return initialState;
    case USER_REGISTER:
      return {
        ...state,
        logInStatus: true,
        hasProfile: true,
        ...payload
      };
    case UPDATE_USER_PROFILE:
      return {
        ...state,
        profile: { ...state.profile, ...payload }
      };
    case PIN_SERVICEABLE:
      return {
        ...state,
        ...payload
      };
    case ADD_ORDERS:
      return {
        ...state,
        ...payload
      };
    case SET_ORDER_FILTER:
      return {
        ...state,
        ...payload
      };
    case RESET_ORDER_FILTER:
      return {
        ...state,
        appliedOrderDateFilter: [],
        appliedOrderStatusFilter: []
      };
    default:
      return state;
  }
};

export default userReducer;
