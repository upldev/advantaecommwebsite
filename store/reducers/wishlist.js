/* eslint-disable max-len */
import {
  ADD_ONLY_TO_WISHLIST, ADD_TO_WISHLIST, FETCH_UPDATES, LISTENER_UPDATE_WISHLIST, REMOVE_FROM_WISHLIST,
  RESET_WISHLIST, SET_WISHLIST_PRODUCTS, UPDATE_TO_BACKEND
} from '../actions/cart';
import { USER_LOGIN, USER_LOGOUT } from '../actions/user';
import { LOGIN_WISHLIST_UPDATE } from '../actions/wishlist';

const wishlistReducer = (state = { wishlistItems: [], wishlistProducts: [], isUpdating: false }, action) => {
  const { type, payload } = action;

  switch (type) {
    case ADD_TO_WISHLIST:
      const { materialCode } = payload;
      const isExists = state.wishlistItems.includes(materialCode);
      if (isExists) {
        return state;
      }
      return {
        ...state,
        wishlistItems: [...state.wishlistItems, materialCode],
      };
    case ADD_ONLY_TO_WISHLIST:
      const currItem = payload;
      const alreadyInWishlist = state.wishlistItems.includes(materialCode);
      if (alreadyInWishlist) {
        return state;
      }
      return {
        ...state,
        wishlistItems: [...state.wishlistItems, currItem],
      };
    case SET_WISHLIST_PRODUCTS:
      return {
        ...state,
        wishlistProducts: payload,
      };
    case REMOVE_FROM_WISHLIST:
      const removeId = payload;
      return {
        ...state,
        wishlistItems: state.wishlistItems.filter((x) => x !== removeId),
        wishlistProducts: state.wishlistProducts.filter((x) => x.materialCode !== removeId),
      };
    case RESET_WISHLIST:
      return {
        ...state,
        wishlistItems: [],
      };
    case LISTENER_UPDATE_WISHLIST:
      if (payload) {
        return {
          ...state,
          wishlistItems: payload
        };
      }
      return {
        ...state,
        isUpdating: false
      };
    case UPDATE_TO_BACKEND:
      return {
        ...state,
        isUpdating: true
      };
    case USER_LOGIN:
      return {
        ...state,
        isUpdating: true
      };
    case FETCH_UPDATES:
      return {
        ...state,
        wishlistItems: payload.wishList
      };
    case USER_LOGOUT:
      return { wishlistItems: [], wishlistProducts: [], isUpdating: false };
    case LOGIN_WISHLIST_UPDATE:
      return { ...state, wishlistItems: [...payload] };
    default:
      return state;
  }
};

export default wishlistReducer;
