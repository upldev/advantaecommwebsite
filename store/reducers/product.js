import {
  AVAILABLE_CATEGORIES,
  AVAILABLE_SUBCATEGORIES,
  RESET_FILTER_DATA,
  SAVE_PRODUCTS,
  SET_CAT_FILTERED_PRODUCTS,
  SET_CURRENT_PRODUCT,
  SET_PRODUCT_DETAILS,
  SET_PRODUCT_LIST_LOADER,
  SET_SCAT_FILTERED_PRODUCTS,
  SET_SIMILAR_PRODUCTS,
  SORT_ALPHABETICALLY,
  SORT_PRODUCTS,
  SORT_PRODUCTS_PRICERANGE
} from '../actions/product';

const initialState = {
  products: [],
  filteredProducts: [],
  priceFiltered: [],
  subcategoryFiltered: [],
  categoryFiltered: [],
  availableCategories: [],
  availableSubcategories: [],
  currentProduct: {},
  productDetails: []
};

const productReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SAVE_PRODUCTS:
      return {
        ...state,
        products: payload
      };
    case SET_CURRENT_PRODUCT:
      return {
        ...state,
        currentProduct: payload
      };
    case SORT_PRODUCTS:
    case SORT_ALPHABETICALLY:
      return {
        ...state,
        filteredProducts: payload
      };
    case SORT_PRODUCTS_PRICERANGE:
      return {
        ...state,
        priceFiltered: payload.priceFiltered,
        filteredProducts: payload.filteredProducts
      };
    case AVAILABLE_CATEGORIES:
      return {
        ...state,
        availableCategories: payload
      };
    case AVAILABLE_SUBCATEGORIES:
      return {
        ...state,
        availableSubcategories: payload
      };
    case SET_CAT_FILTERED_PRODUCTS:
      return {
        ...state,
        categoryFiltered: payload.categoryFiltered,
        filteredProducts: payload.filteredProducts
      };
    case SET_SCAT_FILTERED_PRODUCTS:
      return {
        ...state,
        subcategoryFiltered: payload.subcategoryFiltered,
        filteredProducts: payload.filteredProducts
      };
    case RESET_FILTER_DATA:
      return {
        ...state,
        filteredProducts: [],
        priceFiltered: [],
        categoryFiltered: [],
        subcategoryFiltered: []
      };
    case SET_PRODUCT_LIST_LOADER:
      return {
        ...state,
        ...payload
      };
    case SET_PRODUCT_DETAILS:
      return {
        ...state,
        productDetails: state.productDetails?.concat([payload]) || [payload]
      };
    case SET_SIMILAR_PRODUCTS:
      return {
        ...state,
        similarProducts: payload
      };
    default:
      return state;
  }
};

export default productReducer;
