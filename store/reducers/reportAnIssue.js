import { CREATE_AN_ISSUE, NEW_ISSUE } from '../actions/reportAnIssue';

const initialState = {
  complaintId: '',
  farmerMobile: '',
  complaintType: '',
  complaintDesc: ''
};

const reportAnIssueReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CREATE_AN_ISSUE:
      const { complaintId, farmerMobile } = payload;
      return {
        ...state,
        complaintId,
        farmerMobile
      };
    case NEW_ISSUE:
      const { newComplaintId } = payload;
      return {
        ...state,
        complaintId: newComplaintId
      };
    default:
      return state;
  }
};

export default reportAnIssueReducer;
