/* eslint-disable no-console */
import {
  CROSS_FILTER_CAT, RESET_FILTER_CAT, SET_CATEGORIES, TICK_CAT
} from '../actions/category';

const initialValue = {};

const categoryReducer = (state = initialValue, action) => {
  const { type, payload } = action;
  switch (type) {
    case TICK_CAT:
      const item = payload;
      const toggledFilter = !state[item];
      return { ...state, [payload]: toggledFilter };
    case CROSS_FILTER_CAT:
      return { ...state, [payload]: false };
    case RESET_FILTER_CAT:
      return payload;
    case SET_CATEGORIES:
      return payload;
    default:
      return state;
  }
};

export default categoryReducer;
