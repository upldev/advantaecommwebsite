/* eslint-disable max-len */

import { ADD_PRODUCT_REVIEWS, CLEAR_PRODUCT_REVIEWS } from '../actions/review';

const initialState = {
  productReviews: []
};

const reviewReducer = (state = {}, action) => {
  const { type, payload } = action;
  switch (type) {
    case ADD_PRODUCT_REVIEWS:
      return {
        ...state,
        productReviews: payload
      };
    case CLEAR_PRODUCT_REVIEWS:
      return {
        ...state,
        productReviews: []
      };
    default:
      return state;
  }
};

export default reviewReducer;
