import {
  ADD_CURRENT_LOGGED_USER,
  ADD_CURRENT_SHIP_TO,
  ADD_LOCAL_GUEST,
  REMOVE_CURRENT_SHIP_TO,
  RESET_CHECKOUT
} from '../actions/checkout';
import {
  ADD_CARTCOUPON,
  ADD_PRODUCTCOUPON,
  ADD_REFERRAL,
  RADIO_ADD_CARTCOUPON,
  RADIO_ADD_PRODUCTCOUPON,
  REMOVE_CARTCOUPON,
  REMOVE_PRODUCTCOUPON,
  RESET_COUPONS,
  SET_COUPONS
} from '../actions/coupons';
import { USER_LOGOUT } from '../actions/user';

const initialState = {
  currentUser: {},
  currentShipTo: {},
  appliedCartCoupons: [],
  appliedProductCoupons: [],
  otherDiscounts: [],
};

const checkoutReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case RESET_CHECKOUT:
      return initialState;
    case REMOVE_CURRENT_SHIP_TO:
      return {
        ...state,
        currentShipTo: {},
      };
    case ADD_CURRENT_SHIP_TO:
      return {
        ...state,
        currentShipTo: payload,
      };
    case ADD_LOCAL_GUEST:
      return {
        ...state,
        guest: payload,
      };
    case ADD_CURRENT_LOGGED_USER:
      return {
        ...state,
        currentUser: payload,
      };
    case ADD_CARTCOUPON:
      return {
        ...state,
        appliedCartCoupons: [...state.appliedCartCoupons, payload],
      };
    case ADD_PRODUCTCOUPON:
      let added = false;
      const coupons = state.appliedProductCoupons.map((item) => {
        if (item.sku === payload.sku) {
          added = true;
          return {
            sku: item.sku,
            materialCode: item.materialCode,
            discounts: [...item.discounts, payload.discount],
          };
        }
        return item;
      });
      if (!added) {
        coupons.push({
          sku: payload.sku,
          materialCode: payload.materialCode,
          discounts: [payload.discount],
        });
      }
      return {
        ...state,
        appliedProductCoupons: coupons,
      };
    case REMOVE_CARTCOUPON:
      return {
        ...state,
        appliedCartCoupons: state.appliedCartCoupons.filter(
          (item) => item.discountId !== payload
        ),
      };
    case REMOVE_PRODUCTCOUPON:
      return {
        ...state,
        appliedProductCoupons: state.appliedProductCoupons.map((item) => {
          if (item.sku === payload.sku) {
            return {
              sku: item.sku,
              materialCode: item.materialCode,
              discounts: item.discounts.filter(
                (coup) => coup.discountId !== payload.discountId
              ),
            };
          }
          return item;
        }),
      };
    case RADIO_ADD_CARTCOUPON:
      const { autoApply } = state;
      let discounts;
      if (autoApply) {
        discounts = state.appliedProductCoupons.map((item) => {
          return {
            sku: item.sku,
            materialCode: item.materialCode,
            discounts: item.discounts.filter((discount) => discount.isQtyBased)
          };
        });
      }
      return {
        ...state,
        appliedProductCoupons: autoApply ? [...discounts] : [],
        appliedCartCoupons: autoApply ? [...state.appliedCartCoupons.filter(
          (discount) => discount.isCartQtyBased || discount.isCartValueBased
        ), payload] : [payload],
      };
    case RADIO_ADD_PRODUCTCOUPON:
      const { autoApply: autoApply1 } = state;
      added = false;
      discounts = [];
      if (autoApply1) {
        discounts = state.appliedProductCoupons.map((item) => {
          if (item.sku === payload.sku) {
            added = true;
            return {
              sku: item.sku,
              materialCode: item.materialCode,
              discounts: [...item.discounts.filter(
                (discount) => discount.isQtyBased
              ), payload.discount],
            };
          }
          return {
            sku: item.sku,
            materialCode: item.materialCode,
            discounts: item.discounts.filter((discount) => discount.isQtyBased)
          };
        });
      }
      if (!added) {
        discounts.push({
          sku: payload.sku,
          materialCode: payload.materialCode,
          discounts: [payload.discount],
        });
      }
      return {
        ...state,
        appliedCartCoupons: autoApply1 ? [...state.appliedCartCoupons.filter(
          (discount) => discount.isCartQtyBased || discount.isCartValueBased
        )] : [],
        appliedProductCoupons: [...discounts],
      };
    case ADD_REFERRAL:
      return {
        ...state,
        otherDiscounts: [payload, ...state.otherDiscounts],
      };
    case SET_COUPONS:
      return {
        ...state,
        appliedCartCoupons: [...payload.cartdiscounts],
        appliedProductCoupons: [...payload.productdiscounts],
        otherDiscounts: [...payload.otherDiscounts],
        autoApply: payload.autoApply
      };
    case RESET_COUPONS:
    case USER_LOGOUT:
      return {
        ...state,
        appliedCartCoupons: [],
        appliedProductCoupons: [],
        otherDiscounts: [],
      };
    default:
      return state;
  }
};

export default checkoutReducer;
