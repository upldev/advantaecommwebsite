import {
  POP_TOKEN,
  PUSH_TOKEN, PUT_ORDER_COUNT, RESET_ORDERS_FILTERS,
  RESET_TOKENS,
  SET_ORDERS,
  TOGGLE_ORDERS_FILTER
} from '../actions/orders';

const initialState = {
  orders: [],
  tokens: [null],
  filters: [],
  orderCount: 0
};

const ordersReducer = (state = initialState, action) => {
  const { type, payload = null } = action;

  switch (type) {
    case SET_ORDERS:
      return {
        ...state,
        orders: payload,
      };
    case PUSH_TOKEN:
      return {
        ...state,
        tokens: [...state.tokens, payload],
      };
    case POP_TOKEN:
      const tokens = state.tokens.slice(0, state.tokens.length - 1);

      return {
        ...state,
        tokens,
      };
    case RESET_TOKENS:
      return {
        ...state,
        tokens: [null],
      };
    case TOGGLE_ORDERS_FILTER:
      const isExists = state.filters.find((item) => item.key === payload.key);

      if (isExists) {
        const filtered = state.filters.filter(
          (item) => item.key !== payload.key
        );

        return {
          ...state,
          filters: filtered,
        };
      }

      return {
        ...state,
        filters: [...state.filters, payload],
      };
    case RESET_ORDERS_FILTERS:
      return {
        ...state,
        filters: [],
      };
    case PUT_ORDER_COUNT:
      return {
        ...state,
        orderCount: payload
      };
    default:
      return state;
  }
};

export default ordersReducer;
