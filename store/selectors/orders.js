import { createSelector } from 'reselect';

const ordersSelector = (state) => state.orders;

export const selectOrders = createSelector(
  ordersSelector,
  (orders) => orders.orders
);

export const selectIsPrev = createSelector(ordersSelector, (orders) => {
  return orders.tokens.length > 2;
});

export const selectIsNext = createSelector(ordersSelector, (orders) => {
  const { length } = orders.tokens;
  return orders.tokens.length > 1 && orders.tokens[length - 1];
});

export const selectDescription = createSelector(ordersSelector, (orders) => {
  const ordersLength = orders.orders.length;
  const low = (orders.tokens.length - 2) * 5;

  return low >= 0
    ? `${low + 1} - ${low + ordersLength} orders  of ${orders.orderCount}`
    : '0 results';
});

export const selectIsFilterSelected = (item) => {
  return createSelector(ordersSelector, (orders) => {
    return orders.filters.find((filter) => filter.key === item.key);
  });
};
