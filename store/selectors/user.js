import { createSelector } from 'reselect';

const userSelector = (state) => state.user;
const checkoutSelector = (state) => state.checkout;

export const selectDefaultAddress = createSelector(
  checkoutSelector,
  (checkout) => {
    return (
      checkout?.currentUser?.shipping?.find((address) => address.isDefault)
      || {}
    );
  }
);

export const selectShippingAddresses = createSelector(
  checkoutSelector,
  (checkout) => {
    return (
      checkout?.currentUser?.shipping?.filter(
        (address) => !address.isDefault
      ) || []
    );
  }
);
