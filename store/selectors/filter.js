import { createSelector } from 'reselect';

const filterSelector = (state) => state.filter;

export const selectSort = createSelector(
  filterSelector,
  (filter) => filter.sort
);

export const selectSeedProducts = createSelector(
  filterSelector,
  (filter) => filter.seedProducts
);

export const selectSeedTypes = createSelector(
  filterSelector,
  (filter) => filter.seedTypes
);

export const selectBrandTypes = createSelector(
  filterSelector,
  (filter) => filter.brandTypes
);

export const selectPriceRange = createSelector(
  filterSelector,
  (filter) => filter.priceRange
);

export const selectCurrentPriceRange = createSelector(
  filterSelector,
  (filter) => filter.priceRange.find((range) => range.isChecked)?.name || ''
);
