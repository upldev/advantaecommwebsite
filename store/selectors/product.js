import { createSelector } from 'reselect';
import {
  ABOVE_5000,
  BELOW_1000,
  BETWEEN_1000_5000
} from '../../constants/priceRange';
import {
  A_TO_Z,
  BEST_SELLERS,
  HIGH_TO_LOW,
  LOW_TO_HIGH,
  TRENDING,
  Z_TO_A
} from '../../constants/sortBy';

const productSelector = (state) => state.product;
const filterSelector = (state) => state.filter;

export const selectProducts = createSelector(
  productSelector,
  filterSelector,
  (product, filter) => {
    let filteredProducts = [...product.products];

    const appliedFilters = [
      ...filter.seedProducts.reduce((filtered, item) => {
        if (item.isChecked) {
          filtered.push(item.name);
        }
        return filtered;
      }, []),
      ...filter.seedTypes.reduce((filtered, item) => {
        if (item.isChecked) {
          filtered.push(item.name);
        }
        return filtered;
      }, []),
      ...filter.brandTypes.reduce((filtered, item) => {
        if (item.isChecked) {
          filtered.push(item.name);
        }
        return filtered;
      }, []),
    ];

    const priceRange = filter.priceRange.reduce((filtered, item) => {
      if (item.isChecked) {
        filtered.push(item.name);
      }
      return filtered;
    }, []);

    if (appliedFilters?.length > 0 || priceRange?.length > 0) {
      filteredProducts = filteredProducts.filter(
        ({
          filterCategory, category, crop, sku
        }) => {
          const inFilters = appliedFilters.some((value) => {
            if (
              value === filterCategory
              || value === category
              || value === crop
            ) {
              return true;
            }

            return false;
          });

          const inPrice = priceRange.some((range) => {
            const { ecomPrice } = sku[0];

            if (range === BELOW_1000) {
              return ecomPrice <= 1000;
            }
            if (range === BETWEEN_1000_5000) {
              return ecomPrice > 1000 && ecomPrice < 5000;
            }
            if (range === ABOVE_5000) {
              return ecomPrice >= 5000;
            }

            return false;
          });

          return inFilters || inPrice;
        }
      );
    }

    const { sort } = filter;

    if (sort !== '') {
      if (sort === BEST_SELLERS) {
        filteredProducts.sort((a, b) => {
          if (a.isBestSeller) {
            return -1;
          }
          if (b.isBestSeller) {
            return 1;
          }

          return 0;
        });
      }

      if (sort === TRENDING) {
        filteredProducts.sort((a, b) => {
          if (a.isTrending) {
            return -1;
          }
          if (b.isTrending) {
            return 1;
          }

          return 0;
        });
      }

      if (sort === LOW_TO_HIGH) {
        filteredProducts.sort((a, b) => {
          const ecomA = a.sku[0].ecomPrice;
          const ecomB = b.sku[0].ecomPrice;

          return ecomA - ecomB;
        });
      }

      if (sort === HIGH_TO_LOW) {
        filteredProducts.sort((a, b) => {
          const ecomA = a.sku[0].ecomPrice;
          const ecomB = b.sku[0].ecomPrice;

          return ecomB - ecomA;
        });
      }

      if (sort === A_TO_Z) {
        filteredProducts.sort((a, b) => {
          const cropA = a.crop.toUpperCase();
          const cropB = b.crop.toUpperCase();

          if (cropA < cropB) {
            return -1;
          }
          if (cropA > cropB) {
            return 1;
          }

          return 0;
        });
      }

      if (sort === Z_TO_A) {
        filteredProducts.sort((a, b) => {
          const cropA = a.crop.toUpperCase();
          const cropB = b.crop.toUpperCase();

          if (cropA > cropB) {
            return -1;
          }
          if (cropA < cropB) {
            return 1;
          }

          return 0;
        });
      }
    }

    return filteredProducts;
  }
);
