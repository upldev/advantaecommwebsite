import { createSelector } from 'reselect';

const cartSelector = (state) => state.cart;

export const selectProductInCart = (materialCode) => createSelector(cartSelector, (cart) => {
  return cart.cartItems?.find((item) => item.materialCode === materialCode);
});

export const selectSkuInCart = (ecomCode) => createSelector(cartSelector, (cart) => {
  return cart.cartItems?.find((item) => item.sku.ecomCode === ecomCode);
});

export const selectTotalItems = createSelector(cartSelector, (cart) => {
  return cart.cartItems?.reduce((sum, item) => sum + item.qty, 0);
});
export const selectCartItems = createSelector(
  [cartSelector],
  (cart) => cart.cartItems
);
export const selectTotalEcomPrice = createSelector(cartSelector, (cart) => {
  return cart.cartProducts?.reduce(
    (sum, { sku }, idx) => sum + sku.ecomPrice * cart.cartItems[idx].qty,
    0
  );
});

export const selectTotalDiscount = createSelector(cartSelector, (cart) => {
  return cart.cartProducts?.reduce(
    (sum, { sku }, idx) => sum
      + sku.MRP * cart.cartItems[idx].qty
      - sku.ecomPrice * cart.cartItems[idx].qty,
    0
  );
});

export const selectTotalMRP = createSelector(cartSelector, (cart) => {
  return cart.cartProducts?.reduce(
    (sum, { sku }, idx) => sum + sku.MRP * cart.cartItems[idx].qty,
    0
  );
});

export const selectItemQuantity = (ecomCode) => createSelector(cartSelector, (cart) => {
  return (
    cart.cartItems.find((item) => item?.sku?.ecomCode === ecomCode)?.qty || 0
  );
});

export const selectOrderItems = createSelector(
  cartSelector,
  ({ cartProducts, cartItems }) => {
    const tempItems = [];
    cartProducts.forEach((cartProduct) => {
      cartItems.forEach((cartItem) => {
        if (cartItem.materialCode === cartProduct.materialCode) {
          tempItems.push({
            ...cartProduct,
            ...cartItem,
          });
        }
      });
    });

    console.log(tempItems);

    return tempItems;
  }
);

export const selectIsInvalid = createSelector(
  cartSelector,
  ({ cartProducts }) => {
    const unavailable = cartProducts?.filter(
      (item) => item.sku?.qtyAvailable <= 0
    );

    return { isInvalid: unavailable?.length > 0, unavailable };
  }
);
