import { createSelector } from 'reselect';
import { selectCartItems, selectTotalEcomPrice } from './cart';

const checkoutSelector = (state) => state?.checkout;

export const selectPaymentInfo = createSelector(
  [checkoutSelector, selectCartItems, selectTotalEcomPrice],
  (checkout, cartItems, totalEcomPrice) => {
    const { appliedCartCoupons, appliedProductCoupons, otherDiscounts } = checkout;

    // let sum = 0;
    const initial = [];
    let bulkMinus = 0;
    let couponMinus = 0;
    let referralMinus = 0;

    cartItems.forEach((item) => {
      // iterate over all cart items
      // let add = false;
      appliedProductCoupons.forEach((coupon) => {
        // for each sku check if product coupons applied
        if (coupon.sku === item.sku.ecomCode && coupon.discounts.length > 0) {
          coupon.discounts.forEach((dis) => {
            if (dis.isQtyBased) {
              if (dis.discount.isFixed) {
                bulkMinus += dis.discount.amount * item.qty; // add product qty
              } else {
                bulkMinus
                  += item.sku?.ecomPrice * 0.01 * dis.discount.amount * item.qty;
              }
            } else if (dis.discount.isFixed) {
              couponMinus += dis.discount.amount * item.qty;
            } else {
              couponMinus
                += item.sku?.ecomPrice * 0.01 * dis.discount.amount * item.qty;
            }
          });
        }
      });
    });

    appliedCartCoupons.forEach((item) => {
      if (item.discount.isFixed) {
        if (item.isCartQtyBased || item.isCartValueBased) {
          // cart qty & cart value discount
          bulkMinus += item.discount.amount;
        } else {
          // cart coupon
          couponMinus += item.discount.amount;
        }
      }
      // discount in %
      if (item.isCartQtyBased || item.isCartValueBased) {
        bulkMinus += totalEcomPrice * 0.01 * item.discount.amount;
      } else {
        couponMinus += totalEcomPrice * 0.01 * item.discount.amount;
      }
    });

    otherDiscounts.forEach((item) => {
      if (item.discount.isFixed) {
        if (
          item.couponType === 'REFERRAL_REFERER'
          || item.couponType === 'REFERRAL_REFEREE'
        ) {
          referralMinus += item.discount.amount; // referral or referee
        }

        if (
          item.couponType === 'FIRSTPURCHASE'
          || item.couponType === 'SECONDPURCHASE'
        ) {
          initial.push({
            discountId: item.discountId,
            couponType: item.couponType,
            couponName: item.couponName,
            discount: item.discount,
          });
        }
      } else {
        // discount in %
        if (
          item.couponType === 'REFERRAL_REFERER'
          || item.couponType === 'REFERRAL_REFEREE'
        ) {
          referralMinus += totalEcomPrice * (0.01 * item.discount.amount);
        }
        if (
          item.couponType === 'FIRSTPURCHASE'
          || item.couponType === 'SECONDPURCHASE'
        ) {
          initial.push({
            discountId: item.discountId,
            couponType: item.couponType,
            couponName: item.couponName,
            discount: totalEcomPrice * (0.01 * item.discount.amount),
          });
        }
      }
    });

    return {
      bulkDiscount: bulkMinus,
      couponDiscount: couponMinus,
      referralDiscount: referralMinus,
      finalTotal: totalEcomPrice - bulkMinus - couponMinus - referralMinus,
      initialPurchase: initial,
    };
  }
);
