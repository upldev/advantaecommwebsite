import { createSelector } from 'reselect';

const orderSelector = (state) => state.user;

export const filteredOrders = createSelector(orderSelector, (orderData) => {
  if (orderData.appliedOrderStatusFilter.length === 0
    && orderData.appliedOrderDateFilter.length === 0) {
    const orders = [...orderData.orders];
    orders.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    return orders;
  }
  const filtered = [...orderData.orders.filter((order) => (
    orderData.appliedOrderStatusFilter.includes(order.orderStatus.toLowerCase())
    || orderData.appliedOrderDateFilter.includes(order.orderDate.substr(0, 4))
    || (orderData.appliedOrderDateFilter.includes('Last 30 days')
    && Math.abs((new Date(order.orderDate) - new Date()) / (1000 * 60 * 60 * 24)) < 30)
  ))];
  filtered.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
  return filtered;
});
