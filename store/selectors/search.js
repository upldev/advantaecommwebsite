import { createSelector } from 'reselect';
import { productDetailsPage } from '../../lib/router';

const searchSelector = (state) => state?.search;
const homeSelector = (state) => state?.home;

export const selectSearchPhrase = createSelector(
  [searchSelector],
  (search) => search.phrase
);

export const selectSearchedProducts = createSelector(
  [searchSelector, homeSelector],
  (search, home) => {
    const products = search.filteredProducts?.map((product) => ({
      value: productDetailsPage(product.materialCode),
      label: product.product,
    })) || [];

    const services = home.filteredServices?.map((service) => ({
      value: service.redirect,
      label: service.key,
    })) || [];

    return [...products, ...services];
  }
);

export const selectIsSearching = createSelector(
  [searchSelector],
  (search) => search.isSearching
);

export const selectIsSearched = createSelector(
  [searchSelector],
  (search) => search.phrase !== ''
);
