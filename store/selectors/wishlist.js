import { createSelector } from 'reselect';

const wishlistSelector = (state) => state.wishlist;

export const selectIsWishlisted = (materialCode) => createSelector(wishlistSelector, (wishlist) => {
  return wishlist.wishlistItems.find((item) => item === materialCode);
});
