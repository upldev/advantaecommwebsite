import { createSelector } from 'reselect';

const homeSelector = (state) => state.home;

export const selectTrendingProducts = createSelector(homeSelector, (home) => {
  const trending = [...home.trending];

  trending.sort((a, b) => {
    return b.sku[0].qtyAvailable - a.sku[0].qtyAvailable;
  });

  return trending;
});

export const selectBestSellerProducts = createSelector(homeSelector, (home) => {
  const bestseller = [...home.bestseller];

  bestseller.sort((a, b) => {
    return b.sku[0].qtyAvailable - a.sku[0].qtyAvailable;
  });

  return bestseller;
});

export const selectHeroBanners = createSelector(
  homeSelector,
  (home) => home.hero
);

export const selectOffers = createSelector(homeSelector, (home) => home.offers);
