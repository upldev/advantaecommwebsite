/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable no-unused-expressions */
/* eslint-disable quotes */
/* eslint-disable no-var */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable no-await-in-loop */
/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_BANNERTABLE_ARN
	API_EAMS5TAPI01_BANNERTABLE_NAME
	API_EAMS5TAPI01_COMPLAINTTABLE_ARN
	API_EAMS5TAPI01_COMPLAINTTABLE_NAME
	API_EAMS5TAPI01_FAQTABLE_ARN
	API_EAMS5TAPI01_FAQTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	API_EAMS5TAPI01_PAYMENTTABLE_ARN
	API_EAMS5TAPI01_PAYMENTTABLE_NAME
	API_EAMS5TAPI01_PRODUCTREVIEWTABLE_ARN
	API_EAMS5TAPI01_PRODUCTREVIEWTABLE_NAME
	API_EAMS5TAPI01_PRODUCTTABLE_ARN
	API_EAMS5TAPI01_PRODUCTTABLE_NAME
	API_EAMS5TAPI01_REFEREETABLE_ARN
	API_EAMS5TAPI01_REFEREETABLE_NAME
	API_EAMS5TAPI01_REFERRERTABLE_ARN
	API_EAMS5TAPI01_REFERRERTABLE_NAME
	API_EAMS5TAPI01_SAPSTOCKTABLE_ARN
	API_EAMS5TAPI01_SAPSTOCKTABLE_NAME
	ENV
	REGION
	STORAGE_EAMS5TS301_BUCKETNAME
Amplify Params - DO NOT EDIT *//*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

// declare a new express app
const app = express();
app.use(bodyParser.json({ limit: "100mb" }));
app.use(bodyParser.urlencoded({ limit: "100mb", extended: true }));
app.use(awsServerlessExpressMiddleware.eventContext());

const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const docClient = new AWS.DynamoDB.DocumentClient();

const soapRequest = require('easy-soap-request');
const xmljs = require('xml-js');

const {
  getProduct
} = require('./stock');
const {
  getBanner, updateClicks, addBanner, updateBanner
} = require('./banner');

// Enable CORS for all methods
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate, public');
  res.header('Pragma', 'no-cache');
  res.header('X-Frame-Options', 'SAMEORIGIN');
  res.header('Strict-Transport-Security', 'max-age=16070400; include Subdomains');
  res.header('X-Content-Type-Option', 'nosniff');
  res.header('X-XSS-Protection', '1; mode=block');
  next();
});

app.post('/checkPincode', async (req, res) => {
  const { pincode } = req.body;
  console.log(pincode);
  try {
    const url = 'http://netconnect.bluedart.com/Ver1.9/Demo/ShippingAPI/Finder/ServiceFinderQuery.svc';
    const xml = `
    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:sapi="http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin" xmlns:a="http://www.w3.org/2005/08/addressing">
   <soap:Header>
   <a:Action soap:mustUnderstand="1">http://tempuri.org/IServiceFinderQuery/GetServicesforProduct</a:Action>
   </soap:Header>
   <soap:Body>
      <tem:GetServicesforProduct>
         <!--Optional:-->
         <tem:pinCode>${pincode}</tem:pinCode>
         <!--Optional:-->
         <tem:pProductCode>A</tem:pProductCode>
         <!--Optional:-->
         <tem:pSubProductCode>P</tem:pSubProductCode>
         <!--Optional:-->
         <tem:profile>
            <!--Optional:-->
            <sapi:Api_type>S</sapi:Api_type>
            <!--Optional:-->
            <sapi:Area>HYD</sapi:Area>
            <!--Optional:-->
            <sapi:Customercode>099960</sapi:Customercode>
            <!--Optional:-->
            <sapi:IsAdmin></sapi:IsAdmin>
            <!--Optional:-->
            <sapi:LicenceKey>a28f0bb8690c75ce3368bb1c76ea98bc</sapi:LicenceKey>
            <!--Optional:-->
            <sapi:LoginID>MAA00001</sapi:LoginID>
            <!--Optional:-->
            <sapi:Password></sapi:Password>
            <!--Optional:-->
            <sapi:Version>1.10</sapi:Version>
         </tem:profile>
      </tem:GetServicesforProduct>
   </soap:Body>
</soap:Envelope>`;
    const requestHeaders = {
      'Content-Type': 'application/soap+xml',
      soapAction: 'http://netconnect.bluedart.com/Ver1.9/Demo/ShippingAPI/Finder/ServiceFinderQuery.svc'
    };
    const { response } = await soapRequest({ url, headers: requestHeaders, xml });
    const { headers, body, statusCode } = response;
    console.log(headers);
    console.log(body);
    console.log(statusCode);
    const jsonData = xmljs.xml2json(body, { compact: true, spaces: 4 });
    const parsedData = JSON.parse(jsonData);
    console.log(parsedData);

    const bodyResponse = parsedData['s:Envelope']['s:Body'].GetServicesforProductResponse.GetServicesforProductResult;
    console.log(bodyResponse);
    const servicable = bodyResponse['b:IsError']._text === 'false';
    console.log('Servicability: ', servicable);
    const errorMsg = bodyResponse['b:ErrorMessage']._text;
    console.log('Error Message: ', errorMsg);
    const resPincode = servicable ? bodyResponse['b:PinCode']._text : pincode;
    console.log('Pincode: ', resPincode);
    const desc = servicable ? bodyResponse['b:PinDescription']._text : 'Invalid Pincode';
    console.log('Desc: ', desc);
    const inbound = servicable ? bodyResponse['b:Service']._text.includes('I/B') : false;
    console.log('Inbound: ', inbound);

    return res.status(200).json({
      success: true,
      servicable: servicable && inbound,
      errorMsg,
      pincode: resPincode,
      desc
    });

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, msg: "Server error occurred" });
  }
});

app.post('/plpFilterData', async (req, res) => {
  console.log('inside product filters');
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
  };
  try {
    let scanResults = [];

    docClient.scan(params).eachPage((err, data, done) => {
      if (err) return res.status(200).json({ success: false, msg: 'Err in callback' });
      if (data != null) {
        data.Items.forEach((item) => scanResults.push(item));
      } else {
        const BELOW_1000 = 'thousandAndBelow';
        const BETWEEN_1000_5000 = 'oneToFiveThousand';
        const ABOVE_5000 = 'fiveThousandAndAbove';

        const PRICE_RANGE = [
          {
            name: BELOW_1000,
            label: '₹1000 and Below',
            quantity: 0,
            isChecked: false,
          },
          {
            name: BETWEEN_1000_5000,
            label: '₹1000 - ₹5000',
            quantity: 0,
            isChecked: false,
          },
          {
            name: ABOVE_5000,
            label: '₹5000 and Above',
            quantity: 0,
            isChecked: false,
          },
        ];

        const availableSeedProduct = {};
        const availableSeedTypes = {};
        const availableBrandTypes = {};
        const priceRange = [...PRICE_RANGE];

        const productsFormatted = scanResults.filter((item) => item.isPublic === true);

        productsFormatted.forEach(({
          filterCategory, category, crop, sku
        }) => {
          availableSeedProduct[filterCategory] = availableSeedProduct[filterCategory] + 1 || 1;
          availableSeedTypes[category] = availableSeedTypes[category] + 1 || 1;
          availableBrandTypes[crop] = availableBrandTypes[crop] + 1 || 1;

          const { ecomPrice } = sku[0];

          if (ecomPrice <= 1000) {
            priceRange[0].quantity++;
          } else if (ecomPrice > 1000 && ecomPrice < 5000) {
            priceRange[1].quantity++;
          } else {
            priceRange[2].quantity++;
          }
        });

        const seedProducts = Object.keys(availableSeedProduct).map((product) => ({
          name: product,
          quantity: availableSeedProduct[product],
          isChecked: false,
        }));

        const seedTypes = Object.keys(availableSeedTypes).map((product) => ({
          name: product,
          quantity: availableSeedTypes[product],
          isChecked: false,
        }));

        const brandTypes = Object.keys(availableBrandTypes).map((product) => ({
          name: product,
          quantity: availableBrandTypes[product],
          isChecked: false,
        }));

        return res.status(200).json({
          success: true,
          filters: {
            seedProducts,
            seedTypes,
            brandTypes,
            priceRange
          }
        });
      }
      done();
    });

  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error fetching Products' });
  }
});

app.post('/countFarmerOrders', async (req, res) => {
  const { farmerMobile } = req.body;
  console.log(farmerMobile);
  try {
    const params = {
      TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
      FilterExpression: 'farmerMobile =:farmerMobile',
      ExpressionAttributeValues: {
        ':farmerMobile': farmerMobile,
      },
      Select: "COUNT"
    };
    let count = 0;
    docClient.scan(params).eachPage((err, data, done) => {
      if (err) return res.status(200).json({ success: false, msg: 'Err in callback' });
      if (data != null) {
        count += data.Count;
        console.log(count);
      } else {
        return res.status(200).json({ success: true, count });
      }
      done();
    });

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, msg: "Error Counting Items" });
  }
});

app.post('/putBannerImage', async (req, res) => {
  const { filename, img, contentType } = req.body;
  console.log(req.body);
  if (
    filename === ""
    || filename === null
    || img === ""
    || img === null
    || contentType === ""
    || contentType === null
  ) {
    return res
      .status(200)
      .json({ success: false, msg: "Please pass all required fields" });
  }

  if (!(img.startsWith('data:image/') && contentType.includes('image/'))) {
    return res
      .status(200)
      .json({ success: false, msg: "Invalid Image Type" });
  }

  try {
    const params = {
      Bucket: process.env.STORAGE_EAMS5TS301_BUCKETNAME,
      Key: `banner_img/${filename}`,
      Body: Buffer.from(img.replace(/^data:image\/\w+;base64,/, ""), "base64"),
      ACL: "public-read",
      ContentType: contentType,
    };
    console.log(params);

    const s3response = await s3.upload(params).promise();
    console.log(s3response);

    if (s3response.Location) {
      return res.status(200).json({ success: true, url: s3response.Location });
    }
    return res
      .status(200)
      .json({ success: false, error: s3response, from: "s3" });

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, error: e });
  }

});

app.post('/addBanner', async (req, res) => {
  const {
    bannerId,
    bannerImg,
    bannerMobileImg,
    bannerType,
    clicks,
    redirectUrl,
    isActive,
    isUpdate
  } = req.body;
  console.log(req.body);

  if (redirectUrl !== "" && !redirectUrl.startsWith('https://shopuat.advantaseeds.com')) {
    return res.status(500).json({ success: false, msg: "Redirect URL Forbidden error" });
  }

  try {

    if (isUpdate) {
      const updRes = await updateBanner(
        bannerId, bannerImg, bannerMobileImg, bannerType, clicks, redirectUrl, isActive
      );
      console.log(updRes);
      return res.status(200).json({ success: true, msg: 'Successfully Updated Banner' });
    }

    const addRes = await addBanner(bannerId, bannerImg, bannerMobileImg,
      bannerType, clicks, redirectUrl, isActive);
    console.log(addRes);
    return res.status(200).json({ success: true, msg: 'Successfully Added Banner' });

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, msg: 'Error Modifying Banners' });
  }

});

app.post('/incBannerClick', async (req, res) => {
  const { bannerId } = req.body;
  console.log(bannerId);
  try {
    const banner = await getBanner(bannerId);
    console.log(banner);
    const updBanner = await updateClicks(bannerId, banner.clicks + 1);
    console.log(updBanner);
    return res.status(200).json({ success: true, msg: "Incremented Clicks" });
  } catch (e) {
    return res.status(200).json({ success: false, msg: "Click Increment Failed" });
  }
});

app.post('/getAllBanners', async (req, res) => {
  // Add your code here
  var params = {
    TableName: process.env.API_EAMS5TAPI01_BANNERTABLE_NAME,
  };
  try {
    let scanResults = [];
    docClient.scan(params).eachPage((err, data, done) => {
      if (err) return res.status(200).json({ success: false, msg: 'Err in callback' });
      if (data != null) {
        data.Items.forEach((item) => scanResults.push(item));
      } else {
        return res.status(200).json({ success: true, items: scanResults });
      }
      done();
    });
  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error fetching Banners' });
  }
});

app.post('/getAllOrders', async (req, res) => {
  const { filters, nextToken, isASC } = req.body;
  console.log(filters);
  console.log(nextToken);
  console.log(isASC);
  // Add your code here
  try {
    if (filters === null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
        ScanIndexForward: isASC,
      };
      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;
      console.log(finalParams);

      const orderPage = await docClient.scan(finalParams).promise();
      console.log(orderPage);

      return res.status(200).json(
        {
          success: true,
          count: orderPage.Count,
          items: orderPage.Items,
          nextToken: orderPage.LastEvaluatedKey
        }
      );
    }

    if (filters.orderId !== null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
        ScanIndexForward: isASC,
        FilterExpression: 'orderId = :orderId',
        ExpressionAttributeValues: {
          ':orderId': filters.orderId
        },
      };
      console.log(params);
      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

      console.log(finalParams);

      const orderPage = await docClient.scan(finalParams).promise();
      console.log(orderPage);

      return res.status(200).json(
        {
          success: true,
          count: orderPage.Count,
          items: orderPage.Items,
          nextToken: orderPage.LastEvaluatedKey
        }
      );
    }

    if (filters.masterOrderId !== null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
        ScanIndexForward: isASC,
        FilterExpression: 'masterOrderId = :masterOrderId',
        ExpressionAttributeValues: {
          ':masterOrderId': filters.masterOrderId
        },
      };
      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

      console.log(finalParams);

      const orderPage = await docClient.scan(finalParams).promise();
      console.log(orderPage);

      return res.status(200).json(
        {
          success: true,
          count: orderPage.Count,
          items: orderPage.Items,
          nextToken: orderPage.LastEvaluatedKey
        }
      );
    }

    if (filters.sapOrderId !== null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
        ScanIndexForward: isASC,
        FilterExpression: 'sapOrderId = :sapOrderId',
        ExpressionAttributeValues: {
          ':sapOrderId': filters.sapOrderId
        },

      };

      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

      console.log(finalParams);

      const orderPage = await docClient.scan(finalParams).promise();
      console.log(orderPage);

      return res.status(200).json(
        {
          success: true,
          count: orderPage.Count,
          items: orderPage.Items,
          nextToken: orderPage.LastEvaluatedKey
        }
      );
    }

    const params = {
      TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
      ScanIndexForward: isASC,
      FilterExpression: '',
      ExpressionAttributeValues: {}
    };

    if (filters.orderStatus) {
      params.FilterExpression += 'orderStatus =:orderStatus';
      params.ExpressionAttributeValues[':orderStatus'] = filters.orderStatus;
    }

    if (filters.farmerMobile) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'farmerMobile =:farmerMobile';
      params.ExpressionAttributeValues[':farmerMobile'] = filters.farmerMobile;
    }

    if (filters.profitCenter) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'profitCenter = :profitCenter';
      params.ExpressionAttributeValues[':profitCenter'] = filters.profitCenter;
    }

    if (filters.isMobile != null) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'isMobile = :isMobile';
      params.ExpressionAttributeValues[':isMobile'] = filters.isMobile;
    }

    if (filters.startDate && filters.endDate) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'orderDate BETWEEN :startDate and :endDate';
      params.ExpressionAttributeValues[':startDate'] = filters.startDate;
      params.ExpressionAttributeValues[':endDate'] = filters.endDate;
    }

    const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

    console.log(finalParams);

    const orderPage = await docClient.scan(finalParams).promise();
    console.log(orderPage);

    return res.status(200).json(
      {
        success: true,
        count: orderPage.Count,
        items: orderPage.Items,
        nextToken: orderPage.LastEvaluatedKey
      }
    );

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, msg: 'Error Fetching Orders' });
  }
});

app.post('/getAllProducts', async (req, res) => {
  // Add your code here
  var params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
  };
  try {
    let scanResults = [];

    docClient.scan(params).eachPage((err, data, done) => {
      if (err) return res.status(200).json({ success: false, msg: 'Err in callback' });
      if (data != null) {
        data.Items.forEach((item) => scanResults.push(item));
      } else {
        console.log(scanResults.length);
        return res.status(200).json({ success: true, items: scanResults });
      }
      done();
    });

  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error fetching Products' });
  }
});

app.post('/getAllPayments', async (req, res) => {
  const { filters, nextToken, isASC } = req.body;
  console.log(filters);
  console.log(nextToken);
  console.log(isASC);
  // Add your code here
  try {
    if (filters === null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
        ScanIndexForward: isASC,
        // Limit: 100
      };
      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;
      console.log(finalParams);

      const paymentPage = await docClient.scan(finalParams).promise();
      console.log(paymentPage);

      return res.status(200).json(
        {
          success: true,
          count: paymentPage.Count,
          items: paymentPage.Items,
          nextToken: paymentPage.LastEvaluatedKey
        }
      );
    }

    if (filters.ecomOrderId !== null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
        ScanIndexForward: isASC,
        FilterExpression: 'ecomOrderId = :ecomOrderId',
        ExpressionAttributeValues: {
          ':ecomOrderId': filters.ecomOrderId
        },
        // Limit: 100

      };
      console.log(params);
      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

      console.log(finalParams);

      const paymentPage = await docClient.scan(finalParams).promise();
      console.log(paymentPage);

      return res.status(200).json(
        {
          success: true,
          count: paymentPage.Count,
          items: paymentPage.Items,
          nextToken: paymentPage.LastEvaluatedKey
        }
      );
    }

    if (filters.sapPaymentId !== null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
        ScanIndexForward: isASC,
        FilterExpression: 'sapPaymentId = :sapPaymentId',
        ExpressionAttributeValues: {
          ':sapPaymentId': filters.sapPaymentId
        },
        // Limit: 100

      };
      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

      console.log(finalParams);

      const paymentPage = await docClient.scan(finalParams).promise();
      console.log(paymentPage);

      return res.status(200).json(
        {
          success: true,
          count: paymentPage.Count,
          items: paymentPage.Items,
          nextToken: paymentPage.LastEvaluatedKey
        }
      );
    }

    if (filters.rzpPaymentId !== null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
        ScanIndexForward: isASC,
        FilterExpression: 'rzpPaymentId = :rzpPaymentId',
        ExpressionAttributeValues: {
          ':rzpPaymentId': filters.rzpPaymentId
        },
        // Limit: 100

      };

      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

      console.log(finalParams);

      const paymentPage = await docClient.scan(finalParams).promise();
      console.log(paymentPage);

      return res.status(200).json(
        {
          success: true,
          count: paymentPage.Count,
          items: paymentPage.Items,
          nextToken: paymentPage.LastEvaluatedKey
        }
      );
    }

    const params = {
      TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
      ScanIndexForward: isASC,
      FilterExpression: '',
      ExpressionAttributeValues: {},
      // Limit: 100

    };

    if (filters.farmerMobile) {
      params.FilterExpression += 'farmerMobile =:farmerMobile';
      params.ExpressionAttributeValues[':farmerMobile'] = filters.farmerMobile;
    }

    if (filters.isMobile != null) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'isMobile =:isMobile';
      params.ExpressionAttributeValues[':isMobile'] = filters.isMobile;
    }

    if (filters.startDate && filters.endDate) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'createdAt BETWEEN :startDate and :endDate';
      params.ExpressionAttributeValues[':startDate'] = filters.startDate;
      params.ExpressionAttributeValues[':endDate'] = filters.endDate;
    }

    const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

    console.log(finalParams);

    const paymentPage = await docClient.scan(finalParams).promise();
    console.log(paymentPage);

    return res.status(200).json(
      {
        success: true,
        count: paymentPage.Count,
        items: paymentPage.Items,
        nextToken: paymentPage.LastEvaluatedKey
      }
    );

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, msg: 'Error Fetching Orders' });
  }

});

app.post('/getAllComplaints', async (req, res) => {
  const { filters, nextToken, isASC } = req.body;
  console.log(filters);
  console.log(nextToken);
  console.log(isASC);
  // Add your code here
  try {

    const allParams = {
      TableName: process.env.API_EAMS5TAPI01_COMPLAINTTABLE_NAME,
    };

    let allResults = [];
    let todayCount = 0;
    let openCount = 0;
    let closedTodayCount = 0;
    let notClosed24 = 0;
    let notClosed48 = 0;

    await new Promise((resolve, reject) => {
      docClient.scan(allParams).eachPage((err, data, done) => {
        if (err) reject({ success: false, msg: 'Err in fetching KPIs' });
        if (data != null) {
          data.Items.forEach((item) => allResults.push(item));
        } else {
          allResults.forEach((item) => {
            var dateDiff = Math.abs(
              (new Date(item.complaintDate) - new Date()) / (1000 * 60 * 60 * 24)
            );
            if (dateDiff === 0) {
              todayCount += 1;
              if (item.status === 'Resolved with no claim'
                || item.status === 'Resolved with claim') {
                closedTodayCount += 1;
              }
            }
            if (item.status === ''
              || item.status === 'unresolved'
              || item.status === 'In process') {
              openCount += 1;
            }

            if (dateDiff > 1 && dateDiff < 2 && (item.status === ''
              || item.status === 'unresolved'
              || item.status === 'In process')) {
              notClosed24 += 1;
            }

            if (dateDiff > 2 && (item.status === ''
              || item.status === 'unresolved'
              || item.status === 'In process')) {
              notClosed48 += 1;
            }
          });
          resolve(true);
        }
        done();
      });
    });

    if (filters === null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_COMPLAINTTABLE_NAME,
        ScanIndexForward: isASC,
      };
      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;
      console.log(finalParams);

      const complaintPage = await docClient.scan(finalParams).promise();
      console.log(complaintPage);

      let rowsSelected = [];
      let rowsSelectedCount = 0;

      await new Promise((resolve, reject) => {
        docClient.scan(params).eachPage((err, data, done) => {
          if (err) return reject({ success: false, msg: 'Err in fetching Rows Selected' });
          if (data != null) {
            data.Items.forEach((item) => rowsSelected.push(item));
          } else {
            rowsSelectedCount = rowsSelected.length;
            resolve(true);
          }
          done();
        });
      });

      console.log('Today Count: ', todayCount);
      console.log('Open Count: ', openCount);
      console.log('Closed Today Count: ', closedTodayCount);
      console.log('Not Closed 24: ', notClosed24);
      console.log('Not Closed 48: ', notClosed48);
      console.log('Rows Selected: ', rowsSelectedCount);

      return res.status(200).json(
        {
          success: true,
          count: complaintPage.Count,
          items: complaintPage.Items,
          nextToken: complaintPage.LastEvaluatedKey,
          todayCount,
          openCount,
          closedTodayCount,
          notClosed24,
          notClosed48,
          rowsSelectedCount
        }
      );
    }

    const params = {
      TableName: process.env.API_EAMS5TAPI01_COMPLAINTTABLE_NAME,
      ScanIndexForward: isASC,
      FilterExpression: '',
      ExpressionAttributeValues: {},
    };
    console.log(params);

    if (filters.status) {
      params.FilterExpression += '#text =:status';
      params.ExpressionAttributeNames = {};
      params.ExpressionAttributeNames['#text'] = 'status';
      params.ExpressionAttributeValues[':status'] = filters.status;
    }

    if (filters.farmerMobile) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'farmerMobile =:farmerMobile';
      params.ExpressionAttributeValues[':farmerMobile'] = filters.farmerMobile;
    }

    if (filters.complaintType) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'complaintType = :complaintType';
      params.ExpressionAttributeValues[':complaintType'] = filters.complaintType;
    }

    if (filters.startDate && filters.endDate) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'complaintDate BETWEEN :startDate and :endDate';
      params.ExpressionAttributeValues[':startDate'] = filters.startDate;
      params.ExpressionAttributeValues[':endDate'] = filters.endDate;
    }

    const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

    console.log(finalParams);

    const complaintPage = await docClient.scan(finalParams).promise();
    console.log(complaintPage);

    // Code to get Complaints KPI

    let rowsSelected = [];
    let rowsSelectedCount = 0;

    await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) return reject({ success: false, msg: 'Err in fetching Rows Selected' });
        if (data != null) {
          data.Items.forEach((item) => rowsSelected.push(item));
        } else {
          rowsSelectedCount = rowsSelected.length;
          resolve(true);
        }
        done();
      });
    });

    console.log('Today Count: ', todayCount);
    console.log('Open Count: ', openCount);
    console.log('Closed Today Count: ', closedTodayCount);
    console.log('Not Closed 24: ', notClosed24);
    console.log('Not Closed 48: ', notClosed48);
    console.log('Rows Selected: ', rowsSelectedCount);

    return res.status(200).json(
      {
        success: true,
        count: complaintPage.Count,
        items: complaintPage.Items,
        nextToken: complaintPage.LastEvaluatedKey,
        todayCount,
        openCount,
        closedTodayCount,
        notClosed24,
        notClosed48,
        rowsSelectedCount
      }
    );

  } catch (e) {
    console.log(e);
    return res.status(200).json({ success: false, msg: 'Error Fetching Complaints' });
  }
});

app.post('/getAllReviews', async (req, res) => {
  const { filters, nextToken, isASC } = req.body;
  console.log(filters);
  console.log(nextToken);
  console.log(isASC);
  try {

    if (filters === null) {

      const params = {
        TableName: process.env.API_EAMS5TAPI01_PRODUCTREVIEWTABLE_NAME,
        ScanIndexForward: isASC,
        // Limit: 100
      };
      console.log(params);

      const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;
      console.log(finalParams);

      const reviewPage = await docClient.scan(finalParams).promise();
      console.log(reviewPage);

      return res.status(200).json(
        {
          success: true,
          count: reviewPage.Count,
          items: reviewPage.Items,
          nextToken: reviewPage.LastEvaluatedKey
        }
      );
    }

    const params = {
      TableName: process.env.API_EAMS5TAPI01_PRODUCTREVIEWTABLE_NAME,
      ScanIndexForward: isASC,
      FilterExpression: '',
      ExpressionAttributeValues: {},
      // Limit: 100
    };
    console.log(params);

    if (filters.category) {
      params.FilterExpression += 'category =:category';
      params.ExpressionAttributeValues[':category'] = filters.category;
    }

    if (filters.crop) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'crop =:crop';
      params.ExpressionAttributeValues[':crop'] = filters.crop;
    }

    if (filters.product) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'product =:product';
      params.ExpressionAttributeValues[':product'] = filters.product;
    }

    if (filters.farmerMobile) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'farmerMobile =:farmerMobile';
      params.ExpressionAttributeValues[':farmerMobile'] = filters.farmerMobile;
    }

    if (filters.farmerName) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'farmerName =:farmerName';
      params.ExpressionAttributeValues[':farmerName'] = filters.farmerName;
    }

    if (filters.advantaKisan !== null && filters.advantaKisan !== undefined) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'advantaKisan = :advantaKisan';
      params.ExpressionAttributeValues[':advantaKisan'] = filters.advantaKisan;
    }

    if (filters.isModerated !== null && filters.isModerated !== undefined) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'isModerated = :isModerated';
      params.ExpressionAttributeValues[':isModerated'] = filters.isModerated;
    }

    if (filters.isPublic !== null && filters.isPublic !== undefined) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'isPublic = :isPublic';
      params.ExpressionAttributeValues[':isPublic'] = filters.isPublic;
    }

    if (filters.rating) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'rating = :rating';
      params.ExpressionAttributeValues[':rating'] = filters.rating;
    }

    if (filters.startDate && filters.endDate) {
      params.FilterExpression = params.FilterExpression.length > 0 ? `${params.FilterExpression} and ` : params.FilterExpression;
      params.FilterExpression += 'reviewDate BETWEEN :startDate and :endDate';
      params.ExpressionAttributeValues[':startDate'] = filters.startDate;
      params.ExpressionAttributeValues[':endDate'] = filters.endDate;
    }

    const finalParams = nextToken ? { ...params, ExclusiveStartKey: nextToken } : params;

    console.log(finalParams);

    const reviewPage = await docClient.scan(finalParams).promise();
    console.log(reviewPage);

    return res.status(200).json(
      {
        success: true,
        count: reviewPage.Count,
        items: reviewPage.Items,
        nextToken: reviewPage.LastEvaluatedKey
      }
    );

  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error fetching Reviews' });
  }
});

app.post('/getAllReferrer', async (req, res) => {
  var params = {
    TableName: process.env.API_EAMS5TAPI01_REFERRERTABLE_NAME,
  };
  try {
    let scanResults = [];

    docClient.scan(params).eachPage((err, data, done) => {
      if (err) return res.status(200).json({ success: false, msg: 'Err in callback' });
      if (data != null) {
        data.Items.forEach((item) => scanResults.push(item));
      } else {
        return res.status(200).json({ success: true, items: scanResults });
      }
      done();
    });

  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error fetching Faqs' });
  }
});

app.post('/getAllReferee', async (req, res) => {
  var params = {
    TableName: process.env.API_EAMS5TAPI01_REFEREETABLE_NAME,
  };
  try {
    let scanResults = [];

    docClient.scan(params).eachPage((err, data, done) => {
      if (err) return res.status(200).json({ success: false, msg: 'Err in callback' });
      if (data != null) {
        data.Items.forEach((item) => scanResults.push(item));
      } else {
        return res.status(200).json({ success: true, items: scanResults });
      }
      done();
    });

  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error fetching Faqs' });
  }
});

app.post('/checkStockAvail', async (req, res) => {
  const { cartItems } = req.body;
  const promiseres = await Promise.all(
    cartItems.map(async (cartItem) => {
      const product = await getProduct(cartItem.materialCode);

      let existFlag = true;
      product.item.sku.map((skuItem) => {
        if (skuItem.sapSkuCode === cartItem.sku.sapSkuCode && skuItem.qtyAvailable <= 0) {
          existFlag = false;
        }
      });
      return existFlag;

    })
  );

  const stockAvail = promiseres.every((val) => val === true);
  return res.status(200).json({ success: stockAvail });
});

// Export the app object. When executing the application local this does nothing. However,
module.exports = app;
