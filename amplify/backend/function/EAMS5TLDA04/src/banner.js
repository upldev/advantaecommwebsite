const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();

exports.getBanner = async (bannerId) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_BANNERTABLE_NAME,
    Key: { bannerId },
  };
  try {
    const data = await docClient.get(params).promise();
    return data.Item;
  } catch (err) {
    console.log(err);
    return err;
  }
};

exports.addBanner = async (
  bannerId,
  bannerImg,
  bannerMobileImg,
  bannerType,
  clicks,
  redirectUrl,
  isActive
) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_BANNERTABLE_NAME,
    Item: {
      bannerId,
      bannerImg,
      bannerMobileImg,
      bannerType,
      clicks,
      redirectUrl,
      isActive,
      __typename: 'Banner',
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
    },
  };
  try {
    const data = await docClient.put(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'error in adding banner' };
  }
};

exports.updateBanner = async (bannerId,
  bannerImg,
  bannerMobileImg,
  bannerType,
  clicks,
  redirectUrl,
  isActive) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_BANNERTABLE_NAME,
    Key: { bannerId },
    UpdateExpression: 'set bannerImg=:bannerImg, bannerMobileImg =:bannerMobileImg, bannerType=:bannerType, redirectUrl=:redirectUrl,isActive=:isActive,#var =:clicks',
    ExpressionAttributeNames: {
      '#var': 'clicks',
    },
    ExpressionAttributeValues: {
      ':clicks': clicks,
      ':bannerImg': bannerImg,
      ':bannerMobileImg': bannerMobileImg,
      ':bannerType': bannerType,
      ':redirectUrl': redirectUrl,
      ':isActive': isActive
    },
  };
  console.log(params);
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    console.log(err);
    return err;
  }
};

exports.updateClicks = async (bannerId, clicks) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_BANNERTABLE_NAME,
    Key: { bannerId },
    UpdateExpression: 'set #var =:clicks',
    ExpressionAttributeNames: {
      '#var': 'clicks',
    },
    ExpressionAttributeValues: {
      ':clicks': clicks,
    },
  };
  console.log(params);
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    console.log(err);
    return err;
  }
};
