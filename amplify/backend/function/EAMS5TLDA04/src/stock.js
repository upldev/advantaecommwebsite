const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();

exports.getProduct = async (materialCode) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
    Key: { materialCode }
  };
  try {
    const product = await docClient.get(params).promise();
    return { success: true, item: product.Item };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error in getProduct' };
  }
};
