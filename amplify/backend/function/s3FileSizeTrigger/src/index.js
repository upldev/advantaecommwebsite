/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */
console.log('Loading function');

const AWS = require('aws-sdk');

const S3 = new AWS.S3({ apiVersion: '2006-03-01' });

exports.handler = async (event, context) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  console.log(event.Records[0].s3.object);
  // Get the object from the event and show its content type
  const bucket = event.Records[0].s3.bucket.name;
  const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
  const params = {
    Bucket: bucket,
    Key: key,
  };
  try {
    if (event.Records[0].s3.object.size > 10485760) {
      const res = await S3.deleteObject(params).promise();
      console.log(res);
    } else {
      const { ContentType } = await S3.getObject(params).promise();

      if (!(ContentType.toString().includes('image') || ContentType.toString().includes('pdf'))) {
        const res = await S3.deleteObject(params).promise();
        console.log(res);
      }
    }

  } catch (err) {
    console.log(err);
    const message = `Error getting object ${key} from bucket ${bucket}. Make sure they exist and your bucket is in the same region as this function.`;
    console.log(message);
    throw new Error(message);
  }
};
