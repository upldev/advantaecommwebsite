/* eslint-disable no-param-reassign */

exports.handler = async (event, context) => {

  console.log(JSON.stringify(event));

  if (event.request.session.length === 0) { // first time
    event.response.issueTokens = false;
    event.response.failAuthentication = false;
    event.response.challengeName = 'CUSTOM_CHALLENGE';
  } else if (event.request.session
    && event.request.session.length >= 5 // fail on 5 wrong answers
    && event.request.session.slice(-1)[0].challengeResult === false) {
    event.response.issueTokens = false;
    event.response.failAuthentication = true;
  } else if (event.request.session
    && event.request.session.length
    && event.request.session.slice(-1)[0].challengeName === 'CUSTOM_CHALLENGE' // Doubly stitched, holds better
    && event.request.session.slice(-1)[0].challengeResult === true) {
    // The user provided the right answer; succeed auth
    event.response.issueTokens = true;
    event.response.failAuthentication = false;
  } else {
    // The user did not provide a correct answer yet
    event.response.issueTokens = false;
    event.response.failAuthentication = false;
    event.response.challengeName = 'CUSTOM_CHALLENGE'; // present new challenge for next session
  }

  console.log(JSON.stringify(event));

  context.done(null, event);
};
