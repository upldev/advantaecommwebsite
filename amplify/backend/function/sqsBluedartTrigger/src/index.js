/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */const AWS = require('aws-sdk');

const secretsManager = new AWS.SSM();

const docClient = new AWS.DynamoDB.DocumentClient();

const soapRequest = require('easy-soap-request');
const xmljs = require('xml-js');
const axios = require('axios');

async function bluedartWaybillPost(xml) {
  const url = 'https://netconnect.bluedart.com/Ver1.10/Demo/ShippingAPI/WayBill/WayBillGeneration.svc';
  const requestHeaders = {
    'Content-Type': 'application/soap+xml',
    soapAction: url
  };

  try {
    const { response } = await soapRequest({ url, headers: requestHeaders, xml });
    // console.log(response);
    const { headers, body, statusCode } = response;
    console.log('Waybill Headers: ', headers);
    console.log('Waybill Body: ', body);
    console.log('Waybill Status', statusCode);

    const jsonData = xmljs.xml2json(body, { compact: true, spaces: 4 });
    const parsedData = JSON.parse(jsonData);
    console.log(parsedData);

    return { success: true, awb: parsedData['s:Envelope']['s:Body'].GenerateWayBillResponse.GenerateWayBillResult['b:AWBNo']._text };
  } catch (e) {
    console.error('Error: ', e);
    return { success: false, msg: 'Error Posting Bluedart Waybill' };
  }
}

async function bluedartTrackingPost(awb) {
  try {
    const url = `https://api.bluedart.com/servlet/RoutingServlet?handler=tnt&action=custawbquery&loginid=MAA00001&format=xml&lickey=a28f0bb8690c75ce3368bb1c76ea98bc&verno=1.3&scan=1&awb=awb&numbers=${awb}`;
    const response = await axios.get(url);
    // console.log(response);
    const { headers, data, status } = response;
    console.log('Tracking Headers: ', headers);
    console.log('Tracking Body: ', data);
    console.log('Tracking Status: ', status);

    const jsonData = xmljs.xml2json(data, { compact: true, spaces: 4 });
    const parsedData = JSON.parse(jsonData);
    console.log(JSON.stringify(parsedData));

    const shipment = parsedData.ShipmentData.Shipment;
    const scans = shipment.Scans.ScanDetail;
    if (scans !== null && scans !== undefined) {
      const pickupDate = shipment.PickUpDate._text;
      const pickupTime = shipment.PickUpTime._text;
      const estimatedDeliveryDate = shipment.ExpectedDeliveryDate._text;
      const trackingStatus = shipment.Status._text;
      const statusDate = shipment.StatusDate._text;
      const statusTime = shipment.StatusTime._text;
      const scanDetails = scans.map((item) => {
        return {
          status: item.Scan._text,
          date: item.ScanDate._text,
          time: item.ScanTime._text,
          location: item.ScannedLocation._text
        };
      });

      console.log('Pickup Date: ', pickupDate);
      console.log('Pickup Time: ', pickupTime);
      console.log('Estimated Delivery Date: ', estimatedDeliveryDate);
      console.log('Tracking Status: ', trackingStatus);
      console.log('Status Date: ', statusDate);
      console.log('Status Time: ', statusTime);
      console.log('Scan Details: ', scanDetails);

      return {
        success: true,
        status: trackingStatus,
        bluedartTrack: {
          pickupDate,
          pickupTime,
          estimatedDeliveryDate,
          statusDate,
          statusTime,
          scanResults: scanDetails
        }
      };
    }

    return { success: true, status: 'NEW', bluedartTrack: null };
  } catch (e) {
    console.error('Error: ', e);
    return { success: false, msg: 'Error Posting Bluedart Tracking' };
  }
}

async function updateBluedartAwb(orderId, bluedartAwbNo) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    Key: { orderId },
    UpdateExpression: 'set bluedartAwbNo = :bluedartAwbNo',
    ExpressionAttributeValues: {
      ':bluedartAwbNo': bluedartAwbNo
    },
  };
  try {
    const data = await docClient.update(params).promise();
    console.log(data);
    return { success: true, data };
  } catch (err) {
    console.log(err);
    return { success: false, err, msg: 'Error Updating Bluedart AWB' };
  }
}

async function updateTracking(orderId, orderStatus, bluedartTrack) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    Key: { orderId },
    UpdateExpression: 'set orderStatus = :orderStatus,bluedartTrack =:bluedartTrack',
    ExpressionAttributeValues: {
      ':orderStatus': orderStatus,
      ':bluedartTrack': bluedartTrack
    },
  };
  try {
    const data = await docClient.update(params).promise();
    console.log(data);
    return { success: true, data };
  } catch (err) {
    console.log(err);
    return { success: false, err, msg: 'Error Updating Bluedart Tracking' };
  }
}
exports.handler = async (event) => {

  const res = await Promise.all(
    event.Records.map(async (item) => {
      const data = JSON.parse(item.body);
      console.log(data);
      if (data.type === 'ORDER') {
        const response = await bluedartWaybillPost(data.bluedartWaybill);
        console.log(response);
        if (response.success) {
          const updOrder = await updateBluedartAwb(data.orderId, response.awb);
          console.log(updOrder);
        }
      } else {
        const response = await bluedartTrackingPost(data.bluedartAwbNo);
        console.log(response);
        if (response.success) {
          const updOrder = await updateTracking(
            data.orderId, response.status, response.bluedartTrack
          );
          console.log(updOrder);
        }
      }
    })
  );
};
