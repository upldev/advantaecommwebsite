/* eslint-disable consistent-return */
/* eslint-disable no-await-in-loop */
/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["CPI_USERNAME","CPI_PASSWORD"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_FARMERTABLE_ARN
	API_EAMS5TAPI01_FARMERTABLE_NAME
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_ARN
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	API_EAMS5TAPI01_PAYMENTTABLE_ARN
	API_EAMS5TAPI01_PAYMENTTABLE_NAME
	API_EAMS5TAPI01_PRODUCTTABLE_ARN
	API_EAMS5TAPI01_PRODUCTTABLE_NAME
	API_EAMS5TAPI01_SAPSTOCKTABLE_ARN
	API_EAMS5TAPI01_SAPSTOCKTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const axios = require('axios');
const AWS = require('aws-sdk');

const secretsManager = new AWS.SSM();
const docClient = new AWS.DynamoDB.DocumentClient();

async function getSecrets() {
  const { Parameters } = await secretsManager
    .getParameters({
      Names: ['CPI_USERNAME', 'CPI_PASSWORD'].map((secretName) => process.env[secretName]),
      WithDecryption: true,
    })
    .promise();

  const CPI_USERNAME = Parameters[1].Value;
  const CPI_PASSWORD = Parameters[0].Value;

  return {
    CPI_USERNAME,
    CPI_PASSWORD,
  };
}

async function cpiOauth() {
  const { CPI_USERNAME, CPI_PASSWORD } = await getSecrets();
  const auth = await axios.post('https://oauthasservices-ae7888026.hana.ondemand.com/oauth2/api/v1/token?grant_type=client_credentials', {}, {
    auth: {
      username: CPI_USERNAME,
      password: CPI_PASSWORD
    }
  });
  return auth;
}

async function getOrders() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    FilterExpression: 'sapOrderId = :sapOrderId',
    ExpressionAttributeValues: { ':sapOrderId': null }
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) reject();
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    console.log(afterPromiseRes);

    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

async function getSAPStockBySku(sapSkuCode) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_SAPSTOCKTABLE_NAME,
    FilterExpression: 'sapSkuCode = :sapSkuCode',
    ExpressionAttributeValues: { ':sapSkuCode': sapSkuCode }
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) reject();
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    console.log(afterPromiseRes);

    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

async function getCustomer(mobile) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile }
  };
  try {
    const data = await docClient.get(params).promise();
    return data.Item;
  } catch (err) {
    return err;
  }
}

async function updateOrder(orderId, sapOrderId) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    Key: { orderId },
    UpdateExpression: 'set sapOrderId = :x',
    ExpressionAttributeValues: {
      ':x': sapOrderId,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
}

async function updateTriggerLock(orderId, sapTriggerLock) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    Key: { orderId },
    UpdateExpression: 'set sapTriggerLock =:sapTriggerLock',
    ExpressionAttributeValues: {
      ':sapTriggerLock': sapTriggerLock,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
}

async function createSAPOrder(order, userInfo) {

  const cartItems = await Promise.all(order.orderCart.map(async (item, key) => {
    const sapStockVal = await getSAPStockBySku(item.sku.sapSkuCode);

    return {
      itemNumber: (key + 1) * 10,
      sapSkuCode: item.sku.sapSkuCode,
      plantCode: sapStockVal.filter(
        (item) => item.qtyAvailable > 0
      )[0].plantCode,
      salesUOM: sapStockVal[0].salesUOM,
      qty: item.qty,
      itemDiscount: Math.round(item.itemDiscount * 100) / 100,
      ecomPrice: item.sku.ecomPrice,
      currency: 'INR',
      packSize: item.sku.packSize,
      packUnit: item.sku.packUnit
    };
  }));

  return {
    orderId: order.masterOrderId,
    subOrderId: order.orderId,
    sapCustCode: userInfo.customerSAPCode,
    division: order.division,
    cartItems,
    shipping: {
      name: order.shipping.name,
      mobile: userInfo.mobile,
      address: order.shipping.address.replace(/[^a-zA-Z0-9, ]/g, ' '),
      city: order.shipping.city,
      state: order.shipping.state,
      country: 'IN',
      pincode: order.shipping.pincode
    },
    totalHeaderDiscount: Math.round(order.totalHeaderDiscount * 100) / 100,
    shippingCharges: order.shippingCharges
  };
}

async function getGlobalLock() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME,
  };
  try {
    const lock = await docClient.scan(params).promise();
    return lock.Items[0];
  } catch (e) {
    console.error(e);
    return null;
  }
}

exports.handler = async (event) => {
  const lock = await getGlobalLock();
  console.log(lock);
  if (!lock.sapOrderLock) {

    const auth = await cpiOauth();
    const orders = await getOrders();

    const afterPromiseRes = await Promise.all(orders.map(async (order) => {
      const farmer = await getCustomer(order.farmerMobile);

      if (order.sapTriggerLock !== null && order.sapTriggerLock === true) {
        console.log(`${order.orderId} is already processed by other thread`);
        return `${order.orderId} is already processed by other thread`;
      }
      if (farmer) {
        await updateTriggerLock(order.orderId, true);
        const sapOrder = await createSAPOrder(
          order, farmer
        );
        const response = await axios.post('https://l4097-iflmap.hcisbp.eu1.hana.ondemand.com/http/Order_Creation', sapOrder, {
          headers: {
            Authorization: `Bearer ${auth.data.access_token}`
          }
        });
        if (response.data.Status === '1') {
          const updOrder = await updateOrder(order.orderId, response.data.salesDocumentNO);
        }
        await updateTriggerLock(order.orderId, false);
        return true;
      }
    }));
  } else {
    console.log('SAP Order Creation Integration is currently stopped , Please check GlobalLock Table in DynamoDB');
  }
};
