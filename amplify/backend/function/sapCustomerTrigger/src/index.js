/* eslint-disable prefer-const */
/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["CPI_USERNAME","CPI_PASSWORD"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* eslint-disable no-await-in-loop */
/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_FARMERTABLE_ARN
	API_EAMS5TAPI01_FARMERTABLE_NAME
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_ARN
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const axios = require('axios');
const AWS = require('aws-sdk');

const secretsManager = new AWS.SSM();
const docClient = new AWS.DynamoDB.DocumentClient();

async function getSecrets() {
  const { Parameters } = await secretsManager
    .getParameters({
      Names: ['CPI_USERNAME', 'CPI_PASSWORD'].map((secretName) => process.env[secretName]),
      WithDecryption: true,
    })
    .promise();

  const CPI_USERNAME = Parameters[1].Value;
  const CPI_PASSWORD = Parameters[0].Value;

  return {
    CPI_USERNAME,
    CPI_PASSWORD,
  };
}

async function cpiOauth() {
  const { CPI_USERNAME, CPI_PASSWORD } = await getSecrets();
  const auth = await axios.post('https://oauthasservices-ae7888026.hana.ondemand.com/oauth2/api/v1/token?grant_type=client_credentials', {}, {
    auth: {
      username: CPI_USERNAME,
      password: CPI_PASSWORD
    }
  });
  return auth;
}

async function getFarmers() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    FilterExpression: 'customerSAPCode = :customerSAPCode',
    ExpressionAttributeValues: { ':customerSAPCode': null }
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) reject();
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    console.log(afterPromiseRes);

    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

async function updateFarmer(mobile, customerSAPCode) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set customerSAPCode = :x',
    ExpressionAttributeValues: {
      ':x': customerSAPCode,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
}

async function updateTriggerLock(mobile, sapCreateLock) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set sapCreateLock =:sapCreateLock',
    ExpressionAttributeValues: {
      ':sapCreateLock': sapCreateLock,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
}

async function getGlobalLock() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME,
  };
  try {
    const lock = await docClient.scan(params).promise();
    return lock.Items[0];
  } catch (e) {
    console.error(e);
    return null;
  }
}

exports.handler = async (event) => {
  const lock = await getGlobalLock();
  console.log(lock);
  if (!lock.sapCustCreateLock) {
    const auth = await cpiOauth();
    const farmers = await getFarmers();
    if (farmers && farmers.length > 0) {
      const afterPromiseRes = await Promise.all(farmers.map(async (item) => {

        if (item.sapCreateLock !== null && item.sapCreateLock === true) {
          console.log(`${item.mobile} is already processed by other thread`);
          return `${item.mobile} is already processed by other thread`;
        }
        await updateTriggerLock(item.mobile, true);
        const {
          google, fb, cart, wishList, createdAt, updatedAt, mobile, customerSAPCode, billing,
          shipping, ...farmer
        } = item;
        const response = await axios.post('https://l4097-iflmap.hcisbp.eu1.hana.ondemand.com/http/Customer_creation', {
          ...farmer, mobile: mobile.substring(3), customerSAPCode: '', billing: { ...billing, address: billing.address.replace(/[^a-zA-Z0-9, ]/g, ' ') }
        }, {
          headers: {
            Authorization: `Bearer ${auth.data.access_token}`
          }
        });
        console.log(response);
        if (response.data.Status === '1') {
          const updatedFarmer = await updateFarmer(item.mobile, response.data.CustomerSAPCode);
          console.log(updatedFarmer);
        }
        await updateTriggerLock(item.mobile, false);
      }));
    }
  } else {
    console.log('SAP Customer Create Integration is currently stopped, Please check GlobalLock Table in DynamoDB');
  }
};
