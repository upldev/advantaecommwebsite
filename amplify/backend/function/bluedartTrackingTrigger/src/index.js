/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const AWS = require('aws-sdk');

const SQS = new AWS.SQS({ region: 'eu-west-2' });

const docClient = new AWS.DynamoDB.DocumentClient();

async function getOrders() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    FilterExpression: 'orderStatus <>:orderStatus and bluedartAwbNo <>:bluedartAwbNo and sapOrderId <>:sapOrderId and invoiceUrl <>:invoiceUrl',
    ExpressionAttributeValues: {
      ':orderStatus': 'SHIPMENT DELIVERED', ':bluedartAwbNo': null, ':sapOrderId': null, ':invoiceUrl': null
    }
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) {
          console.log(err);
          reject();
        }
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });

    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

exports.handler = async (event) => {

  const orders = await getOrders();
  console.log(orders);

  if (orders && orders.length > 0) {
    const afterPromiseRes = await Promise.all(orders.map(async (order) => {
      const response = await SQS.sendMessage({
        MessageBody: JSON.stringify({ bluedartAwbNo: order.bluedartAwbNo, orderId: order.orderId, type: 'TRACKING' }),
        QueueUrl: 'https://sqs.eu-west-2.amazonaws.com/364494007171/advantaBluedartSQS',
      }).promise();
      console.log(response);
    }));
  }

};
