/*
Copyright 2017 - 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at
    http://aws.amazon.com/apache2.0/
or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
*/

const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

const passport = require('passport');
const { Strategy } = require('passport-facebook');
const session = require('express-session');
const cors = require('cors');
const path = require('path');
const axios = require('axios');

// declare a new express app
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

// Enable CORS for all methods
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate, public');
  res.header('Pragma', 'no-cache');
  res.header('X-Frame-Options', 'SAMEORIGIN');
  res.header('Strict-Transport-Security', 'max-age=16070400; include Subdomains');
  res.header('X-Content-Type-Option', 'nosniff');
  res.header('X-XSS-Protection', '1; mode=block');
  next();
});

const CALLBACK_URL = 'https://shopuat.advantaseeds.com/connected-to-fb';

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((obj, done) => {
  done(null, obj);
});

passport.use(new Strategy({
  clientID: process.env.FACEBOOK_APP_ID,
  clientSecret: process.env.FACEBOOK_APP_SECRET,
  callbackURL: CALLBACK_URL,
  profileFields: ['id', 'email', 'gender', 'name', 'birthday', 'location'],
},
((accessToken, refreshToken, profile, done) => {
  console.log('Access Token', accessToken);
  console.log('Refresh Token', refreshToken);
  console.log('profile', profile);
  return done(null, profile);
})));

app.get('/facebookAuth', passport.authenticate('facebook', (data) => {
  console.log('authenticate', data);
}));

app.post('/facebookDetails', async (req, res) => {
  const { code } = req.body;
  console.log(code);
  try {
    const { data } = await axios({
      url: 'https://graph.facebook.com/v11.0/oauth/access_token',
      method: 'get',
      params: {
        client_id: process.env.FACEBOOK_APP_ID,
        redirect_uri: CALLBACK_URL,
        client_secret: process.env.FACEBOOK_APP_SECRET,
        code,
      },
    });
    console.log(data);
    const userData = await axios({
      url: 'https://graph.facebook.com/me',
      method: 'GET',
      params: {
        fields: ['id', 'email', 'gender', 'name', 'birthday', 'location'].join(','),
        access_token: data.access_token,
      },
    });
    console.log(userData);
    return res.status(200).json({ success: true, data: userData.data });
  } catch (error) {
    console.log(error);
    return res.status(200).json({ success: false, error });
  }
});

app.listen(3000, () => {
  console.log('App started');
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
