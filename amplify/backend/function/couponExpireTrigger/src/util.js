/* eslint-disable no-await-in-loop */
/** @format */

const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();

exports.getProductDiscount = async () => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_NAME,
  };
  try {
    const scanResults = [];
    let items;
    do {
      items = await docClient.scan(params).promise();
      items.Items.forEach((item) => scanResults.push(item));
      params.ExclusiveStartKey = items.LastEvaluatedKey;
    } while (items.LastEvaluatedKey);
    return { success: true, items: scanResults };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error in getProductDiscount' };
  }
};

exports.getCartDiscount = async () => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_CARTDISCOUNTTABLE_NAME,
  };
  try {
    const scanResults = [];
    let items;
    do {
      items = await docClient.scan(params).promise();
      items.Items.forEach((item) => scanResults.push(item));
      params.ExclusiveStartKey = items.LastEvaluatedKey;
    } while (items.LastEvaluatedKey);
    return { success: true, items: scanResults };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error in getCartDiscount' };
  }
};

exports.updateProductDiscount = async (discountId) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_NAME,
    Key: { discountId },
    UpdateExpression: 'set isActive =:isActive',
    ExpressionAttributeValues: {
      ':isActive': false
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
};

exports.updateCartDiscount = async (discountId) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_CARTDISCOUNTTABLE_NAME,
    Key: { discountId },
    UpdateExpression: 'set isActive =:isActive',
    ExpressionAttributeValues: {
      ':isActive': false
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
};
