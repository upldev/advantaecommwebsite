/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_CARTDISCOUNTTABLE_ARN
	API_EAMS5TAPI01_CARTDISCOUNTTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_ARN
	API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const {
  getProductDiscount, getCartDiscount, updateProductDiscount, updateCartDiscount
} = require('./util');

exports.handler = async (event) => {
  // TODO implement
  const productDiscounts = await getProductDiscount();
  const cartDiscounts = await getCartDiscount();

  const filteredProductDiscounts = await productDiscounts.items.filter(
    (item) => Date.now() > Date.parse(item.endDate)
  );

  const filteredCartDiscounts = await cartDiscounts.items.filter(
    (item) => Date.now() > Date.parse(item.endDate)
  );

  const product_res = await Promise.all(filteredProductDiscounts.map(async (item) => {
    const upd_res = await updateProductDiscount(item.discountId);
    return upd_res;
  }));

  const cart_res = await Promise.all(filteredCartDiscounts.map(async (item) => {
    const upd_res = await updateCartDiscount(item.discountId);
    return upd_res;
  }));

};
