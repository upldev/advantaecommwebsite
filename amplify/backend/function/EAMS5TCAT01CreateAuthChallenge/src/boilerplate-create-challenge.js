/* tslint:disable */
/* eslint-disable */

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();

const getIntruder = async (mobile) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_INTRUDERTABLE_NAME,
    Key: { mobile },
  };
  try {
    const data = await docClient.get(params).promise();
    return data.Item;
  } catch (err) {
    console.log(err);
    return err;
  }
};

const updateIntruder = async (mobile, lastLogin, numberOfHits) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_INTRUDERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set lastLogin =:lastLogin, numberOfHits=:numberOfHits',
    ExpressionAttributeValues: {
      ':lastLogin': lastLogin,
      ':numberOfHits': numberOfHits
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    console.log(err);
    return err;
  }
};

const putIntruder = async (mobile, lastLogin, numberOfHits) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_INTRUDERTABLE_NAME,
    Item: {
      mobile,
      lastLogin,
      numberOfHits,
      __typename: 'Intruder',
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
    },
  };
  try {
    const data = await docClient.put(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'error in putIntruder' };
  }
};

function delay(delayInms) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(2);
    }, delayInms);
  });
}

exports.handler = async (event, context, callback) => {

  await delay(1000);
  
  console.log(event);
  if (event.request.session.length === 0) { // for first session send sms
    
  const { phone_number } = event.request.userAttributes;
  console.log(phone_number);
  const intruder = await getIntruder(phone_number);
  console.log(intruder);

  const currentDateTime = new Date();
  console.log(currentDateTime);

  if (intruder !== null && intruder !== undefined) {
    const lastLoginDateTime = new Date(intruder.lastLogin);
    console.log(lastLoginDateTime);
    const diffInMin = Math.abs(currentDateTime - lastLoginDateTime) / 60000;
    console.log(diffInMin);
    if (diffInMin < 1 && intruder.numberOfHits > 2) {
      const err = new Error('Please try again after 1 minute');
      await updateIntruder(phone_number, currentDateTime.toISOString(), intruder.numberOfHits + 1);
      console.log('Declined');
      callback(err, event);
      context.done(err, event);
      return false;
    } else if (diffInMin >= 1) {
      await updateIntruder(phone_number, currentDateTime.toISOString(), 1);
      console.log('Reset Hit Count');
    } else if (intruder.numberOfHits <= 2) {
      await updateIntruder(phone_number, currentDateTime.toISOString(), intruder.numberOfHits + 1);
      console.log('Increment Hit Count');
    }
  } else {
    await putIntruder(phone_number, currentDateTime.toISOString(), 1);
  }
    
    //generate OTP
    const challengeAnswer = Math.random().toString(10).substr(2, 6);
    console.log(challengeAnswer);
    
    //sns sms
    const sns = new AWS.SNS({ region: 'eu-west-2' });
    sns.publish(
      {
        Message: 'Welcome to Shop Advanta, Your OTP is ' + challengeAnswer,
        PhoneNumber: phone_number,
        MessageStructure: 'string',
        MessageAttributes: {
          'AWS.SNS.SMS.SenderID': {
            DataType: 'String',
            StringValue: 'ADVANTA',
          },
          'AWS.SNS.SMS.SMSType': {
            DataType: 'String',
            StringValue: 'Transactional',
          },
        },
      },
      function (err, data) {
        console.log('Data: ',data);
        console.log('Error: ',err);
        return;
      }
    );

    //set return params
    event.response.privateChallengeParameters = {};
    event.response.privateChallengeParameters.answer = challengeAnswer;
    event.response.challengeMetadata = JSON.stringify({
      'answer': challengeAnswer,
      'challengeType': 'SMS_CODE'
    });
  } else { // wrong OTP entered, provide same challenge answer in new session
    const currentSession = event.request.session.length - 1;
    const metadata = JSON.parse(event.request.session[currentSession].challengeMetadata);
    event.response.publicChallengeParameters = { challengeType: 'SMS_CODE' };
    event.response.privateChallengeParameters = { answer: metadata.answer };
    event.response.challengeMetadata = JSON.stringify({
      'answer': metadata.answer,
      'challengeType': 'SMS_CODE'
    });
  }
  callback(null, event);
};