/* eslint-disable consistent-return */
/* eslint-disable no-await-in-loop */
/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["CPI_USERNAME","CPI_PASSWORD"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_FARMERTABLE_ARN
	API_EAMS5TAPI01_FARMERTABLE_NAME
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_ARN
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	API_EAMS5TAPI01_PAYMENTTABLE_ARN
	API_EAMS5TAPI01_PAYMENTTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const axios = require('axios');
const AWS = require('aws-sdk');

const secretsManager = new AWS.SSM();
const docClient = new AWS.DynamoDB.DocumentClient();

async function getSecrets() {
  const { Parameters } = await secretsManager
    .getParameters({
      Names: ['CPI_USERNAME', 'CPI_PASSWORD'].map((secretName) => process.env[secretName]),
      WithDecryption: true,
    })
    .promise();

  const CPI_USERNAME = Parameters[1].Value;
  const CPI_PASSWORD = Parameters[0].Value;

  return {
    CPI_USERNAME,
    CPI_PASSWORD,
  };
}

async function cpiOauth() {
  const { CPI_USERNAME, CPI_PASSWORD } = await getSecrets();
  const auth = await axios.post('https://oauthasservices-ae7888026.hana.ondemand.com/oauth2/api/v1/token?grant_type=client_credentials', {}, {
    auth: {
      username: CPI_USERNAME,
      password: CPI_PASSWORD
    }
  });
  return auth;
}

async function getPayments() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
    FilterExpression: 'sapPaymentId = :sapPaymentId',
    ExpressionAttributeValues: { ':sapPaymentId': null }
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) reject();
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    console.log(afterPromiseRes);

    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

async function getOrder(masterOrderId) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    FilterExpression: 'masterOrderId = :masterOrderId',
    ExpressionAttributeValues: { ':masterOrderId': masterOrderId }
  };
  try {
    const data = await docClient.scan(params).promise();
    return data.Items;
  } catch (err) {
    return err;
  }
}

async function getCustomer(mobile) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile }
  };
  try {
    const data = await docClient.get(params).promise();
    return data.Item;
  } catch (err) {
    return err;
  }
}

async function updatePayment(ecomPaymentId, sapPaymentId) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
    Key: { ecomPaymentId },
    UpdateExpression: 'set sapPaymentId = :x',
    ExpressionAttributeValues: {
      ':x': sapPaymentId,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
}

async function toggleSAPLock(ecomPaymentId, sapLock) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
    Key: { ecomPaymentId },
    UpdateExpression: 'set sapLock = :sapLock',
    ExpressionAttributeValues: {
      ':sapLock': sapLock,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
}

async function getGlobalLock() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME,
  };
  try {
    const lock = await docClient.scan(params).promise();
    return lock.Items[0];
  } catch (e) {
    console.error(e);
    return null;
  }
}

exports.handler = async (event) => {
  const lock = await getGlobalLock();
  console.log(lock);
  if (!lock.sapPaymentLock) {

    const auth = await cpiOauth();
    const payments = await getPayments();
    const afterPromiseRes = await Promise.all(payments.map(async (payment) => {
      if (!payment.sapLock) {
        await toggleSAPLock(payment.ecomPaymentId, true);
        const orders = await getOrder(payment.ecomOrderId);
        if (orders) {
          const farmer = await getCustomer(payment.farmerMobile);
          if (farmer) {
            const sapPaymentDetails = {
              paymentDate: orders[0].orderDate,
              ecomOrderId: payment.ecomOrderId,
              state: farmer.billing.state,
              paymentInfo: orders.map((order) => {
                return {
                  customerSAPCode: farmer.customerSAPCode,
                  profitCenter: order.profitCenter,
                  rzpPaymentId: payment.rzpPaymentId,
                  sapItemText: `${payment.ecomOrderId}_${payment.rzpPaymentId}`,
                  currency: 'INR',
                  paymentAmt: order.totalPayable
                };
              })
            };
            const response = await axios.post('https://l4097-iflmap.hcisbp.eu1.hana.ondemand.com/http/Advance_Payment', sapPaymentDetails, {
              headers: {
                Authorization: `Bearer ${auth.data.access_token}`
              }
            });

            if (response.data.Status === '1') {
              const updPayment = await updatePayment(payment.ecomPaymentId, response.data.DocumentNO);
            }
            await toggleSAPLock(payment.ecomPaymentId, false);
          }
        }
      }
    }));
  } else {
    console.log('SAP Payment Integration is currently stopped , Please check GlobalLock Table in DynamoDB');
  }
};
