const aws = require('aws-sdk');

exports.handler = (event, context, callback) => {
  const cognitoISP = new aws.CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
  });

  const params = {
    GroupName: process.env.GROUP,
    UserPoolId: event.userPoolId,
    Username: event.userName,
  };

  cognitoISP
    .adminAddUserToGroup(params)
    .promise()
    .then(() => callback(null, event))
    .catch((err) => callback(err, event));
};
