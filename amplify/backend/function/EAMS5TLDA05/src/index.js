/*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["CPI_USERNAME","CPI_PASSWORD"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_FARMERTABLE_ARN
	API_EAMS5TAPI01_FARMERTABLE_NAME
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_ARN
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	ENV
	REGION
Amplify Params - DO NOT EDIT */
const axios = require('axios');
const AWS = require('aws-sdk');

const secretsManager = new AWS.SSM();
const docClient = new AWS.DynamoDB.DocumentClient();

const getSecrets = async () => {
  const { Parameters } = await secretsManager
    .getParameters({
      Names: ['CPI_USERNAME', 'CPI_PASSWORD'].map((secretName) => process.env[secretName]),
      WithDecryption: true,
    })
    .promise();

  const CPI_USERNAME = Parameters[1].Value;
  const CPI_PASSWORD = Parameters[0].Value;

  return {
    CPI_USERNAME,
    CPI_PASSWORD,
  };
};

const cpiOauth = async () => {
  const { CPI_USERNAME, CPI_PASSWORD } = await getSecrets();
  const auth = await axios.post('https://oauthasservices-ae7888026.hana.ondemand.com/oauth2/api/v1/token?grant_type=client_credentials', {}, {
    auth: {
      username: CPI_USERNAME,
      password: CPI_PASSWORD
    }
  });
  return auth;
};

const getUpdateUsers = async () => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    FilterExpression: 'sapUpdate =:sapUpdate',
    ExpressionAttributeValues: { ':sapUpdate': true }
  };
  console.log(params);
  const scanResults = [];
  try {

    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) {
          console.log('Error fetching products: ', err);
          reject();
        }
        if (data != null) {
          console.log(data);
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    return scanResults;
  } catch (e) {
    console.log(e);
    return null;
  }

};

const updateFarmer = async (mobile, sapUpdate) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set sapUpdate = :sapUpdate',
    ExpressionAttributeValues: {
      ':sapUpdate': sapUpdate
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
};

async function getGlobalLock() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME,
  };
  try {
    const lock = await docClient.scan(params).promise();
    return lock.Items[0];
  } catch (e) {
    console.error(e);
    return null;
  }
}

exports.handler = async (event) => {
  const lock = await getGlobalLock();
  console.log(lock);

  if (!lock.sapCustUpdateLock) {

    try {
      const users = await getUpdateUsers();
      console.log('Updated Users: ', users);
      const auth = await cpiOauth();
      if (users && users.length > 0) {
        const afterPromiseRes = await Promise.all(users.map(async (farmer) => {
          if (farmer.customerSAPCode) {
            const response = await axios.post('https://l4097-iflmap.hcisbp.eu1.hana.ondemand.com/http/Update_Customer', farmer, {
              headers: {
                Authorization: `Bearer ${auth.data.access_token}`
              }
            });
            console.log(response);
            if (response.data.Status === '1') {
              const res = await updateFarmer(farmer.mobile, false);
              console.log(res);
              console.log(`Farmer Updated: ${farmer.mobile}`);
              return true;
            }
            console.log(`Farmer Update Fail: ${farmer.mobile}`);
            return false;
          }
          return 'Customer not created!';
        }));
        console.log(afterPromiseRes);
      }
    } catch (e) {
      console.error(e);
    }
  } else {
    console.log('SAP Customer Update Integration is currently stopped , Please check GlobalLock Table in DynamoDB');
  }

};
