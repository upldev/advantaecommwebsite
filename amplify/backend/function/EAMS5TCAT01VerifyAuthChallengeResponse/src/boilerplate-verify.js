/* eslint-disable no-param-reassign */

const md5 = require('md5');

exports.handler = (event, context) => {
  console.log(JSON.stringify(event));
  console.log('Client Hash: ', event.request.challengeAnswer);
  console.log('Server Hash: ', md5(event.request.privateChallengeParameters.answer));
  if (md5(event.request.privateChallengeParameters.answer) === event.request.challengeAnswer) {
    console.log('OTP Correct');
    event.response.answerCorrect = true;
  } else {
    console.log('OTP Incorrect');
    event.response.answerCorrect = false;
  }
  console.log(JSON.stringify(event));
  context.done(null, event);
};
