/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_CARTDISCOUNTTABLE_ARN
	API_EAMS5TAPI01_CARTDISCOUNTTABLE_NAME
	API_EAMS5TAPI01_DISCOUNTAPPLIEDTABLE_ARN
	API_EAMS5TAPI01_DISCOUNTAPPLIEDTABLE_NAME
	API_EAMS5TAPI01_DISCOUNTDATATABLE_ARN
	API_EAMS5TAPI01_DISCOUNTDATATABLE_NAME
	API_EAMS5TAPI01_FARMERTABLE_ARN
	API_EAMS5TAPI01_FARMERTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_ARN
	API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_NAME
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	API_EAMS5TAPI01_PAYMENTTABLE_ARN
	API_EAMS5TAPI01_PAYMENTTABLE_NAME
	API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_ARN
	API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_NAME
	API_EAMS5TAPI01_PRODUCTTABLE_ARN
	API_EAMS5TAPI01_PRODUCTTABLE_NAME
	API_EAMS5TAPI01_REFEREETABLE_ARN
	API_EAMS5TAPI01_REFEREETABLE_NAME
	API_EAMS5TAPI01_REFERRERTABLE_ARN
	API_EAMS5TAPI01_REFERRERTABLE_NAME
	API_EAMS5TAPI01_SAPSTOCKTABLE_ARN
	API_EAMS5TAPI01_SAPSTOCKTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT *//*
Use the following code to retrieve configured secrets from SSM:

const aws = require('aws-sdk');

const { Parameters } = await (new aws.SSM())
  .getParameters({
    Names: ["RAZORPAY_KEY_ID","RAZORPAY_SECRET"].map(secretName => process.env[secretName]),
    WithDecryption: true,
  })
  .promise();

Parameters will be of the form { Name: 'secretName', Value: 'secretValue', ... }[]
*/
/* eslint-disable no-tabs */
/* eslint-disable import/no-unresolved */

const express = require('express');
const bodyParser = require('body-parser');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
const Razorpay = require('razorpay');
const uuid = require('uuid');
const AWS = require('aws-sdk');
const crypto = require('crypto');
const axios = require('axios');

const secretsManager = new AWS.SSM();
const docClient = new AWS.DynamoDB.DocumentClient();

const { updateStockLevels } = require('./stock');
const { createEcomOrder } = require('./order');

// declare a new express app
const app = express();
app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate, public');
  res.header('Pragma', 'no-cache');
  res.header('X-Frame-Options', 'SAMEORIGIN');
  res.header('Strict-Transport-Security', 'max-age=16070400; include Subdomains');
  res.header('X-Content-Type-Option', 'nosniff');
  res.header('X-XSS-Protection', '1; mode=block');
  next();
});

async function getSecrets() {
  const { Parameters } = await secretsManager
    .getParameters({
      Names: ['RAZORPAY_KEY_ID', 'RAZORPAY_SECRET'].map((secretName) => process.env[secretName]),
      WithDecryption: true,
    })
    .promise();

  const RAZORPAY_KEY_ID = Parameters[0].Value;
  const RAZORPAY_SECRET = Parameters[1].Value;

  const instance = new Razorpay({
    key_id: RAZORPAY_KEY_ID,
    key_secret: RAZORPAY_SECRET,
  });

  return {
    RAZORPAY_KEY_ID,
    RAZORPAY_SECRET,
    instance,
  };
}

async function getOrderAmount(orderInfo, coupons) {

  const results = await Promise.all(orderInfo.map(async (item) => {
    const params = {
      TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
      Key: { materialCode: item.materialCode },
    };
    const data = await docClient.get(params).promise();

    const sku = data.Item.sku.filter((dataSku) => dataSku.ecomCode === item.sku.ecomCode)[0];

    let productDiscount = 0;
    if (coupons.appliedProductCoupons.length > 0) {

      const afterProductDiscount = await Promise.all(
        coupons.appliedProductCoupons.map(async (productCoupon) => {
          if (productCoupon.materialCode === item.materialCode
              && productCoupon.sku === item.sku.ecomCode) {
            const fetchProductDiscount = await Promise.all(productCoupon.discounts.map(
              async (dis) => {
                const getProdDis = await docClient.get({
                  TableName: process.env.API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_NAME,
                  Key: { discountId: dis.discountId }
                }).promise();

                const { isQtyBased, qty, discount } = getProdDis.Item;

                if (isQtyBased) {
                  if (item.qty >= qty) {
                    if (discount.isFixed) {
                      return parseFloat(discount.amount) * parseFloat(item.qty);
                    }

                    return (parseFloat(discount.amount)
                      * parseFloat(sku.ecomPrice) * parseFloat(item.qty)) / 100;
                  }
                  return 0;

                }

                if (discount.isFixed) {
                  return parseFloat(discount.amount) * parseFloat(item.qty);
                }

                return (parseFloat(discount.amount)
                  * parseFloat(sku.ecomPrice) * parseFloat(item.qty)) / 100;
              }
            ));
            return fetchProductDiscount.reduce((a, b) => { return a + b; }, 0);
          }
          return 0;
        })
      );
      productDiscount = afterProductDiscount.reduce((a, b) => { return a + b; }, 0);
    }

    return {
      ecomPrice: parseFloat(sku.ecomPrice) * parseFloat(item.qty),
      itemDiscount: parseFloat(productDiscount)
    };
  }));

  let totalCartValue = 0;
  let totalProductDiscount = 0;

  results.forEach((item) => {
    totalCartValue += item.ecomPrice;
    if (coupons.appliedProductCoupons.length > 0) {
      totalProductDiscount += Math.round(item.itemDiscount * 100) / 100;
    }
  });

  let cartDiscount = 0;
  if (coupons.appliedCartCoupons.length > 0) {
    cartDiscount = await Promise.all(coupons.appliedCartCoupons.map(async (cartCoupon) => {
      const getCartCoupon = await docClient.get({
        TableName: process.env.API_EAMS5TAPI01_CARTDISCOUNTTABLE_NAME,
        Key: { discountId: cartCoupon.discountId }
      }).promise();
      const { discount } = getCartCoupon.Item;
      if (discount.isFixed) {
        return parseFloat(discount.amount);
      }
      return (parseFloat(discount.amount) * totalCartValue) / 100;
    }));
  }

  const totalCartDiscount = cartDiscount === 0
    ? cartDiscount : cartDiscount.reduce((a, b) => { return a + b; }, 0);

  let otherDiscounts = 0;

  const getOtherCoupon = await docClient.scan({
    TableName: process.env.API_EAMS5TAPI01_DISCOUNTDATATABLE_NAME,
  }).promise();

  const discountConfig = getOtherCoupon.Items[0];

  if (coupons.otherDiscounts.length > 0) {
    otherDiscounts = await Promise.all(coupons.otherDiscounts.map(async (otherDis) => {
      switch (otherDis.couponType) {
        case 'FIRSTPURCHASE':
          if (discountConfig.firstPurchase.isFixed) {
            return parseFloat(discountConfig.firstPurchase.amount);
          }
          return (parseFloat(discountConfig.firstPurchase.amount) * totalCartValue) / 100;

        case 'SECONDPURCHASE':
          if (discountConfig.secondPurchase.isFixed) {
            return parseFloat(discountConfig.secondPurchase.amount);
          }
          return (parseFloat(discountConfig.secondPurchase.amount) * totalCartValue) / 100;

        case 'REFERRAL_REFEREE':
          const refereeDis = await docClient.get({
            TableName: process.env.API_EAMS5TAPI01_REFERRERTABLE_NAME,
            Key: { referralCode: otherDis.referralCode }
          }).promise();
          return parseFloat(refereeDis.Item.farmerConAmt);

        case 'REFERRAL_REFERER':
          const referrerDis = await docClient.get({
            TableName: process.env.API_EAMS5TAPI01_REFERRERTABLE_NAME,
            Key: { referralCode: otherDis.referralCode }
          }).promise();
          return parseFloat(referrerDis.Item.farmerGenAmt);

        default:
          return 0;
      }
    }));
  }

  const totalOtherDiscounts = otherDiscounts === 0
    ? otherDiscounts : otherDiscounts.reduce((a, b) => { return a + b; }, 0);

  return Math.round(
    (totalCartValue - totalProductDiscount - totalCartDiscount - totalOtherDiscounts) * 100
  ) / 100;

}

app.post('/createOrder', async (req, res) => {
  try {
    const { orderInfo, coupons } = req.body;
    console.log(orderInfo);
    console.log(coupons);
    const secret = await getSecrets();

    const { RAZORPAY_KEY_ID, RAZORPAY_SECRET, instance } = secret;
    console.log(RAZORPAY_KEY_ID);
    console.log(RAZORPAY_SECRET);
    console.log(instance);

    const amount = await getOrderAmount(orderInfo, coupons);

    console.log(amount);

    const options = {
      amount: Math.round(amount * 100), // amount in smallest currency unit
      currency: 'INR',
      receipt: uuid.v4(),
    };

    console.log(options);

    instance.orders.create(options, (error, order) => {
      console.log(order);
      console.log(error);
      if (!order) { return res.status(200).json({ success: false, message: 'Some error occured' }); }
      return res.status(200).json({
        success: true,
        order,
        key: RAZORPAY_KEY_ID
      });
    });
  } catch (e) {
    console.error(e);
    return res.status(200).json({
      success: false,
      msg: e
    });
  }
});

app.post('/verifyPaymentWithOrder', async (req, res) => {
  const {
    orderCreationId,
    razorpayPaymentId,
    razorpayOrderId,
    razorpaySignature,
    amount,
    orderInfo,
    userInfo,
    coupons,
    shipping,
    isMobile
  } = req.body;
  try {

    const secret = await getSecrets();
    const { RAZORPAY_KEY_ID, RAZORPAY_SECRET, instance } = secret;

    const verifyAmount = await getOrderAmount(orderInfo, coupons);
    console.log(amount);
    console.log(verifyAmount * 100);

    if (Math.abs(amount - Math.round(verifyAmount * 100)) > 0) { return res.status(200).json({ success: false, msg: 'Difference in Price Found' }); }

    // Creating our own digest
    // The format should be like this:
    // digest = hmac_sha256(orderCreationId + "|" + razorpayPaymentId, secret);
    const digest = crypto
      .createHmac('sha256', RAZORPAY_SECRET)
      .update(`${orderCreationId}|${razorpayPaymentId}`)
      .digest('hex');

    // comaparing our digest with the actual signature
    if (digest !== razorpaySignature) { return res.status(200).json({ success: false, msg: 'Transaction not legit!' }); }

    // THE PAYMENT IS LEGIT & VERIFIED
    // YOU CAN SAVE THE DETAILS IN YOUR DATABASE IF YOU WANT

    const token = Buffer.from(`${RAZORPAY_KEY_ID}:${RAZORPAY_SECRET}`).toString('base64');
    console.log(token);

    const paymentMethodRes = await axios.get(`https://api.razorpay.com/v1/payments/${razorpayPaymentId}`, {
      headers: {
        Authorization: `Basic ${token}`
      }
    });

    // Decrease Stock Levels
    const updStock = await updateStockLevels(orderInfo, false);
    const afterOrder = await createEcomOrder(
      {
        rzpPaymentId: razorpayPaymentId,
        rzpOrderId: razorpayOrderId,
        amount: parseFloat(amount) / 100,
      },
      orderInfo,
      userInfo,
      coupons,
      shipping,
      isMobile,
      paymentMethodRes.data.method
    );

    console.log(afterOrder);

    return res.status(200).json(afterOrder);
  } catch (e) {
    console.error(e);
    return res.status(200).json({ success: false, msg: 'Error in payment' });
  }
});

app.listen(3000, () => {
  console.log('App started');
});

module.exports = app;
