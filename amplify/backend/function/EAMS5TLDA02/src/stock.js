const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();

const getProduct = async (materialCode) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
    Key: { materialCode }
  };
  try {
    const product = await docClient.get(params).promise();
    return { success: true, item: product.Item };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error in getProduct' };
  }
};

const getSAPStock = async (sapSkuCode) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_SAPSTOCKTABLE_NAME,
    FilterExpression: 'sapSkuCode =:sapSkuCode',
    ExpressionAttributeValues: {
      ':sapSkuCode': sapSkuCode,
    },
  };
  try {
    const product = await docClient.scan(params).promise();
    return { success: true, items: product.Items };
  } catch (e) {
    return { success: false, msg: 'Error in getProduct' };
  }
};

const updateProduct = async (materialCode, sku) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
    Key: { materialCode },
    UpdateExpression: 'set sku =:sku',
    ExpressionAttributeValues: {
      ':sku': sku
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
};

const updateSAPStock = async (sapStockCode, qtyAvailable) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_SAPSTOCKTABLE_NAME,
    Key: { sapStockCode },
    UpdateExpression: 'set qtyAvailable =:qtyAvailable',
    ExpressionAttributeValues: {
      ':qtyAvailable': qtyAvailable
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return data;
  } catch (err) {
    return err;
  }
};

exports.updateStockLevels = async (cartItems, isInc) => {
  const promiseres = await Promise.all(cartItems.map(async (cartItem) => {
    const product = await getProduct(cartItem.materialCode);
    const updatedSku = product.item.sku.map((skuItem) => {
      if (skuItem.sapSkuCode === cartItem.sku.sapSkuCode) {
        if (isInc) {
          return {
            ...skuItem,
            qtyAvailable: skuItem.qtyAvailable + cartItem.qty
          };
        }
        return {
          ...skuItem,
          qtyAvailable: skuItem.qtyAvailable - cartItem.qty
        };

      }
      return skuItem;
    });

    const updProduct = await updateProduct(cartItem.materialCode, updatedSku);
    const sapStock = await getSAPStock(cartItem.sku.sapSkuCode);

    const updatedStock = await Promise.all(sapStock.items.map(async (stockItem) => {
      if (stockItem.qtyAvailable > 0) {
        if (isInc) {
          const updStock = await updateSAPStock(
            stockItem.sapStockCode, stockItem.qtyAvailable + cartItem.qty
          );
          return updStock;
        }
        const updStock = await updateSAPStock(
          stockItem.sapStockCode, stockItem.qtyAvailable - cartItem.qty
        );
        return updStock;
      }
    }));
    return true;
  }));
  return promiseres;
};
