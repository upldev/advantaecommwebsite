const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();

exports.putEcomPayment = async ({
  ecomPaymentId,
  ecomOrderId,
  amount,
  sapPaymentId,
  farmerMobile,
  rzpPaymentId,
  rzpOrderId,
  isMobile,
  paymentMethod,
  owner
}) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PAYMENTTABLE_NAME,
    Item: {
      ecomPaymentId,
      ecomOrderId,
      amount,
      sapPaymentId,
      farmerMobile,
      rzpPaymentId,
      rzpOrderId,
      isMobile,
      paymentMethod,
      __typename: 'Payment',
      owner,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
    },
  };
  try {
    const data = await docClient.put(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'error in putEcomPayment' };
  }
};
