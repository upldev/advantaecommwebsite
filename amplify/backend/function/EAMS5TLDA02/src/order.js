/* eslint-disable no-param-reassign */
const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();
const sns = new AWS.SNS({ region: 'eu-west-2' });

const uuid = require('uuid');
const { putEcomPayment } = require('./payment');

const getFarmer = async (mobile) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile }
  };

  try {
    const product = await docClient.get(params).promise();

    return { success: true, item: product.Item };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error in getProduct' };
  }
};

const getSequenceNumber = async () => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_NAME,
  };

  console.log(params);
  try {
    const sequenceNumbers = await docClient.scan(params).promise();

    return { success: true, item: sequenceNumbers.Items[0] };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error in getSequenceNumber' };
  }
};

const updateMasterNumber = async (tableId, masterNumber, lastMasterUpdated) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_NAME,
    Key: { tableId },
    UpdateExpression: 'set masterNumber =:masterNumber, lastMasterUpdated =:lastMasterUpdated',
    ExpressionAttributeValues: {
      ':masterNumber': masterNumber,
      ':lastMasterUpdated': lastMasterUpdated
    }
  };
  console.log(params);
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating Master Number' };
  }
};

const updateFcNumber = async (tableId, fcNumber, lastFcUpdated) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_NAME,
    Key: { tableId },
    UpdateExpression: 'set fcNumber =:fcNumber, lastFcUpdated =:lastFcUpdated',
    ExpressionAttributeValues: {
      ':fcNumber': fcNumber,
      ':lastFcUpdated': lastFcUpdated
    }
  };
  console.log(params);
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating FC Number' };
  }
};

const updateVcNumber = async (tableId, vcNumber, lastVcUpdated) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_NAME,
    Key: { tableId },
    UpdateExpression: 'set vcNumber =:vcNumber, lastVcUpdated =:lastVcUpdated',
    ExpressionAttributeValues: {
      ':vcNumber': vcNumber,
      ':lastVcUpdated': lastVcUpdated
    }
  };
  console.log(params);
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating VC Number' };
  }
};

const updateFrNumber = async (tableId, frNumber, lastFrUpdated) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERSEQUENCENUMBERTABLE_NAME,
    Key: { tableId },
    UpdateExpression: 'set frNumber =:frNumber, lastFrUpdated =:lastFrUpdated',
    ExpressionAttributeValues: {
      ':frNumber': frNumber,
      ':lastFrUpdated': lastFrUpdated
    }
  };
  console.log(params);
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating FR Number' };
  }
};

const getRandomOrderId = async (isMaster, profitCenter) => {
  const string_length = 16;
  let randomstring = 'IN';
  randomstring += isMaster ? 'MTR' : 'S';

  const {
    fcNumber,
    lastFcUpdated,
    vcNumber,
    lastVcUpdated,
    frNumber,
    lastFrUpdated,
    masterNumber,
    lastMasterUpdated,
    tableId
  } = (await getSequenceNumber()).item;

  const currentDate = new Date();
  let latestNumber;

  // profitCenter: 'DO1303',plantCode: 'AD03' Vegetable
  // profitCenter: 'DO1302',plantCode: 'AD08' Field
  // profitCenter: 'DO1305',plantCode: 'LUD1' Forage

  switch (profitCenter) {
    case 'DO1302':
      randomstring += 'FC';
      const isNewFcMonth = currentDate.getMonth() !== new Date(lastFcUpdated).getMonth();
      latestNumber = isNewFcMonth ? 1 : fcNumber + 1;
      await updateFcNumber(
        tableId, latestNumber, currentDate.toISOString()
      );
      break;
    case 'DO1303':
      randomstring += 'VC';
      const isNewVcMonth = currentDate.getMonth() !== new Date(lastVcUpdated).getMonth();
      latestNumber = isNewVcMonth ? 1 : vcNumber + 1;
      await updateVcNumber(
        tableId, latestNumber, currentDate.toISOString()
      );
      break;
    case 'DO1305':
      randomstring += 'FR';
      const isNewFrMonth = currentDate.getMonth() !== new Date(lastFrUpdated).getMonth();
      latestNumber = isNewFrMonth ? 1 : frNumber + 1;
      await updateFrNumber(
        tableId, latestNumber, currentDate.toISOString()
      );
      break;
    default:
      console.log('Master Order');
      const isNewMonth = currentDate.getMonth() !== new Date(lastMasterUpdated).getMonth();
      latestNumber = isNewMonth ? 1 : masterNumber + 1;
      await updateMasterNumber(
        tableId, latestNumber, currentDate.toISOString()
      );
  }

  const currentMonth = currentDate.getMonth();
  const currentYear = currentDate.getFullYear().toString().slice(2);

  randomstring += currentYear;

  switch (currentMonth) {
    case 0:
      randomstring += 'J';
      break;
    case 1:
      randomstring += 'K';
      break;
    case 2:
      randomstring += 'L';
      break;
    case 3:
      randomstring += 'A';
      break;
    case 4:
      randomstring += 'B';
      break;
    case 5:
      randomstring += 'C';
      break;
    case 6:
      randomstring += 'D';
      break;
    case 7:
      randomstring += 'E';
      break;
    case 8:
      randomstring += 'F';
      break;
    case 9:
      randomstring += 'G';
      break;
    case 10:
      randomstring += 'H';
      break;
    case 11:
      randomstring += 'I';
      break;
    default:
      console.log('Error in month');
  }

  const numberOfZeros = string_length - randomstring.length - latestNumber.toString().length;
  console.log(numberOfZeros);

  for (let i = 0; i < numberOfZeros; i++) {
    randomstring += '0';
  }

  randomstring += latestNumber;

  console.log(randomstring);

  return randomstring;
};

const getProductPrice = async (materialCode, ecomCode) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PRODUCTTABLE_NAME,
    Key: { materialCode },
  };
  const data = await docClient.get(params).promise();
  const sku = data.Item.sku.filter((dataSku) => dataSku.ecomCode === ecomCode)[0];
  const { ecomPrice, MRP } = sku;
  return { ecomPrice, MRP };
};

const getOrderForPost = async (
  orderInfo,
  coupons,
  orderDate,
  masterOrderId,
  userInfo,
  shipping
) => {
  let totalPayable = 0;
  let totalQty = 0;
  let totalItemDiscount = 0;
  let totalHeaderDiscount = 0;
  let totalEcomPrice = 0;

  const vegetableCart = [];
  let vegetablePayable = 0;
  let vegetableQty = 0;
  let vegetableItemDiscount = 0;
  let vegetableHeader = 0;
  let vegetableEcom = 0;

  const fieldCart = [];
  let fieldPayable = 0;
  let fieldQty = 0;
  let fieldItemDiscount = 0;
  let fieldHeader = 0;
  let fieldEcom = 0;

  const forageCart = [];
  let foragePayable = 0;
  let forageQty = 0;
  let forageItemDiscount = 0;
  let forageHeader = 0;
  let forageEcom = 0;

  await Promise.all(orderInfo.map(async (item) => {
    const { ecomPrice: itemPrice, MRP } = await
    getProductPrice(item.materialCode, item.sku.ecomCode);
    totalEcomPrice += itemPrice * item.qty;
    totalQty += item.qty;
    // eslint-disable-next-line no-param-reassign
    let itemDiscount = 0;
    if (coupons.appliedProductCoupons.length > 0) {
      const afterProductDiscount = await Promise.all(
        coupons.appliedProductCoupons.map(async (productCoupon) => {
          if (productCoupon.materialCode === item.materialCode
              && productCoupon.sku === item.sku.ecomCode) {
            const fetchProductDiscount = await Promise.all(productCoupon.discounts.map(
              async (dis) => {
                const getProdDis = await docClient.get({
                  TableName: process.env.API_EAMS5TAPI01_PRODUCTDISCOUNTTABLE_NAME,
                  Key: { discountId: dis.discountId }
                }).promise();

                const { isQtyBased, qty, discount } = getProdDis.Item;

                if (isQtyBased) {
                  if (item.qty >= qty) {
                    if (discount.isFixed) {
                      return parseFloat(discount.amount) * parseFloat(item.qty);
                    }

                    return (parseFloat(discount.amount)
                      * parseFloat(itemPrice) * parseFloat(item.qty)) / 100;
                  }
                  return 0;

                }

                if (discount.isFixed) {
                  return parseFloat(discount.amount) * parseFloat(item.qty);
                }

                return (parseFloat(discount.amount)
                  * parseFloat(itemPrice) * parseFloat(item.qty)) / 100;
              }
            ));
            return fetchProductDiscount.reduce((a, b) => { return a + b; }, 0);
          }
          return 0;
        })
      );
      itemDiscount = afterProductDiscount.reduce((a, b) => { return a + b; }, 0);
    }

    item.itemDiscount = Math.round(itemDiscount * 100) / 100;
    totalItemDiscount += Math.round(item.itemDiscount * 100) / 100;
    switch (item.category) {
      case 'Vegetable Crops':
        vegetableCart.push({ ...item, sku: { ...item.sku, ecomPrice: itemPrice, MRP } });
        vegetableEcom += itemPrice * item.qty;
        vegetableQty += item.qty;
        // eslint-disable-next-line no-param-reassign
        vegetableItemDiscount += Math.round(item.itemDiscount * 100) / 100;
        break;
      case 'Field Crops':
        fieldCart.push({ ...item, sku: { ...item.sku, ecomPrice: itemPrice, MRP } });
        fieldEcom += itemPrice * item.qty;
        fieldQty += item.qty;
        fieldItemDiscount += Math.round(item.itemDiscount * 100) / 100;
        break;
      case 'Forages':
        forageCart.push({ ...item, sku: { ...item.sku, ecomPrice: itemPrice, MRP } });
        forageEcom += itemPrice * item.qty;
        forageQty += item.qty;
        forageItemDiscount += Math.round(item.itemDiscount * 100) / 100;
        break;
      default:
        break;
    }

    return true;
  }));

  let cartDiscount = 0;
  if (coupons.appliedCartCoupons.length > 0) {
    cartDiscount = await Promise.all(coupons.appliedCartCoupons.map(async (cartCoupon) => {
      const getCartCoupon = await docClient.get({
        TableName: process.env.API_EAMS5TAPI01_CARTDISCOUNTTABLE_NAME,
        Key: { discountId: cartCoupon.discountId }
      }).promise();
      const { discount } = getCartCoupon.Item;
      if (discount.isFixed) {
        return Math.round(parseFloat(discount.amount) * 100) / 100;
      }
      return Math.round(
        ((parseFloat(discount.amount) * totalEcomPrice) / 100) * 100
      ) / 100;
    }));
  }

  totalHeaderDiscount = cartDiscount === 0
    ? cartDiscount : cartDiscount.reduce((a, b) => { return a + b; }, 0);

  let otherDiscounts = 0;

  const getOtherCoupon = await docClient.scan({
    TableName: process.env.API_EAMS5TAPI01_DISCOUNTDATATABLE_NAME,
  }).promise();

  const discountConfig = getOtherCoupon.Items[0];

  if (coupons.otherDiscounts.length > 0) {
    otherDiscounts = await Promise.all(coupons.otherDiscounts.map(async (otherDis) => {
      switch (otherDis.couponType) {
        case 'FIRSTPURCHASE':
          if (discountConfig.firstPurchase.isFixed) {
            return Math.round(parseFloat(discountConfig.firstPurchase.amount) * 100) / 100;
          }
          return Math.round(
            ((parseFloat(discountConfig.firstPurchase.amount) * totalEcomPrice) / 100) * 100
          ) / 100;

        case 'SECONDPURCHASE':
          if (discountConfig.secondPurchase.isFixed) {
            return Math.round(parseFloat(discountConfig.secondPurchase.amount) * 100) / 100;
          }
          return Math.round(
            ((parseFloat(discountConfig.secondPurchase.amount) * totalEcomPrice) / 100) * 100
          ) / 100;

        case 'REFERRAL_REFEREE':
          const refereeDis = await docClient.get({
            TableName: process.env.API_EAMS5TAPI01_REFERRERTABLE_NAME,
            Key: { referralCode: otherDis.referralCode }
          }).promise();
          return Math.round(parseFloat(refereeDis.Item.farmerConAmt) * 100) / 100;

        case 'REFERRAL_REFERER':
          const referrerDis = await docClient.get({
            TableName: process.env.API_EAMS5TAPI01_REFERRERTABLE_NAME,
            Key: { referralCode: otherDis.referralCode }
          }).promise();
          return Math.round(parseFloat(referrerDis.Item.farmerGenAmt) * 100) / 100;

        default:
          return 0;
      }
    }));
  }

  totalHeaderDiscount += (otherDiscounts === 0
    ? otherDiscounts : otherDiscounts.reduce((a, b) => { return a + b; }, 0));

  totalPayable = Math.round(
    (totalEcomPrice - totalItemDiscount - totalHeaderDiscount) * 100
  ) / 100;
  vegetableHeader = Math.round((vegetableEcom / totalEcomPrice) * totalHeaderDiscount * 100)
    / 100;
  vegetablePayable = Math.round(
    (vegetableEcom - vegetableItemDiscount - vegetableHeader) * 100
  ) / 100;
  fieldHeader = Math.round((fieldEcom / totalEcomPrice) * totalHeaderDiscount * 100) / 100;
  fieldPayable = Math.round((fieldEcom - fieldItemDiscount - fieldHeader) * 100) / 100;
  forageHeader = Math.round((forageEcom / totalEcomPrice) * totalHeaderDiscount * 100) / 100;
  foragePayable = Math.round((forageEcom - forageItemDiscount - forageHeader) * 100) / 100;

  // profitCenter: 'DO1303',plantCode: 'AD03' Vegetable
  // profitCenter: 'DO1302',plantCode: 'AD08' Field
  // profitCenter: 'DO1305',plantCode: 'LUD1' Forage

  const vegetableSubOrder = vegetableCart.length > 0 ? await getRandomOrderId(false, 'DO1303') : 'dummyId';
  const fieldSubOrder = fieldCart.length > 0 ? await getRandomOrderId(false, 'DO1302') : 'dummyId';
  const forageSubOrder = forageCart.length > 0 ? await getRandomOrderId(false, 'DO1305') : 'dummyId';

  const vegetableCropOrder = {
    orderId: vegetableSubOrder,
    masterOrderId,
    farmerMobile: userInfo.mobile,
    orderDate,
    shipping,
    profitCenter: 'DO1303',
    plantCode: 'AD03',
    division: 95,
    orderCart: vegetableCart,
    totalPayable: vegetablePayable,
    totalQty: vegetableQty,
    totalItemDiscount: vegetableItemDiscount,
    totalHeaderDiscount: vegetableHeader,
    totalEcomPrice: vegetableEcom,
    orderStatus: 'NEW',
    orderDelivered: null,
    invoiceUrl: null,
    shiplyteOrderId: null,
    sapOrderId: null,
    shippingCharges: 0,
  };

  const fieldCropOrder = {
    orderId: fieldSubOrder,
    masterOrderId,
    farmerMobile: userInfo.mobile,
    orderDate,
    shipping,
    profitCenter: 'DO1302',
    plantCode: 'AD08',
    division: 94,
    orderCart: fieldCart,
    totalPayable: fieldPayable,
    totalQty: fieldQty,
    totalItemDiscount: fieldItemDiscount,
    totalHeaderDiscount: fieldHeader,
    totalEcomPrice: fieldEcom,
    orderStatus: 'NEW',
    orderDelivered: null,
    invoiceUrl: null,
    shiplyteOrderId: null,
    sapOrderId: null,
    shippingCharges: 0,
  };

  const forageOrder = {
    orderId: forageSubOrder,
    masterOrderId,
    farmerMobile: userInfo.mobile,
    orderDate,
    shipping,
    profitCenter: 'DO1305',
    plantCode: 'LUD1',
    division: 94,
    orderCart: forageCart,
    totalPayable: foragePayable,
    totalQty: forageQty,
    totalItemDiscount: forageItemDiscount,
    totalHeaderDiscount: forageHeader,
    totalEcomPrice: forageEcom,
    orderStatus: 'NEW',
    orderDelivered: null,
    invoiceUrl: null,
    shiplyteOrderId: null,
    sapOrderId: null,
    shippingCharges: 0,
  };

  console.log(vegetableCropOrder);
  console.log(fieldCropOrder);
  console.log(forageOrder);

  return {
    total: {
      totalEcomPrice,
      totalHeaderDiscount,
      totalItemDiscount,
      totalPayable,
      totalQty,
    },
    vegetableCropOrder,
    fieldCropOrder,
    forageOrder,
  };
};

const putEcomOrder = async ({
  orderId,
  masterOrderId,
  farmerMobile,
  orderDate,
  shipping,
  profitCenter,
  plantCode,
  division,
  orderCart,
  totalPayable,
  totalQty,
  totalItemDiscount,
  totalHeaderDiscount,
  totalEcomPrice,
  orderStatus,
  orderDelivered,
  invoiceUrl,
  shiplyteOrderId,
  sapOrderId,
  shippingCharges,
  owner,
  isMobile,
}) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    Item: {
      orderId,
      masterOrderId,
      farmerMobile,
      orderDate,
      shipping,
      profitCenter,
      plantCode,
      division,
      orderCart,
      totalPayable,
      totalQty,
      totalItemDiscount,
      totalHeaderDiscount,
      totalEcomPrice,
      orderStatus,
      orderDelivered,
      invoiceUrl,
      shiplyteOrderId,
      sapOrderId,
      shippingCharges,
      isMobile,
      bluedartAwbNo: null,
      bluedartTrack: null,
      __typename: 'Order',
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      owner
    },
  };
  try {
    const data = await docClient.put(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'error in putEcomOrder' };
  }
};

const putDiscountApplied = async ({
  discountApplyId,
  orderId,
  farmerMobile,
  discountId,
  discount,
  orderDate,
  owner,
  isMobile
}) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_DISCOUNTAPPLIEDTABLE_NAME,
    Item: {
      discountApplyId,
      orderId,
      farmerMobile,
      discountId,
      discount,
      orderDate,
      isMobile,
      __typename: 'DiscountApplied',
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      owner
    },
  };
  try {
    const data = await docClient.put(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'error in putDiscountApplied' };
  }
};

const putReferee = async ({
  referralCode,
  farmerConPhone,
  farmerConName,
  farmerConConsumed,
  owner
}) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_REFEREETABLE_NAME,
    Item: {
      referralCode,
      farmerConPhone,
      farmerConName,
      farmerConConsumed,
      __typename: 'Referee',
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      owner
    },
  };
  try {
    const data = await docClient.put(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'error in putReferee' };
  }
};

const updateReferrer = async ({ referralCode, farmerGenConsumed }) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_REFERRERTABLE_NAME,
    Key: { referralCode },
    UpdateExpression: 'set farmerGenConsumed =:farmerGenConsumed',
    ExpressionAttributeValues: {
      ':farmerGenConsumed': farmerGenConsumed,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating referrer data' };
  }
};

const updateFirstPurchase = async (mobile) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set firstPurchase =:firstPurchase, secondPurchase =:secondPurchase',
    ExpressionAttributeValues: {
      ':firstPurchase': false,
      ':secondPurchase': true
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating first purchase order status' };
  }
};

const updateSecondPurchase = async (mobile) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set secondPurchase =:secondPurchase',
    ExpressionAttributeValues: {
      ':secondPurchase': false,
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error updating second purchase order status' };
  }
};

const emptyCart = async (mobile) => {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile },
    UpdateExpression: 'set cart =:cart',
    ExpressionAttributeValues: {
      ':cart': [],
    },
  };
  try {
    const data = await docClient.update(params).promise();
    return { success: true, data };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error Emptying cart after order' };
  }
};

const sendSecondPurchaseSMS = async (mobile) => {
  try {
    const url = 'https://shopuat.advantaseeds.com/second-purchase-discount';
    const smsRes = await new Promise((resolve) => {
      sns.publish(
        {
          Message: `Get Discount on second Purchase! Go to ${url} to avail it!`,
          PhoneNumber: mobile,
          MessageStructure: 'string',
          MessageAttributes: {
            'AWS.SNS.SMS.SenderID': {
              DataType: 'String',
              StringValue: 'ADVANTA',
            },
            'AWS.SNS.SMS.SMSType': {
              DataType: 'String',
              StringValue: 'Transactional',
            },
          },
        },
        (err, data) => {
          if (err) {
            console.error(err.stack);
            resolve({ success: false, msg: 'Error sending Second Purchase SMS' });
          }
          resolve({ success: true, msg: 'Successfully sent Second Purchase SMS' });
        }
      );
    });
    console.log(smsRes);
    return smsRes;
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error sending Second Purchase SMS' };
  }
};

const orderConfirmationSMS = async (mobile, totalPayable, orderDate) => {
  try {
    const smsRes = await new Promise((resolve, reject) => {
      sns.publish(
        {
          Message: `Thank you for shopping with Advanta Seeds. Your order was received successfully dated on ${orderDate} with amount - INR ${Math.round(parseFloat(totalPayable) * 100) / 100}.`,
          PhoneNumber: mobile,
          MessageStructure: 'string',
          MessageAttributes: {
            'AWS.SNS.SMS.SenderID': {
              DataType: 'String',
              StringValue: 'ADVANTA',
            },
            'AWS.SNS.SMS.SMSType': {
              DataType: 'String',
              StringValue: 'Transactional',
            },
          },
        },
        (err, data) => {
          if (err) {
            console.log(data);
            console.error(err.stack);
            resolve({ success: false, msg: 'Error sending Order Confirmation SMS' });
          }
          resolve({ success: true, msg: 'Successfully sent Order Confirmation SMS' });
        }
      );
    });
    return smsRes;
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error sending Order Confirmation SMS' };
  }
};

exports.createEcomOrder = async (
  rzp, orderInfo, userInfo, coupons, shipping, isMobile, paymentMethod
) => {
  const masterOrderId = await getRandomOrderId(true, null);
  const orderDate = new Date().toISOString().split('T')[0];

  const farmer = await getFarmer(userInfo.mobile);
  console.log(farmer);
  const {
    vegetableCropOrder, fieldCropOrder, forageOrder
  } = await getOrderForPost(
    orderInfo,
    coupons,
    orderDate,
    masterOrderId,
    userInfo,
    shipping
  );

  const ecomPaymentRes = await putEcomPayment({
    ecomPaymentId: uuid.v4(),
    ecomOrderId: masterOrderId,
    amount: rzp.amount,
    sapPaymentId: null,
    farmerMobile: farmer.item.mobile,
    rzpPaymentId: rzp.rzpPaymentId,
    rzpOrderId: rzp.rzpOrderId,
    isMobile,
    paymentMethod,
    owner: farmer.item.owner
  });

  console.log(JSON.stringify(ecomPaymentRes));

  const combinedOrder = [fieldCropOrder, vegetableCropOrder, forageOrder]
    .filter((subOrder) => subOrder.orderCart.length !== 0)
    .map((subOrder) => {
      return {
        ...subOrder,
        orderCart: subOrder.orderCart.map((item) => {
          return {
            materialCode: item.materialCode,
            sku: item.sku,
            qty: item.qty,
            itemDiscount: item.itemDiscount,
            product: item.product,
          };
        }),
        owner: farmer.item.owner,
        isMobile
      };
    });

  console.log(combinedOrder);

  try {
    await Promise.all(
      combinedOrder.map(async (order) => {
        const orderRes = await putEcomOrder(order);
      })
    );
    if (coupons.appliedProductCoupons.length > 0) {
      await Promise.all(
        coupons.appliedProductCoupons.map(async (productCoupon) => {
          const afterProductCoupon = await Promise.all(
            productCoupon.discounts.map(async (itemDis) => {
              const res = await putDiscountApplied({
                discountApplyId: uuid.v4(),
                masterOrderId,
                farmerMobile: farmer.item.mobile,
                discountId: itemDis.discountId,
                discount: itemDis.discount,
                orderDate,
                owner: farmer.item.owner,
                isMobile
              });
              return res;
            })
          );
          return afterProductCoupon;
        })
      );
    }

    if (coupons.appliedCartCoupons.length > 0) {
      await Promise.all(
        coupons.appliedCartCoupons.map(async (cartCoupon) => {
          const res = await putDiscountApplied({
            discountApplyId: uuid.v4(),
            masterOrderId,
            farmerMobile: farmer.item.mobile,
            discountId: cartCoupon.discountId,
            discount: cartCoupon.discount,
            orderDate,
            owner: farmer.item.owner,
            isMobile
          });
          return res;
        })
      );
    }

    if (coupons.otherDiscounts.length > 0) {
      await Promise.all(
        coupons.otherDiscounts.map(async (otherCoupon) => {
          const res = await putDiscountApplied({
            discountApplyId: uuid.v4(),
            masterOrderId,
            farmerMobile: farmer.item.mobile,
            discountId: otherCoupon.discountId,
            discount: otherCoupon.discount,
            orderDate,
            owner: farmer.item.owner,
            isMobile
          });
          if (otherCoupon.couponType === 'REFERRAL_REFEREE') {
            const refres = await putReferee({
              referralCode: otherCoupon.referralCode,
              farmerConPhone: farmer.item.mobile,
              farmerConName: farmer.item.name,
              farmerConConsumed: true,
              owner: farmer.item.owner
            });
          }
          if (otherCoupon.couponType === 'REFERRAL_REFERER') {
            const refres = await updateReferrer({
              referralCode: otherCoupon.referralCode,
              farmerGenConsumed: true,
            });
          }
          return res;
        })
      );
    }

    if (farmer.item.firstPurchase) {
      const updFirstPurchase = await updateFirstPurchase(farmer.item.mobile);
      console.log(updFirstPurchase);
      const smsRes = await sendSecondPurchaseSMS(farmer.item.mobile);
      console.log(smsRes);
    }
    if (farmer.item.secondPurchase) {
      const updSecondPurchase = await updateSecondPurchase(farmer.item.mobile);
      console.log(updSecondPurchase);
    }

    const totalPayable = fieldCropOrder.totalPayable
      + forageOrder.totalPayable + vegetableCropOrder.totalPayable;

    console.log(totalPayable);

    const smsRes = await orderConfirmationSMS(farmer.item.mobile, totalPayable, orderDate);
    console.log(smsRes);

    const cartRes = await emptyCart(farmer.item.mobile);
    console.log(cartRes);

    return { success: true, msg: 'Order Placed Successfully' };
  } catch (e) {
    console.error(e);
    return { success: false, msg: 'Error occured while creating order' };
  }
};
