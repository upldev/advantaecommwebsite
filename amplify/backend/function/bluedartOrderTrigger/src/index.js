/* eslint-disable prefer-destructuring */
/* Amplify Params - DO NOT EDIT
	API_EAMS5TAPI01_FARMERTABLE_ARN
	API_EAMS5TAPI01_FARMERTABLE_NAME
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_ARN
	API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME
	API_EAMS5TAPI01_GRAPHQLAPIIDOUTPUT
	API_EAMS5TAPI01_ORDERTABLE_ARN
	API_EAMS5TAPI01_ORDERTABLE_NAME
	API_EAMS5TAPI01_PACKAGINGTABLE_ARN
	API_EAMS5TAPI01_PACKAGINGTABLE_NAME
	API_EAMS5TAPI01_SAPSTOCKTABLE_ARN
	API_EAMS5TAPI01_SAPSTOCKTABLE_NAME
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const AWS = require('aws-sdk');

const docClient = new AWS.DynamoDB.DocumentClient();
const SQS = new AWS.SQS({ region: 'eu-west-2' });

async function getOrders() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_ORDERTABLE_NAME,
    FilterExpression: 'bluedartAwbNo = :bluedartAwbNo and sapOrderId <> :sapOrderId and invoiceUrl <> :invoiceUrl',
    ExpressionAttributeValues: { ':bluedartAwbNo': null, ':sapOrderId': null, ':invoiceUrl': null }
  };
  console.log(params);

  const scanResults = [];
  try {
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) {
          console.log(err);
          reject();
        }
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    return scanResults;
  } catch (e) {
    console.log(e);
    return null;
  }
}

async function getCustomer(mobile) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_FARMERTABLE_NAME,
    Key: { mobile }
  };
  try {
    const data = await docClient.get(params).promise();
    return data.Item;
  } catch (err) {
    console.log(err);
    return err;
  }
}

async function getSAPStockBySku(sapSkuCode) {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_SAPSTOCKTABLE_NAME,
    FilterExpression: 'sapSkuCode = :sapSkuCode',
    ExpressionAttributeValues: { ':sapSkuCode': sapSkuCode }
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) reject();
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    // console.log(afterPromiseRes);

    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

async function getPackaging() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_PACKAGINGTABLE_NAME
  };
  try {
    const scanResults = [];
    const afterPromiseRes = await new Promise((resolve, reject) => {
      docClient.scan(params).eachPage((err, data, done) => {
        if (err) reject();
        if (data != null) {
          data.Items.forEach((item) => scanResults.push(item));
        } else {
          resolve(scanResults);
        }
        done();
      });
    });
    return scanResults;
  } catch (e) {
    console.error(e);
    return null;
  }
}

async function calculateShipmentDimensions(orderCart, packaging) {
  const weights = await Promise.all(orderCart.map(async (item) => {
    const sapSku = await getSAPStockBySku(item.sku.sapSkuCode);
    return Math.round(Math.abs(sapSku[0].grossSkuWeight) * 100) / 100;
  }));
  const weight = weights.reduce((a, b) => { return a + b; }, 0);
  let currPackage = packaging
    .filter((pack) => weight >= pack.minCapacityInKg)
    .filter((pack) => weight < pack.maxCapacityInKg)[0];
  if (currPackage === undefined) {
    currPackage = packaging
      .filter((pack) => weight >= pack.minCapacityInKg)[0];
  }
  // console.log(currPackage);
  return {
    length: Math.round((Math.abs(currPackage.lengthInMM) / 10) * 100) / 100,
    breadth: Math.round((Math.abs(currPackage.widthInMM) / 10) * 100) / 100,
    height: Math.round((Math.abs(currPackage.heightInMM) / 10) * 100) / 100,
    weight: Math.round((weight + Math.abs(currPackage.weightInKg)) * 100) / 100
  };
}

async function createBluedartWaybill(order, farmer, packaging) {

  // const pickup_location = order.profitCenter === 'DO1305' ? 'Ludhiana FR' : 'Nutankal FC/VC';

  const {
    length, breadth, height, weight
  } = await calculateShipmentDimensions(order.orderCart, packaging);

  console.log('Length: ', length);
  console.log('Breadth: ', breadth);
  console.log('Height: ', height);
  console.log('Weight: ', weight);

  const customerAddressParts = order.shipping.address.match(/[\s\S]{1,30}/g) || [];
  const pickupDate = new Date().toISOString().split('T')[0];
  console.log('Pickup Date: ', pickupDate);
  const pickupTime = new Date().toLocaleTimeString('en-GB', { timeZone: 'Asia/Kolkata' }).split(':')[0].toString() + new Date().toLocaleTimeString('en-GB', { timeZone: 'Asia/Kolkata' }).split(':')[1].toString();
  console.log(new Date().toLocaleTimeString('en-GB', { timeZone: 'Asia/Kolkata' }));
  console.log('Pickup Time: ', pickupTime);
  const invoiceNumber = order.invoiceUrl.split('.')[0].split('_')[2];

  let itemString = '';
  order.orderCart.forEach((item, index) => {
    const itemVal = item.itemDiscount ? item.sku.ecomPrice - item.itemDiscount : item.sku.ecomPrice;
    itemString = itemString.concat(`
      <sapi:ItemDetails>
        <sapi:CGSTAmount>0.0</sapi:CGSTAmount>
        <sapi:IGSTAmount>0.0</sapi:IGSTAmount>
        <sapi:Instruction></sapi:Instruction>
        <sapi:InvoiceDate>${pickupDate}</sapi:InvoiceDate>
        <sapi:InvoiceNumber>${invoiceNumber}</sapi:InvoiceNumber> 
        <sapi:ItemID>item${index + 1}</sapi:ItemID>
        <sapi:ItemName>${item.product}</sapi:ItemName> 
        <sapi:ItemValue>${itemVal * item.qty}</sapi:ItemValue>
        <sapi:Itemquantity>${item.qty}</sapi:Itemquantity> 
        <sapi:PerUnitRate>${itemVal}</sapi:PerUnitRate>
        <sapi:SKUNumber>${item.sku.sapSkuCode}</sapi:SKUNumber>
        <sapi:TaxableAmount>0.0</sapi:TaxableAmount>
        <sapi:TotalValue>${itemVal * item.qty}</sapi:TotalValue>
        <sapi:Unit>${item.sku.packUnit}</sapi:Unit>
        <sapi:Weight>${item.sku.packSize}</sapi:Weight> 
        <sapi:cessAmount>0.0</sapi:cessAmount>
      </sapi:ItemDetails>
      `);
  });

  const returnFullAddress = order.plantCode === 'LUD1' ? 'FRONTIER AGENCIES PRIVATE LIMITED BHAGWATI WAREHOUSE,MALERKOTLA ROAD,VILL KAIND,LUDHIANA' : 'Bharathi Brahma Seeds,Sy No. 824,825 and 829,Nutankal Village Medchal-Mandal District';
  const returnAddressParts = returnFullAddress.match(/[\s\S]{1,30}/g) || [];
  const returnContact = 'Mohd Aasim';
  const returnEmail = 'mohd.aasim@upl-ltd.com';
  const returnMobile = '6389900444';
  const returnPincode = order.plantCode === 'LUD1' ? '141116' : '501401';
  const returnAddress = `
    <sapi:Returnadds>
                <sapi:ReturnAddress1>${returnAddressParts[0]}</sapi:ReturnAddress1>
                <sapi:ReturnAddress2>${returnAddressParts[1] || ''}</sapi:ReturnAddress2>
                <sapi:ReturnAddress3>${returnFullAddress[2] || ''}</sapi:ReturnAddress3>
                <sapi:ReturnAddressinfo></sapi:ReturnAddressinfo>
                <sapi:ReturnContact>${returnContact}</sapi:ReturnContact>
                <sapi:ReturnEmailID>${returnEmail}</sapi:ReturnEmailID>
                <sapi:ReturnMobile>${returnMobile}</sapi:ReturnMobile>
                <sapi:ReturnPincode>${returnPincode}</sapi:ReturnPincode>
                <sapi:ReturnTelephone></sapi:ReturnTelephone>
             </sapi:Returnadds>
    `;
  const pickupAddress = `
  <sapi:Shipper>
                <sapi:CustomerAddress1>${returnAddressParts[0]}</sapi:CustomerAddress1>
                <sapi:CustomerAddress2>${returnAddressParts[1] || ''}</sapi:CustomerAddress2>
                <sapi:CustomerAddress3>${returnAddressParts[2] || ''}</sapi:CustomerAddress3>
                <sapi:CustomerAddressinfo></sapi:CustomerAddressinfo>
                <sapi:CustomerCode>099960</sapi:CustomerCode>
                <sapi:CustomerEmailID>${returnEmail}</sapi:CustomerEmailID>
                <sapi:CustomerGSTNumber></sapi:CustomerGSTNumber>
                <sapi:CustomerMaskedContactNumber></sapi:CustomerMaskedContactNumber> 
                <sapi:CustomerMobile>${returnMobile}</sapi:CustomerMobile>
                <sapi:CustomerName>${returnContact}</sapi:CustomerName>
                <sapi:CustomerPincode>${returnPincode}</sapi:CustomerPincode>
                <sapi:CustomerTelephone></sapi:CustomerTelephone>
                <sapi:IsToPayCustomer>${order.plantCode === 'LUD1'}</sapi:IsToPayCustomer>
                <sapi:OriginArea>HYD</sapi:OriginArea>
                <sapi:Sender>Advanta</sapi:Sender>
                <sapi:VendorCode>099960</sapi:VendorCode>
             </sapi:Shipper>
  `;
  console.log('Return Address: ', returnAddress);
  console.log('Pickup Address: ', pickupAddress);

  const orderBody = `
    <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:tem="http://tempuri.org/" xmlns:sapi="http://schemas.datacontract.org/2004/07/SAPI.Entities.WayBillGeneration" xmlns:sapi1="http://schemas.datacontract.org/2004/07/SAPI.Entities.Admin" xmlns:a="http://www.w3.org/2005/08/addressing">
    <soap:Header>
      <a:Action soap:mustUnderstand="1">http://tempuri.org/IWayBillGeneration/GenerateWayBill</a:Action>
    </soap:Header>
    <soap:Body>
       <tem:GenerateWayBill>
          <tem:Request>
             <sapi:Consignee>
                <sapi:ConsigneeAddress1>${customerAddressParts[0]}</sapi:ConsigneeAddress1>
                <sapi:ConsigneeAddress2>${customerAddressParts[1] || ''}</sapi:ConsigneeAddress2>
                <sapi:ConsigneeAddress3>${customerAddressParts[2] || ''}</sapi:ConsigneeAddress3>
                <sapi:ConsigneeAddressType></sapi:ConsigneeAddressType>
                <sapi:ConsigneeAddressinfo></sapi:ConsigneeAddressinfo>
                <sapi:ConsigneeAttention>${order.shipping.name}</sapi:ConsigneeAttention>
                <sapi:ConsigneeBusinessPartyTypeCode></sapi:ConsigneeBusinessPartyTypeCode>
                <sapi:ConsigneeEmailID>${farmer.email}</sapi:ConsigneeEmailID>
                <sapi:ConsigneeFullAddress>${customerAddressParts[3] || ''}</sapi:ConsigneeFullAddress>
                <sapi:ConsigneeGSTNumber></sapi:ConsigneeGSTNumber>
                <sapi:ConsigneeMaskedContactNumber></sapi:ConsigneeMaskedContactNumber>
                <sapi:ConsigneeMobile>${farmer.mobile.substring(3)}</sapi:ConsigneeMobile>
                <sapi:ConsigneeName>${order.shipping.name}</sapi:ConsigneeName>
                <sapi:ConsigneePincode>${order.shipping.pincode}</sapi:ConsigneePincode>
             </sapi:Consignee>
             ${returnAddress}
             <sapi:Services>
                <sapi:ActualWeight>${weight}</sapi:ActualWeight>
                <sapi:CollectableAmount>0.0</sapi:CollectableAmount>
                <sapi:Commodity>
                   <sapi:CommodityDetail1>Agriculture Seeds</sapi:CommodityDetail1>
                   <sapi:CommodityDetail2></sapi:CommodityDetail2>
                   <sapi:CommodityDetail3></sapi:CommodityDetail3>
                </sapi:Commodity>
                <sapi:CreditReferenceNo>${order.orderId}</sapi:CreditReferenceNo>
                <sapi:CreditReferenceNo2></sapi:CreditReferenceNo2>
                <sapi:CreditReferenceNo3></sapi:CreditReferenceNo3>
                <sapi:DeclaredValue>${order.totalPayable}</sapi:DeclaredValue>
                <sapi:Dimensions>
                   <sapi:Dimension>
                      <sapi:Breadth>${breadth}</sapi:Breadth>
                      <sapi:Count>1</sapi:Count>
                      <sapi:Height>${height}</sapi:Height>
                      <sapi:Length>${length}</sapi:Length>
                   </sapi:Dimension>
                </sapi:Dimensions>
                <sapi:InvoiceNo>${invoiceNumber}</sapi:InvoiceNo>
                <sapi:IsCargoShipment>false</sapi:IsCargoShipment> 
                <sapi:IsEcomUser>false</sapi:IsEcomUser> 
                <sapi:IsForcePickup>false</sapi:IsForcePickup> 
                <sapi:IsPartialPickup>false</sapi:IsPartialPickup> 
                <sapi:IsReversePickup>false</sapi:IsReversePickup>
                <sapi:ItemCount>${order.orderCart.length}</sapi:ItemCount>
                <sapi:Officecutofftime>1730</sapi:Officecutofftime>
                <sapi:PDFOutputNotRequired>true</sapi:PDFOutputNotRequired>
                <sapi:PackType></sapi:PackType>
                <sapi:ParcelShopCode></sapi:ParcelShopCode>
                <sapi:PickupDate>${pickupDate}</sapi:PickupDate>
                <sapi:PickupMode></sapi:PickupMode>
                <sapi:PickupTime>${pickupTime}</sapi:PickupTime>
                <sapi:PickupType></sapi:PickupType>
                <sapi:PieceCount>1</sapi:PieceCount>
                <sapi:ProductCode>A</sapi:ProductCode>
                <sapi:ProductType>Dutiables</sapi:ProductType>
                <sapi:RegisterPickup>false</sapi:RegisterPickup>
                <sapi:SpecialInstruction>Handle with Care</sapi:SpecialInstruction>
                <sapi:SubProductCode>P</sapi:SubProductCode>
                <sapi:itemdtl>
                   ${itemString}
                </sapi:itemdtl>
             </sapi:Services>
             ${pickupAddress}
          </tem:Request>
          <tem:Profile>
             <sapi1:Api_type>S</sapi1:Api_type>
             <sapi1:LicenceKey>a28f0bb8690c75ce3368bb1c76ea98bc</sapi1:LicenceKey>
             <sapi1:LoginID>MAA00001</sapi1:LoginID>
             <sapi1:Version>1.10</sapi1:Version>
          </tem:Profile>
       </tem:GenerateWayBill>
    </soap:Body>
 </soap:Envelope>
    `;

  return orderBody;
}

async function getGlobalLock() {
  const params = {
    TableName: process.env.API_EAMS5TAPI01_GLOBALLOCKSTABLE_NAME,
  };
  try {
    const lock = await docClient.scan(params).promise();
    return lock.Items[0];
  } catch (e) {
    console.error(e);
    return null;
  }
}

exports.handler = async (event) => {

  const lock = await getGlobalLock();
  console.log(lock);

  if (!lock.bluedartOrderLock) {
    const orders = await getOrders();
    console.log(orders);

    const packaging = await getPackaging();
    console.log(packaging);

    if (orders !== null && orders.length > 0) {
      const afterPromiseRes = await Promise.all(orders.map(async (order) => {
        const farmer = await getCustomer(order.farmerMobile);
        console.log(farmer);
        if (farmer) {
          const bluedartWaybill = await createBluedartWaybill(
            order, farmer, packaging
          );
          console.log(bluedartWaybill);

          const response = await SQS.sendMessage({
            MessageBody: JSON.stringify({ bluedartWaybill, orderId: order.orderId, type: 'ORDER' }),
            QueueUrl:
                  'https://sqs.eu-west-2.amazonaws.com/364494007171/advantaBluedartSQS',
          }).promise();
          console.log(response);
        }
      }));
    }
  } else {
    console.log('Bluedart Order Integration is currently stopped , Please check GlobalLock Table in DynamoDB');
  }
};
