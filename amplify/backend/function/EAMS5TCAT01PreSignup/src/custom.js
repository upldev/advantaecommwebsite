/* eslint-disable no-param-reassign */
exports.handler = (event, context, callback) => {
  event.response.autoConfirmUser = true;
  event.response.autoVerifyPhone = true;
  callback(null, event);
};
