// protected routes

export const redirectBackTo = (redirectBackToRoute) => {
  return `/login?redirectBackTo=${redirectBackToRoute}`;
};

// home
export const HOME = '/';
export const OUR_SEEDS = '/#our-seeds';

// auth
export const FORGOTPASSWORD = '/forgot-password';
export const LOGIN = '/login';
export const REGISTER = '/register';
export const CONFIRM = '/confirm';

// account
export const USER_PROFILE = '/user-profile';
export const CART = '/cart';
export const WISHLIST = '/wishlist';
export const ORDER_HISTORY = '/user-profile/order-history';
export const USER_ADDRESS = '/user-profile/address';
export const CREDITS = '/user-profile/credits';
export const CREATE_USER_PROFILE = '/create-user-profile';

// order
export const ORDER_SUMMARY = '/order-summary';
export const getOrderDetails = (id) => {
  return `/order-details/${id}`;
};

// PLP, PDP
export const productDetailsPage = (id) => {
  return `/product/${id}`;
};
export const CATEGORY = '/products';
export const All_CATEGORY = '/all-categories';
export const OFFERS = '/offers';

// checkout
// export const CHECKOUT = 'checkout';
export const CHECKOUT_AUTHENTICATE = '/checkout/authenticate';
export const CHECKOUT_ORDER_SUMMARY = '/checkout/order-summary';
export const CHECKOUT_PLACE_ORDER = '/checkout/place-order';
export const CHECKOUT_SELECT_ADDRESS = '/checkout/shipping';

// customer support
export const CHATBOT = '/chatbot';
export const CONTACT_US = '/contact-us';
export const FAQS = '/faqs';
export const REPORT_AN_ISSUE = '/report-an-issue';
export const CUSTOMER_SERVICES = '/customer-services';
export const ABOUT_US = '/about-us';

// policies
export const TERMS_AND_CONDITION = '/terms-and-condition';
export const PRIVACY_POLICY = '/privacy-policy';
export const CUSTOMER_COMPLAINT_POLICY = '/customer-complaint-policy';

// mobile view
export const SEARCH = '/search';

// connect to fb // add absolute path
export const FB_LOGIN = 'https://ce1fl10ci8.execute-api.eu-west-2.amazonaws.com/dev/facebookAuth';

// scratch card
export const SCRATCH_CARD = '/second-purchase-discount';
