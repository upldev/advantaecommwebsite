import Storage from '@aws-amplify/storage';

export const formatProducts = async (product) => {
  const imgUrls = await Promise.all(
    product.productImg.map(async (img) => {
      const imgUrl = await Storage.get(img);
      return imgUrl;
    })
  );
  const newProduct = { ...product, productImg: imgUrls };
  return newProduct;
};
