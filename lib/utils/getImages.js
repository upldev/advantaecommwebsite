import Storage from '@aws-amplify/storage';

export const getImages = async (images) => {
  return Promise.all(images.map(async (image) => Storage.get(image)));
};
