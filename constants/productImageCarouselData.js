import uuid from 'uuid';

export const productImageCarouselData = [
  {
    data: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2019/12/Sustainability-Banner.jpg',
    isImage: true,
    id: uuid.v4()
  },
  {
    data: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2020/04/Article-Header.png',
    isImage: true,
    id: uuid.v4()
  },
  {
    data: 'https://www.youtube.com/watch?v=DAwiC5kxWIk',
    isImage: false,
    id: uuid.v4()
  },
  {
    data: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2020/04/Website-Crop-Header-4.png',
    isImage: true,
    id: uuid.v4()
  },
  // {
  //   data: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2019/12/Sustainability-Banner.jpg',
  //   isImage: true,
  //   id: uuid.v4()
  // },
  // {
  //   data: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2020/04/Article-Header.png',
  //   isImage: true,
  //   id: uuid.v4()
  // },
];
