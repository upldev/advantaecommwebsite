export const carouselData = [
  {
    url: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2020/04/Website-Crop-Header-4.png',
    title: '2020 Vegetable Catalog',
    description:
      'Browse our portfolio of hybrid vegetables from our 2020 catalog.',
    linkTo: ''
  },
  {
    url: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2020/04/Article-Header.png',
    title: '2020 Forage Catalog',
    description: 'Browse our selection of hybrid forages in our 2020 catalog and discover why Advanta is the Forage Specialist.',
    linkTo: ''
  },
  {
    url: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2020/04/Innovation-Award-2-scaled.jpg',
    title: 'Advanta Seeds bags two Innovation Awards',
    description: 'The Confederation of Indian Industry (CII) recognized Advanta for its innovation excellence in two categories. Top 25 Innovative Companies of 2019 Top Innovative Company',
    linkTo: ''
  },
  {
    url: 'https://advantaseeds.com/in/wp-content/uploads/sites/16/2019/12/Sustainability-Banner.jpg',
    title: 'Advanta Seeds embeds sustainability into its DNA.',
    description: 'Advanta Seeds acts on sustainability globally supporting the United Nations Sustainable Development Goals. Advanta Seeds, the UPL seed company, puts its commitment',
    linkTo: ''
  }
];
