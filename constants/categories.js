export const categories = [
  {
    src: './field-corn.png',
    title: 'Field Corn',
    link: 'Field Corn'
  },
  {
    src: '/rice.png',
    title: 'Rice',
    link: 'Rice'
  },
  {
    src: '/Okra.png',
    title: 'Okra',
    link: 'Okra'
  },
  {
    src: '/tomato.png',
    title: 'Tomato',
    link: 'Tomato'
  },
  {
    src: '/Goruds.png',
    title: 'Gourds',
    link: 'Gourds'
  },
  {
    src: '/cabbage-and-cauliflower.png',
    title: 'Cauliflower & Cabbage',
    link: 'Cauliflower and Cabbage'
  },
  {
    src: '/Other-Vegetables.png',
    title: 'Other Vegetables',
    link: 'Other Vegetables'
  },
  {
    src: '/Forages.png',
    title: 'Forages',
    link: 'Forages'
  }
];
