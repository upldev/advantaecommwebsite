export const indianCurrencyFormatter = (input) => {
  const value = input?.toString();
  const isDecimal = value?.includes('.');
  let numbersAfterDecimal = 0;
  if (isDecimal) {
    const s = value?.split('.');
    numbersAfterDecimal = s[1].length + 1;
  }
  let lastThree = value?.substring(value?.length - (3 + numbersAfterDecimal));
  const otherNumbers = value?.substring(0, value?.length - (3 + numbersAfterDecimal));
  if (otherNumbers !== '') { lastThree = `,${lastThree}`; }
  const res = otherNumbers?.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree;
  return res;
};
