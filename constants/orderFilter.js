export const orderStatus = [
  { key: 'New', value: { orderStatus: { contains: 'NEW' } } },
];

const today = new Date();
const oneDay = 24 * 60 * 60 * 1000;
const last7 = new Date(today.getTime() - 7 * oneDay).toISOString();
const last30 = new Date(today.getTime() - 30 * oneDay).toISOString();

export const orderTime = [
  {
    key: 'Last 7 days',
    value: { between: [last7, today.toISOString()] },
    field: 'orderDate',
  },
  {
    key: 'Last 30 days',
    value: { between: [last30, today.toISOString()] },
    field: 'orderDate',
  },
  {
    key: today.getFullYear(),
    value: { contains: today.getFullYear().toString() },
    field: 'orderDate',
  },
  {
    key: today.getFullYear() - 1,
    value: { contains: (today.getFullYear() - 1).toString() },
    field: 'orderDate',
  },
];
