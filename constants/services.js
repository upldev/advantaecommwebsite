import {
  CONTACT_US,
  CUSTOMER_SERVICES,
  FAQS, PRIVACY_POLICY, REPORT_AN_ISSUE, TERMS_AND_CONDITION
} from '../lib/router';

export const services = [
  {
    key: 'CUSTOMER SERVICES',
    redirect: CUSTOMER_SERVICES,
    icon: 'IoCallOutline'
  },
  {
    key: 'REPORT AN ISSUE',
    redirect: REPORT_AN_ISSUE,
    icon: 'IoMegaphoneOutline'
  },
  {
    key: 'CONTACT US',
    redirect: CONTACT_US,
    icon: 'IoChatboxOutline'
  },
  {
    key: 'FAQS',
    redirect: FAQS,
    icon: 'IoBookmarkOutline'
  },
  {
    key: 'PRIVACY POLICY',
    redirect: PRIVACY_POLICY,
    icon: 'IoShieldCheckmarkOutline'
  },
  {
    key: 'TERMS AND CONDITIONS',
    redirect: TERMS_AND_CONDITION,
    icon: 'IoDocumentTextOutline'
  }
];
