export const BEST_SELLERS = 'bestSellers';
export const TRENDING = 'trending';
export const LOW_TO_HIGH = 'lowToHigh';
export const HIGH_TO_LOW = 'highToLow';
export const A_TO_Z = 'aToZ';
export const Z_TO_A = 'zToA';

export const sortBy = [
  { name: BEST_SELLERS, label: 'Best Sellers' },
  { name: TRENDING, label: 'Trending Products' },
  { name: LOW_TO_HIGH, label: 'Price - Low to High' },
  { name: HIGH_TO_LOW, label: 'Price - High to Low' },
  { name: A_TO_Z, label: 'Sort (A-Z)' },
  { name: Z_TO_A, label: 'Sort (Z-A)' },
];
