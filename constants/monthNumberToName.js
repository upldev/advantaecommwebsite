/* eslint-disable consistent-return */
export const monthNumberToName = (input) => {
  const value = parseInt(input);
  if (value === 1) {
    return 'Jan';
  }
  if (value === 2) {
    return 'Feb';
  }
  if (value === 3) {
    return 'March';
  }
  if (value === 4) {
    return 'April';
  }
  if (value === 5) {
    return 'May';
  }
  if (value === 6) {
    return 'June';
  }
  if (value === 7) {
    return 'July';
  }
  if (value === 8) {
    return 'Aug';
  }
  if (value === 9) {
    return 'Sept';
  }
  if (value === 10) {
    return 'Oct';
  }
  if (value === 11) {
    return 'Nov';
  }
  if (value === 12) {
    return 'Dec';
  }
};

export const dateToDay = (input) => {
  const val = parseInt(input);
  if (val === 0) { return 'Sun'; }
  if (val === 1) { return 'Mon'; }
  if (val === 2) { return 'Tues'; }
  if (val === 3) { return 'Wed'; }
  if (val === 4) { return 'Thurs'; }
  if (val === 5) { return 'Fri'; }
  if (val === 6) { return 'Sat'; }
};
