export const offersData = [
  {
    image: 'https://blog.hubspot.com/hubfs/limited-time-offer.jpg'
  }, {
    image: 'https://image.shutterstock.com/image-vector/discount-banner-shape-special-offer-260nw-1467549638.jpg'
  }, {
    image: 'https://st.depositphotos.com/1029663/1384/i/600/depositphotos_13840733-stock-photo-special-offer.jpg'
  }, {
    image: 'https://previews.123rf.com/images/arcady31/arcady311606/arcady31160600002/59113161-special-offer-red-star-icon.jpg'
  }, {
    image: 'https://st.depositphotos.com/2036077/2629/i/600/depositphotos_26296093-stock-photo-3d-special-offer-word-on.jpg'
  }, {
    image: 'https://img.freepik.com/free-vector/special-offer-sale-discount-banner_180786-46.jpg?size=626&ext=jpg'
  }];
