export const BELOW_1000 = 'thousandAndBelow';
export const BETWEEN_1000_5000 = 'oneToFiveThousand';
export const ABOVE_5000 = 'fiveThousandAndAbove';

export const PRICE_RANGE = [
  {
    name: BELOW_1000,
    label: '₹1000 and Below',
    quantity: 0,
    isChecked: false,
  },
  {
    name: BETWEEN_1000_5000,
    label: '₹1000 - ₹5000',
    quantity: 0,
    isChecked: false,
  },
  {
    name: ABOVE_5000,
    label: '₹5000 and Above',
    quantity: 0,
    isChecked: false,
  },
];
