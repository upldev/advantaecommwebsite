export const homereviews = [
  {
    id: '1',
    review: 'Lorem ipsum dolor sit amet. Et quam molestiae sit voluptatibus maxime eum numquam harum. Ut pariatur vero et aliquam sequi qui numquam voluptatem aut unde eaque qui exercitationem voluptates. Ut perferendis repudiandae ea similique officia eum amet enim nam delectus enim aut aliquam dolores. Qui maxime vel incidunt quas et dignissimos voluptatem.',
    farmerName: 'Bhagwan Das',
  },
  {
    id: '2',
    review:
      'Est adipisci similique aut similique voluptates ut dolores sequi dolorem galisum nihil sunt non odit architecto. Aut temporibus saepe ut iure laudantium et soluta animi qui nihil magnam et repudiandae sequi sed blanditiis ipsum ut harum reiciendis. Sit ratione explicabo voluptatem recusandae et perferendis placeat hic molestiae accusantium.',
    farmerName: 'Lakhan'
  },
  {
    id: '3',
    review:
      'Et ullam minima et voluptates voluptatem ut delectus reiciendis ut provident nihil et modi vero? Eos dolorem inventore qui veritatis quia et dicta minima ea sequi rerum est asperiores tempora aut molestiae omnis. Ad doloribus rerum id dignissimos consequatur 33 dignissimos veritatis. Et quasi ratione id incidunt dolorem aut eius fugit et consequuntur voluptatem et provident nobis et blanditiis consequatur?',
    farmerName: 'John Doe'
  },
  {
    id: '4',
    review:
      'Aut earum voluptas et neque esse aut numquam sequi est porro repudiandae. Ea tenetur quae quo eveniet magni qui minus dolorum ea aliquam quia.Non fugit veritatis nam distinctio libero ad exercitationem internos non optio reiciendis. In excepturi esse sed dolorem numquam qui Quis iste sit repellat molestiae et deserunt minima sit laboriosam deserunt.',
    farmerName: 'Jane Doe'
  },
  {
    id: '5',
    review:
      ' Quis iste sit repellat molestiae et deserunt minima sit laboriosam deserunt. Aut quam quaerat ea facilis minima est ullam asperiores non provident nobis aut similique deserunt aut omnis eligendi cum rerum ducimus. Ut voluptas eveniet eos rerum atque sed laboriosam expedita?s iste sit repellat molestiae et deserunt minima sit laboriosam deserunt.',
    farmerName: 'Anonymous'
  }
];
