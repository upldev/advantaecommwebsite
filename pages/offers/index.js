import React from 'react';
import { HOME } from '../../lib/router';
import OffersContainer from '../../src/containers/shared/Offers';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed
// Need to remove whole code recursively

const Offers = () => {
  return (
    <MainLayout title="Offers">
      <OffersContainer />
    </MainLayout>
  );
};

export async function getServerSideProps() {
  return {
    redirect: {
      destination: HOME,
      permanent: false,
    },
  };
}

export default Offers;
