import { withSSRContext } from 'aws-amplify';
import { CHECKOUT_SELECT_ADDRESS, redirectBackTo } from '../../../lib/router';
import SelectAddressContainer from '../../../src/containers/order/SelectAddress';
import MainLayout from '../../../src/layouts/shared/MainLayout';

// Code Review Completed

const SelectAddress = () => {
  return (
    <MainLayout title="Select a delivery address">
      <SelectAddressContainer />
    </MainLayout>
  );
};

export async function getServerSideProps({ req, res }) {
  const { Auth } = withSSRContext({ req });
  const user = await Auth.currentAuthenticatedUser().catch(console.error);

  if (!user) {
    return {
      redirect: {
        destination: redirectBackTo(CHECKOUT_SELECT_ADDRESS),
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
}

export default SelectAddress;
