import { API, withSSRContext } from 'aws-amplify';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { listProducts } from '../../../graphql/queries';
import { CHECKOUT_ORDER_SUMMARY, redirectBackTo } from '../../../lib/router';
import CheckoutC from '../../../src/containers/checkout/Checkout';
import MainLayout from '../../../src/layouts/shared/MainLayout';
import { razorpayInit } from '../../../store/actions/checkout';

// Code Review Completed

const Checkout = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(razorpayInit());
  }, [dispatch]);

  return (
    <MainLayout>
      <CheckoutC isCartCheckout />
    </MainLayout>
  );
};

export async function getServerSideProps({ req, res }) {
  const { Auth } = withSSRContext({ req });
  const user = await Auth.currentAuthenticatedUser().catch(console.error);

  if (!user) {
    return {
      redirect: {
        destination: redirectBackTo(CHECKOUT_ORDER_SUMMARY),
        permanent: false,
      },
    };
  }
  try {
    const trending = await API.graphql({
      query: listProducts,
      authMode: 'API_KEY',
      variables: {
        filter: {
          isTrending: { eq: true },
        },
      },
    });
    return {
      props: {
        initializeReduxState: {
          home: { trending: trending.data.listProducts.items },
        },
      },
    };
  } catch (e) {
    return {
      props: {},
    };
  }
}

export default Checkout;
