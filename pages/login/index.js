import { withSSRContext } from 'aws-amplify';
import React from 'react';
import { HOME } from '../../lib/router';
import LoginContainer from '../../src/containers/auth/Login';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const Login = () => {

  return (
    <MainLayout title="Login">
      <LoginContainer />
    </MainLayout>
  );
};

export async function getServerSideProps({ req }) {
  const { Auth } = withSSRContext({ req });
  const user = await Auth.currentAuthenticatedUser().catch(console.error);

  if (user) {
    return {
      redirect: {
        destination: HOME,
        permanent: false,
      },
    };
  }

  return {
    props: { },
  };
}

export default Login;
