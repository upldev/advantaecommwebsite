import { withSSRContext } from 'aws-amplify';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getFarmer } from '../../graphql/queries';
import { redirectBackTo, USER_PROFILE } from '../../lib/router';
import AddressList from '../../src/components/account/AddressList';
import MainLayout from '../../src/layouts/shared/MainLayout';
import { addCurrentShipTo, ADD_CURRENT_LOGGED_USER } from '../../store/actions/checkout';

// Code Review Completed

const UserAddress = ({ profile }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: ADD_CURRENT_LOGGED_USER,
      payload: profile
    });
    const defaultAdd = profile.shipping.filter((item) => item.isDefault);
    if (defaultAdd.length > 0) dispatch(addCurrentShipTo(defaultAdd[0]));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <MainLayout title="Your Profile" showCategories>
      <AddressList />
    </MainLayout>

  );
};

export async function getServerSideProps({ req }) {
  try {
    const { Auth, API } = withSSRContext({ req });
    const user = await Auth.currentAuthenticatedUser();
    const data = await API.graphql({
      query: getFarmer,
      variables: {
        mobile: user.username
      },
      authMode: 'AMAZON_COGNITO_USER_POOLS'
    });
    return {
      props: {
        profile: data.data.getFarmer,
      }
    };
  } catch (err) {
    console.error(err);
    if (err === 'The user is not authenticated') {
      return {
        redirect: {
          destination: redirectBackTo(USER_PROFILE),
          permanent: false,
        },
      };
    }
    return {
      props: { },
    };
  }
}

export default UserAddress;
