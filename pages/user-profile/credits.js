import { graphqlOperation, withSSRContext } from 'aws-amplify';
import { listReferees, listReferrers } from '../../graphql/queries';
import { redirectBackTo, USER_PROFILE } from '../../lib/router';
import Credits from '../../src/components/account/Credits';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const UserCredits = ({ referralCodes, referees }) => {

  return (
    <MainLayout title="Your Profile" showCategories>
      <Credits referralCodes={referralCodes} referees={referees} />
    </MainLayout>

  );
};

export async function getServerSideProps({ req }) {
  try {
    const { Auth, API } = withSSRContext({ req });
    const user = await Auth.currentAuthenticatedUser();
    let referralCodes = await API.graphql(graphqlOperation(listReferrers, {
      filter: {
        farmerGenPhone: {
          eq: user.username
        }
      }
    }));
    referralCodes = referralCodes.data.listReferrers.items;
    const filters = referralCodes.map(
      (item) => ({ referralCode: { eq: item.referralCode } })
    );
    let referees;
    if (filters.length > 0) {
      referees = await API.graphql(graphqlOperation(listReferees, {
        filter: {
          or: filters
        }
      }));
    }
    return {
      props: {
        referralCodes,
        referees: referees.data.listReferees.items,
      }
    };
  } catch (err) {
    console.error(err);
    if (err === 'The user is not authenticated') {
      return {
        redirect: {
          destination: redirectBackTo(USER_PROFILE),
          permanent: false,
        },
      };
    }
    return {
      props: { },
    };
  }
}

export default UserCredits;
