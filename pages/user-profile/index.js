import { withSSRContext } from 'aws-amplify';
import { getFarmer } from '../../graphql/queries';
import { redirectBackTo, USER_PROFILE } from '../../lib/router';
import UserProfileContainer from '../../src/containers/account/UserProfile';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const UserProfile = ({ profile }) => {

  return (
    <MainLayout title="Your Profile" showCategories>
      <UserProfileContainer profile={profile} />
    </MainLayout>

  );
};

export async function getServerSideProps({ req }) {
  try {
    const { Auth, API } = withSSRContext({ req });
    const user = await Auth.currentAuthenticatedUser();
    const data = await API.graphql({
      query: getFarmer,
      variables: {
        mobile: user.username
      },
      authMode: 'AMAZON_COGNITO_USER_POOLS'
    });
    return {
      props: {
        profile: data.data.getFarmer,
      }
    };
  } catch (err) {
    console.error(err);
    if (err === 'The user is not authenticated') {
      return {
        redirect: {
          destination: redirectBackTo(USER_PROFILE),
          permanent: false,
        },
      };
    }
    return {
      props: { },
    };
  }
}

export default UserProfile;
