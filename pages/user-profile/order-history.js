import { withSSRContext } from 'aws-amplify';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getProduct, orderByFarmer } from '../../graphql/queries';
import { redirectBackTo, USER_PROFILE } from '../../lib/router';
import OrderHistoryContainer from '../../src/containers/account/OrderHistory';
import MainLayout from '../../src/layouts/shared/MainLayout';
import { resetCart } from '../../store/actions/cart';
import { resetCheckout } from '../../store/actions/checkout';

// Code Review Completed

const OrderHistory = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    if (router.query?.fromCheckout) {
      dispatch(resetCart());
      dispatch(resetCheckout());
    }
  }, [dispatch, router]);

  return (
    <MainLayout title="Your Profile" showCategories>
      <OrderHistoryContainer />
    </MainLayout>
  );
};

export async function getServerSideProps({ req }) {
  try {
    const { Auth, API } = withSSRContext({ req });
    const farmerMobile = (await Auth.currentAuthenticatedUser()).attributes
      .phone_number;
    const orders = await API.graphql({
      query: orderByFarmer,
      variables: {
        farmerMobile,
        limit: 5,
        sortDirection: 'DESC',
      },
    });

    const formatted = await Promise.all(
      orders.data.orderByFarmer.items.map(async (order) => {
        const orderCart = await Promise.all(
          order.orderCart.map(async (orderItem) => {
            const { data } = await API.graphql({
              query: getProduct,
              variables: {
                materialCode: orderItem.materialCode,
              },
              authMode: 'API_KEY',
            });

            const {
              crop, product, materialCode, category, avgrating
            } = data.getProduct;

            return {
              ...orderItem,
              productDetails: {
                crop,
                product,
                productImg: data.getProduct.productImg[0],
                materialCode,
                category,
                avgrating,
              },
            };
          })
        );

        return { ...order, orderCart };
      })
    );

    const orderCount = await API.post('EAMS5TAPI05', '/countFarmerOrders', {
      body: {
        farmerMobile,
      },
    });

    return {
      props: {
        initializeReduxState: {
          orders: {
            orders: formatted,
            tokens: [null, orders.data.orderByFarmer.nextToken],
            filters: [],
            orderCount: orderCount.count,
          },
        },
      },
    };
  } catch (err) {
    console.error(err);
    if (err === 'The user is not authenticated') {
      return {
        redirect: {
          destination: redirectBackTo(USER_PROFILE),
          permanent: false,
        },
      };
    }
    return {
      props: {},
    };
  }
}

export default OrderHistory;
