import { API } from 'aws-amplify';
import React from 'react';
import { getProduct } from '../../graphql/queries';
import ProductContainer from '../../src/containers/products/Product';
import MainLayout from '../../src/layouts/shared/MainLayout';

const Product = ({ product }) => {
  return (
    <MainLayout title={product.product}>
      <ProductContainer product={product} />
    </MainLayout>
  );
};

export async function getStaticPaths() {
  const products = await API.post('EAMS5TAPI05', '/getAllProducts');
  const productsFormatted = products.items.filter(
    (item) => item.isPublic === true
  );
  return {
    paths: productsFormatted.map((product) => ({
      params: { id: product.materialCode },
    })),
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  const product = await API.graphql({
    query: getProduct,
    variables: { materialCode: params.id },
    authMode: 'API_KEY',
  }).catch(console.error);

  const similarProducts = await Promise.all(
    product.data.getProduct.similarProducts.map(async (materialCode) => {
      const { data } = await API.graphql({
        query: getProduct,
        variables: {
          materialCode,
        },
        authMode: 'API_KEY',
      });

      return data.getProduct;
    })
  );

  return {
    props: {
      product: product.data.getProduct,
      initializeReduxState: {
        product: {
          productDetails: {
            ...product.data.getProduct,
            similarProducts,
          },
        },
      },
    },
    revalidate: 60 * 5,
  };
}

export default Product;
