import React from 'react';
import ContactUsContainer from '../../src/containers/shared/ContactUs';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const ContactUs = () => {
  return (
    <MainLayout title="Contact Us">
      <ContactUsContainer />
    </MainLayout>
  );
};

export default ContactUs;
