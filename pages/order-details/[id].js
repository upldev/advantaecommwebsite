/* eslint-disable react-hooks/rules-of-hooks */
import { withSSRContext } from 'aws-amplify';
import React from 'react';
import { getOrder, listPayments } from '../../graphql/queries';
import {
  getOrderDetails,
  ORDER_HISTORY,
  redirectBackTo
} from '../../lib/router';
import OrderDetailsRedirect from '../../src/containers/order/OrderDetailsRedirect';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const orderDetails = ({ order }) => {
  return (
    <MainLayout>
      <OrderDetailsRedirect latestOrder={order} />
    </MainLayout>
  );
};

export async function getServerSideProps({ req, params }) {
  try {
    const { Auth, API } = withSSRContext({ req });
    await Auth.currentAuthenticatedUser();
    const order = await API.graphql({
      query: getOrder,
      variables: {
        orderId: params.id,
      },
      authMode: 'AMAZON_COGNITO_USER_POOLS',
    });

    if (order.data.getOrder) {
      const { masterOrderId } = order.data.getOrder;
      const payment = await API.graphql({
        query: listPayments,
        variables: {
          filter: {
            ecomOrderId: {
              eq: masterOrderId,
            },
          },
        },
      });

      const { paymentMethod } = payment.data.listPayments.items[0] || {};

      return {
        props: {
          order: paymentMethod
            ? { ...order.data.getOrder, paymentMethod }
            : order.data.getOrder,
        },
      };
    }

    return {
      redirect: {
        permanent: false,
        destination: ORDER_HISTORY,
      },
    };
  } catch (err) {
    console.error(err);
    if (err === 'The user is not authenticated') {
      return {
        redirect: {
          destination: redirectBackTo(getOrderDetails(params.id)),
          permanent: false,
        },
      };
    }
    return {
      props: {},
    };
  }
}

export default orderDetails;
