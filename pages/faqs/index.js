/* eslint-disable max-len */
import { API } from 'aws-amplify';
import React from 'react';
import { listFaqs } from '../../graphql/queries';
import FaqsContainer from '../../src/containers/shared/Faqs';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const Faqs = ({ faqs }) => {
  return (
    <MainLayout title="FAQs">
      <FaqsContainer faqs={faqs} />
    </MainLayout>
  );
};

export async function getStaticProps() {
  const faqs = await API.graphql({ query: listFaqs, authMode: 'API_KEY' });

  return {
    props: {
      faqs: faqs.data.listFaqs.items
    },
    revalidate: 60 * 60 * 24
  };
}

export default Faqs;
