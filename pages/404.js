import React from 'react';
import ErrorPageContainer from '../src/containers/shared/ErrorPage';
import MainLayout from '../src/layouts/shared/MainLayout';

// Code Review Completed

const ErrorPage = () => {
  return (
    <MainLayout title="Not Found" showCategories>
      <ErrorPageContainer />
    </MainLayout>
  );
};

export default ErrorPage;
