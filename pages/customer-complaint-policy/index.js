import React from 'react';
import CustomerComplaintPolicyContainer from '../../src/containers/shared/CustomerComplaintPolicy';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const CustomerComplaintPolicy = () => {
  return (
    <MainLayout title="Customer Complaint Policy">
      <CustomerComplaintPolicyContainer />
    </MainLayout>
  );
};

export default CustomerComplaintPolicy;
