import { API } from 'aws-amplify';
import {
  listBanners,
  listProductReviews,
  listProducts
} from '../graphql/queries';
import HomeContainer from '../src/containers/shared/Home';
import MainLayout from '../src/layouts/shared/MainLayout';
// Code Review Completed

const Home = () => {

  return (
    <MainLayout showCategories>
      <HomeContainer />
    </MainLayout>
  );
};

export async function getStaticProps() {
  try {
    const bestseller = await API.graphql({
      query: listProducts,
      authMode: 'API_KEY',
      variables: {
        filter: {
          isBestSeller: { eq: true },
        },
      },
    });

    const trending = await API.graphql({
      query: listProducts,
      authMode: 'API_KEY',
      variables: {
        filter: {
          isTrending: { eq: true },
        },
      },
    });

    const advantaKisan = await API.graphql({
      query: listProductReviews,
      authMode: 'API_KEY',
      variables: {
        filter: {
          and: [
            { advantaKisan: { eq: true } },
            { isModerated: { eq: true } },
            { isPublic: { eq: true } },
          ],
        },
      },
    });

    const banners = await API.graphql({
      query: listBanners,
      authMode: 'API_KEY',
      variables: {
        filter: {
          isActive: { eq: true },
        },
      },
    });

    const hero = banners.data.listBanners.items.filter(
      (banner) => banner.bannerType === 'HERO'
    );

    const offers = banners.data.listBanners.items.filter(
      (banner) => banner.bannerType === 'OFFERS'
    );

    const formattedBestSeller = bestseller.data.listProducts.items.map(
      (product) => {
        return {
          ...product,
          sku: product.sku.sort((a) => {
            if (a.isBestSeller) {
              return -1;
            }
            return 1;
          }),
        };
      }
    );

    const formattedTrending = trending.data.listProducts.items.map(
      (product) => {
        return {
          ...product,
          sku: product.sku.sort((a) => {
            if (a.isTrending) {
              return -1;
            }
            return 1;
          }),
        };
      }
    );

    return {
      props: {
        initializeReduxState: {
          home: {
            bestseller: formattedBestSeller || [],
            trending: formattedTrending || [],
            advantaKisan: advantaKisan.data.listProductReviews.items || [],
            hero,
            offers,
            filteredServices: [],
          },
        },
      },
      revalidate: 60 * 5,
    };
  } catch (e) {
    console.error(e);
    return {
      props: {},
    };
  }
}

export default Home;
