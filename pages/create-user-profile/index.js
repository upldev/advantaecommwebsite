import { withSSRContext } from 'aws-amplify';
import React from 'react';
import { LOGIN } from '../../lib/router';
import CreateProfileForm from '../../src/components/account/CreateProfileForm';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const CreateProfile = () => {

  return (
    <MainLayout title="Create Profile">
      <CreateProfileForm />
    </MainLayout>
  );
};

export async function getServerSideProps({ req }) {
  try {
    const { Auth } = withSSRContext({ req });
    await Auth.currentAuthenticatedUser();
    return {
      props: { },
    };
  } catch (err) {
    console.error(err);
    if (err === 'The user is not authenticated') {
      return {
        redirect: {
          destination: LOGIN,
          permanent: false,
        },
      };
    }
    return {
      props: { },
    };
  }
}

export default CreateProfile;
