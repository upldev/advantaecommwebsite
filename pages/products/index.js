/* eslint-disable react-hooks/exhaustive-deps */
import { API } from 'aws-amplify';
import { useRouter } from 'next/router';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import CategoryContainer from '../../src/containers/category/Category';
import MainLayout from '../../src/layouts/shared/MainLayout';
import { resetFilters, setSeedsProducts } from '../../store/actions/filter';

// Code Review Completed

const ProductsContainer = () => {
  const dispatch = useDispatch();

  const { query } = useRouter();

  useEffect(async () => {
    if (query.type) {
      dispatch(
        setSeedsProducts(
          query.type === 'Cauliflower and Cabbage'
            ? 'Cauliflower & Cabbage'
            : query.type
        )
      );
    }

    // const filterProducts = await API.post('EAMS5TAPI05', '/plpFilterData/').catch(console.log);
    // console.log(filter);

    return () => dispatch(resetFilters());
  }, [dispatch, query]);

  return (
    <MainLayout title="Product Listing">
      <CategoryContainer />
    </MainLayout>
  );
};

export async function getStaticProps() {
  try {
    const products = await API.post('EAMS5TAPI05', '/getAllProducts');
    // console.log(products.items.length);
    const filterProducts = await API.post('EAMS5TAPI05', '/plpFilterData');
    // console.log(filterProducts);

    const productsFormatted = products.items.filter((item) => item.isPublic === true);

    const {
      seedProducts, seedTypes, brandTypes, priceRange
    } = filterProducts.filters;

    return {
      props: {
        initializeReduxState: {
          product: { products: productsFormatted },
          filter: {
            seedProducts,
            seedTypes,
            brandTypes,
            priceRange,
          },
        },
      },
      revalidate: 60 * 5,
    };
  } catch (e) {
    console.error(e);
    return {
      props: {},
    };
  }
}

export default ProductsContainer;
