import React from 'react';
import TermsAndConditionContainer from '../../src/containers/shared/TermsAndCondition';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const TermsAndCondition = () => {
  return (
    <MainLayout title="Terms And Condition">
      <TermsAndConditionContainer />
    </MainLayout>
  );
};

export default TermsAndCondition;
