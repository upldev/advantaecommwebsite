import { ChakraProvider } from '@chakra-ui/react';
import '@fontsource/barlow-condensed';
import '@fontsource/noto-sans';
import Amplify, { Storage } from 'aws-amplify';
import 'bootstrap/dist/css/bootstrap.min.css';
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import Swiper, {
  Autoplay, FreeMode,
  Mousewheel,
  Navigation,
  Pagination
} from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import awsmobile from '../aws-exports';
import FeedbackProvider from '../src/components/shared/FeedbackProvider';
import SplashScreen from '../src/components/shared/SplashScreen';
import { useStore } from '../store';
import '../styles/global.css';
import { theme } from '../theme/index';

Swiper.use([
  Pagination,
  Navigation,
  FreeMode,
  Mousewheel,
  Autoplay,
]);

NProgress.configure({
  minimum: 0.3,
  easing: 'ease',
  speed: 800,
  showSpinner: false,
});

Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

Amplify.configure({
  ...awsmobile,
  ssr: true,
  Auth: {
    authenticationFlowType: 'CUSTOM_AUTH',
  },
});

Storage.configure({ track: true });

function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initializeReduxState);
  const persistor = persistStore(store, {}, () => persistor.persist());

  return (
    <Provider store={store}>
      <PersistGate loading={<SplashScreen />} persistor={persistor}>
        <ChakraProvider theme={theme}>
          <FeedbackProvider />
          <Component {...pageProps} />
        </ChakraProvider>
      </PersistGate>
    </Provider>
  );
}

export default MyApp;
