import React from 'react';
import WishListContainer from '../../src/containers/account/WishList';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const Cart = () => {
  return (
    <MainLayout>
      <WishListContainer />
    </MainLayout>
  );
};

export default Cart;
