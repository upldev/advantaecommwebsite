import { API } from 'aws-amplify';
import React from 'react';
import { listProducts } from '../../graphql/queries';
import CartContainer from '../../src/containers/account/Cart';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Complete

const Cart = () => {
  return (
    <MainLayout>
      <CartContainer isCartCheckout={false} />
    </MainLayout>
  );
};

export default Cart;

export async function getStaticProps() {
  try {
    const trending = await API.graphql({
      query: listProducts,
      authMode: 'API_KEY',
      variables: {
        filter: {
          isTrending: { eq: true },
        },
      },
    });
    return {
      props: {
        initializeReduxState: {
          home: { trending: trending.data.listProducts.items },
        },
      },
      revalidate: 60 * 5,
    };
  } catch (e) {
    return {
      props: {},
    };
  }
}
