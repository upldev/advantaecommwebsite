import React from 'react';
import PrivacyPolicyContainer from '../../src/containers/shared/PrivacyPolicy';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const PrivacyPolicy = () => {
  return (
    <MainLayout title="Privacy Policy">
      <PrivacyPolicyContainer />
    </MainLayout>
  );
};

export default PrivacyPolicy;
