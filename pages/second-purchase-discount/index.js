import { Flex, Heading, Spinner } from '@chakra-ui/react';
import {
  API, Auth, graphqlOperation, withSSRContext
} from 'aws-amplify';
import { useEffect, useState } from 'react';
import { getFarmer, listDiscountData } from '../../graphql/queries';
import { redirectBackTo, SCRATCH_CARD } from '../../lib/router';
import ScratchCardContainer from '../../src/containers/checkout/ScratchCard';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const SecondPurchaseDiscount = () => {
  const [discount, setDiscount] = useState(null);
  const [invalid, setInvalid] = useState(false);

  useEffect(() => {
    fetchFarmer();
    if (!invalid) getDiscountData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getDiscountData = async () => {
    try {
      const data = await API.graphql(graphqlOperation(listDiscountData));
      const { secondPurchase, secondPurchaseActive } = data.data.listDiscountData.items[0];
      setDiscount({ secondPurchase, secondPurchaseActive });
    } catch (err) {
      console.error(err);
    }
  };

  const fetchFarmer = async () => {
    try {
      const user = await Auth.currentAuthenticatedUser();
      const data = await API.graphql(graphqlOperation(getFarmer, {
        mobile: user.username
      }));
      if (!data.data.getFarmer.secondPurchase) setInvalid(true);
    } catch (err) {
      console.error(err);
    }
  };

  if (invalid) {
    return (
      <MainLayout title="Second Purchase Discount">
        <Flex width="full" height="300" alignItems="center" justifyContent="center">
          <Heading>Discount Invalid!</Heading>
        </Flex>
      </MainLayout>
    );
  }

  return (
    <MainLayout title="Second Purchase Discount">
      {
        discount
          ? (
            <>
              {
            discount.secondPurchaseActive && discount.secondPurchase.amount !== 0
              ? (
                <ScratchCardContainer discount={discount.secondPurchase} />
              )
              : (
                <Flex width="full" height="300" alignItems="center" justifyContent="center">
                  <Heading>ERROR OCCURED!</Heading>
                </Flex>
              )
          }
            </>
          )
          : (
            <Flex width="full" height="300" alignItems="center" justifyContent="center">
              <Spinner />
            </Flex>
          )
      }
    </MainLayout>
  );
};

export async function getServerSideProps({ req }) {
  const { Auth } = withSSRContext({ req });
  const user = await Auth.currentAuthenticatedUser().catch(console.error);

  if (!user) {
    return {
      redirect: {
        destination: redirectBackTo(SCRATCH_CARD),
        permanent: false,
      },
    };
  }

  return {
    props: {},
  };
}

export default SecondPurchaseDiscount;
