import React from 'react';
import AboutUsContainer from '../../src/containers/shared/AboutUs';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Complete

const AboutUs = () => {
  return (
    <MainLayout title="About Us">
      <AboutUsContainer />
    </MainLayout>
  );
};

export default AboutUs;
