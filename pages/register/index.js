import { withSSRContext } from 'aws-amplify';
import React from 'react';
import { HOME } from '../../lib/router';
import RegisterContainer from '../../src/containers/auth/Register';
import MainLayout from '../../src/layouts/shared/MainLayout';

// Code Review Completed

const Register = () => {
  return (
    <MainLayout title="Register">
      <RegisterContainer />
    </MainLayout>
  );
};

export async function getServerSideProps({ req }) {
  const { Auth } = withSSRContext({ req });
  const user = await Auth.currentAuthenticatedUser().catch(console.error);

  if (user) {
    return {
      redirect: {
        destination: HOME,
        permanent: false,
      },
    };
  }

  return {
    props: { },
  };
}

export default Register;
